/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/material.dart';
import 'package:vzm_chat_sdk/src/sdk_initializer.dart';
import 'package:vzm_chat_sdk/ui/contact/contact_screen.dart';
import 'package:vzm_chat_sdk/ui/home/home_screen.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

import 'di/app_locator.dart';
import 'ui/login.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: Colors.red,
          onPrimary: Colors.white,
          // Colors that are not relevant to AppBar in LIGHT mode:
          primaryVariant: Colors.grey,
          secondary: Colors.grey,
          secondaryVariant: Colors.grey,
          onSecondary: Colors.grey,
          background: Colors.grey,
          onBackground: Colors.grey,
          surface: Colors.grey,
          onSurface: Colors.grey,
          error: Colors.grey,
          onError: Colors.grey,
        ),
      ),
      title: _title,
      routes: {
        '/': (context) => const _MyStatefulWidget(),
        '/contact': (context) => const ContactScreen(),
      },
    );
  }
}

class _MyStatefulWidget extends StatefulWidget {
  const _MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<_MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<_MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    setUp(context);
    return FutureBuilder(
      future: appServiceLocator.get<SdkInitializer>().init(),
      builder: (ctx, snapshot) {
        if (snapshot.hasData) {
          return landingWidget(ctx);
        }
        // Displaying LoadingSpinner to indicate waiting state
        return const Center(
          child: SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
        );
      },
    );
  }

  Widget landingWidget(BuildContext ctx) {
    if (appServiceLocator.get<VzmClientSdk>().isProvisioned()) {
      appServiceLocator.get<VzmClientSdk>().startSync();
      return const HomeScreen();
    } else {
      return const MyHomePage(title: "Homepage");
    }
  }
}
