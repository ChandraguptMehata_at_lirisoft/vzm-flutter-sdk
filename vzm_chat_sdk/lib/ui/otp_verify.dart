/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_chat_sdk/src/sdk_initializer.dart';
import 'package:vzm_chat_sdk/util/Utis.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

import 'home/home_screen.dart';

class OTPVERIFY extends StatefulWidget {

  String? mobilenumber;
  OTPVERIFY({Key? key,this.mobilenumber}) : super(key: key);


  @override
  _OTPVERIFYState createState() => _OTPVERIFYState(mobilenumber);
}

class _OTPVERIFYState extends State<OTPVERIFY> {

  final _controller = TextEditingController(text: '');
  final _formKey = GlobalKey<FormState>();
  late Utils utils;
  String? mobileNumber;

  _OTPVERIFYState(this.mobileNumber);

  @override
  Widget build(BuildContext context) {
    utils = appServiceLocator.get<Utils>();
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('OTP VERIFY'),
      ),
      body: Form(
        key: _formKey,
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _controller,
                  maxLength: 6,
                  cursorColor: Colors.greenAccent,
                  decoration: InputDecoration(
                      hintText: "Enter otp",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)
                      )
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please enter otp";
                    } else if (value.length < 6) {
                      return "Otp should be 6 digit";
                    } else
                      return null;
                  },
                ),
              ),
              submit_button()
            ],
          ),
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      // onPressed: _incrementCounter,
      // tooltip: 'Increment',
      // child: const Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );


  }
  Widget submit_button() {
    return RaisedButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            validateOTP(mobileNumber!, _controller.text);
          } else {

          }
        },
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80.0)),
        child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(80.0))),
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: const Text('Submit', style: TextStyle(fontSize: 20))));
  }

  Future<void> validateOTP(String mobileNumber, String otp) async {
    utils.showLoaderDialog(context);
    SdkInitializer sdk = appServiceLocator.get<SdkInitializer>();
    var data = await sdk.clientSdk
        .getAuthManager()
        .verifyOtp(mobileNumber, otp);
    Navigator.pop(context);
    if (data!.status != AuthStatus.SUCCESS) {
      (Platform.isWindows)
          ? utils.showAlertDialog(context, data.status.toString())
          : utils.showToast(data.status.toString());
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const HomeScreen()));
    }
  }
}
