
import 'package:chat_ui_kit/chat_ui_kit.dart' hide ChatMessageImage;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:functional_widget_annotation/functional_widget_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:vzm_chat_sdk/util/chat_message_image.dart';
import 'package:vzm_chat_sdk/util/date_formatter.dart';
import 'package:vzm_chat_sdk/util/switch_appbar.dart';

import 'chat_viewmodel.dart';
import 'models/chat.dart';
import 'models/chat_message.dart';
import 'models/chat_user.dart';

part 'chat_screen.g.dart';

class ChatScreenArgs {
  /// Pass the chat for an already existing chat
  final ChatWithMembers chat;

  ChatScreenArgs({required this.chat});
}

class ChatScreen extends StatefulWidget {
  final ChatScreenArgs args;

  ChatScreen(this.args);

  @override
  _ChatScreenSate createState() => _ChatScreenSate();
}

class _ChatScreenSate extends State<ChatScreen> with TickerProviderStateMixin, WidgetsBindingObserver {
  final ChatViewModel _model = ChatViewModel();

  final TextEditingController _textController = TextEditingController();

  /// Whether at least 1 message is selected
  int _selectedItemsCount = 0;

  /// Whether it's a group chat (more than 2 users)
  bool get _isGroupChat => widget.args.chat.members.length > 2;

  ChatWithMembers get _chat => widget.args.chat;

  String get _conversationId => widget.args.chat.chat!.id!;

  ChatUser get _currentUser => _model.localUser;

  @override
  void initState() {
    _model.messageListController = MessagesListController();
    _model.subscribeForMessageChange(_conversationId);
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        _model.notifyMqttManager(false);
        break;

      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
      _model.notifyMqttManager(true);
        break;

      default:
        break;
    }
  }

  /// Called when the user pressed the top right corner icon
  void onChatDetailsPressed() {
    print("Chat details pressed");
  }

  /// Called when a user tapped an item
  void onItemPressed(int index, MessageBase message) {
    print("item pressed, you could display images in full screen or play videos with this callback");
  }

  void onMessageSend(String text) {
    _model.sendMessage(text, _conversationId);
  }

  void onTypingEvent(TypingEvent event) {
    print("typing event received: $event");
  }

  /// Copy the selected comment's comment to the clipboard.
  /// Reset selection once copied.
  void copyContent() {
    String text = "";
    _model.messageListController.selectedItems.forEach((element) {
      text += element.text ?? "";
      text += '\n';
    });
    Clipboard.setData(ClipboardData(text: text)).then((value) {
      print("text selected");
      _model.messageListController.unSelectAll();
    });
  }

  void deleteSelectedMessages() {
    _model.messageListController.removeSelectedItems();
    //update app bar
    setState(() {});
  }

  Widget _buildChatTitle() {
    return Text(_chat.name);
  }

  Widget _buildMessageBody(context, index, item, messagePosition, MessageFlow messageFlow) {
    final _chatMessage = item as ChatMessage;
    Widget _child;

    if (_chatMessage.type == ChatMessageType.text) {
      _child = _ChatMessageText(index, item, messagePosition, messageFlow);
    } else if (_chatMessage.type == ChatMessageType.image) {
      _child = ChatMessageImage(index, item, messagePosition, messageFlow, callback: () => onItemPressed(index, item));
    } else if (_chatMessage.type == ChatMessageType.video) {
      _child = ChatMessageVideo(index, item, messagePosition, messageFlow);
    } else if (_chatMessage.type == ChatMessageType.audio) {
      _child = ChatMessageAudio(index, item, messagePosition, messageFlow);
    } else {
      //return text message as default
      _child = _ChatMessageText(index, item, messagePosition, messageFlow);
    }

    if (messageFlow == MessageFlow.incoming) return _child;
    return SizedBox(width: MediaQuery.of(context).size.width, child: Align(alignment: Alignment.centerRight, child: _child));
  }

  Widget _buildDate(BuildContext context, DateTime date) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Align(
                child: Text(DateFormatter.getVerboseDateTimeRepresentation(context, date),
                    style: TextStyle(color: Theme.of(context).disabledColor)))));
  }

  Widget _buildEventMessage(context, animation, index, item, messagePosition) {
    final _chatMessage = item as ChatMessage;
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Align(
                child: Text(
              _chatMessage.messageText(_currentUser.id),
              style: TextStyle(color: Theme.of(context).disabledColor),
              textAlign: TextAlign.center,
            ))));
  }

  Widget _buildMessagesList() {
    IncomingMessageTileBuilders incomingBuilders = _isGroupChat
        ? IncomingMessageTileBuilders(
            bodyBuilder: (context, index, item, messagePosition) =>
                _buildMessageBody(context, index, item, messagePosition, MessageFlow.incoming),
            avatarBuilder: (context, index, item, messagePosition) {
              final _chatMessage = item as ChatMessage;
              return Padding(padding: EdgeInsets.only(right: 16), child: ClipOval(child: getAvatarImage(_chatMessage)));
            })
        : IncomingMessageTileBuilders(
            bodyBuilder: (context, index, item, messagePosition) =>
                _buildMessageBody(context, index, item, messagePosition, MessageFlow.incoming),
            titleBuilder: null);

    return Expanded(
        child: MessagesList(
            controller: _model.messageListController,
            appUserId: _currentUser.id,
            useCustomTile: (i, item, pos) {
              final msg = item as ChatMessage;
              return msg.isTypeEvent;
            },
            messagePosition: _messagePosition,
            builders: MessageTileBuilders(
                customTileBuilder: _buildEventMessage,
                customDateBuilder: _buildDate,
                incomingMessageBuilders: incomingBuilders,
                outgoingMessageBuilders: OutgoingMessageTileBuilders(
                    bodyBuilder: (context, index, item, messagePosition) =>
                        _buildMessageBody(context, index, item, messagePosition, MessageFlow.outgoing)))));
  }

  /// Override [MessagePosition] to return [MessagePosition.isolated] when
  /// our [ChatMessage] is an event
  MessagePosition _messagePosition(
      MessageBase? previousItem, MessageBase currentItem, MessageBase? nextItem, bool Function(MessageBase currentItem) shouldBuildDate) {
    ChatMessage? _previousItem = previousItem as ChatMessage?;
    final ChatMessage _currentItem = currentItem as ChatMessage;
    ChatMessage? _nextItem = nextItem as ChatMessage?;

    if (shouldBuildDate(_currentItem)) {
      _previousItem = null;
    }

    if (_nextItem?.isTypeEvent == true) _nextItem = null;
    if (_previousItem?.isTypeEvent == true) _previousItem = null;

    if (_previousItem?.author?.id == _currentItem.author?.id && _nextItem?.author?.id == _currentItem.author?.id) {
      return MessagePosition.surrounded;
    } else if (_previousItem?.author?.id == _currentItem.author?.id && _nextItem?.author?.id != _currentItem.author?.id) {
      return MessagePosition.surroundedTop;
    } else if (_previousItem?.author?.id != _currentItem.author?.id && _nextItem?.author?.id == _currentItem.author?.id) {
      return MessagePosition.surroundedBot;
    } else {
      return MessagePosition.isolated;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _model.fetchChat(_conversationId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ChangeNotifierProvider<MessagesListController>(
                create: (context) {
                  return _model.messageListController;
                },
                child: Scaffold(
                    appBar: SwitchAppBar(
                      showSwitch: _model.messageListController.isSelectionModeActive,
                      switchLeadingCallback: () => _model.messageListController.unSelectAll(),
                      primaryAppBar: AppBar(
                        title: _buildChatTitle(),
                        actions: getPopupMenuActionButton(),
                      ),
                      switchTitle: Text(_selectedItemsCount.toString(), style: TextStyle(color: Colors.black)),
                      switchActions: [
                        IconButton(icon: Icon(Icons.content_copy), color: Colors.black, onPressed: copyContent),
                        IconButton(color: Colors.black, icon: Icon(Icons.delete), onPressed: deleteSelectedMessages),
                      ],
                    ),
                    body: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildMessagesList(),
                        Row(
                          children: [
                            MessageInput(textController: _textController, sendCallback: onMessageSend, typingCallback: onTypingEvent)],
                        )
                      ],
                    )));
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  @override
  void dispose() {
    _model.messageListController.dispose();
    _textController.dispose();
    super.dispose();
  }

  Widget getAvatarImage(ChatMessage chatMessage) {
    if (chatMessage.author != null && chatMessage.author!.avatar.isNotEmpty) {
      return Image.network(chatMessage.author!.avatar, width: 32, height: 32, fit: BoxFit.cover);
    }
    return Image.asset('assets/avatars/woman2.jpg', width: 32, height: 32, fit: BoxFit.cover);
  }

  showBottomSheet() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          // we set up a container inside which
          // we create center column and display text
          return Container(
            height: 200,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ElevatedButton(
                    onPressed: _openCamera,
                    child: Text("Camera"),
                  ),
                  ElevatedButton(
                    onPressed: _openImage,
                    child: Text("Image"),
                  ),
                  ElevatedButton(
                    onPressed: _openAudio,
                    child: Text("Audio"),
                  ),
                  ElevatedButton(
                    onPressed: _openVideo,
                    child: Text("Video"),
                  ),
                ],
              ),
            ),
          );
        });
  }

  _openCamera() async {
    var picture = await ImagePicker.platform.pickImage(
      source: ImageSource.camera,
    );
    _model.sendFile(picture!.path, _conversationId, null);
    Navigator.pop(context);
  }

  _openImage() {
    _openMediaImageFiles(FileType.image);
  }

  _openAudio() {
    _openMediaFiles(FileType.audio);
  }

  _openVideo() {
    _openMediaFiles(FileType.video);
  }

  Future<void> _openMediaImageFiles(FileType fileType) async {
    var picture = await ImagePicker.platform.pickImage(
      source: ImageSource.gallery,
    );
    _model.sendFile(picture!.path, _conversationId, null);
    Navigator.pop(context);
  }

  Future<void> _openMediaFiles(FileType fileType) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(type: fileType);
    if (result != null) {
      _model.sendFile(result.files.single.path!, _conversationId, null);
      Navigator.pop(context);
    } else {
      // User canceled the picker
    }
  }

  List<Widget> getPopupMenuActionButton() {
    return [
      PopupMenuButton<int>(
          onSelected: (item) => onSelected(context, item),
          itemBuilder: (context) => [
            const PopupMenuItem(
              child: Text("add and send Attachment"),
              value: 1,
            )
          ])
    ];
  }

  onSelected(BuildContext context, int item) {
    switch (item) {
      case 1:
        showBottomSheet();
        break;
    }
  }
}

///************************************************ Functional widgets used in the screen ***************************************

@swidget
Widget _chatMessageText(BuildContext context, int index, ChatMessage message, MessagePosition messagePosition, MessageFlow messageFlow) {
  return MessageContainer(
      decoration: messageDecoration(context, messagePosition: messagePosition, messageFlow: messageFlow),
      child: Wrap(
          runSpacing: 4.0,
          alignment: WrapAlignment.end,
          children: [Text(message.text ?? ""), ChatMessageFooter(index, message, messagePosition, messageFlow)]));
}

@swidget
Widget chatMessageFooter(BuildContext context, int index, ChatMessage message, MessagePosition messagePosition, MessageFlow messageFlow) {
  final Widget _date = _ChatMessageDate(index, message, messagePosition);
  return messageFlow == MessageFlow.incoming
      ? _date
      : Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.end, children: [
          _date,
        ]);
}

@swidget
Widget _chatMessageDate(BuildContext context, int index, ChatMessage message, MessagePosition messagePosition) {
  final color = message.isTypeMedia ? Colors.white : Theme.of(context).disabledColor;
  return Padding(
      padding: EdgeInsets.only(left: 8),
      child:
          Text(DateFormatter.getVerboseDateTimeRepresentation(context, message.createdAt, timeOnly: true), style: TextStyle(color: color)));
}

