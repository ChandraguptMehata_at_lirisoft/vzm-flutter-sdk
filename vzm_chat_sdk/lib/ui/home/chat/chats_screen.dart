import 'package:chat_ui_kit/chat_ui_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:provider/provider.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_chat_sdk/ui/home/compose/create_conversation_screen.dart';
import 'package:vzm_chat_sdk/ui/home/compose/create_group_conversation_screen.dart';
import 'package:vzm_chat_sdk/ui/home/compose/create_mms_conversation_screen.dart';
import 'package:vzm_chat_sdk/ui/home/profile/profile_screen.dart';
import 'package:vzm_chat_sdk/util/app_colors.dart';
import 'package:vzm_chat_sdk/util/date_formatter.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

import 'chat_screen.dart';
import 'chat_viewmodel.dart';
import 'models/chat.dart';
import 'models/chat_message.dart';

class ChatsScreen extends StatefulWidget {
  const ChatsScreen({Key? key}) : super(key: key);

  @override
  _ChatsScreenSate createState() => _ChatsScreenSate();
}

class _ChatsScreenSate extends State<ChatsScreen> with WidgetsBindingObserver {
  final ChatViewModel _model = ChatViewModel();

  @override
  void initState() {
    super.initState();
    _model.controller = ChatsListController();
    _model.subscribeForConversationChange();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        _model.notifyMqttManager(false);
        break;

      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        _model.notifyMqttManager(true);
        break;

      default:
        break;
    }
  }

  /// Called from [NotificationListener] when the user scrolls
  void handleScrollEvent(ScrollNotification scroll) {
    if (scroll.metrics.pixels == scroll.metrics.maxScrollExtent) {
      //_model.getMoreChats();
    }
  }

  /// Called when the user pressed an item (a chat)
  void onItemPressed(ChatWithMembers chat) {
    //navigate to the chat
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChatScreen(ChatScreenArgs(chat: chat))));
    //reset unread count
    if (chat.isUnread) {
      chat.chat!.unreadCount = 0;
    }
  }

  /// Called when the user long pressed an item (a chat)
  void onItemLongPressed(ChatBase chat) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(content: Text("This chat and any related message will be deleted permanently."), actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  //delete in DB, from the current list in memory and update UI
                  _model.controller.removeItem(chat);
                },
                child: Text("ok")),
            Padding(
              padding: EdgeInsets.only(left: 16),
              child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("cancel")),
            ),
          ]);
        });
  }

  /// Build the last message depending on how many members the Chat has
  /// and on the message type [ChatMessage.type]
  Widget _buildLastMessage(BuildContext context, int index, ChatBase item) {
    final _chat = item as ChatWithMembers;
    //display avatar only if not a 1 to 1 conversation
    final bool displayAvatar = false;
    //display an icon if there's an attachment
    Widget? attachmentIcon;
    if (_chat.lastMessage!.hasAttachment) {
      final _type = _chat.lastMessage!.type;
      final iconColor = AppColors.chatsAttachmentIconColor(context);
      if (_type == ChatMessageType.audio) {
        attachmentIcon = Icon(Icons.keyboard_voice, color: iconColor);
      } else if (_type == ChatMessageType.video) {
        attachmentIcon = Icon(Icons.videocam, color: iconColor);
      } else if (_type == ChatMessageType.image) {
        attachmentIcon = Icon(Icons.image, color: iconColor);
      }
    }

    //get the message label
    String messageText = _chat.lastMessage!.messageText(_model.localUser.id);

    return Padding(
        padding: EdgeInsets.only(top: 8),
        child: Row(children: [
          if (displayAvatar)
            Padding(
                padding: EdgeInsets.only(right: 8),
                child: ClipOval(child: getAvatarImage(item))),
          if (attachmentIcon != null) Padding(padding: EdgeInsets.only(right: 8), child: attachmentIcon),
          Expanded(
              child: Text(
            messageText,
            overflow: TextOverflow.ellipsis,
          ))
        ]));
  }

  Widget _buildTileWrapper(BuildContext context, int index, ChatBase item, Widget child) {
    return InkWell(
        onTap: () => onItemPressed(item as ChatWithMembers),
        onLongPress: () => onItemLongPressed(item),
        child: Column(children: [
          Padding(padding: const EdgeInsets.only(right: 16), child: child),
          Divider(
            height: 1.5,
            thickness: 1.5,
            color: AppColors.chatsSeparatorLineColor(context),
            //56 default GroupAvatar size + 32 padding
            indent: 56.0 + 32.0,
            endIndent: 16.0,
          )
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Chats"),
          automaticallyImplyLeading: false,
          actions: getPopupMenuActionButton(),
        ),
        body: FutureBuilder(
            future: _model.fetchChats(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ChangeNotifierProvider<ChatsListController>(
                    create: (context) {
                      return _model.controller;
                    },
                    child: Scaffold(
                      floatingActionButton: _buildFloatingActionWidget(),
                      body: ChatsList(
                          controller: _model.controller,
                          appUserId: _model.localUser.id,
                          scrollHandler: handleScrollEvent,
                          groupAvatarStyle: GroupAvatarStyle(
                              withSeparator: true,
                              separatorColor: Colors.white,
                              shape: GroupAvatarShape.circle,
                              mode: GroupAvatarMode.stackedCircles),
                          builders: ChatsListTileBuilders(
                              groupAvatarBuilder: (context, imageIndex, itemIndex, size, item) {
                                final chat = item as ChatWithMembers;
                                return getAvatarImage(chat);
                              },
                              lastMessageBuilder: _buildLastMessage,
                              wrapper: _buildTileWrapper,
                              dateBuilder: (context, date) => Padding(
                                  padding: EdgeInsets.only(left: 16),
                                  child: Text(DateFormatter.getVerboseDateTimeRepresentation(context, date)))),
                          areItemsTheSame: (ChatBase oldItem, ChatBase newItem) => oldItem.id == newItem.id),
                    ));
              } else {
                return const CircularProgressIndicator(
                  color: Colors.amber,
                );
              }
            }));
  }

  List<Widget> getPopupMenuActionButton() {
    return [
      PopupMenuButton<int>(
          onSelected: (item) => onSelected(context, item),
          itemBuilder: (context) => [
                const PopupMenuItem(
                  child: Text("My Profile"),
                  value: 0,
                ),
                const PopupMenuItem(
                  child: Text("Sync"),
                  value: 1,
                ),
                const PopupMenuItem(
                  child: Text("Restore"),
                  value: 2,
                ),
                const PopupMenuItem(
                  child: Text("Delete"),
                  value: 3,
                ),
                const PopupMenuItem(
                  child: Text("Clear DB"),
                  value: 4,
                ),
                const PopupMenuItem(
                  child: Text("Perform DB load"),
                  value: 5,
                )
              ])
    ];
  }

  void onSelected(BuildContext context, int item) {
    final VzmClientSdk _vzmClientSdk = appServiceLocator.get<VzmClientSdk>();
    switch (item) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ProfileScreen()));
        break;
      case 1:
        _vzmClientSdk.startSync();
        break;
      case 2:
        _vzmClientSdk.startRestore();
        break;
      case 4:
        _vzmClientSdk.clearDb();
        break;
      case 5:
        performLoadTest(_vzmClientSdk);
        break;
    }
  }

  void _showProgress() {
    setState(() {
      buildShowDialog(context);
    });
  }

  void _hideProgress() {
    setState(() {
      Navigator.of(context).pop();
    });
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Future<void> performLoadTest(VzmClientSdk vzmClientSdk) async {
    _showProgress();
    await vzmClientSdk.performDBLoad();
    _hideProgress();
  }

    @override
  void dispose() {
    _model.controller.dispose();
    super.dispose();
  }

  Widget getAvatarImage(ChatWithMembers item) {
    if (item.chat != null) {
      Chat? chat = item.chat;
      if (chat != null && chat.avatar != null) {
        return Image.network(chat.avatar!, width: 32, height: 32, fit: BoxFit.cover);
      }
    }
    return Image.asset('assets/avatars/woman2.jpg', width: 32, height: 32, fit: BoxFit.cover);
  }

  Widget _buildFloatingActionWidget() {
    return SpeedDial(
      animatedIcon: AnimatedIcons.add_event,
      child: IconButton(
        icon: const Icon(Icons.add),
        onPressed: () {},
      ),
      backgroundColor: Colors.black,
      overlayColor: Colors.grey,
      overlayOpacity: 0.5,
      spacing: 15,
      spaceBetweenChildren: 15,
      closeDialOnPop: true,
      visible: true,
      curve: Curves.bounceInOut,
      children: [
        SpeedDialChild(
            child: const Icon(Icons.chat),
            label: 'MMS',
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CreateMMSConversationScreen()));
            }),
        SpeedDialChild(
            child: const Icon(Icons.chat),
            label: 'Group',
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CreateGroupConversationScreen()));
            }),
        SpeedDialChild(
            child: const Icon(Icons.chat),
            label: 'One to One',
            backgroundColor: Colors.blue,
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CreateConversationScreen()));
            }),
      ],
    );
  }
}
