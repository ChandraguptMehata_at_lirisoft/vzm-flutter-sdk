import 'dart:io';

import 'package:chat_ui_kit/chat_ui_kit.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

import 'models/chat.dart';
import 'models/chat_message.dart';
import 'models/chat_user.dart';

class ChatViewModel {
  ChatViewModel();

  final VzmClientSdk _vzmClientSdk = appServiceLocator.get<VzmClientSdk>();

  late ChatsListController controller;
  late MessagesListController messageListController;

  ChatUser? _user;

  ChatUser get localUser => getLocalUser();

  Future<bool> fetchChats() async {
    controller.items.clear();
    List<Conversation> list = await _vzmClientSdk.getConversationManager().getAllConversation();
    controller.addAll(convertToChatBaseList(list).toList());
    return true;
  }

  List<ChatBase> convertToChatBaseList(List<Conversation> list) {
    List<ChatWithMembers> chatBaseList = <ChatWithMembers>[];
    for (Conversation item in list) {
      ChatWithMembers? chatWithMembers = toChatBase(item);
      if (chatWithMembers != null) {
        chatBaseList.add(chatWithMembers);
      }
    }
    return chatBaseList;
  }

  ChatWithMembers? toChatBase(Conversation conversation) {
    List<ChatUser> members =
        conversation.recipients!.map((e) => ChatUser(id: e.id.toString(), username: e.address, avatarURL: e.avatarUrl)).toList();
    Chat chat = Chat(id: conversation.id.toString(), name: getConversationTitle(conversation), unreadCount: 0, avatar: getAvatarImage(conversation));
    if (members.isEmpty) {
      print("#chat.fetch.messages: toChatBase member is empty: ${conversation.serverId}");
      return null;
    }
    ChatWithMembers chatWithMembers = ChatWithMembers(
        lastMessage: ChatMessage(text: conversation.snippet, author: members[0], creationTimestamp: getTimeStamp(conversation)),
        members: members,
        chat: chat);
    return chatWithMembers;
  }

  String getAvatarImage(Conversation conversation) {
    String? avatar = conversation.groupAvatarUrl;
    if (avatar != null && avatar.isNotEmpty) {
      return conversation.groupAvatarUrl!;
    }
    return "https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
  }

  String getConversationTitle(Conversation? conversation) {
    if (conversation != null) {
      if (conversation.groupName != null) {
        return conversation.groupName!;
      } else {
        var addressList = conversation.recipients!.map((e) => e.address);
        return addressList.toString();
      }
    }
    return "";
  }

  int getTimeStamp(Conversation conversation) {
    if (conversation.time != null) {
      return conversation.time!;
    }
    return 0;
  }

  Future<bool> fetchChat(String conversationId) async {
    messageListController.items.clear();
    List<Message> list = await _vzmClientSdk.getMessageRepository().getAllMessages(int.parse(conversationId));
    messageListController.addAll(list.map((e) => toChatMessage(e)).toList().toList());
    return true;
  }

  ChatMessage toChatMessage(Message element) {
    ChatUser chatUser = ChatUser(
        id: element.sender.address.toString(),
        username: element.sender.name ?? element.sender.address,
        avatarURL: element.sender.avatarUrl);
    ChatMessage chatMessage = ChatMessage(author: chatUser, text: element.body ?? "no body found", creationTimestamp: element.time);
    if (element.attachments != null && element.attachments!.isNotEmpty) {
      Attachment attachment = element.attachments![0];
      print("#chat.fetch.message: toChatMessage ${attachment.toString()}");
      if (element.inbound == 1 || attachment.status == AttachmentStatus.downloaded) {
        chatMessage.attachment = attachment.path.toString();
        chatMessage.type = getChatMessageType(attachment.fileType);
      }
    }
    return chatMessage;
  }

  void sendMessage(String text, String conversationId) {
    _vzmClientSdk.sendMessage(int.parse(conversationId), text);
  }

  Future<void> sendFile(String path, String conversationId, String? text) async {
    bool shouldCompress = await _vzmClientSdk.getFileManager().shouldCompressImage(path, conversationId);
    if (shouldCompress) {
      File newFile = await compressImage(File(path));
      path = newFile.path;
    }
    _vzmClientSdk.sendFile(path, conversationId, text);
  }

  ChatUser getLocalUser() {
    if (_user == null) {
      Contact contact = _vzmClientSdk.getMyContact();
      _user = ChatUser(id: contact.address, username: contact.name, fullname: contact.name, avatarURL: contact.avatarUrl);
    }
    return _user!;
  }

  void subscribeForMessageChange(String conversationId) {
    _vzmClientSdk.getMessageRepository().subscribeForMessageChange().listen((newMessage) {
      print("#chat.new.message: subscribeForMessageChange new message $newMessage");
      newMessage = newMessage as Message;
      print("#chat.new.message: subscribeForMessageChange conversationId >> $conversationId,  new message ${newMessage.toString()}");
      if (newMessage.conversationId.toString() == conversationId) {
        fetchChat(conversationId);
      }
    });
  }

  void subscribeForConversationChange() {
    _vzmClientSdk.getConversationRepository().subscribeForConversationChange().listen((newConversation) {
      print("#chat.new.conversation: subscribeForConversationChange new conversation ${newConversation.toString()}");
      fetchChats();
    });
  }

  ChatMessageType? getChatMessageType(FileType? fileType) {
    switch(fileType) {
      case FileType.image:
        return ChatMessageType.image;
      case FileType.video:
        return ChatMessageType.video;
      case FileType.audio:
        return ChatMessageType.audio;
      default:
        return ChatMessageType.text;
    }
  }

  //todo: need to compress up to allowed max size
  Future<File> compressImage(File file) async {
    final filePath = file.absolute.path;
    final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";

    final compressedImage = await FlutterImageCompress.compressAndGetFile(
        filePath,
        outPath,
        minWidth: 600,
        minHeight: 600,
        quality: 80);
    return compressedImage!;
  }

  void notifyMqttManager(bool isInBackground) {
    _vzmClientSdk.notifyMqttManager(isInBackground: isInBackground);
  }
}
