/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:file_picker/file_picker.dart' as picker;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vzm_chat_sdk/ui/home/profile/profile_viewmodel.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final TextEditingController _recipientController = TextEditingController();
  final ProfileViewModel _model = ProfileViewModel();

  @override
  void dispose() {
    _recipientController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
        automaticallyImplyLeading: false,
      ),
      body: FutureBuilder(
        future: _model.getContact(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Contact contact = snapshot.data as Contact;
            return Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                    GestureDetector(
                      onTap: () {
                        showBottomSheet();
                      },
                      child: ClipOval(child: getAvatarImage(contact)),
                    ),
                    Row(
                      children: const [
                        Text(
                          "Name",
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                    buildNameWidget(contact),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Row(
                        children: const [
                          Text(
                            "Address",
                            style: TextStyle(color: Colors.grey, fontSize: 20),
                            textAlign: TextAlign.start,
                          )
                        ],
                      ),
                    ),
                    buildAddressWidget(contact)
                  ]),
                ));
          } else {
            return const CircularProgressIndicator();
          }
        },
      ),
    );
  }

  buildNameWidget(Contact contact) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            contact.name != null ? contact.name! : "",
            style: const TextStyle(color: Colors.black, fontSize: 20),
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
            onPressed: () {
              onEditNamePressed(contact.name != null ? contact.name! : "");
            },
            child: const Icon(Icons.edit),
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent)),
          )
        ],
      ),
    );
  }

  buildAddressWidget(Contact contact) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            contact.address,
            style: const TextStyle(color: Colors.black, fontSize: 20),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  getAvatarImage(Contact contact) {
    if (contact.avatarUrl != null && contact.avatarUrl!.isNotEmpty) {
      return Image.network(contact.avatarUrl!, width: 232, height: 232, fit: BoxFit.cover);
    }
    return Image.asset('assets/avatars/woman2.jpg', width: 232, height: 232, fit: BoxFit.cover);
  }

  showAlertDialog(BuildContext context, String name) {
    final TextEditingController _textController = TextEditingController();
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: () async {
        updateName(_textController.text);
      },
    );
    Widget cancelButton = TextButton(
      child: const Text("Cancel", style: TextStyle(color: Colors.black)),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    AlertDialog alert = AlertDialog(
      title: const Text("Enter your name"),
      content: TextField(
        keyboardType: TextInputType.text,
        controller: _textController,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: name,
        ),
      ),
      actions: [
        okButton, cancelButton
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> onEditNamePressed(String name) async {
    showAlertDialog(context, name);
  }

  Future<void> updateName(String updatedName) async {
    if (updatedName.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Please enter the name"),
      ));
    } else {
      Navigator.of(context).pop();
      _showProgress();
      await _model.updateName(updatedName, context);
      _hideProgress();
    }
  }

  void _showProgress() {
    setState(() {
      buildShowDialog(context);
    });
  }

  void _hideProgress() {
    setState(() {
      Navigator.of(context).pop();
    });
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  showBottomSheet() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          // we set up a container inside which
          // we create center column and display text
          return SizedBox(
            height: 200,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ElevatedButton(
                    onPressed: _openCamera,
                    child: Text("Camera"),
                  ),
                  ElevatedButton(
                    onPressed: _openImage,
                    child: Text("Image"),
                  ),
                ],
              ),
            ),
          );
        });
  }

  _openCamera() async {
    var picture = await ImagePicker.platform.pickImage(
      source: ImageSource.camera,
    );
    Navigator.pop(context);
    _showProgress();
    await _model.updateAvatar(picture!.path, context);
    _hideProgress();
  }

  _openImage() {
    _openMediaFiles(picker.FileType.image);
  }

  Future<void> _openMediaFiles(picker.FileType fileType) async {
    var picture = await ImagePicker.platform.pickImage(
      source: ImageSource.gallery,
    );
    Navigator.pop(context);
    _showProgress();
    await _model.updateAvatar(picture!.path, context);
    _hideProgress();
  }
}
