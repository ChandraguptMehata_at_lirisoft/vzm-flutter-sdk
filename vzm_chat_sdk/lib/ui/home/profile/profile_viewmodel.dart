/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:flutter/material.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

class ProfileViewModel {

  final VzmClientSdk _vzmClientSdk = appServiceLocator.get<VzmClientSdk>();

  Future<Contact> getContact() async {
    return _vzmClientSdk.getProfileManager().getProfile();
  }

  updateName(String name, BuildContext context) async {
    try {
      await _vzmClientSdk.getProfileManager().setName(name);
    } on Exception catch (_e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(_e.toString()),
      ));
    }
  }

  Future<void> updateAvatar(String path, BuildContext context) async {
    try {
      await _vzmClientSdk.getProfileManager().setAvatar(path);
    } on Exception catch (_e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(_e.toString()),
      ));
    }
  }

}
