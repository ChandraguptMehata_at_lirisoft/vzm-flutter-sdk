/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/material.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_chat_sdk/ui/home/chat/chat_screen.dart';
import 'package:vzm_chat_sdk/ui/home/chat/chat_viewmodel.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

class CreateConversationViewModel extends ChatViewModel {
  final VzmClientSdk _vzmClientSdk = appServiceLocator.get<VzmClientSdk>();

  Future<void> createOneToOneConversation(String participant, BuildContext context) async {
    Conversation conversation = await _vzmClientSdk.getChatManager().create(participant);
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => ChatScreen(ChatScreenArgs(chat: toChatBase(conversation)!))));
  }

  Future<void> createGroupConversation(List<String> participants, String? groupName, String? avatar, BuildContext context) async {
    try {
      Conversation? conversation = await _vzmClientSdk.getGroupChatManager().createOpenGroupChat(participants, groupName, avatar);
      if (conversation != null) {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => ChatScreen(ChatScreenArgs(chat: toChatBase(conversation)!))));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Could not created conversation"),
        ));
      }
    } on Exception catch (_e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(_e.toString()),
      ));
    }
  }

  Future<void> createMMSConversation(Set<String> participants, BuildContext context) async {
    try {
      Conversation? conversation = await _vzmClientSdk.getChatManager().createMmsGroupChat(participants);
      if (conversation != null) {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => ChatScreen(ChatScreenArgs(chat: toChatBase(conversation)!))));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Could not created conversation"),
        ));
      }
    } on Exception catch (_e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(_e.toString()),
      ));
    }
  }
}
