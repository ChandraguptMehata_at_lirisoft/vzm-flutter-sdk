/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/material.dart';

import 'create_conversation_viewmodel.dart';

class CreateConversationScreen extends StatefulWidget {
  const CreateConversationScreen({Key? key}) : super(key: key);

  @override
  State<CreateConversationScreen> createState() => _CreateConversationScreenState();
}

class _CreateConversationScreenState extends State<CreateConversationScreen> {
  final TextEditingController _textController = TextEditingController();
  final CreateConversationViewModel _model = CreateConversationViewModel();

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Conversation"),
        automaticallyImplyLeading: false,
      ),
      body: Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: TextField(
                  keyboardType: TextInputType.phone,
                  controller: _textController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter the recipient number',
                  ),
                ),
              ),
              ElevatedButton(
                  onPressed: _createConversation,
                  child: const Text(
                    "Create",
                    style: TextStyle(fontSize: 18),
                  ))
            ],
          )),
    );
  }

  _createConversation() {
    if (_textController.text.isNotEmpty) {
      _model.createOneToOneConversation(_textController.text, context);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Please add recipients"),
      ));
    }
  }
}
