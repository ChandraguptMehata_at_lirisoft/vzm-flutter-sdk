/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/material.dart';

import 'create_conversation_viewmodel.dart';

class CreateGroupConversationScreen extends StatefulWidget {
  const CreateGroupConversationScreen({Key? key}) : super(key: key);

  @override
  State<CreateGroupConversationScreen> createState() => _CreateConversationScreenState();
}

class _CreateConversationScreenState extends State<CreateGroupConversationScreen> {
  final TextEditingController _recipientController = TextEditingController();
  final TextEditingController _groupTextController = TextEditingController();
  final CreateConversationViewModel _model = CreateConversationViewModel();
  final Set<String> _recipients = {};

  @override
  void dispose() {
    _recipientController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Group"),
        automaticallyImplyLeading: false,
      ),
      body: Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 200,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: _recipients.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _buildChip(_recipients.toList()[index], Colors.blue);
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: TextField(
                  keyboardType: TextInputType.text,
                  controller: _groupTextController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter the group name',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 5, 20, 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 250,
                      child: TextField(
                        keyboardType: TextInputType.phone,
                        controller: _recipientController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Enter the recipient number',
                        ),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: _addRecipient,
                        child: const Text(
                          "Add",
                          style: TextStyle(fontSize: 18),
                        )),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: ElevatedButton(
                      onPressed: _createGroup,
                      child: const Text(
                        "Create Group",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  )
                ],
              )
            ],
          )),
    );
  }

  Widget _buildChip(String label, Color color) {
    return Chip(
      labelPadding: const EdgeInsets.all(8.0),
      avatar: const CircleAvatar(
        backgroundColor: Colors.grey,
        child: Icon(Icons.group),
      ),
      label: Text(
        label,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: color,
      elevation: 6.0,
      shadowColor: Colors.grey[60],
      padding: const EdgeInsets.all(8.0),
    );
  }

  _addRecipient() {
    setState(() {
      if (_recipientController.text.isNotEmpty) {
        _recipients.add(_recipientController.text);
        _recipientController.clear();
      }
    });
  }

  _createGroup() {
    if (_recipients.isNotEmpty) {
      _model.createGroupConversation(_recipients.toList(), _groupTextController.text, null, context);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Please add recipients"),
      ));
    }
  }
}
