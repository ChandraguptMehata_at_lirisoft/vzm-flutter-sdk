/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

class ChatView extends StatefulWidget {
  final int? threadId;

  const ChatView(this.threadId, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ChatViewState();
  }
}

class _ChatViewState extends State<ChatView> {

  _ChatViewState();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    ConversationRepository _conversationRepository = appServiceLocator.get<VzmClientSdk>().getConversationRepository();

    return ChangeNotifierProvider<MessageRepository>(
        create: (context) => appServiceLocator.get<VzmClientSdk>().getMessageManager(),
        child: Consumer<MessageRepository>(
            builder: (context, messageRepository, child) {
          return FutureBuilder(
              future: _conversationRepository.findConversationByThreadId(widget.threadId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Scaffold(
                    appBar: AppBar(title: Text(getTitleName(snapshot.data as Conversation))),
                    body: FutureBuilder(
                      future: messageRepository.getAllMessages(widget.threadId),
                      builder: (context, snapshotMessage) {
                        if (snapshotMessage.hasData) {
                          List<Message> messageList =
                          snapshotMessage.data as List<Message>;
                          return ListView.builder(
                              itemCount: messageList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return ListTile(
                                    leading: Icon(Icons.list),
                                    trailing: Text(
                                      "GFG",
                                      style: TextStyle(
                                          color: Colors.green, fontSize: 15),
                                    ),
                                    title:
                                        Text(getMsgBody(messageList[index])));
                              });
                        } else {
                          return CircularProgressIndicator();
                        }
                        ;
                      },
                    ),
                  );
                } else {
                  return CircularProgressIndicator();
                }
              });
        }));
  }

  String getMsgBody(Message message) {
    if (message.body == null) {
      return "no body found";
    }
    return message.body!;
  }

  String getTitleName(Conversation conversation) {
    if (conversation.groupName != null) {
      return conversation.groupName!;
    } else {
      var addressList = conversation.recipients!.map((e) => e.address);
      return addressList.toString();
    }
  }
}
