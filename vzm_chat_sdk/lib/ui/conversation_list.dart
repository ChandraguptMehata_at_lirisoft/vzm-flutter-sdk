/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

class ConversationList extends StatefulWidget {

  const ConversationList({Key? key}) : super(key: key);

  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> with WidgetsBindingObserver {
  var flexWindowMobile = 0;
  var flexLeftRight = 0;
  final VzmClientSdk _vzmClientSdk = appServiceLocator.get<VzmClientSdk>();

  @override
  void initState() {
    super.initState();
    (Platform.isWindows) ? flexWindowMobile = 8 : flexWindowMobile = 1;
    (Platform.isWindows) ? flexLeftRight = 2 : flexLeftRight = 0;
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        _vzmClientSdk.notifyMqttManager(isInBackground: false);
        break;

      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
      _vzmClientSdk.notifyMqttManager(isInBackground: true);
      break;

      default:
        break;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Conversation'),
          automaticallyImplyLeading: false,
          actions: getPopupMenuActionButton(),
        ),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            margin: (!Platform.isWindows)
                ? const EdgeInsets.only(left: 15, right: 15)
                : EdgeInsets.only(left: 0, right: 0),
            child: Row(children: [
              Expanded(flex: flexLeftRight, child: Container()),
              Expanded(
                flex: flexWindowMobile,
                child: Consumer<ConversationRepository>(
                  builder: (context, conversationRepository, child) {
                    return FutureBuilder(
                      future: conversationRepository.getAllConversation(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          List<Conversation> convList = snapshot.data as List<Conversation>;
                          return ListView.builder(
                            shrinkWrap: true,
                            itemCount: convList.length,
                            itemBuilder: (BuildContext ctxt, int index) {
                              if (convList.isNotEmpty) {
                                var conversation = convList[index];
                                return _getListItemTile(conversation);
                              } else {
                                return CircularProgressIndicator();
                              }
                              //  return new Text('index is $index');
                            }, // use it
                          );
                        } else {
                          return CircularProgressIndicator();
                        }
                      }
                    );
                  },
                ),
              ),
              Expanded(flex: flexLeftRight, child: Container()),
            ]),
          ),
        ));
  }

  Widget _getListItemTile(Conversation conversation) {
    return GestureDetector(
      onTap: () {
        _onConvItemClick(conversation);
      },
      child: Container(
        margin: EdgeInsets.only(top: 15, bottom: 15),
        child: Row(
          children: [
            Stack(children: [
              Container(
                height: 90,
                width: 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(55),
                  image: DecorationImage(
                    image: NetworkImage(getAvatarImage(conversation)),
                    //whatever image you can put here
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: 0,
                left: 65,
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: new BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.circle,
                  ),
                  child: Text(
                    '25',
                    style: TextStyle(
                        color: Colors.white, fontSize: 10),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            ]),
            SizedBox(width: 15),
            Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      getConversationTitle(conversation),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 5),
                    Text(conversation.snippet != null ? conversation.snippet! : "",
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.normal),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis),
                    SizedBox(height: 5),
                    Text(getTimeStamp(conversation),
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.normal)),
                  ]),
            ),
            Icon(
              Icons.voice_over_off_outlined,
              size: Platform.isWindows ? 20 : 15,
            )
          ],
        ),
      ),
    );
  }

  List<Widget> getPopupMenuActionButton() {
    return [
      PopupMenuButton<int>(
          onSelected: (item) => onSelected(context, item),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("Sync"),
              value: 1,
            ),
            PopupMenuItem(
              child: Text("Restore"),
              value: 2,
            ),
            PopupMenuItem(
              child: Text("Delete"),
              value: 3,
            ),
            PopupMenuItem(
              child: Text("Clear DB"),
              value: 4,
            )
          ])
    ];
  }

  String getConversationTitle(Conversation? conversation) {
    if (conversation != null) {
      if (conversation.groupName != null) {
        return conversation.groupName!;
      } else {
        var addressList = conversation.recipients!.map((e) => e.address);
        return addressList.toString();
      }
    }
    return "";
  }

  String getTimeStamp(Conversation conversation) {
    if (conversation.time != null) {
      var time = new DateTime.fromMillisecondsSinceEpoch(conversation.time!);
      return time.toString();
    }
    return "";
  }

  void onSelected(BuildContext context, int item) {
    switch(item) {
      case 1:
        performSync();
        break;
      case 2:
        performRestore();
        break;
      case 4:
        _vzmClientSdk.clearDb();
        break;
    }
  }

  String getAvatarImage(Conversation conversation) {
    String? avatar = conversation.groupAvatarUrl;
    if (avatar != null && avatar.isNotEmpty) {
      return conversation.groupAvatarUrl!;
    }
    return "https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
  }

  static int value = 0;

  void _onConvItemClick(Conversation conversation) {
    /*Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ChatPage()),
    );*/
  }

  void performRestore() {
    _vzmClientSdk.startRestore();
  }

  void performSync() {
    _vzmClientSdk.startSync();
  }

}
