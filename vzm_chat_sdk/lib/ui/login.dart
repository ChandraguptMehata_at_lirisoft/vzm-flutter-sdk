/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_chat_sdk/src/sdk_initializer.dart';
import 'package:vzm_chat_sdk/util/Utis.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

import 'otp_verify.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _controller = TextEditingController(text: '9255422539');
  final _formKey = GlobalKey<FormState>();
  late String iserrorapi = "";
  late Utils utils;

  @override
  Widget build(BuildContext context) {
    utils = appServiceLocator.get<Utils>();
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Form(
        key: _formKey,
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 1.8,
                child: TextFormField(
                  controller: _controller,
                  keyboardType: TextInputType.phone,
                  maxLength: 10,
                  cursorColor: Colors.greenAccent,
                  decoration: InputDecoration(
                      errorText:
                          iserrorapi.length == 0 ? null : iserrorapi.toString(),
                      hintText: "Enter mobile number",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0))),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please enter mobile number";
                    } else if (value.length < 10) {
                      return "Mobile number should be 10 digit";
                    } else
                      return null;
                  },
                ),
              ),
              submit_button()
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget submit_button() {
    return RaisedButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            FocusScope.of(context).unfocus();
            callApi();
          }
        },
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(80.0))),
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: const Text('Submit', style: TextStyle(fontSize: 20))));
  }

  Future<Null> callApi() async {
    utils.showLoaderDialog(context);
    SdkInitializer sdk = appServiceLocator.get<SdkInitializer>();
    var data = await sdk.clientSdk
        .getAuthManager()
        .requestOtp(_controller.text.toString());
    Navigator.pop(context);
    //Responce Parse
    if (data!.status != AuthStatus.SUCCESS) {
      (Platform.isWindows)
          ? utils.showAlertDialog(context, data.status.toString())
          : utils.showToast(data.status.toString());
    } else {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) =>
              OTPVERIFY(mobilenumber: _controller.text.toString())));
    }
  }
}
