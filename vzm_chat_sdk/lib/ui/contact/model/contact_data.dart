/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:typed_data';

class ContactData {
  String displayName;
  String initials;
  String? givenName;
  String? middleName;
  String? familyName;
  Uint8List? avatar;

  ContactData(this.displayName, this.initials, this.avatar);
}
