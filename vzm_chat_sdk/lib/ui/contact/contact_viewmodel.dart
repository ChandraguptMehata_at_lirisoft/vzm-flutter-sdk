import 'package:contacts_service/contacts_service.dart';

import 'model/contact_data.dart';

class ContactViewModel {
  ContactViewModel();

  Future<Iterable<ContactData>> fetchContacts() async {
    List<ContactData> contactModelList = [];
    final Iterable<Contact> contacts = await ContactsService.getContacts();
    for (Contact contact in contacts) {
      contactModelList.add(ContactData(contact.displayName != null ? contact.displayName! : "", contact.initials(), contact.avatar));
    }
    return contactModelList;
  }
}