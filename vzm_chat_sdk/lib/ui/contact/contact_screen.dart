/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'contact_viewmodel.dart';
import 'model/contact_data.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  State<ContactScreen> createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  Iterable<ContactData>? _contacts;
  final ContactViewModel _model = ContactViewModel();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    getContacts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (Navigator.canPop(context)) {
          Navigator.of(context).pop();
        } else {
          SystemNavigator.pop();
        }
        return false;
      },
      child: Scaffold(
        appBar: AppBar(title: (const Text('Contacts')), automaticallyImplyLeading: false),
        body: _contacts != null
            ? ListView.builder(
                itemCount: _contacts!.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  ContactData? contact = _contacts!.elementAt(index);
                  return ListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 2, horizontal: 18),
                    leading: (contact.avatar != null && contact.avatar!.isNotEmpty)
                        ? CircleAvatar(
                            backgroundImage: MemoryImage(contact.avatar!),
                          )
                        : CircleAvatar(
                            child: Text(contact.initials),
                            backgroundColor: Theme.of(context).accentColor,
                          ),
                    title: Text(contact.displayName ?? ''),
                  );
                },
              )
            : const Center(child: CircularProgressIndicator()),
      ),
    );
  }

  Future<void> getContacts() async {
    final Iterable<ContactData> contacts = await _model.fetchContacts();
    setState(() {
      _contacts = contacts;
    });
  }
}
