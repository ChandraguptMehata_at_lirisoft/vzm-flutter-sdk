/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/src/widgets/framework.dart';
import 'package:vzm_chat_sdk/di/app_locator.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

class SdkInitializer {
  late VzmClientSdk clientSdk;
  BuildContext? context;
  bool isSDKInitialized = false;

  SdkInitializer(this.context);

  Future<bool> init() async {
    print('App Initialized:  Started');
    clientSdk = appServiceLocator.get<VzmClientSdk>();
    await clientSdk.init(context);
    print('App Initialized:  Done');
    return true;
  }
}
