
import 'package:flutter/src/widgets/framework.dart';
import 'package:get_it/get_it.dart';
import 'package:vzm_chat_sdk/src/sdk_initializer.dart';
import 'package:vzm_chat_sdk/util/Utis.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

final appServiceLocator = GetIt.instance;
bool isRegistered = false;

void setUp(BuildContext context){


  if (!isRegistered) {
    isRegistered = true;
    appServiceLocator.registerLazySingleton<VzmClientSdk>(
            () => VzmClientSdk(context));
    appServiceLocator.registerLazySingleton<SdkInitializer>(
            () => SdkInitializer(context));
    appServiceLocator.registerLazySingleton<Utils>(
            () => Utils());
  }

}