// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: camel_case_types

import 'dart:typed_data';

import 'package:objectbox/flatbuffers/flat_buffers.dart' as fb;
import 'package:objectbox/internal.dart'; // generated code can access "internal" functionality
import 'package:objectbox/objectbox.dart';
import 'package:objectbox_flutter_libs/objectbox_flutter_libs.dart';

import 'src/sdk-impl/database/entity/attachment_entity.dart';
import 'src/sdk-impl/database/entity/contact_entity.dart';
import 'src/sdk-impl/database/entity/conversation_entity.dart';
import 'src/sdk-impl/database/entity/file_queue_entity.dart';
import 'src/sdk-impl/database/entity/message_entity.dart';
import 'src/sdk-impl/database/entity/msg_report_entity.dart';
import 'src/sdk-impl/database/entity/profile_entity.dart';
import 'src/sdk-impl/database/entity/restore_queue_entity.dart';
import 'src/sdk-impl/database/entity/send_queue_entity.dart';
import 'src/sdk-impl/database/entity/upload_queue_entity.dart';

export 'package:objectbox/objectbox.dart'; // so that callers only have to import this file

final _entities = <ModelEntity>[
  ModelEntity(
      id: const IdUid(1, 3946092755380858796),
      name: 'AttachmentEntity',
      lastPropertyId: const IdUid(11, 4525947849569747318),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 8304521101021149469),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 6431414253706879545),
            name: 'mimeType',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 517706980901716542),
            name: 'progress',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 3190113773757279020),
            name: 'serverUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 8419222978000003940),
            name: 'duration',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 7440916496893263953),
            name: 'attachmentStatus',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 5293664027218840994),
            name: 'thumbnailUri',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(9, 3380626728683109180),
            name: 'localUri',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(10, 3872947854737489940),
            name: 'fileType',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(11, 4525947849569747318),
            name: 'size',
            type: 8,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(2, 6078151980846501960),
      name: 'ConversationEntity',
      lastPropertyId: const IdUid(17, 2801410798839648068),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 6765221814309571476),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 5672441553122239996),
            name: 'recipients',
            type: 30,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 8050966882129570742),
            name: 'creatorId',
            type: 11,
            flags: 520,
            indexId: const IdUid(1, 6311703608913106813),
            relationTarget: 'ContactEntity'),
        ModelProperty(
            id: const IdUid(4, 8587252174001184039),
            name: 'time',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 2575099611078162949),
            name: 'unreadCount',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 2659868883858066659),
            name: 'muted',
            type: 1,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 8033678878806624174),
            name: 'snippet',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 5081724313681659264),
            name: 'read',
            type: 1,
            flags: 0),
        ModelProperty(
            id: const IdUid(9, 6894487628555907116),
            name: 'groupName',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(10, 8957315326670585674),
            name: 'groupAvatarUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(11, 8251011028382160318),
            name: 'groupBackgroundUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(12, 5925440080660515940),
            name: 'lastMessageId',
            type: 11,
            flags: 520,
            indexId: const IdUid(2, 6443999064557555736),
            relationTarget: 'MessageEntity'),
        ModelProperty(
            id: const IdUid(13, 3460995529765832827),
            name: 'serverId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(14, 588529416290116162),
            name: 'ottId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(15, 8950075793941663912),
            name: 'rcsId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(16, 2037005963068047764),
            name: 'groupUpdatedTime',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(17, 2801410798839648068),
            name: 'conversationType',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[
        ModelRelation(
            id: const IdUid(1, 3281025151043607556),
            name: 'groupMembers',
            targetId: const IdUid(7, 8262447157379235778)),
        ModelRelation(
            id: const IdUid(2, 1903703394751529843),
            name: 'groupAdmins',
            targetId: const IdUid(7, 8262447157379235778))
      ],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(3, 308219736157413773),
      name: 'MessageEntity',
      lastPropertyId: const IdUid(13, 3747817089641425590),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 1644409680931036844),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 4470775517775243929),
            name: 'time',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 3931238167925951340),
            name: 'unread',
            type: 1,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 8047552416892161462),
            name: 'clientMsgId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 3397845523325166703),
            name: 'body',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 7674386892851437498),
            name: 'subject',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 1741326624022107211),
            name: 'conversationId',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 3737610027823647302),
            name: 'senderId',
            type: 11,
            flags: 520,
            indexId: const IdUid(4, 1371623638665042258),
            relationTarget: 'ContactEntity'),
        ModelProperty(
            id: const IdUid(9, 1392681646063936769),
            name: 'serverId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(10, 8748902771348162655),
            name: 'read',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(11, 2797041781774500280),
            name: 'parentMsgId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(12, 3631514459896245370),
            name: 'messageType',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(13, 3747817089641425590),
            name: 'messageStatus',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[
        ModelRelation(
            id: const IdUid(3, 4101994307992819252),
            name: 'recipients',
            targetId: const IdUid(7, 8262447157379235778)),
        ModelRelation(
            id: const IdUid(4, 8660499754158243338),
            name: 'reports',
            targetId: const IdUid(4, 7803439852860133003)),
        ModelRelation(
            id: const IdUid(5, 8779587493911177861),
            name: 'attachments',
            targetId: const IdUid(1, 3946092755380858796))
      ],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(4, 7803439852860133003),
      name: 'MessageReportEntity',
      lastPropertyId: const IdUid(3, 1094211346107631855),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 7045814433283249446),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 8231617097701242796),
            name: 'mdn',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 1094211346107631855),
            name: 'time',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(5, 6813796404777283861),
      name: 'ProfileEntity',
      lastPropertyId: const IdUid(8, 1202726861067514739),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 3300519890509446705),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 5518840430378292627),
            name: 'address',
            type: 9,
            flags: 2080,
            indexId: const IdUid(5, 5238486769737416638)),
        ModelProperty(
            id: const IdUid(3, 1225577156016115935),
            name: 'name',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 435165698171314876),
            name: 'profileId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 5188426836890574056),
            name: 'avatarUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 3033068061935055965),
            name: 'avatarChecksum',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 935000234742454632),
            name: 'updatedTime',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 1202726861067514739),
            name: 'createdTime',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(6, 2642808375670100219),
      name: 'RestoreQueueEntity',
      lastPropertyId: const IdUid(3, 6601663784232314408),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 8172538328301385017),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 8039410162667048614),
            name: 'groupId',
            type: 9,
            flags: 2080,
            indexId: const IdUid(6, 1716512114631623980)),
        ModelProperty(
            id: const IdUid(3, 6601663784232314408),
            name: 'updatedTime',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(7, 8262447157379235778),
      name: 'ContactEntity',
      lastPropertyId: const IdUid(5, 2822043526241319),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 4913240299051595778),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 216883259818071717),
            name: 'address',
            type: 9,
            flags: 2080,
            indexId: const IdUid(7, 806910747018405533)),
        ModelProperty(
            id: const IdUid(3, 5637549200096768582),
            name: 'name',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 6425573765407846756),
            name: 'avatarUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 2822043526241319),
            name: 'email',
            type: 9,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(8, 2292680877601950759),
      name: 'SendQueueEntity',
      lastPropertyId: const IdUid(7, 6043962122109338080),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 2358083500123319265),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 259962980611407828),
            name: 'luId',
            type: 6,
            flags: 40,
            indexId: const IdUid(65, 4313886120745746506)),
        ModelProperty(
            id: const IdUid(3, 6313540489064752003),
            name: 'serverId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 2250642355054235317),
            name: 'time',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 4733172841822094274),
            name: 'sendMessageStatus',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(9, 6363934171683182850),
      name: 'FileQueueEntity',
      lastPropertyId: const IdUid(10, 5863807536467950255),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 25155594718454151),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(3, 5236300052172243683),
            name: 'attachmentId',
            type: 6,
            flags: 40,
            indexId: const IdUid(67, 952843249596236421)),
        ModelProperty(
            id: const IdUid(4, 8889936899470327884),
            name: 'uploadUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 7458575779473344424),
            name: 'downloadUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 9087075950664493388),
            name: 'mimeType',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 5049013707680975291),
            name: 'fileTransferState',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 697609941275819119),
            name: 'fileTransferType',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(9, 2946802744880367446),
            name: 'localUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(10, 5863807536467950255),
            name: 'localMsgId',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[]),
  ModelEntity(
      id: const IdUid(10, 6531641540929076803),
      name: 'UploadQueueEntity',
      lastPropertyId: const IdUid(9, 7176110909120913636),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 7191774940314046293),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 2483740585365216181),
            name: 'messageId',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 7772281431263827357),
            name: 'serverId',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 4984531073489840612),
            name: 'attachmentId',
            type: 6,
            flags: 40,
            indexId: const IdUid(69, 1953026370781763131)),
        ModelProperty(
            id: const IdUid(5, 7644148570786782698),
            name: 'retryCount',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(6, 2009100386224615639),
            name: 'uploadUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(7, 2710501754260200337),
            name: 'downloadUrl',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(8, 1884498514772778589),
            name: 'uploadType',
            type: 6,
            flags: 0),
        ModelProperty(
            id: const IdUid(9, 7176110909120913636),
            name: 'uploadState',
            type: 6,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[])
];

/// Open an ObjectBox store with the model declared in this file.
Future<Store> openStore(
        {String? directory,
        int? maxDBSizeInKB,
        int? fileMode,
        int? maxReaders,
        bool queriesCaseSensitiveDefault = true,
        String? macosApplicationGroup}) async =>
    Store(getObjectBoxModel(),
        directory: directory ?? (await defaultStoreDirectory()).path,
        maxDBSizeInKB: maxDBSizeInKB,
        fileMode: fileMode,
        maxReaders: maxReaders,
        queriesCaseSensitiveDefault: queriesCaseSensitiveDefault,
        macosApplicationGroup: macosApplicationGroup);

/// ObjectBox model definition, pass it to [Store] - Store(getObjectBoxModel())
ModelDefinition getObjectBoxModel() {
  final model = ModelInfo(
      entities: _entities,
      lastEntityId: const IdUid(10, 6531641540929076803),
      lastIndexId: const IdUid(69, 1953026370781763131),
      lastRelationId: const IdUid(5, 8779587493911177861),
      lastSequenceId: const IdUid(0, 0),
      retiredEntityUids: const [],
      retiredIndexUids: const [
        7891148445050216518,
        9037546949612219325,
        1568968362527039573,
        200701851636697307,
        6244716849276069410,
        394386735883583296,
        3193625799923198765,
        6405882913491758030,
        6290642457619148665,
        5031249424206735809,
        8501603018448597697,
        7317740354370809254,
        3345770588173411837,
        8797562635876520874,
        8807414539180893798,
        94731882245184862,
        3075690816118820840,
        5129741631332946469,
        3438532551873295445,
        3510334605750610077,
        5834370484601781873,
        3259556258574268550,
        5292841901289486346,
        6942877335938960856,
        8567065327873950077,
        1112744230142704036,
        2297405424827984633,
        6624031671034189697,
        3729574304806610916,
        7712723661319334298,
        3336084864936342105,
        8888640481539872431,
        674125801144816926,
        5048808373914242360,
        3765717865355815531,
        8679432504922193943,
        827577641315793465,
        5216442226243347700,
        5940057553645480000,
        1247496097236613758,
        4250496206105761508,
        5768559715873537926,
        528547009810819994,
        3518951785954098660,
        4530124370873552522,
        9221535587641167073,
        9089230392700388829,
        8602065345646835357,
        6348401921918270177,
        324046776090122101,
        3554697067801758617,
        8869287348191415270,
        2149847379383921202,
        4337116543339235929,
        450911555163731511,
        3573673777284168469,
        4682169541570794484,
        7640527600451270040,
        2459555854880091733,
        4393962892736017676
      ],
      retiredPropertyUids: const [
        697648826452744832,
        6043962122109338080,
        25168507529650807,
        2048249380384297170
      ],
      retiredRelationUids: const [],
      modelVersion: 5,
      modelVersionParserMinimum: 5,
      version: 1);

  final bindings = <Type, EntityDefinition>{
    AttachmentEntity: EntityDefinition<AttachmentEntity>(
        model: _entities[0],
        toOneRelations: (AttachmentEntity object) => [],
        toManyRelations: (AttachmentEntity object) => {},
        getId: (AttachmentEntity object) => object.id,
        setId: (AttachmentEntity object, int id) {
          object.id = id;
        },
        objectToFB: (AttachmentEntity object, fb.Builder fbb) {
          final mimeTypeOffset = object.mimeType == null
              ? null
              : fbb.writeString(object.mimeType!);
          final serverUrlOffset = object.serverUrl == null
              ? null
              : fbb.writeString(object.serverUrl!);
          final thumbnailUriOffset = object.thumbnailUri == null
              ? null
              : fbb.writeString(object.thumbnailUri!);
          final localUriOffset = object.localUri == null
              ? null
              : fbb.writeString(object.localUri!);
          fbb.startTable(12);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, mimeTypeOffset);
          fbb.addInt64(2, object.progress);
          fbb.addOffset(3, serverUrlOffset);
          fbb.addInt64(4, object.duration);
          fbb.addInt64(6, object.attachmentStatus);
          fbb.addOffset(7, thumbnailUriOffset);
          fbb.addOffset(8, localUriOffset);
          fbb.addInt64(9, object.fileType);
          fbb.addFloat64(10, object.size);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = AttachmentEntity()
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..mimeType =
                const fb.StringReader().vTableGetNullable(buffer, rootOffset, 6)
            ..progress =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 8)
            ..serverUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 10)
            ..duration =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 12)
            ..attachmentStatus =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 16)
            ..thumbnailUri = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 18)
            ..localUri = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 20)
            ..fileType =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 22)
            ..size =
                const fb.Float64Reader().vTableGet(buffer, rootOffset, 24, 0);

          return object;
        }),
    ConversationEntity: EntityDefinition<ConversationEntity>(
        model: _entities[1],
        toOneRelations: (ConversationEntity object) =>
            [object.creator, object.lastMessage],
        toManyRelations: (ConversationEntity object) => {
              RelInfo<ConversationEntity>.toMany(1, object.id):
                  object.groupMembers,
              RelInfo<ConversationEntity>.toMany(2, object.id):
                  object.groupAdmins
            },
        getId: (ConversationEntity object) => object.id,
        setId: (ConversationEntity object, int id) {
          object.id = id;
        },
        objectToFB: (ConversationEntity object, fb.Builder fbb) {
          final recipientsOffset = fbb.writeList(
              object.recipients.map(fbb.writeString).toList(growable: false));
          final snippetOffset =
              object.snippet == null ? null : fbb.writeString(object.snippet!);
          final groupNameOffset = object.groupName == null
              ? null
              : fbb.writeString(object.groupName!);
          final groupAvatarUrlOffset = object.groupAvatarUrl == null
              ? null
              : fbb.writeString(object.groupAvatarUrl!);
          final groupBackgroundUrlOffset = object.groupBackgroundUrl == null
              ? null
              : fbb.writeString(object.groupBackgroundUrl!);
          final serverIdOffset = object.serverId == null
              ? null
              : fbb.writeString(object.serverId!);
          final ottIdOffset =
              object.ottId == null ? null : fbb.writeString(object.ottId!);
          final rcsIdOffset =
              object.rcsId == null ? null : fbb.writeString(object.rcsId!);
          fbb.startTable(18);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, recipientsOffset);
          fbb.addInt64(2, object.creator.targetId);
          fbb.addInt64(3, object.time);
          fbb.addInt64(4, object.unreadCount);
          fbb.addBool(5, object.muted);
          fbb.addOffset(6, snippetOffset);
          fbb.addBool(7, object.read);
          fbb.addOffset(8, groupNameOffset);
          fbb.addOffset(9, groupAvatarUrlOffset);
          fbb.addOffset(10, groupBackgroundUrlOffset);
          fbb.addInt64(11, object.lastMessage.targetId);
          fbb.addOffset(12, serverIdOffset);
          fbb.addOffset(13, ottIdOffset);
          fbb.addOffset(14, rcsIdOffset);
          fbb.addInt64(15, object.groupUpdatedTime);
          fbb.addInt64(16, object.conversationType);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = ConversationEntity()
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..recipients =
                const fb.ListReader<String>(fb.StringReader(), lazy: false)
                    .vTableGet(buffer, rootOffset, 6, [])
            ..time =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 10)
            ..unreadCount =
                const fb.Int64Reader().vTableGet(buffer, rootOffset, 12, 0)
            ..muted =
                const fb.BoolReader().vTableGet(buffer, rootOffset, 14, false)
            ..snippet = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 16)
            ..read =
                const fb.BoolReader().vTableGet(buffer, rootOffset, 18, false)
            ..groupName = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 20)
            ..groupAvatarUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 22)
            ..groupBackgroundUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 24)
            ..serverId = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 28)
            ..ottId = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 30)
            ..rcsId = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 32)
            ..groupUpdatedTime =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 34)
            ..conversationType = const fb.Int64Reader()
                .vTableGetNullable(buffer, rootOffset, 36);
          object.creator.targetId =
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 8, 0);
          object.creator.attach(store);
          object.lastMessage.targetId =
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 26, 0);
          object.lastMessage.attach(store);
          InternalToManyAccess.setRelInfo(
              object.groupMembers,
              store,
              RelInfo<ConversationEntity>.toMany(1, object.id),
              store.box<ConversationEntity>());
          InternalToManyAccess.setRelInfo(
              object.groupAdmins,
              store,
              RelInfo<ConversationEntity>.toMany(2, object.id),
              store.box<ConversationEntity>());
          return object;
        }),
    MessageEntity: EntityDefinition<MessageEntity>(
        model: _entities[2],
        toOneRelations: (MessageEntity object) => [object.sender],
        toManyRelations: (MessageEntity object) => {
              RelInfo<MessageEntity>.toMany(3, object.id): object.recipients,
              RelInfo<MessageEntity>.toMany(4, object.id): object.reports,
              RelInfo<MessageEntity>.toMany(5, object.id): object.attachments
            },
        getId: (MessageEntity object) => object.id,
        setId: (MessageEntity object, int id) {
          object.id = id;
        },
        objectToFB: (MessageEntity object, fb.Builder fbb) {
          final clientMsgIdOffset = object.clientMsgId == null
              ? null
              : fbb.writeString(object.clientMsgId!);
          final bodyOffset =
              object.body == null ? null : fbb.writeString(object.body!);
          final subjectOffset =
              object.subject == null ? null : fbb.writeString(object.subject!);
          final serverIdOffset = object.serverId == null
              ? null
              : fbb.writeString(object.serverId!);
          final parentMsgIdOffset = object.parentMsgId == null
              ? null
              : fbb.writeString(object.parentMsgId!);
          fbb.startTable(14);
          fbb.addInt64(0, object.id);
          fbb.addInt64(1, object.time);
          fbb.addBool(2, object.unread);
          fbb.addOffset(3, clientMsgIdOffset);
          fbb.addOffset(4, bodyOffset);
          fbb.addOffset(5, subjectOffset);
          fbb.addInt64(6, object.conversationId);
          fbb.addInt64(7, object.sender.targetId);
          fbb.addOffset(8, serverIdOffset);
          fbb.addInt64(9, object.read);
          fbb.addOffset(10, parentMsgIdOffset);
          fbb.addInt64(11, object.messageType);
          fbb.addInt64(12, object.messageStatus);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = MessageEntity()
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..time = const fb.Int64Reader().vTableGet(buffer, rootOffset, 6, 0)
            ..unread =
                const fb.BoolReader().vTableGet(buffer, rootOffset, 8, false)
            ..clientMsgId = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 10)
            ..body = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 12)
            ..subject = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 14)
            ..conversationId =
                const fb.Int64Reader().vTableGet(buffer, rootOffset, 16, 0)
            ..serverId = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 20)
            ..read =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 22)
            ..parentMsgId = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 24)
            ..messageType =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 26)
            ..messageStatus = const fb.Int64Reader()
                .vTableGetNullable(buffer, rootOffset, 28);
          object.sender.targetId =
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 18, 0);
          object.sender.attach(store);
          InternalToManyAccess.setRelInfo(
              object.recipients,
              store,
              RelInfo<MessageEntity>.toMany(3, object.id),
              store.box<MessageEntity>());
          InternalToManyAccess.setRelInfo(
              object.reports,
              store,
              RelInfo<MessageEntity>.toMany(4, object.id),
              store.box<MessageEntity>());
          InternalToManyAccess.setRelInfo(
              object.attachments,
              store,
              RelInfo<MessageEntity>.toMany(5, object.id),
              store.box<MessageEntity>());
          return object;
        }),
    MessageReportEntity: EntityDefinition<MessageReportEntity>(
        model: _entities[3],
        toOneRelations: (MessageReportEntity object) => [],
        toManyRelations: (MessageReportEntity object) => {},
        getId: (MessageReportEntity object) => object.id,
        setId: (MessageReportEntity object, int id) {
          object.id = id;
        },
        objectToFB: (MessageReportEntity object, fb.Builder fbb) {
          final mdnOffset = fbb.writeString(object.mdn);
          fbb.startTable(4);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, mdnOffset);
          fbb.addInt64(2, object.time);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = MessageReportEntity()
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..mdn = const fb.StringReader().vTableGet(buffer, rootOffset, 6, '')
            ..time = const fb.Int64Reader().vTableGet(buffer, rootOffset, 8, 0);

          return object;
        }),
    ProfileEntity: EntityDefinition<ProfileEntity>(
        model: _entities[4],
        toOneRelations: (ProfileEntity object) => [],
        toManyRelations: (ProfileEntity object) => {},
        getId: (ProfileEntity object) => object.id,
        setId: (ProfileEntity object, int id) {
          object.id = id;
        },
        objectToFB: (ProfileEntity object, fb.Builder fbb) {
          final addressOffset = fbb.writeString(object.address);
          final nameOffset =
              object.name == null ? null : fbb.writeString(object.name!);
          final profileIdOffset = object.profileId == null
              ? null
              : fbb.writeString(object.profileId!);
          final avatarUrlOffset = object.avatarUrl == null
              ? null
              : fbb.writeString(object.avatarUrl!);
          final avatarChecksumOffset = object.avatarChecksum == null
              ? null
              : fbb.writeString(object.avatarChecksum!);
          fbb.startTable(9);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, addressOffset);
          fbb.addOffset(2, nameOffset);
          fbb.addOffset(3, profileIdOffset);
          fbb.addOffset(4, avatarUrlOffset);
          fbb.addOffset(5, avatarChecksumOffset);
          fbb.addInt64(6, object.updatedTime);
          fbb.addInt64(7, object.createdTime);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = ProfileEntity(
              const fb.StringReader().vTableGet(buffer, rootOffset, 6, ''),
              const fb.StringReader().vTableGetNullable(buffer, rootOffset, 8),
              const fb.StringReader().vTableGetNullable(buffer, rootOffset, 10),
              const fb.StringReader().vTableGetNullable(buffer, rootOffset, 12),
              const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 16),
              const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 18))
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..avatarChecksum = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 14);

          return object;
        }),
    RestoreQueueEntity: EntityDefinition<RestoreQueueEntity>(
        model: _entities[5],
        toOneRelations: (RestoreQueueEntity object) => [],
        toManyRelations: (RestoreQueueEntity object) => {},
        getId: (RestoreQueueEntity object) => object.id,
        setId: (RestoreQueueEntity object, int id) {
          object.id = id;
        },
        objectToFB: (RestoreQueueEntity object, fb.Builder fbb) {
          final groupIdOffset = fbb.writeString(object.groupId);
          fbb.startTable(4);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, groupIdOffset);
          fbb.addInt64(2, object.updatedTime);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = RestoreQueueEntity(
              const fb.StringReader().vTableGet(buffer, rootOffset, 6, ''))
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..updatedTime =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 8);

          return object;
        }),
    ContactEntity: EntityDefinition<ContactEntity>(
        model: _entities[6],
        toOneRelations: (ContactEntity object) => [],
        toManyRelations: (ContactEntity object) => {},
        getId: (ContactEntity object) => object.id,
        setId: (ContactEntity object, int id) {
          object.id = id;
        },
        objectToFB: (ContactEntity object, fb.Builder fbb) {
          final addressOffset = fbb.writeString(object.address);
          final nameOffset =
              object.name == null ? null : fbb.writeString(object.name!);
          final avatarUrlOffset = object.avatarUrl == null
              ? null
              : fbb.writeString(object.avatarUrl!);
          final emailOffset =
              object.email == null ? null : fbb.writeString(object.email!);
          fbb.startTable(6);
          fbb.addInt64(0, object.id);
          fbb.addOffset(1, addressOffset);
          fbb.addOffset(2, nameOffset);
          fbb.addOffset(3, avatarUrlOffset);
          fbb.addOffset(4, emailOffset);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = ContactEntity()
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..address =
                const fb.StringReader().vTableGet(buffer, rootOffset, 6, '')
            ..name =
                const fb.StringReader().vTableGetNullable(buffer, rootOffset, 8)
            ..avatarUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 10)
            ..email = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 12);

          return object;
        }),
    SendQueueEntity: EntityDefinition<SendQueueEntity>(
        model: _entities[7],
        toOneRelations: (SendQueueEntity object) => [],
        toManyRelations: (SendQueueEntity object) => {},
        getId: (SendQueueEntity object) => object.id,
        setId: (SendQueueEntity object, int id) {
          object.id = id;
        },
        objectToFB: (SendQueueEntity object, fb.Builder fbb) {
          final serverIdOffset = object.serverId == null
              ? null
              : fbb.writeString(object.serverId!);
          fbb.startTable(8);
          fbb.addInt64(0, object.id);
          fbb.addInt64(1, object.luId);
          fbb.addOffset(2, serverIdOffset);
          fbb.addInt64(3, object.time);
          fbb.addInt64(5, object.sendMessageStatus);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = SendQueueEntity(
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 6, 0))
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..serverId =
                const fb.StringReader().vTableGetNullable(buffer, rootOffset, 8)
            ..time =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 10)
            ..sendMessageStatus = const fb.Int64Reader()
                .vTableGetNullable(buffer, rootOffset, 14);

          return object;
        }),
    FileQueueEntity: EntityDefinition<FileQueueEntity>(
        model: _entities[8],
        toOneRelations: (FileQueueEntity object) => [],
        toManyRelations: (FileQueueEntity object) => {},
        getId: (FileQueueEntity object) => object.id,
        setId: (FileQueueEntity object, int id) {
          object.id = id;
        },
        objectToFB: (FileQueueEntity object, fb.Builder fbb) {
          final uploadUrlOffset = object.uploadUrl == null
              ? null
              : fbb.writeString(object.uploadUrl!);
          final downloadUrlOffset = object.downloadUrl == null
              ? null
              : fbb.writeString(object.downloadUrl!);
          final mimeTypeOffset = object.mimeType == null
              ? null
              : fbb.writeString(object.mimeType!);
          final localUrlOffset = object.localUrl == null
              ? null
              : fbb.writeString(object.localUrl!);
          fbb.startTable(11);
          fbb.addInt64(0, object.id);
          fbb.addInt64(2, object.attachmentId);
          fbb.addOffset(3, uploadUrlOffset);
          fbb.addOffset(4, downloadUrlOffset);
          fbb.addOffset(5, mimeTypeOffset);
          fbb.addInt64(6, object.fileTransferState);
          fbb.addInt64(7, object.fileTransferType);
          fbb.addOffset(8, localUrlOffset);
          fbb.addInt64(9, object.localMsgId);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = FileQueueEntity(
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 22, 0),
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 8, 0))
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..uploadUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 10)
            ..downloadUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 12)
            ..mimeType = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 14)
            ..fileTransferState =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 16)
            ..fileTransferType =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 18)
            ..localUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 20);

          return object;
        }),
    UploadQueueEntity: EntityDefinition<UploadQueueEntity>(
        model: _entities[9],
        toOneRelations: (UploadQueueEntity object) => [],
        toManyRelations: (UploadQueueEntity object) => {},
        getId: (UploadQueueEntity object) => object.id,
        setId: (UploadQueueEntity object, int id) {
          object.id = id;
        },
        objectToFB: (UploadQueueEntity object, fb.Builder fbb) {
          final serverIdOffset = object.serverId == null
              ? null
              : fbb.writeString(object.serverId!);
          final uploadUrlOffset = object.uploadUrl == null
              ? null
              : fbb.writeString(object.uploadUrl!);
          final downloadUrlOffset = object.downloadUrl == null
              ? null
              : fbb.writeString(object.downloadUrl!);
          fbb.startTable(10);
          fbb.addInt64(0, object.id);
          fbb.addInt64(1, object.messageId);
          fbb.addOffset(2, serverIdOffset);
          fbb.addInt64(3, object.attachmentId);
          fbb.addInt64(4, object.retryCount);
          fbb.addOffset(5, uploadUrlOffset);
          fbb.addOffset(6, downloadUrlOffset);
          fbb.addInt64(7, object.uploadType);
          fbb.addInt64(8, object.uploadState);
          fbb.finish(fbb.endTable());
          return object.id;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = UploadQueueEntity(
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 6, 0),
              const fb.Int64Reader().vTableGet(buffer, rootOffset, 10, 0))
            ..id = const fb.Int64Reader().vTableGet(buffer, rootOffset, 4, 0)
            ..serverId =
                const fb.StringReader().vTableGetNullable(buffer, rootOffset, 8)
            ..retryCount =
                const fb.Int64Reader().vTableGetNullable(buffer, rootOffset, 12)
            ..uploadUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 14)
            ..downloadUrl = const fb.StringReader()
                .vTableGetNullable(buffer, rootOffset, 16)
            ..uploadType =
                const fb.Int64Reader().vTableGet(buffer, rootOffset, 18, 0)
            ..uploadState =
                const fb.Int64Reader().vTableGet(buffer, rootOffset, 20, 0);

          return object;
        })
  };

  return ModelDefinition(model, bindings);
}

/// [AttachmentEntity] entity fields to define ObjectBox queries.
class AttachmentEntity_ {
  /// see [AttachmentEntity.id]
  static final id =
      QueryIntegerProperty<AttachmentEntity>(_entities[0].properties[0]);

  /// see [AttachmentEntity.mimeType]
  static final mimeType =
      QueryStringProperty<AttachmentEntity>(_entities[0].properties[1]);

  /// see [AttachmentEntity.progress]
  static final progress =
      QueryIntegerProperty<AttachmentEntity>(_entities[0].properties[2]);

  /// see [AttachmentEntity.serverUrl]
  static final serverUrl =
      QueryStringProperty<AttachmentEntity>(_entities[0].properties[3]);

  /// see [AttachmentEntity.duration]
  static final duration =
      QueryIntegerProperty<AttachmentEntity>(_entities[0].properties[4]);

  /// see [AttachmentEntity.attachmentStatus]
  static final attachmentStatus =
      QueryIntegerProperty<AttachmentEntity>(_entities[0].properties[5]);

  /// see [AttachmentEntity.thumbnailUri]
  static final thumbnailUri =
      QueryStringProperty<AttachmentEntity>(_entities[0].properties[6]);

  /// see [AttachmentEntity.localUri]
  static final localUri =
      QueryStringProperty<AttachmentEntity>(_entities[0].properties[7]);

  /// see [AttachmentEntity.fileType]
  static final fileType =
      QueryIntegerProperty<AttachmentEntity>(_entities[0].properties[8]);

  /// see [AttachmentEntity.size]
  static final size =
      QueryDoubleProperty<AttachmentEntity>(_entities[0].properties[9]);
}

/// [ConversationEntity] entity fields to define ObjectBox queries.
class ConversationEntity_ {
  /// see [ConversationEntity.id]
  static final id =
      QueryIntegerProperty<ConversationEntity>(_entities[1].properties[0]);

  /// see [ConversationEntity.recipients]
  static final recipients =
      QueryStringVectorProperty<ConversationEntity>(_entities[1].properties[1]);

  /// see [ConversationEntity.creator]
  static final creator = QueryRelationToOne<ConversationEntity, ContactEntity>(
      _entities[1].properties[2]);

  /// see [ConversationEntity.time]
  static final time =
      QueryIntegerProperty<ConversationEntity>(_entities[1].properties[3]);

  /// see [ConversationEntity.unreadCount]
  static final unreadCount =
      QueryIntegerProperty<ConversationEntity>(_entities[1].properties[4]);

  /// see [ConversationEntity.muted]
  static final muted =
      QueryBooleanProperty<ConversationEntity>(_entities[1].properties[5]);

  /// see [ConversationEntity.snippet]
  static final snippet =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[6]);

  /// see [ConversationEntity.read]
  static final read =
      QueryBooleanProperty<ConversationEntity>(_entities[1].properties[7]);

  /// see [ConversationEntity.groupName]
  static final groupName =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[8]);

  /// see [ConversationEntity.groupAvatarUrl]
  static final groupAvatarUrl =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[9]);

  /// see [ConversationEntity.groupBackgroundUrl]
  static final groupBackgroundUrl =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[10]);

  /// see [ConversationEntity.lastMessage]
  static final lastMessage =
      QueryRelationToOne<ConversationEntity, MessageEntity>(
          _entities[1].properties[11]);

  /// see [ConversationEntity.serverId]
  static final serverId =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[12]);

  /// see [ConversationEntity.ottId]
  static final ottId =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[13]);

  /// see [ConversationEntity.rcsId]
  static final rcsId =
      QueryStringProperty<ConversationEntity>(_entities[1].properties[14]);

  /// see [ConversationEntity.groupUpdatedTime]
  static final groupUpdatedTime =
      QueryIntegerProperty<ConversationEntity>(_entities[1].properties[15]);

  /// see [ConversationEntity.conversationType]
  static final conversationType =
      QueryIntegerProperty<ConversationEntity>(_entities[1].properties[16]);

  /// see [ConversationEntity.groupMembers]
  static final groupMembers =
      QueryRelationToMany<ConversationEntity, ContactEntity>(
          _entities[1].relations[0]);

  /// see [ConversationEntity.groupAdmins]
  static final groupAdmins =
      QueryRelationToMany<ConversationEntity, ContactEntity>(
          _entities[1].relations[1]);
}

/// [MessageEntity] entity fields to define ObjectBox queries.
class MessageEntity_ {
  /// see [MessageEntity.id]
  static final id =
      QueryIntegerProperty<MessageEntity>(_entities[2].properties[0]);

  /// see [MessageEntity.time]
  static final time =
      QueryIntegerProperty<MessageEntity>(_entities[2].properties[1]);

  /// see [MessageEntity.unread]
  static final unread =
      QueryBooleanProperty<MessageEntity>(_entities[2].properties[2]);

  /// see [MessageEntity.clientMsgId]
  static final clientMsgId =
      QueryStringProperty<MessageEntity>(_entities[2].properties[3]);

  /// see [MessageEntity.body]
  static final body =
      QueryStringProperty<MessageEntity>(_entities[2].properties[4]);

  /// see [MessageEntity.subject]
  static final subject =
      QueryStringProperty<MessageEntity>(_entities[2].properties[5]);

  /// see [MessageEntity.conversationId]
  static final conversationId =
      QueryIntegerProperty<MessageEntity>(_entities[2].properties[6]);

  /// see [MessageEntity.sender]
  static final sender = QueryRelationToOne<MessageEntity, ContactEntity>(
      _entities[2].properties[7]);

  /// see [MessageEntity.serverId]
  static final serverId =
      QueryStringProperty<MessageEntity>(_entities[2].properties[8]);

  /// see [MessageEntity.read]
  static final read =
      QueryIntegerProperty<MessageEntity>(_entities[2].properties[9]);

  /// see [MessageEntity.parentMsgId]
  static final parentMsgId =
      QueryStringProperty<MessageEntity>(_entities[2].properties[10]);

  /// see [MessageEntity.messageType]
  static final messageType =
      QueryIntegerProperty<MessageEntity>(_entities[2].properties[11]);

  /// see [MessageEntity.messageStatus]
  static final messageStatus =
      QueryIntegerProperty<MessageEntity>(_entities[2].properties[12]);

  /// see [MessageEntity.recipients]
  static final recipients = QueryRelationToMany<MessageEntity, ContactEntity>(
      _entities[2].relations[0]);

  /// see [MessageEntity.reports]
  static final reports =
      QueryRelationToMany<MessageEntity, MessageReportEntity>(
          _entities[2].relations[1]);

  /// see [MessageEntity.attachments]
  static final attachments =
      QueryRelationToMany<MessageEntity, AttachmentEntity>(
          _entities[2].relations[2]);
}

/// [MessageReportEntity] entity fields to define ObjectBox queries.
class MessageReportEntity_ {
  /// see [MessageReportEntity.id]
  static final id =
      QueryIntegerProperty<MessageReportEntity>(_entities[3].properties[0]);

  /// see [MessageReportEntity.mdn]
  static final mdn =
      QueryStringProperty<MessageReportEntity>(_entities[3].properties[1]);

  /// see [MessageReportEntity.time]
  static final time =
      QueryIntegerProperty<MessageReportEntity>(_entities[3].properties[2]);
}

/// [ProfileEntity] entity fields to define ObjectBox queries.
class ProfileEntity_ {
  /// see [ProfileEntity.id]
  static final id =
      QueryIntegerProperty<ProfileEntity>(_entities[4].properties[0]);

  /// see [ProfileEntity.address]
  static final address =
      QueryStringProperty<ProfileEntity>(_entities[4].properties[1]);

  /// see [ProfileEntity.name]
  static final name =
      QueryStringProperty<ProfileEntity>(_entities[4].properties[2]);

  /// see [ProfileEntity.profileId]
  static final profileId =
      QueryStringProperty<ProfileEntity>(_entities[4].properties[3]);

  /// see [ProfileEntity.avatarUrl]
  static final avatarUrl =
      QueryStringProperty<ProfileEntity>(_entities[4].properties[4]);

  /// see [ProfileEntity.avatarChecksum]
  static final avatarChecksum =
      QueryStringProperty<ProfileEntity>(_entities[4].properties[5]);

  /// see [ProfileEntity.updatedTime]
  static final updatedTime =
      QueryIntegerProperty<ProfileEntity>(_entities[4].properties[6]);

  /// see [ProfileEntity.createdTime]
  static final createdTime =
      QueryIntegerProperty<ProfileEntity>(_entities[4].properties[7]);
}

/// [RestoreQueueEntity] entity fields to define ObjectBox queries.
class RestoreQueueEntity_ {
  /// see [RestoreQueueEntity.id]
  static final id =
      QueryIntegerProperty<RestoreQueueEntity>(_entities[5].properties[0]);

  /// see [RestoreQueueEntity.groupId]
  static final groupId =
      QueryStringProperty<RestoreQueueEntity>(_entities[5].properties[1]);

  /// see [RestoreQueueEntity.updatedTime]
  static final updatedTime =
      QueryIntegerProperty<RestoreQueueEntity>(_entities[5].properties[2]);
}

/// [ContactEntity] entity fields to define ObjectBox queries.
class ContactEntity_ {
  /// see [ContactEntity.id]
  static final id =
      QueryIntegerProperty<ContactEntity>(_entities[6].properties[0]);

  /// see [ContactEntity.address]
  static final address =
      QueryStringProperty<ContactEntity>(_entities[6].properties[1]);

  /// see [ContactEntity.name]
  static final name =
      QueryStringProperty<ContactEntity>(_entities[6].properties[2]);

  /// see [ContactEntity.avatarUrl]
  static final avatarUrl =
      QueryStringProperty<ContactEntity>(_entities[6].properties[3]);

  /// see [ContactEntity.email]
  static final email =
      QueryStringProperty<ContactEntity>(_entities[6].properties[4]);
}

/// [SendQueueEntity] entity fields to define ObjectBox queries.
class SendQueueEntity_ {
  /// see [SendQueueEntity.id]
  static final id =
      QueryIntegerProperty<SendQueueEntity>(_entities[7].properties[0]);

  /// see [SendQueueEntity.luId]
  static final luId =
      QueryIntegerProperty<SendQueueEntity>(_entities[7].properties[1]);

  /// see [SendQueueEntity.serverId]
  static final serverId =
      QueryStringProperty<SendQueueEntity>(_entities[7].properties[2]);

  /// see [SendQueueEntity.time]
  static final time =
      QueryIntegerProperty<SendQueueEntity>(_entities[7].properties[3]);

  /// see [SendQueueEntity.sendMessageStatus]
  static final sendMessageStatus =
      QueryIntegerProperty<SendQueueEntity>(_entities[7].properties[4]);
}

/// [FileQueueEntity] entity fields to define ObjectBox queries.
class FileQueueEntity_ {
  /// see [FileQueueEntity.id]
  static final id =
      QueryIntegerProperty<FileQueueEntity>(_entities[8].properties[0]);

  /// see [FileQueueEntity.attachmentId]
  static final attachmentId =
      QueryIntegerProperty<FileQueueEntity>(_entities[8].properties[1]);

  /// see [FileQueueEntity.uploadUrl]
  static final uploadUrl =
      QueryStringProperty<FileQueueEntity>(_entities[8].properties[2]);

  /// see [FileQueueEntity.downloadUrl]
  static final downloadUrl =
      QueryStringProperty<FileQueueEntity>(_entities[8].properties[3]);

  /// see [FileQueueEntity.mimeType]
  static final mimeType =
      QueryStringProperty<FileQueueEntity>(_entities[8].properties[4]);

  /// see [FileQueueEntity.fileTransferState]
  static final fileTransferState =
      QueryIntegerProperty<FileQueueEntity>(_entities[8].properties[5]);

  /// see [FileQueueEntity.fileTransferType]
  static final fileTransferType =
      QueryIntegerProperty<FileQueueEntity>(_entities[8].properties[6]);

  /// see [FileQueueEntity.localUrl]
  static final localUrl =
      QueryStringProperty<FileQueueEntity>(_entities[8].properties[7]);

  /// see [FileQueueEntity.localMsgId]
  static final localMsgId =
      QueryIntegerProperty<FileQueueEntity>(_entities[8].properties[8]);
}

/// [UploadQueueEntity] entity fields to define ObjectBox queries.
class UploadQueueEntity_ {
  /// see [UploadQueueEntity.id]
  static final id =
      QueryIntegerProperty<UploadQueueEntity>(_entities[9].properties[0]);

  /// see [UploadQueueEntity.messageId]
  static final messageId =
      QueryIntegerProperty<UploadQueueEntity>(_entities[9].properties[1]);

  /// see [UploadQueueEntity.serverId]
  static final serverId =
      QueryStringProperty<UploadQueueEntity>(_entities[9].properties[2]);

  /// see [UploadQueueEntity.attachmentId]
  static final attachmentId =
      QueryIntegerProperty<UploadQueueEntity>(_entities[9].properties[3]);

  /// see [UploadQueueEntity.retryCount]
  static final retryCount =
      QueryIntegerProperty<UploadQueueEntity>(_entities[9].properties[4]);

  /// see [UploadQueueEntity.uploadUrl]
  static final uploadUrl =
      QueryStringProperty<UploadQueueEntity>(_entities[9].properties[5]);

  /// see [UploadQueueEntity.downloadUrl]
  static final downloadUrl =
      QueryStringProperty<UploadQueueEntity>(_entities[9].properties[6]);

  /// see [UploadQueueEntity.uploadType]
  static final uploadType =
      QueryIntegerProperty<UploadQueueEntity>(_entities[9].properties[7]);

  /// see [UploadQueueEntity.uploadState]
  static final uploadState =
      QueryIntegerProperty<UploadQueueEntity>(_entities[9].properties[8]);
}
