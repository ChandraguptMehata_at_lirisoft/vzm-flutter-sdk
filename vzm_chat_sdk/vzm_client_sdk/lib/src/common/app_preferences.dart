/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

abstract class AppPreferences {
  bool isFirstLaunch();

  void setFirstLaunch(bool value);

  String getVersionName();

  int getVersionCode();

  Future<bool> put(String key, String value);

  Future<bool> putList(String key,  List<String> value);

  void putBoolean(String key, bool value);

  void putInt(String key, int value);


  void putDouble(String key, double value);

  void removeSettings(String key);

  String? getString(String key);

  int? getInt(String key);

  bool getBoolean(String key);

  double? getDouble(String key);

  List<String>? getStringList(String key);

  Future<void> clear();

  Future<void> dumpValues();
}
