/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:logger/logger.dart';

class AppLogger {

  static bool isDebugEnabled = true;
  static bool isInfoEnabled = true;
  static bool isWarnEnabled = true;
  static bool isErrorEnabled = true;
  static bool isMqttLogsEnabled = false;
  static late Logger log;
  static final AppLogger _instance = AppLogger._internal();

  factory AppLogger() => _instance;

  AppLogger._internal() {
    log = Logger(printer: PrettyPrinter());
    debug("Logger Initialized", "");
  }

  void debug(String tag, String msg) {
    log.d("$tag $msg");
  }

  void warn(String tag, String msg) {
    log.w("$tag $msg");
  }

  void info(String tag, String msg) {
    log.i("$tag $msg");
  }

  void error(String tag, String msg, Exception exception) {
    log.e("$tag $msg", exception);
  }

}
