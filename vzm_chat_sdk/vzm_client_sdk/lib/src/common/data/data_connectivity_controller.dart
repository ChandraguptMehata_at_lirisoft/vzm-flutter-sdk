/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:vzm_client_sdk/src/common/data/network_type.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';

import '../../sdk-impl/di/locator.dart';

class DataConnectivityController {
  final StreamController _connectionChangeController = StreamController.broadcast();
  final Connectivity _connectivity = Connectivity();
  final AppLogger _logger = serviceLocator.get<AppLogger>();
  bool _hasConnection = false;

  Stream get connectionChange => _connectionChangeController.stream;

  DataConnectivityController() {
    _initialize();
  }

  void _initialize() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("initialize", "#data.conn.changes: init");
    }
    _connectivity.onConnectivityChanged.listen(_connectionChange);
  }

  void _connectionChange(ConnectivityResult result) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("_connectionChange", "#data.conn.changes: init -> connectivityResult -> $result");
    }
    _checkConnection(result);
  }

  Future<bool> _checkConnection(ConnectivityResult result) async {
    bool previousConnection = _hasConnection;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        _hasConnection = true;
      } else {
        _hasConnection = false;
      }
    } on SocketException catch (exception) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("checkConnection", "#data.conn.changes: checkConnection -> $exception");
      }
      _hasConnection = false;
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug("checkConnection", "#data.conn.changes: checkConnection -> $_hasConnection");
    }

    //The connection status changed send out an update to all listeners
    if (previousConnection != _hasConnection) {
      Network network = Network(hasConnected: _hasConnection, networkType: _getNetworkType(result));
      if (AppLogger.isDebugEnabled) {
        _logger.debug("checkConnection", "#data.conn.changes: checkConnection network -> ${network.toString()}");
      }
      _connectionChangeController.add(network);
    }

    return _hasConnection;
  }

  void dispose() {
    _connectionChangeController.close();
  }

  NetworkType _getNetworkType(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.mobile:
        return NetworkType.mobile;

      case ConnectivityResult.wifi:
        return NetworkType.wifi;

      default:
        return NetworkType.mobile;
    }
  }
}
