/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

class Network {
  late final bool hasConnected;

  late final NetworkType networkType;

  Network({required this.hasConnected, required this.networkType});

  @override
  String toString() {
    return 'Network{hasConnected: $hasConnected, networkType: $networkType}';
  }
}

enum NetworkType { unknown, mobile, wifi, cbs, other }
