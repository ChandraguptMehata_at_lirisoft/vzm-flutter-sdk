

abstract class DeviceManager {
  /// Returns device mdn
  Future<String> getMdn();

  /// Returns Unique device Id
  Future<String> getDeviceId();

  Future<String> generateDeviceId();

  /// Returns device model
  Future<String> getDeviceModel();

  /// Returns device model
  Future<String> getDeviceMake();

  /// Returns device model
  Future<String> getDeviceOSVersion();

  /// Returns device model
  Future<String> getDeviceName();

  Future<String> getDeviceType();

  void setMdn(String? mdn);

/*String isBelow(version: OSVersion);
  String isAboveOrEqual();*/
}
