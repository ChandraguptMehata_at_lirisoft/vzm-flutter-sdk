/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/user_agent_interceptor.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/ott_profile.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/profiles.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_authenticator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/profile/model/update_profile_request.dart';

part 'profile_api.g.dart';

@RestApi(baseUrl: "https://api.vzmessages.com")
abstract class ProfileApi {
  //this RestApi.baseUrl will be ignored if baseUrl is passe
  //factory AmsApi(Dio dio, {String baseUrl}) = _AmsApi;
  factory ProfileApi(Dio dio, {String? baseUrl}) {
    dio.options = BaseOptions(headers: {'Content-Type': 'application/json'});
    dio.interceptors.add(UserAgentInterceptor());
    dio.interceptors.add(OTTAuthenticator());
    return _ProfileApi(dio);
  }

  @POST("/subscriber/all")
  Future<HttpResponse<List<PublicProfiles>>> getPublicProfiles(@Body() List<String> idOrMdns);

  @PUT("/subscriber/")
  Future<HttpResponse<OttProfile>> updateSubscriber(@Body() UpdateProfileRequest request);

  @GET("/subscriber")
  Future<HttpResponse<OttProfile>> getSubscriber();
}
