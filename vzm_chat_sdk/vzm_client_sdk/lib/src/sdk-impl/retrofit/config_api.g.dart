// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ConfigApi implements ConfigApi {
  _ConfigApi(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://config.vzmessages.com';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<HttpResponse<Configuration>> getConfig() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{
      r'APP': 'f869e61f7b38cb69f9378905e77bf4f1'
    };
    _headers.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<HttpResponse<Configuration>>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/config',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = Configuration.fromJson(_result.data!);
    final httpResponse = HttpResponse(value, _result);
    return httpResponse;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
