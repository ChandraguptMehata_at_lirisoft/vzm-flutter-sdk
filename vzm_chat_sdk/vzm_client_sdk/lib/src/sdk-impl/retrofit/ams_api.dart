/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/enroll_device_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/push_token.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/user_agent_interceptor.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_authenticator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/vma/vma_subscriber_body.dart';

part 'ams_api.g.dart';

@RestApi(baseUrl: "https://api.vzmessages.com")
abstract class AmsApi {
  //this RestApi.baseUrl will be ignored if baseUrl is passe
  //factory AmsApi(Dio dio, {String baseUrl}) = _AmsApi;
  factory AmsApi(Dio dio, {String? baseUrl}) {
    dio.options = BaseOptions(headers: {'Content-Type': 'application/json'});
    dio.interceptors.add(UserAgentInterceptor());
    dio.interceptors.add(OTTAuthenticator());
    String baseUrl = OttConfigurationManager.DEFAULT_AMS_URL;
    return _AmsApi(dio, baseUrl: baseUrl);
  }

  @POST("/vma/subscriber/{mdn}")
  Future<HttpResponse<EnrollDeviceResponse>> create(@Header("x-VMA-Token") String vmaToken,
      @Path("mdn") String mdn, @Body() VmaSubscriberBody vmaSubscriberBody);

  @POST("/token/updatepushid")
  Future<void> updatePushId(@Body() PushToken pushToken);
}
