/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/user_agent_interceptor.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_authenticator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/sync/model/change_response.dart';

part 'sync_api.g.dart';

@RestApi(baseUrl: "https://syncor.vzmessages.com")
abstract class SyncApi {
  //this RestApi.baseUrl will be ignored if baseUrl is passe
  //factory SyncApi(Dio dio, {String baseUrl}) = _SyncApi;

  factory SyncApi(Dio dio, {String? baseUrl}) {
    var dio = serviceLocator.get<Dio>();
    dio.options = BaseOptions(headers: {'Content-Type': 'application/json'});
    dio.interceptors.add(UserAgentInterceptor());
    dio.interceptors.add(OTTAuthenticator());
    var ottConfigManager = serviceLocator.get<OttConfigurationManager>();
    String baseUrl = ottConfigManager.getSyncBaseUrl();
    return _SyncApi(dio, baseUrl: baseUrl);
  }

  @GET("/group?deleted=false&rcs=true")
  Future<HttpResponse<GroupIdResponse>> getAllGroupIds();

  @POST("/messageByGroup")
  Future<HttpResponse<MessageByGroupResponse>> getMessageByGroupId(@Body() MessageByGroupRequest body);

  @POST("/getMessages")
  Future<HttpResponse<List<Payload>>> getMessages(@Body() GetMessagesRequest ids);

  @GET("/changeSince/{timestamp}/{limit}?rcs=false")
  Future<HttpResponse<ChangeResponse>> getChanges(@Path("timestamp") int timestamp, @Path("limit") int limit);
}
