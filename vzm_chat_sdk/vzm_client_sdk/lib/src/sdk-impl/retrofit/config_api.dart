/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/configuration.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/user_agent_interceptor.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';

part 'config_api.g.dart';

@RestApi(baseUrl: "https://config.vzmessages.com")
abstract class ConfigApi {
  //this RestApi.baseUrl will be ignored if baseUrl is passe
  //factory ConfigApi(Dio dio, {String baseUrl}) = _ConfigApi;
  factory ConfigApi(Dio dio, {String? baseUrl}) {
    dio.options = BaseOptions(headers: {'Content-Type': 'application/json'});
    dio.interceptors.add(UserAgentInterceptor());
    String baseUrl = OttConfigurationManager.DEFAULT_CONFIG_URL;
    return _ConfigApi(dio, baseUrl: baseUrl);
  }

  @Headers(<String, String>{"APP": "f869e61f7b38cb69f9378905e77bf4f1"})
  @GET("/config")
  Future<HttpResponse<Configuration>> getConfig();
}
