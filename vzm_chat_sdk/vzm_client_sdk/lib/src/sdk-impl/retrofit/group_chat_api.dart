/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/user_agent_interceptor.dart';
import 'package:vzm_client_sdk/src/sdk-impl/chat/model/create_group_request.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/media_urls.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/upload_content_type.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/group.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_authenticator.dart';

part 'group_chat_api.g.dart';

@RestApi(baseUrl: "https://api.vzmessages.com")
abstract class GroupChatApi {
  //this RestApi.baseUrl will be ignored if baseUrl is passe
  //factory AmsApi(Dio dio, {String baseUrl}) = _AmsApi;
  factory GroupChatApi(Dio dio, {String? baseUrl}) {
    dio.options = BaseOptions(headers: {'Content-Type': 'application/json'});
    dio.interceptors.add(UserAgentInterceptor());
    dio.interceptors.add(OTTAuthenticator());
    return _GroupChatApi(dio);
  }

  @POST("/group/all")
  Future<HttpResponse<GroupAndProfiles>> getGroupsAndProfiles(@Body() List<String> groupIds);

  /// Get upload and download URLs for media attachments.
  @PUT("/mediahandler")
  Future<HttpResponse<MediaUrls>> getMediaUrls(@Body() UploadContentType contentType);

  @PUT("/tempmediahandler")
  Future<HttpResponse<MediaUrls>> getTempMediaHandlerUrls(@Body() UploadContentType contentType);


  @POST("/group/v2")
  Future<HttpResponse<List<Payload>>> createGroupChat(@Body() CreateGroupRequest createGroupRequest);

}
