/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/validate_login_pin_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/vma_provisioning_status.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/vma_subscriber_status.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/user_agent_interceptor.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/request_otp_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/vma/vma_authenticator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/vma/vma_config.dart';

part 'vma_api.g.dart';

@RestApi(baseUrl: "https://services.vma.vzw.com")
abstract class VMAApi {
  //this RestApi.baseUrl will be ignored if baseUrl is passe
  //factory VMAApi(Dio dio, {String baseUrl}) = _VMAApi;
  factory VMAApi(Dio dio, {String? baseUrl}) {
    dio.options = BaseOptions(headers: {'Content-Type': 'application/json'});
    dio.interceptors.add(UserAgentInterceptor());
    dio.interceptors.add(VMAAuthenticator());
    String baseUrl = VMAConfig.DEFAULT_VMA_URL;
    return _VMAApi(dio, baseUrl: baseUrl);
  }

  @FormUrlEncoded()
  @POST("/services/v2/generatePIN")
  Future<HttpResponse<RequestOtpResponse>> generateOtp(
      @Field("mdn") String mdn,
      @Field("isPrimary") bool isPrimary,
      @Field("deviceType") String deviceType,
      @Field("deviceName") String deviceName);

  @FormUrlEncoded()
  @POST("/services/v2/loginPINToken")
  Future<HttpResponse<ValidateLoginPinResponse>> validateOtp(
      @Field("mdn") String mdn,
      @Field("pin") String pin,
      @Field("deviceMake") String deviceMake,
      @Field("deviceModel") String deviceModel,
      @Field("version") String version,
      @Field("deviceId") String deviceId);

  @FormUrlEncoded()
  @POST("/services/v3/vma_user_query")
  Future<HttpResponse<VMASubscriberStatus>> getVMASubscriberStatus(
      @Field("mdn") String mdn, @Field("loginToken") String loginToken);

  @FormUrlEncoded()
  @POST("/services/v2/vmaProvisioning")
  Future<HttpResponse<VmaProvisionStatus>> provision(
      @Field("mdn") String mdn, @Field("loginToken") String loginToken);
}
