/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/src/widgets/framework.dart';
import 'package:vzm_client_sdk/src/api/api.dart';
import 'package:vzm_client_sdk/src/api/auth/auth_request.dart';
import 'package:vzm_client_sdk/src/api/chat/chat_manager.dart';
import 'package:vzm_client_sdk/src/api/chat/group_chat_manager.dart';
import 'package:vzm_client_sdk/src/api/profile/profile_manager.dart';
import 'package:vzm_client_sdk/src/api/repositories/conversation_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/message_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/api/sync/sync_manager.dart';
import 'package:vzm_client_sdk/src/common/app_channel.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_utils.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/auth_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/conversation_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/message_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/fcm/push_token_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/load_test/load_db_test.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_connection_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/restore.dart';

import 'media/file_manager.dart';

class VzmClientSdk implements SdkManager {
  late DatabaseManager? databaseManager;
  late MqttConnectionManager _mqttManager;
  late bool initiated = true;

  BuildContext? context;

  VzmClientSdk(this.context);

  @override
  Future<bool> init(BuildContext? context) async {
    print('Sdk Initializing:  Started');
    await setUp(context);
    setUserAgent();
    initFCMToken();
    databaseManager = serviceLocator.get<DatabaseManager>();
    _mqttManager = serviceLocator.get<MqttConnectionManager>();
    await databaseManager!.init();
    print('Sdk initialized: Done $initiated');
    mqttSetup();
    return initiated;
  }

  @override
  Future<bool> reset() async {
    return true;
  }

  @override
  Future<bool> setupdatabase() async {
    bool? issetup = await databaseManager?.init();
    print('databse initialized $issetup');
    return issetup!;
  }

  @override
  Future<bool> mqttSetup({BuildContext? context}) async {
    if (isProvisioned()) {
      _mqttManager.configureAndConnect();
    }
    return true;
  }

  @override
  AuthRequest getAuthManager() {
    return serviceLocator.get<AuthManager>();
  }

  Future<void> setUserAgent() async {
    var ottConfigManager = serviceLocator.get<OttConfigurationManager>();
    ottConfigManager.setUserAgent();
  }

  void initFCMToken() {
    var pushTokenManager = serviceLocator.get<PushTokenManager>();
    pushTokenManager.initialize(context);
  }

  Future<void> startRestore() async {
    serviceLocator.get<RestoreManager>().start();
  }

  bool isProvisioned() {
    AuthManager authManager = serviceLocator.get<AuthManager>();
    return authManager.isProvisioned();
  }

  ConversationRepository getConversationRepository() {
    ConversationRepository conversationRepository = serviceLocator.get<ConversationRepository>();
    return conversationRepository;
  }

  ConversationManager getConversationManager() {
    ConversationManager conversationManager = serviceLocator.get<ConversationManager>();
    return conversationManager;
  }

  MessageRepository getMessageRepository() {
    MessageRepository messageRepository = serviceLocator.get<MessageRepository>();
    return messageRepository;
  }

  MessageRepositoryImpl getMessageManager() {
    MessageRepositoryImpl messageManager = serviceLocator.get<MessageRepositoryImpl>();
    return messageManager;
  }

  Future<void> startSync() async {
    serviceLocator.get<SyncManager>().syncAllChanges();
  }

  void notifyMqttManager({required bool isInBackground}) {
    _mqttManager.notifyMqttManager(isInBackground);
  }

  ChatManager getChatManager() {
    ChatManager chatManager = serviceLocator.get<ChatManager>();
    return chatManager;
  }

  void sendMessage(int conversationId, String body) {
    getChatManager().sendText(conversationId, body);
  }

  void clearDb() {
    AppUtils.clearDb();
    getConversationManager().notifyConversationObjects();
  }

  Contact getMyContact() {
    return serviceLocator.get<ProfileManager>().getProfile();
  }

  ProfileManager getProfileManager() {
    return serviceLocator.get<ProfileManager>();
  }

  void sendFile(String path, String conversationId, String? text) {
    getChatManager().sendMediaMessage(int.parse(conversationId), path);
  }

  FileManager getFileManager() {
    return serviceLocator.get<FileManager>();
  }

  GroupChatManager getGroupChatManager() {
    return serviceLocator.get<GroupChatManager>();
  }

  getChannelName() {
    return AppChannel.CHAT_SDK_CHANNEL;
  }

  Future<bool> performDBLoad() async {
    await serviceLocator.get<LoadDBTest>().loadDummyConversation();
    return true;
  }

}
