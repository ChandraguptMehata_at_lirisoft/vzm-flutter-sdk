/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vzm_client_sdk/src/common/app_preferences.dart';

class AppDefaultSharedPreferences implements AppPreferences {
  final SharedPreferences _prefs;

  AppDefaultSharedPreferences(this._prefs) {
    print("AppDefaultSharedPreferences Initialized.");
  }

  @override
  Future<void> clear() {
    // TODO: implement dumpValues
    throw UnimplementedError();
  }

  @override
  Future<void> dumpValues() {
    // TODO: implement dumpValues
    throw UnimplementedError();
  }

  @override
  bool getBoolean(String key) {
    var result = _prefs.getBool(key);
    if (result != null) {
      return result;
    }
    return false;
  }

  @override
  double? getDouble(String key) {
    return _prefs.getDouble(key);
  }

  @override
  int? getInt(String key) {
    return _prefs.getInt(key);
  }

  @override
  String? getString(String key) {
    return _prefs.getString(key);
  }

  @override
  List<String>? getStringList(String key) {
    return _prefs.getStringList(key);
  }

  @override
  int getVersionCode() {
    // TODO: implement getVersionCode
    throw UnimplementedError();
  }

  @override
  String getVersionName() {
    // TODO: implement getVersionName
    throw UnimplementedError();
  }

  @override
  bool isFirstLaunch() {
    var r = _prefs.getBool("app.first.launch");
    if (r != null) {
      return r;
    }
    return false;
  }

  @override
  Future<bool> put(String key, String value) async {
    return _prefs.setString(key, value);
  }

  @override
  Future<void> putBoolean(String key, bool value) async {
    _prefs.setBool(key, value);
  }

  @override
  void putDouble(String key, double value) {
    _prefs.setDouble(key, value);
  }

  @override
  void putInt(String key, int value) {
    _prefs.setInt(key, value);
  }

  @override
  void removeSettings(String key) {
    _prefs.remove(key);
  }

  @override
  void setFirstLaunch(bool value) {
    _prefs.setBool("app.first.launch", true);
  }

  @override
  Future<bool> putList(String key, List<String> value) {
    return _prefs.setStringList(key, value);
  }
}
