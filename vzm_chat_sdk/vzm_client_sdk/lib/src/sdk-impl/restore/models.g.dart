// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroupId _$GroupIdFromJson(Map<String, dynamic> json) => GroupId(
      json['groupId'] as String,
      json['timeOfLastMsgSeen'] as int?,
    );

Map<String, dynamic> _$GroupIdToJson(GroupId instance) => <String, dynamic>{
      'groupId': instance.groupId,
      'timeOfLastMsgSeen': instance.timeOfLastMsgSeen,
    };

GroupIdResponse _$GroupIdResponseFromJson(Map<String, dynamic> json) =>
    GroupIdResponse(
      json['lastSyncTime'] as int,
      (json['object'] as List<dynamic>)
          .map((e) => GroupId.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GroupIdResponseToJson(GroupIdResponse instance) =>
    <String, dynamic>{
      'lastSyncTime': instance.lastSyncTime,
      'object': instance.object,
    };

MessageByGroupRequest _$MessageByGroupRequestFromJson(
        Map<String, dynamic> json) =>
    MessageByGroupRequest(
      json['groupId'] as String,
      json['number'] as int?,
      json['startTime'] as int?,
    );

Map<String, dynamic> _$MessageByGroupRequestToJson(
    MessageByGroupRequest instance) {
  final val = <String, dynamic>{
    'groupId': instance.groupId,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('number', instance.number);
  writeNotNull('startTime', instance.startTime);
  return val;
}

MessageByGroupResponse _$MessageByGroupResponseFromJson(
        Map<String, dynamic> json) =>
    MessageByGroupResponse(
      (json['messages'] as List<dynamic>?)
          ?.map((e) => Payload.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['readConvStatus'] as List<dynamic>?)
          ?.map(
              (e) => ReadConversationStatus.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['deleteConvStatus'] as List<dynamic>?)
          ?.map((e) =>
              DeleteConversationStatus.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MessageByGroupResponseToJson(
        MessageByGroupResponse instance) =>
    <String, dynamic>{
      'messages': instance.messages,
      'readConvStatus': instance.readConvStatus,
      'deleteConvStatus': instance.deleteConvStatus,
    };

GetMessagesRequest _$GetMessagesRequestFromJson(Map<String, dynamic> json) =>
    GetMessagesRequest(
      (json['messages'] as List<dynamic>)
          .map((e) => GetMessageRequestId.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetMessagesRequestToJson(GetMessagesRequest instance) =>
    <String, dynamic>{
      'messages': instance.messages,
    };

GetMessageRequestId _$GetMessageRequestIdFromJson(Map<String, dynamic> json) =>
    GetMessageRequestId(
      json['messageId'] as String,
      $enumDecode(_$ContentEnumMap, json['content']),
    );

Map<String, dynamic> _$GetMessageRequestIdToJson(
        GetMessageRequestId instance) =>
    <String, dynamic>{
      'messageId': instance.messageId,
      'content': _$ContentEnumMap[instance.content],
    };

const _$ContentEnumMap = {
  Content.all: 'all',
  Content.status: 'status',
};
