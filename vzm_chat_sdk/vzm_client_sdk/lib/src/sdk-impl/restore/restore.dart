/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/objectbox.g.dart';
import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/app_preferences.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/persistence_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/restore_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/group.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/group_chat_api.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/profile_api.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/sync_api.dart';

class RestoreManager {
  static const keyFetchConversationIds = "app.restore.fetch.ids";

  late AppPreferences _pref;
  late SyncApi _syncApi;

  late GroupChatApi _groupApi;
  late ProfileApi _profileApi;

  late DatabaseManager _database;
  late AppLogger _logger;
  late PersistenceManager _persistenceManager;
  late EntityConverter _entityConverter;
  static const String _tag = "RESTORE";

  RestoreManager() {
    _logger = serviceLocator.get<AppLogger>();
    _pref = serviceLocator.get<AppPreferences>();
    _syncApi = serviceLocator.get<SyncApi>();
    _groupApi = serviceLocator.get<GroupChatApi>();
    _profileApi = serviceLocator.get<ProfileApi>();
    _database = serviceLocator.get<DatabaseManager>();
    _logger = serviceLocator.get<AppLogger>();
    _persistenceManager = serviceLocator.get<PersistenceManager>();
    _entityConverter = serviceLocator.get<EntityConverter>();
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.init");
    }
  }

  Future<bool> start() async {
    return execute();
  }

  Future<bool> execute() async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.execute start the restore.");
    }
    if (!_pref.getBoolean(keyFetchConversationIds)) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#restore.execute fetch the conversation ids");
      }
      var res = await _syncApi.getAllGroupIds();
      if (res.response.statusCode == 200) {
        var items = res.data.object.map((e) => RestoreQueueEntity(e.groupId)).toList();
        _database.getRestoreQueue().putMany(items, mode: PutMode.insert);
        _pref.putBoolean(keyFetchConversationIds, true);
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(
              _tag,
              "#restore.execute getAllGroupIds api failed with error code: ${res.response.statusCode}" +
                  "error msg: ${res.response.statusMessage}");
        }
      }
    }
    List<RestoreQueueEntity> restoreQueueEntity = _database.getRestoreQueue().getAll();
    for (var item in restoreQueueEntity) {
      try {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#restore.${item.groupId}: restoreQueueEntity start restoring  ");
        }
        await restore(item.groupId);
        _database.getRestoreQueue().remove(item.id);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#restore.${item.groupId}: restoreQueueEntity restored.");
        }
      } on Exception catch (_e) {
        if (AppLogger.isDebugEnabled) {
          _logger.error(_tag, "#restore.${item.groupId}: restoreQueueEntity failed", _e);
        }
        _database.getRestoreQueue().remove(item.id);
      }
    }

    return false;
  }

  Future<bool> restore(String groupId) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.$groupId: restore conversation");
    }
    bool? isGroupChatId = await _entityConverter.isGroupChatId(groupId);
    if (isGroupChatId != null && isGroupChatId) {
      // Restore Group Chat Conversation
      return await restoreGroupChat(groupId, null, 25);
    } else if (groupId.contains("!")) {
      // Restore OTT 1x1 , or Chat bot Conversation
      return await restoreOtt1x1Chat(groupId, null, 25);
    } else {
      // Length will be 32 , Restore Tel conversation
      return await restoreTelConversation(groupId, null, 25);
    }
  }

  Future<bool> restoreTelConversation(String groupId, int? timestamp, int count) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.$groupId: restoreTelConversation");
    }
    var data = MessageByGroupRequest(groupId, count, timestamp);
    var res = await _syncApi.getMessageByGroupId(data);
    if (res.response.statusCode == 200) {
      MessageByGroupResponse messageByGroupResponse = res.data;
      if (messageByGroupResponse.messages != null) {
        Payload msg = messageByGroupResponse.messages![0];
        var profileIds = _persistenceManager.getMissingProfile(msg.object);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#restore.$groupId: restoreTelConversation getMissingProfile profileIds ${profileIds.toString()}");
        }
        if (profileIds.isNotEmpty) {
          var res = await _profileApi.getPublicProfiles(profileIds);
          if (res.response.statusCode == 200) {
            await _persistenceManager.addOrUpdateProfile(res.data);
          } else {
            if (AppLogger.isDebugEnabled) {
              _logger.debug(
                  _tag,
                  "#restore.$groupId: restoreTelConversation failed with error code: ${res.response.statusCode}" +
                      "error msg: ${res.response.statusMessage}");
            }
          }
        }
        // Save the conversation
        bool restored = messageByGroupResponse.messages!.length < count;
        final conv = await _persistenceManager.createTelephonyConversation(msg, restored);
        bool savedState = await saveMessages(conv, messageByGroupResponse.messages);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#restore.$groupId: restoreTelConversation savedState $savedState");
        }
        return savedState;
      } else if (AppLogger.isDebugEnabled) {
        _logger.info(_tag, "#restore.$groupId: restoreTelConversation No Messages found. ");
      }
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.info(_tag,
            "#restore.$groupId: restoreTelConversation error occurred:  status code: ${res.response.statusCode} msg: ${res.response.statusMessage}");
      }
      return false;
    }
    return true;
  }

  Future<bool> restoreGroupChat(String groupId, int? timestamp, int count) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.$groupId: restoreGroupChat conversation");
    }
    // Check the group is active or not
    var httpResponse = await _groupApi.getGroupsAndProfiles([groupId]);
    if (httpResponse.response.statusCode == 200) {
      GroupAndProfiles groupAndProfileResponse = httpResponse.data;
      // Save the public profiles
      if (groupAndProfileResponse.memberInfo != null) {
        await _persistenceManager.addOrUpdateProfile(groupAndProfileResponse.memberInfo!);
      }
      // Save the conversation
      if (groupAndProfileResponse.groupInfo.isNotEmpty) {
        // get latest message from group
        var gc = groupAndProfileResponse.groupInfo[0];
        if (gc.group != null) {
          var data = MessageByGroupRequest(groupId, count, timestamp);
          var res = await _syncApi.getMessageByGroupId(data);
          if (res.response.statusCode == 200) {
            MessageByGroupResponse messageByGroupResponse = res.data;
            if (messageByGroupResponse.messages != null) {
              bool restored = messageByGroupResponse.messages!.length < count;
              var conv = await _persistenceManager.createGroupChat(gc.group, restored);
              bool savedState = await saveMessages(conv, messageByGroupResponse.messages);
              if (AppLogger.isDebugEnabled) {
                _logger.debug(_tag, "#restore.$groupId: restoreGroupChat length  ${messageByGroupResponse.messages!.length}");
              }
            }
            return true;
          } else {
            if (AppLogger.isDebugEnabled) {
              _logger.debug(
                  _tag,
                  "#restore.$groupId: restoreGroupChat getMessageByGroupId api error occurred status code ${res.response.statusCode} "
                  "+ errorMsg ${res.response.statusMessage}");
            }
            return false;
          }
        } else {
          if (AppLogger.isDebugEnabled) {
            _logger.debug(_tag, "#restore.$groupId: restoreGroupChat Not an active group");
          }
          return false;
        }
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#restore.$groupId: restoreGroupChat Not an active group");
        }
        return false;
      }
    } else {
      print("#restore.$groupId: restoreGroupChat failed with error code: ${httpResponse.response.statusCode}" +
          "error msg: ${httpResponse.response.statusMessage}");
    }
    return false;
  }

  Future<bool> restoreOtt1x1Chat(String groupId, int? i, int j) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.1x1.$groupId: restoreOtt1x1Chat");
    }

    return false;
  }

  Future<bool> saveMessages(Conversation conv, List<Payload>? messagePayloads) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.msg.${conv.serverId}: saveMessages: ${messagePayloads!.length} Message found.");
    }
    for (var payload in messagePayloads!) {
      if (AppLogger.isDebugEnabled) {
        var log = "id = ${payload.object.id},type= ${payload.type}";
        _logger.debug(_tag, "#restore.msg.${payload.object.groupId}: saveMessages: Processing msg $log");
      }
      _logger.debug(_tag,
          "#restore.msg.deleted. ${payload.object.groupId}: saveMessages: deleted msg = ${payload.deleted}  payload type = ${payload.type}");

      if (payload.deleted == true) {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#restore.msg.${payload.object.groupId}:  saveMessages: ignored deleted msg = ${payload.toString()}");
        }
      } else {
        switch (payload.type) {
          case PayloadType.mediaMessage:
          case PayloadType.textMessage:
          case PayloadType.smsMessage:
          case PayloadType.rcsTextMessage:
          case PayloadType.mmsMessage:
            await saveMessage(conv, payload);
            break;
          case PayloadType.createGroupEvent:
            await _persistenceManager.handleCreateGroupEvent(payload);
            break;
          case PayloadType.groupUpdateEvent:
            await _persistenceManager.handleGroupUpdateEvent(payload);
            break;
          case PayloadType.removeFromGroupEvent:
            await _persistenceManager.handleRemoveFromGroupEvent(payload);
            break;
          default:
            if (AppLogger.isDebugEnabled) {
              _logger.debug(_tag, "#restore.msg.${payload.object.groupId}: saveMessages: Not Impl ${payload.type}");
            }
        }
      }
    }
    return true;
  }

  Future<bool> saveMessage(Conversation conv, Payload payload) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.msg.${payload.object.groupId}: saveMessage -> " + "conv = $conv, payload = ${payload.toString()}");
    }
    bool handlePayloadState = await _persistenceManager.handleMessagePayload(conv, payload);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#restore.msg.${payload.object.groupId}: saveMessage -> handlePayloadState $handlePayloadState");
    }
    return handlePayloadState;
  }
}
