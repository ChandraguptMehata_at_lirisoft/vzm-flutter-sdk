/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/conversation_status.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';

part 'models.g.dart';

@JsonSerializable()
class GroupId {
  String groupId;
  int? timeOfLastMsgSeen;

  GroupId(this.groupId, this.timeOfLastMsgSeen);


  factory GroupId.fromJson(Map<String, dynamic> json) =>
      _$GroupIdFromJson(json);

  Map<String, dynamic> toJson() => _$GroupIdToJson(this);
}

@JsonSerializable()
class GroupIdResponse {
   int lastSyncTime;
   List<GroupId> object;

   GroupIdResponse(this.lastSyncTime, this.object);


  factory GroupIdResponse.fromJson(Map<String, dynamic> json) =>
      _$GroupIdResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GroupIdResponseToJson(this);
}

@JsonSerializable(includeIfNull: false)
class MessageByGroupRequest {
  String groupId;
  int? number;
  int? startTime;

  MessageByGroupRequest(this.groupId, this.number, this.startTime);

  factory MessageByGroupRequest.fromJson(Map<String, dynamic> json) =>
      _$MessageByGroupRequestFromJson(json);

  Map<String, dynamic> toJson() => _$MessageByGroupRequestToJson(this);
}

@JsonSerializable()
class MessageByGroupResponse {
  List<Payload>? messages;
  List<ReadConversationStatus>? readConvStatus;
  List<DeleteConversationStatus>? deleteConvStatus;

  MessageByGroupResponse(this.messages, this.readConvStatus,
      this.deleteConvStatus);

  factory MessageByGroupResponse.fromJson(Map<String, dynamic> json) =>
      _$MessageByGroupResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MessageByGroupResponseToJson(this);
}

@JsonSerializable()
class GetMessagesRequest {
  List<GetMessageRequestId> messages;

  GetMessagesRequest(this.messages);

  factory GetMessagesRequest.fromJson(Map<String, dynamic> json) =>
      _$GetMessagesRequestFromJson(json);

  Map<String, dynamic> toJson() => _$GetMessagesRequestToJson(this);
}

enum Content { all, status }

@JsonSerializable()
class GetMessageRequestId {
  String messageId;
  Content content;

  GetMessageRequestId(this.messageId, this.content);

  factory GetMessageRequestId.fromJson(Map<String, dynamic> json) =>
      _$GetMessageRequestIdFromJson(json);

  Map<String, dynamic> toJson() => _$GetMessageRequestIdToJson(this);
}
