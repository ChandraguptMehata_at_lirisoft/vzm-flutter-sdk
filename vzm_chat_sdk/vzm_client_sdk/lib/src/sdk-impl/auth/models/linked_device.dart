/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
part 'linked_device.g.dart';

@JsonSerializable()
class LinkedDevice {

  LinkedDevice(
      {required this.deviceid, required this.deviceName, required this.deviceType,
        required this.deviceMake, required this.deviceModel, required this.osVersion,
        required this.createdTime, required this.failedAttempts, required this.appName,
        required this.password, required this.pushId, required this.isAndroid,
        required this.isIphone, required this.prefix, required this.renewAfterTime});

  @JsonKey(name: 'deviceid')
  late final String? deviceid;

  @JsonKey(name: 'deviceName')
  late final String? deviceName;

  @JsonKey(name: 'deviceType')
  late final String? deviceType;

  @JsonKey(name: 'deviceMake')
  late final String? deviceMake;

  @JsonKey(name: 'deviceModel')
  late final String? deviceModel;

  @JsonKey(name: 'osVersion')
  late final String? osVersion;

  @JsonKey(name: 'createdTime')
  late final dynamic? createdTime;

  @JsonKey(name: 'failedAttempts')
  late final dynamic? failedAttempts;

  @JsonKey(name: 'appName')
  late final String? appName;

  @JsonKey(name: 'password')
  late final String? password;

  @JsonKey(name: 'pushId')
  late final String? pushId;

  @JsonKey(name: 'android')
  late final bool? isAndroid;

  @JsonKey(name: 'iphone')
  late final bool? isIphone;

  @JsonKey(name: 'prefix')
  late final String? prefix;

  @JsonKey(name: 'renewAfterTime')
  late final dynamic? renewAfterTime;

  factory LinkedDevice.fromJson(Map<String, dynamic> json) =>
      _$LinkedDeviceFromJson(json);

  Map<String, dynamic> toJson() => _$LinkedDeviceToJson(this);

  @override
  String toString() {
    return 'LinkedDevice{deviceid: $deviceid, deviceName: $deviceName, deviceType: $deviceType, deviceMake: $deviceMake, deviceModel: $deviceModel, osVersion: $osVersion, createdTime: $createdTime, failedAttempts: $failedAttempts, appName: $appName, password: $password, pushId: $pushId, isAndroid: $isAndroid, isIphone: $isIphone, prefix: $prefix, renewAfterTime: $renewAfterTime}';
  }
}
