// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'push_token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PushToken _$PushTokenFromJson(Map<String, dynamic> json) => PushToken(
      deviceid: json['deviceid'] as String,
      pushId: json['pushId'] as String,
      appName: json['appName'] as String,
    );

Map<String, dynamic> _$PushTokenToJson(PushToken instance) => <String, dynamic>{
      'deviceid': instance.deviceid,
      'pushId': instance.pushId,
      'appName': instance.appName,
    };
