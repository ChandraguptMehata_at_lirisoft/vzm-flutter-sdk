/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
part 'push_token.g.dart';

@JsonSerializable()
class PushToken {

  PushToken({required this.deviceid, required this.pushId, required this.appName});

  @JsonKey(name: 'deviceid')
  late final String deviceid;

  @JsonKey(name: 'pushId')
  late final String pushId;

  @JsonKey(name: 'appName')
  late final String appName;

  factory PushToken.fromJson(Map<String, dynamic> json) =>
      _$PushTokenFromJson(json);

  Map<String, dynamic> toJson() => _$PushTokenToJson(this);

  @override
  String toString() {
    return 'PushToken{deviceid: $deviceid, pushId: $pushId, appName: $appName}';
  }

}

