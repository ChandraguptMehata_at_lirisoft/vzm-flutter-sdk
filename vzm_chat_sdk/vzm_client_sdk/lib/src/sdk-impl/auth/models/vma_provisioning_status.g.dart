// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vma_provisioning_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VmaProvisionStatus _$VmaProvisionStatusFromJson(Map<String, dynamic> json) =>
    VmaProvisionStatus(
      status: json['status'] as String,
      statusInfo: json['statusInfo'] as String?,
    );

Map<String, dynamic> _$VmaProvisionStatusToJson(VmaProvisionStatus instance) =>
    <String, dynamic>{
      'status': instance.status,
      'statusInfo': instance.statusInfo,
    };
