// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'removable_device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RemovableDevice _$RemovableDeviceFromJson(Map<String, dynamic> json) =>
    RemovableDevice(
      devId: json['devId'] as String?,
      devName: json['devName'] as String?,
      createTime: json['createTime'] as String?,
    );

Map<String, dynamic> _$RemovableDeviceToJson(RemovableDevice instance) =>
    <String, dynamic>{
      'devId': instance.devId,
      'devName': instance.devName,
      'createTime': instance.createTime,
    };
