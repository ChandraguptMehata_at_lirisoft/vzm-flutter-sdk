/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/removable_device.dart';

part 'validate_login_pin_response.g.dart';

@JsonSerializable()
class ValidateLoginPinResponse {

  static String STATUS_OK = "OK";
  static String STATUS_FAIL = "FAIL";
  static String STATUS_ERROR = "ERROR";
  static String STATUS_EXCEED_DEVICE_LIMIT = "EXCEEDDEVICELIMIT";
  static String STATUS_V_BLOCK = "VBLOCK";
  static String STATUS_SUSPENDED = "SUSPENDED";
  static String STATUS_NOT_VZW_MDN = "NOTVZWMDN";
  static String STATUS_NOT_ELIGIBLE = "NOTELIGIBLE";
  static String STATUS_NO_TEXT = "NOTEXT";
  static String STATUS_FALLBACK = "FALLBACK";

  ValidateLoginPinResponse(
      {required this.code,
      required this.status,
      required this.mdn,
      required this.loginToken,
      this.statusinfo,
      this.storageType,
      this.amsStartTime,
      this.isHasMsaPair,
      this.devices});

  @JsonKey(name: 'code')
  late final int? code;

  @JsonKey(name: 'status')
  late final String? status;

  @JsonKey(name: 'mdn')
  late final String? mdn;

  @JsonKey(name: 'loginToken')
  late final String? loginToken;

  @JsonKey(name: 'statusinfo')
  late final String? statusinfo;

  @JsonKey(name: 'storageType')
  late final String? storageType;

  @JsonKey(name: 'amsStartTime')
  late final dynamic? amsStartTime;

  @JsonKey(name: 'isHasMsaPair')
  late final bool? isHasMsaPair;

  @JsonKey(name: 'removableDevices')
  final List<RemovableDevice?>? devices;

  factory ValidateLoginPinResponse.fromJson(Map<String, dynamic> json) =>
      _$ValidateLoginPinResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ValidateLoginPinResponseToJson(this);

  bool isOk() {
    return STATUS_OK == status;
  }

  bool isError() {
    return STATUS_ERROR == status;
  }

  bool isFailed() {
    return STATUS_FAIL == status;
  }

  bool isDeviceLimitExceeded() {
    return STATUS_EXCEED_DEVICE_LIMIT == status;
  }

  bool isVBlock() {
    return STATUS_V_BLOCK == status;
  }

  bool isSuspended() {
    return STATUS_SUSPENDED == status;
  }

  bool isNotVzwMdn() {
    return STATUS_NOT_VZW_MDN == status;
  }

  bool isNotEligible() {
    return STATUS_NOT_ELIGIBLE == status;
  }

  bool isUserNoText() {
    return STATUS_NO_TEXT == status;
  }

  bool isFallback() {
    return STATUS_FALLBACK == status;
  }

  bool hasMsaPair() {
    return isHasMsaPair == true;
  }

  @override
  String toString() {
    return 'ValidateLoginPinResponse{code: $code, status: $status, mdn: $mdn, loginToken: $loginToken, statusinfo: $statusinfo, storageType: $storageType, amsStartTime: $amsStartTime, isHasMsaPair: $isHasMsaPair, devices: $devices}';
  }
}
