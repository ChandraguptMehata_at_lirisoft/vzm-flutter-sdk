/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';
part 'vma_subscriber_status.g.dart';

@JsonSerializable()
class VMASubscriberStatus {

  static const String NO = "NO";
  static const String YES = "YES";

  VMASubscriberStatus(
      {required this.voiceUserFlag, required this.vmaUserFlag, required this.volteUserFlag});

  @JsonKey(name: 'VoiceUserFlag')
  late final String? voiceUserFlag;

  @JsonKey(name: 'VMAUserFlag')
  late final String? vmaUserFlag;

  @JsonKey(name: 'VolteUserFlag')
  late final String? volteUserFlag;

  factory VMASubscriberStatus.fromJson(Map<String, dynamic> json) =>
      _$VMASubscriberStatusFromJson(json);

  Map<String, dynamic> toJson() => _$VMASubscriberStatusToJson(this);

  bool isVmaUser() {
    return YES == vmaUserFlag;
  }

  bool isNotVmaUser() {
    return NO == vmaUserFlag;
  }

  bool isVoiceUser() {
    return YES == voiceUserFlag;
  }

  bool isVolteUser() {
    return YES == volteUserFlag;
  }

  @override
  String toString() {
    return 'VMASubscriberStatus{voiceUserFlag: $voiceUserFlag, vMAUserFlag: $vmaUserFlag, volteUserFlag: $volteUserFlag}';
  }

}

