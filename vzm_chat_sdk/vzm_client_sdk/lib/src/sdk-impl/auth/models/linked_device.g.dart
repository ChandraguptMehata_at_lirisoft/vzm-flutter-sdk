// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'linked_device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LinkedDevice _$LinkedDeviceFromJson(Map<String, dynamic> json) => LinkedDevice(
      deviceid: json['deviceid'] as String?,
      deviceName: json['deviceName'] as String?,
      deviceType: json['deviceType'] as String?,
      deviceMake: json['deviceMake'] as String?,
      deviceModel: json['deviceModel'] as String?,
      osVersion: json['osVersion'] as String?,
      createdTime: json['createdTime'],
      failedAttempts: json['failedAttempts'],
      appName: json['appName'] as String?,
      password: json['password'] as String?,
      pushId: json['pushId'] as String?,
      isAndroid: json['android'] as bool?,
      isIphone: json['iphone'] as bool?,
      prefix: json['prefix'] as String?,
      renewAfterTime: json['renewAfterTime'],
    );

Map<String, dynamic> _$LinkedDeviceToJson(LinkedDevice instance) =>
    <String, dynamic>{
      'deviceid': instance.deviceid,
      'deviceName': instance.deviceName,
      'deviceType': instance.deviceType,
      'deviceMake': instance.deviceMake,
      'deviceModel': instance.deviceModel,
      'osVersion': instance.osVersion,
      'createdTime': instance.createdTime,
      'failedAttempts': instance.failedAttempts,
      'appName': instance.appName,
      'password': instance.password,
      'pushId': instance.pushId,
      'android': instance.isAndroid,
      'iphone': instance.isIphone,
      'prefix': instance.prefix,
      'renewAfterTime': instance.renewAfterTime,
    };
