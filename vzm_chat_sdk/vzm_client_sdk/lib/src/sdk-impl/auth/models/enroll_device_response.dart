/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/linked_device.dart';

import 'auto_reply_settings.dart';

part 'enroll_device_response.g.dart';

@JsonSerializable()
class EnrollDeviceResponse {

  EnrollDeviceResponse(
      {required this.id, required this.mdn, required this.email,
        required this.name, required this.avatar, required this.version,
        required this.createdTime, required this.updatedTime, required this.provisioned,
        required this.status, required this.primaryAmsSyncBaseUri, required this.devices,
        required this.mutedGroups, required this.autoreplyUser, required this.autoReplySettings,
        required this.optInFeatures, required this.disable1to1, required this.xmsInAmsStartTime,
        required this.traceLog});

  @JsonKey(name: 'id')
  late final String id;

  @JsonKey(name: 'mdn')
  late final String mdn;

  @JsonKey(name: 'email')
  late final String? email;

  @JsonKey(name: 'name')
  late final String? name;

  @JsonKey(name: 'avatar')
  late final String? avatar;

  @JsonKey(name: 'version')
  late final dynamic? version;

  @JsonKey(name: 'createdTime')
  late final dynamic? createdTime;

  @JsonKey(name: 'updatedTime')
  late final dynamic? updatedTime;

  @JsonKey(name: 'provisioned')
  late final bool? provisioned;

  @JsonKey(name: 'status')
  late final dynamic? status;

  @JsonKey(name: 'primaryAmsSyncBaseUri')
  late final String? primaryAmsSyncBaseUri;

  @JsonKey(name: 'devices')
  late final List<LinkedDevice>? devices;

  @JsonKey(name: 'mutedGroups')
  late final List<String>? mutedGroups;

  @JsonKey(name: 'autoreplyUser')
  late final bool? autoreplyUser;

  @JsonKey(name: 'autoReplySettings')
  late final List<AutoReplySettings>? autoReplySettings;

  @JsonKey(name: 'optInFeatures')
  late final List<String>? optInFeatures;

  @JsonKey(name: 'disable1to1')
  late final bool? disable1to1;

  @JsonKey(name: 'xmsInAmsStartTime')
  late final dynamic? xmsInAmsStartTime;

  @JsonKey(name: 'traceLog')
  late final bool? traceLog;

  factory EnrollDeviceResponse.fromJson(Map<String, dynamic> json) =>
      _$EnrollDeviceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$EnrollDeviceResponseToJson(this);

  @override
  String toString() {
    return 'EnrollDeviceResponse{id: $id, mdn: $mdn, email: $email, name: $name, avatar: $avatar, version: $version, createdTime: $createdTime, updatedTime: $updatedTime, provisioned: $provisioned, status: $status, primaryAmsSyncBaseUri: $primaryAmsSyncBaseUri, devices: $devices, mutedGroups: $mutedGroups, autoreplyUser: $autoreplyUser, autoReplySettings: $autoReplySettings, optInFeatures: $optInFeatures, disable1to1: $disable1to1, xmsInAmsStartTime: $xmsInAmsStartTime, traceLog: $traceLog}';
  }
}
