// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Config _$ConfigFromJson(Map<String, dynamic> json) => Config(
      name: json['name'] as String?,
      value: json['value'],
      overwrite: json['overwrite'] as bool?,
    );

Map<String, dynamic> _$ConfigToJson(Config instance) => <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
      'overwrite': instance.overwrite,
    };
