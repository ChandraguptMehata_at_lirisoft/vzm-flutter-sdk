// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validate_login_pin_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ValidateLoginPinResponse _$ValidateLoginPinResponseFromJson(
        Map<String, dynamic> json) =>
    ValidateLoginPinResponse(
      code: json['code'] as int?,
      status: json['status'] as String?,
      mdn: json['mdn'] as String?,
      loginToken: json['loginToken'] as String?,
      statusinfo: json['statusinfo'] as String?,
      storageType: json['storageType'] as String?,
      amsStartTime: json['amsStartTime'],
      isHasMsaPair: json['isHasMsaPair'] as bool?,
      devices: (json['removableDevices'] as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : RemovableDevice.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ValidateLoginPinResponseToJson(
        ValidateLoginPinResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'status': instance.status,
      'mdn': instance.mdn,
      'loginToken': instance.loginToken,
      'statusinfo': instance.statusinfo,
      'storageType': instance.storageType,
      'amsStartTime': instance.amsStartTime,
      'isHasMsaPair': instance.isHasMsaPair,
      'removableDevices': instance.devices,
    };
