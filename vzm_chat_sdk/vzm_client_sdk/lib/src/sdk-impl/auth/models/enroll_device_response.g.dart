// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enroll_device_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EnrollDeviceResponse _$EnrollDeviceResponseFromJson(
        Map<String, dynamic> json) =>
    EnrollDeviceResponse(
      id: json['id'] as String,
      mdn: json['mdn'] as String,
      email: json['email'] as String?,
      name: json['name'] as String?,
      avatar: json['avatar'] as String?,
      version: json['version'],
      createdTime: json['createdTime'],
      updatedTime: json['updatedTime'],
      provisioned: json['provisioned'] as bool?,
      status: json['status'],
      primaryAmsSyncBaseUri: json['primaryAmsSyncBaseUri'] as String?,
      devices: (json['devices'] as List<dynamic>?)
          ?.map((e) => LinkedDevice.fromJson(e as Map<String, dynamic>))
          .toList(),
      mutedGroups: (json['mutedGroups'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      autoreplyUser: json['autoreplyUser'] as bool?,
      autoReplySettings: (json['autoReplySettings'] as List<dynamic>?)
          ?.map((e) => AutoReplySettings.fromJson(e as Map<String, dynamic>))
          .toList(),
      optInFeatures: (json['optInFeatures'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      disable1to1: json['disable1to1'] as bool?,
      xmsInAmsStartTime: json['xmsInAmsStartTime'],
      traceLog: json['traceLog'] as bool?,
    );

Map<String, dynamic> _$EnrollDeviceResponseToJson(
        EnrollDeviceResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'mdn': instance.mdn,
      'email': instance.email,
      'name': instance.name,
      'avatar': instance.avatar,
      'version': instance.version,
      'createdTime': instance.createdTime,
      'updatedTime': instance.updatedTime,
      'provisioned': instance.provisioned,
      'status': instance.status,
      'primaryAmsSyncBaseUri': instance.primaryAmsSyncBaseUri,
      'devices': instance.devices,
      'mutedGroups': instance.mutedGroups,
      'autoreplyUser': instance.autoreplyUser,
      'autoReplySettings': instance.autoReplySettings,
      'optInFeatures': instance.optInFeatures,
      'disable1to1': instance.disable1to1,
      'xmsInAmsStartTime': instance.xmsInAmsStartTime,
      'traceLog': instance.traceLog,
    };
