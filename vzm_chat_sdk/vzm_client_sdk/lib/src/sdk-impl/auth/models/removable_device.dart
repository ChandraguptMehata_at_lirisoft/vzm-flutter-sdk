import 'package:json_annotation/json_annotation.dart';

part 'removable_device.g.dart';

@JsonSerializable()
class RemovableDevice {
  RemovableDevice({required this.devId, this.devName, this.createTime});

  @JsonKey(name: 'devId')
  late final String? devId;

  @JsonKey(name: 'devName')
  late final String? devName;

  @JsonKey(name: 'createTime')
  late final String? createTime;

  factory RemovableDevice.fromJson(Map<String, dynamic> json) => _$RemovableDeviceFromJson(json);

  Map<String, dynamic> toJson() => _$RemovableDeviceToJson(this);

  @override
  String toString() {
    return 'RemovableDevice{devId: $devId, devName: $devName, createTime: $createTime}';
  }
}
