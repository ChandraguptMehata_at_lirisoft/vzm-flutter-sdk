/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';

part 'vma_provisioning_status.g.dart';

@JsonSerializable()
class VmaProvisionStatus {

  static String STATUS_FAIL = "FAIL";
  static String STATUS_ERROR = "ERROR";
  static String STATUS_V_BLOCK = "VBLOCK";
  static String STATUS_SUSPENDED = "SUSPENDED";
  static String STATUS_OK = "OK";
  static String STATUS_NO_MMS = "NOMMS";

  VmaProvisionStatus(
      {required this.status, required this.statusInfo});

  @JsonKey(name: 'status')
  late final String status;

  @JsonKey(name: 'statusInfo')
  late final String? statusInfo;

  @override
  String toString() {
    return 'VmaProvisionStatus{status: $status, statusInfo: $statusInfo}';
  }

  factory VmaProvisionStatus.fromJson(Map<String, dynamic> json) =>
      _$VmaProvisionStatusFromJson(json);

  Map<String, dynamic> toJson() => _$VmaProvisionStatusToJson(this);

  bool isOk() {
    return STATUS_OK == status;
  }

  bool isError() {
    return STATUS_ERROR == status;
  }

  bool isFailed() {
    return STATUS_FAIL == status;
  }

  bool isVBlock() {
    return STATUS_V_BLOCK == status;
  }

  bool isSuspended() {
    return STATUS_SUSPENDED == status;
  }

  bool isNoMMS() {
    return STATUS_NO_MMS == status;
  }

}
