/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:collection';
import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';

part 'config.g.dart';

@JsonSerializable()
class Config {

  static const String keyApiHost = "apiHost";
  static const String keyMQTTHost = "mqttHost";
  static const String keyABSHost = "absHost";
  static const String keyWearableHost = "wearableHost";
  static const String keyWebPreviewHost = "webpreviewHost";
  static const String keyMojiProxyHost = "mojiproxyHost";
  static const String keyImageSize = "imageSizeMB";
  static const String keyPinLongCodes = "pinLongCodes";
  static const String keySMSLongCodes = "smsLongCodes";
  static const String keyLongCodePatterns = "LongcodePatterns";
  static const String keyMaxMediaSize = "maxMediaSizeMB";
  static const String keyShortCodeNameMappings = "shortcodeNameMappings";
  static const String keyTranslations = "translations";
  static const String keyBots = "bots";
  static const String keyInstabugFeatureEnabled = "instabug_feature";
  static const String keyInstabugShake = "instabugShake";

  Config({this.name, this.value, this.overwrite});

  @JsonKey(name: 'name')
  late final String? name;

  @JsonKey(name: 'value')
  late final dynamic value;

  @JsonKey(name: 'overwrite')
  late final bool? overwrite;

  factory Config.fromJson(Map<String, dynamic> json) =>
      _$ConfigFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigToJson(this);

  bool isApiHost() {
    return name == keyApiHost;
  }

  String? getApiHost() {
    if (isApiHost() && value is String) {
      value as String;
    } else {
      return null;
    }
  }

  bool isMQTTHost() {
    return name == keyMQTTHost;
  }

  String? getMQTTHost() {
    if (isMQTTHost() && value is String) {
      value as String;
    } else {
      return null;
    }
  }

  bool isWearableHost() {
    return name == keyWearableHost;
  }

  String? getWearableHost() {
    if (isWearableHost() && value is String) {
      value as String;
    } else {
      return null;
    }
  }

  bool isWebpreviewHost() {
    return name == keyWebPreviewHost;
  }

  String? getWebpreviewHost() {
    if (isWebpreviewHost() && value is String) {
      value as String;
    } else {
      return null;
    }
  }

  bool isAbsHost() {
    return name == keyABSHost;
  }

  String? getAbsHost() {
    if (isAbsHost() && value is String) {
      value as String;
    } else {
      return null;
    }
  }

  bool isMojiHost() {
    return name == keyMojiProxyHost;
  }

  String? getMojiHost() {
    if (isMojiHost() && value is String) {
      value as String;
    } else {
      return null;
    }
  }

  bool isImageSizeMB() {
    return name == keyImageSize;
  }

  Double? getImageSizeMB() {
    if (isImageSizeMB() && value is Double) {
      value as Double;
    } else {
      return null;
    }
  }

  bool isPinLongCodes() {
    return name == keyPinLongCodes;
  }

  List<String>? getPinLongCodes() {
    if (isPinLongCodes()) {
      value as List;
    } else {
      return null;
    }
  }

  bool isSmsLongCodes() {
    return name == keySMSLongCodes;
  }

  List<String>? getSmsLongCodes() {
    if (isSmsLongCodes()) {
      value as List;
    } else {
      return null;
    }
  }

  bool isLongCodePatterns() {
    return name == keyLongCodePatterns;
  }

  List<String>? longCodesPatterns() {
    if (isLongCodePatterns()) {
      value as List;
    } else {
      return null;
    }
  }

  bool isMaxMediaSizeMB() {
    return name == keyMaxMediaSize;
  }

  String? getMaxMediaSizeMB() {
    if (isMaxMediaSizeMB() && value is Double) {
      value as Double;
    } else {
      return null;
    }
  }

  bool isShortCodeNameMappings() {
    return name == keyShortCodeNameMappings;
  }

  List<Map<String, String>>? shortcodeNameMappings() {
    if (isShortCodeNameMappings() && value is String) {
      value as List<Map<String, String>>;
    } else {
      return null;
    }
  }

  bool isBot() {
    return name == keyBots;
  }

  LinkedHashMap? getBot() {
    if (isBot() && value is LinkedHashMap) {
      value as LinkedHashMap;
    } else {
      return null;
    }
  }

  bool isTranslations() {
    return name == keyTranslations;
  }

  LinkedHashMap? getTranslations() {
    if (isTranslations() && value is LinkedHashMap) {
      value as LinkedHashMap;
    } else {
      return null;
    }
  }

  bool isInstabugShake() {
    return name == keyInstabugShake;
  }

  String? getInstabugShake() {
    if (isInstabugShake()) {
      value as String;
    } else {
      return null;
    }
  }

  @override
  String toString() {
    return 'Config{name: $name, value: $value, overwrite: $overwrite}';
  }
}
