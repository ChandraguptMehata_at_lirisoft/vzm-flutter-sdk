// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auto_reply_settings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AutoReplySettings _$AutoReplySettingsFromJson(Map<String, dynamic> json) =>
    AutoReplySettings(
      endDate: json['endDate'],
      message: json['message'] as String?,
    );

Map<String, dynamic> _$AutoReplySettingsToJson(AutoReplySettings instance) =>
    <String, dynamic>{
      'endDate': instance.endDate,
      'message': instance.message,
    };
