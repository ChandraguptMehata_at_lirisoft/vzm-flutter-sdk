// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vma_subscriber_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VMASubscriberStatus _$VMASubscriberStatusFromJson(Map<String, dynamic> json) =>
    VMASubscriberStatus(
      voiceUserFlag: json['VoiceUserFlag'] as String?,
      vmaUserFlag: json['VMAUserFlag'] as String?,
      volteUserFlag: json['VolteUserFlag'] as String?,
    );

Map<String, dynamic> _$VMASubscriberStatusToJson(
        VMASubscriberStatus instance) =>
    <String, dynamic>{
      'VoiceUserFlag': instance.voiceUserFlag,
      'VMAUserFlag': instance.vmaUserFlag,
      'VolteUserFlag': instance.volteUserFlag,
    };
