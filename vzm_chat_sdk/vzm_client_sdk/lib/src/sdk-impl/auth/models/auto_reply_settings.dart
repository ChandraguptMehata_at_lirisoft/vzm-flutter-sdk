/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
part 'auto_reply_settings.g.dart';

@JsonSerializable()
class AutoReplySettings {

  AutoReplySettings(
      {required this.endDate, required this.message});

  @JsonKey(name: 'endDate')
  late final dynamic? endDate;

  @JsonKey(name: 'message')
  late final String? message;

  factory AutoReplySettings.fromJson(Map<String, dynamic> json) =>
      _$AutoReplySettingsFromJson(json);

  Map<String, dynamic> toJson() => _$AutoReplySettingsToJson(this);

  @override
  String toString() {
    return 'AutoReplySettings{endDate: $endDate, message: $message}';
  }
}

