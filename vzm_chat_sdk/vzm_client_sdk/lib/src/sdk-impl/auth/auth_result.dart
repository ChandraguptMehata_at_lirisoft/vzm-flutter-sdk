/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
enum AuthStatus {
  SUCCESS,
  NOT_A_VALID_MDN,
  NONE_VERIZON_MDN,
  OTP_REQUEST_LIMIT_EXCEEDED,
  OTP_REQUEST_FAILED,
  PIN_VALIDATION_EXPIRED,
  PIN_VALIDATION_TIMEOUT,
  MMS_BLOCK,
  NOT_ELIGIBLE,
  OTP_VALIDATION_FAILED,
  SUSPENDED,
  FAILED,
  ERROR,
  NO_TEXT,
  VBLOCK,
  NO_MMS
}

class AuthResult {
  AuthStatus? status;
  var message;
  int? errorCode;

  AuthResult(this.status, this.message, {this.errorCode});
}
