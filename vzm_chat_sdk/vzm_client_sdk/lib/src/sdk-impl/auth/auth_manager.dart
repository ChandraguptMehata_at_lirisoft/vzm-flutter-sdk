/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */


import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:retrofit/dio.dart';
import 'package:vzm_client_sdk/src/api/auth/auth_request.dart';
import 'package:vzm_client_sdk/src/common/device/device_manager.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_utils.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/enroll_device_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/validate_login_pin_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/vma_provisioning_status.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/vma_subscriber_status.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/profile_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/fcm/push_token_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/request_otp_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/restore.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/ams_api.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/vma_api.dart';
import 'package:vzm_client_sdk/src/sdk-impl/vma/vma_subscriber_body.dart';
import 'package:vzm_client_sdk/vzm_client_sdk.dart';

import 'auth_result.dart';

class AuthManager implements AuthRequest {
  BuildContext? context;
  late DeviceManager _deviceManager;
  late VMAApi _apiClient;
  late AmsApi _amsApiClient;
  late AppCredential _appCredential;
  late OttConfigurationManager _ottConfigurationManager;
  late AppLogger _appLogger;

  AuthManager(this.context) {
    _deviceManager = serviceLocator.get<DeviceManager>();
    _apiClient = serviceLocator.get<VMAApi>();
    _amsApiClient = serviceLocator.get<AmsApi>();
    _appCredential = serviceLocator.get<AppCredential>();
    _ottConfigurationManager = serviceLocator.get<OttConfigurationManager>();
    _appLogger = serviceLocator.get<AppLogger>();
  }

  @override
  isProvisioned() {
    return _appCredential.isOTTProvisioned();
  }

  @override
  Future<AuthResult?> requestOtp(String mdn) async {
    AuthResult? authresult;
    print("#auth.vma.$mdn: requestOtp deviceName");
    if (AppUtils.isNumeric(mdn) && mdn.length != 10) {
      return AuthResult(AuthStatus.NOT_A_VALID_MDN, "Enter valid 10 digit mobile number");
    }
    authresult = await _ottConfigurationManager.downloadConfigurations();
    if (authresult!.status == AuthStatus.FAILED) {
      return AuthResult(
          AuthStatus.FAILED, "Failed to download Ott configurations");
    }

    var deviceType = await _deviceManager.getDeviceType();
    var deviceName = await _deviceManager.getDeviceName();
    var deviceNameFinal = "SDK $deviceName";

    print("#auth.vma.$mdn: requestOtp deviceType $deviceType deviceName $deviceNameFinal");
    await _apiClient
        .generateOtp(mdn, false, deviceType, deviceNameFinal)
        .then((it) => {
              if (it.response.statusCode == 200) {
                print("#auth.vma.$mdn: requestOtp api response" + it.data.toString()),
                authresult = setOtpResponse(it.data)
              }
              else {
                  print("#auth.vma.$mdn: requestOtp api failed with error code: ${it.response.statusCode}" +
                      "error msg: ${it.response.statusMessage}"),
                  authresult = AuthResult(AuthStatus.FAILED, it.response.statusMessage)
                }
            })
        .catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          authresult =
              AuthResult(AuthStatus.OTP_REQUEST_FAILED, obj.toString());
          break;
        default:
      }
    });
    return authresult;
  }

  AuthResult setOtpResponse(RequestOtpResponse data) {
    if (data.isOk) {
      return AuthResult(AuthStatus.SUCCESS, "");
    } else if (data.isNonVZUser) {
      return AuthResult(AuthStatus.NONE_VERIZON_MDN, "");
    } else if (data.isRequestOverLimit) {
      return AuthResult(AuthStatus.OTP_REQUEST_LIMIT_EXCEEDED, "");
    } else if (data.isError) {
      return AuthResult(AuthStatus.ERROR,
          data.status.toString() + " " + data.statusInfo.toString() ?? "");
    } else if (data.isFailed) {
      return AuthResult(AuthStatus.FAILED,
          data.status.toString() + " " + data.statusInfo.toString() ?? "");
    } else {
      return AuthResult(AuthStatus.PIN_VALIDATION_TIMEOUT, "");
    }
  }

  @override
  Future<AuthResult?> verifyOtp(String mdn, String otp) async {
    AuthResult? authResult;
    var deviceMake = await _deviceManager.getDeviceMake();
    var deviceModel = await _deviceManager.getDeviceModel();
    var deviceOSVersion = await _deviceManager.getDeviceOSVersion();
    var deviceId = await _deviceManager.getDeviceId();
    await _apiClient
        .validateOtp(
            mdn, otp, deviceMake, deviceModel, deviceOSVersion, deviceId)
        .then((it) async => {
              print("#auth.vma.$mdn: verifyOtp response" + it.toString()),
              if (it.response.statusCode == 200) {
                print("#auth.vma.$mdn: verifyOtp response body >> " + it.data.toString()),
                if (it.data.isOk()) {
                  //todo: save vma credential
                  _deviceManager.setMdn(it.data.mdn),
                  authResult = await checkVmaSubscriberStatus(mdn, it.data.loginToken!),
                  if (authResult!.status == AuthStatus.SUCCESS) {
                     serviceLocator.get<RestoreManager>().start()
                  }
                } else {
                  authResult = setValidateOTPResponse(it.data)
                }
              }
              else {
                print("#auth.vma.$mdn: verifyOtp api failed with error code: ${it.response.statusCode}" +
                    "error msg: ${it.response.statusMessage}"),
                authResult = AuthResult(AuthStatus.FAILED, it.response.statusMessage)
              }
            })
        .catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          authResult = AuthResult(AuthStatus.OTP_VALIDATION_FAILED, obj.toString());
          break;
        default:
          authResult = AuthResult(AuthStatus.FAILED, obj.toString());
      }
    });
    return authResult;
  }

  AuthResult setValidateOTPResponse(ValidateLoginPinResponse? data) {
    if (data!.isOk()) {
      return AuthResult(AuthStatus.SUCCESS, "");
    } else if (data.isNotVzwMdn()) {
      return AuthResult(AuthStatus.NOT_A_VALID_MDN, "");
    } else if (data.isNotEligible()) {
      return AuthResult(AuthStatus.NOT_ELIGIBLE, "");
    } else if (data.isSuspended()) {
      return AuthResult(AuthStatus.SUSPENDED, "");
    } else if (data.isUserNoText()) {
      return AuthResult(AuthStatus.NO_TEXT, "");
    } else if (data.isVBlock()) {
      return AuthResult(AuthStatus.VBLOCK, "");
    } else if (data.isError()) {
      return AuthResult(AuthStatus.ERROR,
          data.status.toString() + " " + data.statusinfo.toString() ?? "");
    } else if (data.isFailed()) {
      return AuthResult(AuthStatus.FAILED,
          data.status.toString() + " " + data.statusinfo.toString() ?? "");
    } else {
      return AuthResult(AuthStatus.PIN_VALIDATION_TIMEOUT, "");
    }
  }

  Future<AuthResult?> checkVmaSubscriberStatus(String mdn, String loginToken) async {
    AuthResult? authResult;
    print("#auth.vma.$mdn: checkVmaSubscriberStatus");
    try {
      HttpResponse<VMASubscriberStatus> vmaSubscriberStatusResponse = await _apiClient.getVMASubscriberStatus(mdn, loginToken);
      if (vmaSubscriberStatusResponse.response.statusCode == 200) {
        VMASubscriberStatus vmaSubscriberStatus = vmaSubscriberStatusResponse.data;
        print("#auth.vma.$mdn: checkVmaSubscriberStatus ${vmaSubscriberStatus.toString()}");
        authResult = await parseVMASubscriberResponse(vmaSubscriberStatus, mdn, loginToken);
      } else {
        print("#auth.vma.$mdn: checkVmaSubscriberStatus failed with error code: ${vmaSubscriberStatusResponse.response.statusCode}" +
            "error msg: ${vmaSubscriberStatusResponse.response.statusMessage}");
        authResult = AuthResult(AuthStatus.FAILED, vmaSubscriberStatusResponse.response.statusMessage);
      }
    } catch (exception, stacktrace)  {
      print("#auth.vma.$mdn checkVmaSubscriberStatus throwing error ${exception.toString()}, stacktrace $stacktrace");
      switch (exception.runtimeType) {
        case DioError:
          authResult = AuthResult(AuthStatus.OTP_VALIDATION_FAILED, exception.toString());
          break;
        default:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
      }
    }
    return authResult;
  }

  Future<AuthResult?> parseVMASubscriberResponse(VMASubscriberStatus it, String mdn, String vmaToken) async {
    AuthResult? authResult;
    VMASubscriberStatus vmaSubscriberStatus = it;
    if (vmaSubscriberStatus.isVmaUser()) {
      //Existing vma user
      print("#auth.vma.$mdn: parseVMASubscriberResponse -> mdn = $mdn, " +
          "vmaToken = $vmaToken");
      authResult = await getOrCreateOTTAccount(mdn, vmaToken);
      if (authResult!.status == AuthStatus.SUCCESS) {
        _appCredential.saveVmaSubscriberStatus(vmaSubscriberStatus);
        _appCredential.setIsVmaProvisioned(true);
        return AuthResult(AuthStatus.SUCCESS, "");
      }
    } else {
      print("#auth.vma. parseVMASubscriberResponse not a vma user");
      //New VMA user
      authResult = await createVMAUser(mdn, vmaToken);
      if (authResult!.status == AuthStatus.SUCCESS) {
        var ottResult = await getOrCreateOTTAccount(mdn, vmaToken);
        if (ottResult!.status == AuthStatus.SUCCESS) {
          _appCredential.saveVmaSubscriberStatus(vmaSubscriberStatus);
          return AuthResult(AuthStatus.SUCCESS, "");
        }
        return ottResult;
      } else {
        _appCredential.clearVmaCredentials();
      }
    }
    return authResult;
  }

  Future<AuthResult?> getOrCreateOTTAccount(String mdn, String vmaToken) async {
    AuthResult? authResult;
    String fcmToken = _appCredential.getFcmToken();
    print("#auth.ottHandler.$mdn: getOrCreateOTTAccount -> mdn = $mdn, " +
        "vmaToken = $vmaToken, fcmToken = $fcmToken");
    var vmaMdn = mdn;
    if (mdn.startsWith("+1")) {
      vmaMdn = mdn.substring(2);
    }
    VmaSubscriberBody vmaSubscriberBody = VmaSubscriberBody(registrationId: fcmToken);
    try {
      HttpResponse<EnrollDeviceResponse> httpResponse = await _amsApiClient.create(vmaToken, vmaMdn, vmaSubscriberBody);
      if (httpResponse.response.statusCode == 200 || httpResponse.response.statusCode == 201) {
        EnrollDeviceResponse enrollDeviceResponse = httpResponse.data;
        print("#auth.ottHandler.$mdn: getOrCreateOTTAccount -> enrollDeviceResponse = ${enrollDeviceResponse.toString()}");
        if (enrollDeviceResponse.provisioned == true) {
          await parseOttProfile(enrollDeviceResponse);
          authResult = AuthResult(AuthStatus.SUCCESS, "");
        } else {
          authResult = AuthResult(AuthStatus.FAILED, "Provision failed due to system error");
        }
      } else {
        print("#auth.ottHandler.$mdn: getOrCreateOTTAccount -> enrollDeviceResponse failed with error code: ${httpResponse.response.statusCode}" +
            "error msg: ${httpResponse.response.statusMessage}");
        authResult = AuthResult(AuthStatus.FAILED, httpResponse.response.statusMessage);
      }
    } catch (exception, stacktrace)  {
      print("#auth.ottHandler. getOrCreateOTTAccount throwing error ${exception.toString()}, stacktrace $stacktrace");
      switch (exception.runtimeType) {
        case DioError:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
          break;
        default:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
      }
    }
    return authResult;
  }

  Future<AuthResult?> createVMAUser(String mdn, String vmaToken) async{
    AuthResult? authResult;
    print("#auth.vma.$mdn: createVmaUser");
    try {
      HttpResponse<VmaProvisionStatus> vmaProvisionStatusResponse = await _apiClient.provision(mdn, vmaToken);
      if (vmaProvisionStatusResponse.response.statusCode == 200) {
        VmaProvisionStatus vmaProvisionStatus = vmaProvisionStatusResponse.data;
        print("#auth.vma.$mdn: createVmaUser vmaProvisionStatus = ${vmaProvisionStatus.toString()}");
        if (vmaProvisionStatus.isOk()) {
          _appCredential.setIsVmaProvisioned(true);
          authResult = AuthResult(AuthStatus.SUCCESS, "");
        } else if (vmaProvisionStatus.isNoMMS()) {
          authResult = AuthResult(AuthStatus.NO_MMS, vmaProvisionStatus.statusInfo);
        } else if (vmaProvisionStatus.isSuspended()) {
          authResult = AuthResult(AuthStatus.FAILED, vmaProvisionStatus.statusInfo);
        } else if (vmaProvisionStatus.isSuspended()) {
          authResult = AuthResult(AuthStatus.SUSPENDED, vmaProvisionStatus.statusInfo);
        } else if (vmaProvisionStatus.isVBlock()) {
          authResult = AuthResult(AuthStatus.VBLOCK, vmaProvisionStatus.statusInfo);
        } else if (vmaProvisionStatus.isFailed()) {
          authResult = AuthResult(AuthStatus.FAILED, vmaProvisionStatus.statusInfo);
        } else {
          authResult = AuthResult(AuthStatus.ERROR, vmaProvisionStatus.statusInfo);
        }
      } else {
        print("#auth.vma.$mdn: createVmaUser failed with error code: ${vmaProvisionStatusResponse.response.statusCode}" +
            "error msg: ${vmaProvisionStatusResponse.response.statusMessage}");
        authResult = AuthResult(AuthStatus.FAILED, vmaProvisionStatusResponse.response.statusMessage);
      }
    } catch (exception, stacktrace)  {
      print("#auth.vma.$mdn: createVmaUser throwing error ${exception.toString()} stacktrace $stacktrace");
      switch (exception.runtimeType) {
        case DioError:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
          break;
        default:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
      }
    }
    return authResult;
  }

  Future<void> parseOttProfile(EnrollDeviceResponse enrollDeviceResponse) async {
    await _appCredential.saveOttCredentials(enrollDeviceResponse);
    var pushToken = _appCredential.getFcmToken();
    print("#auth.ottHandler.${enrollDeviceResponse.mdn}: parseOttProfile -> " + "pushToken = $pushToken");

    var pushTokenManager = serviceLocator.get<PushTokenManager>();
    if ((Platform.isAndroid)) {
      await pushTokenManager.updatePushToken(pushToken);
      print("#auth.ottHandler.${enrollDeviceResponse.mdn}: parseOttProfile -> " + "pushToken updated");
      startMqttConnection();
    }
    serviceLocator.get<ProfileRepositoryImpl>().addProfile(enrollDeviceResponse);
    //todo: update auto reply settings and mute groups
  }

  void startMqttConnection() {
    serviceLocator.get<VzmClientSdk>().mqttSetup();
  }

}

