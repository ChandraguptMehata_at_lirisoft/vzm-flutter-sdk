import 'package:dio/dio.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';

class UserAgentInterceptor extends Interceptor {
  static const String userAgent = "User-Agent";
  var ottConfigManager = serviceLocator.get<OttConfigurationManager>();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers[userAgent] = ottConfigManager.userAgent;
    print('REQUEST[${options.method}] => PATH: ${options.path} Header ${options.headers} baseUrl ${options.baseUrl}');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print(
      'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}',
    );
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print(
      'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}',
    );
    return super.onError(err, handler);
  }
}
