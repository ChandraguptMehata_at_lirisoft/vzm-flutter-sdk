/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:convert';

import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:vzm_client_sdk/src/common/data/data_connectivity_controller.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/model/send_mqtt_payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_message_sender.dart';

import 'mqtt_sync_manager.dart';

class MQTTManager {
  late AppLogger _logger;
  late DataConnectivityController _dataConnectivityController;
  late AppCredential _appCredential;
  late MqttSyncManager _mqttSyncManager;

  late MqttServerClient _client;
  static const String _topic = "VirtualTopic/Broker/Message";
  static const String _tag = "MQTT";

  MQTTManager() {
    _logger = serviceLocator.get<AppLogger>();
    _dataConnectivityController = serviceLocator.get<DataConnectivityController>();
    _appCredential = serviceLocator.get<AppCredential>();
    _mqttSyncManager = serviceLocator.get<MqttSyncManager>();
  }

  void initializeMQTTClient() {
    String clientId = _appCredential.getSubscriberId()! + "_sdk_" + DateTime.now().millisecondsSinceEpoch.toString();
    _client = MqttServerClient("mqtt.vzmessages.com", clientId);
    _client.port = 443;
    _client.keepAlivePeriod = 30; //seconds
    _client.onDisconnected = _onDisconnected;
    _client.secure = true;
    _client.logging(on: AppLogger.isMqttLogsEnabled);
    _client.onConnected = _onConnected;
    _client.onSubscribed = _onSubscribed;
    _client.setProtocolV311();
    _client.pongCallback = _pong;
    _client.connectionMessage = MqttConnectMessage().startClean();

    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.initializeMQTTClient: client connecting.... ");
    }

    _subscribeForDataConnectivityChange();
  }

  void connect() async {
    try {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.conn.connect: Mosquitto client connecting.... ");
      }
      await _client.connect(_appCredential.getUserName(), _appCredential.getPassword());
    } on Exception catch (e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.conn.connect: client exception: $e");
      }
      disconnect();
    }
  }

  void disconnect() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.disconnect disconnected");
    }
    _client.disconnect();
  }

  Future<bool> _publish(String message) async {
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(message);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.publish msg $message");
    }
    try {
      int msgIdentifier = _client.publishMessage(_topic, MqttQos.exactlyOnce, builder.payload!);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.conn.publish msgIdentifier $msgIdentifier");
      }
      return true;
    } on Exception catch (e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.conn.publish:: exception while publishing message $e");
      }
    }
    return false;
  }

  void _onSubscribed(String topic) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.subscribe: onSubscribed: confirmed for topic $topic");
    }
  }

  void _onDisconnected() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.disconnect: onDisconnected: client callback - Client disconnection");
    }
    if (_client.connectionStatus!.returnCode == MqttConnectReturnCode.noneSpecified) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.conn.disconnect: callback is solicited, this is correct");
      }
    }
  }

  void _onConnected() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.connected: Mosquitto Client connected....");
    }

    _client.subscribe(_topic, MqttQos.atLeastOnce);

    _client.updates!.listen(_onMessageReceived);

    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.onConnected: client callback - client connection was sucessful");
    }
  }

  void _onMessageReceived(List<MqttReceivedMessage<MqttMessage?>>? topicList) {
    final MqttPublishMessage mqttPublishMessage = topicList![0].payload as MqttPublishMessage;

    final String payloadValue = MqttPublishPayload.bytesToStringAsString(mqttPublishMessage.payload.message);

    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.onMessageReceived: change notification:: topic is <${topicList[0].topic}>, payload is <-- $payloadValue -->");
    }

    try {
      Payload payload = Payload.fromJson(json.decode(payloadValue));
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.conn.onMessageReceived: change notification:: payload is <-- ${payload.toString()}");
      }
      _mqttSyncManager.process(payload);
    } on Exception catch (_e) {
      if (AppLogger.isDebugEnabled) {
        _logger.error(_tag, "#mqtt.conn.onMessageReceived: exception in converting to payload", _e);
      }
      throw Exception("Not able to parse mqtt payload");
    }
  }

  void _pong() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.pong: ping response client callback invoked");
    }
  }

  void _subscribeForDataConnectivityChange() {
    _dataConnectivityController.connectionChange.listen((network) {
      bool isConnected = _isConnected();
      bool isProvisioned = _appCredential.isOTTProvisioned();
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag,
            "#mqtt.conn.subscribeForDataConnectivityChange: network: ${network.toString()}, isConnected: $isConnected, isProvisioned: $isProvisioned");
      }
      if (isProvisioned && !isConnected && network.hasConnected) {
        connect();
        _sendPendingEnqueueMessage();
      }
    });
  }

  bool _isConnected() {
    return (_client.connectionStatus!.state == MqttConnectionState.connected || _client.connectionStatus!.state == MqttConnectionState.connecting);
  }

  void notifyMqttManager(bool isInBackground) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.notifyMqttManager: ForegroundStatusChanged isInBackground=$isInBackground");
    }
    if (!isInBackground) {
      if (_appCredential.isOTTProvisioned() && !_isConnected()) {
        connect();
      }
    } else {
      disconnect();
    }
  }

  Future<bool> send(SendMqttPayload payload, int id) async {
    var json = jsonEncode(payload);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.send.$id: msg=$json >> _isConnected ${_isConnected()}");
    }
    if (!_isConnected()) {
      connect();
    }
    return await _publish(json);
  }

  void _sendPendingEnqueueMessage() {
    MqttMessageSender mqttMessageSender = serviceLocator.get<MqttMessageSender>();
    mqttMessageSender.sendPendingItems();
  }
}
