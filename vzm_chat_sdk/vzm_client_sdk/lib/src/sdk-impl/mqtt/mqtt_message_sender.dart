/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:vzm_client_sdk/src/api/repositories/message_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/send_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/model/send_mqtt_payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_connection_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_model_converter.dart';

import '../../../objectbox.g.dart';

class MqttMessageSender {
  late final AppLogger _logger;
  late final MqttModelConverter _mqttModelConverter;
  late final MqttConnectionManager _mqttConnectionManager;
  late final Box<SendQueueEntity> _sendQueueBox;
  late final MessageRepository _messageRepository;

  static const String _tag = "MQTT-MESSAGE-SENDER";

  MqttMessageSender() {
    _logger = serviceLocator.get<AppLogger>();
    _mqttModelConverter = serviceLocator.get<MqttModelConverter>();
    _mqttConnectionManager = serviceLocator.get<MqttConnectionManager>();
    _sendQueueBox = serviceLocator.get<DatabaseManager>().getSendQueueBox();
    _messageRepository = serviceLocator.get<MessageRepository>();
  }

  Future<SendMessageStatus> _sendMessage(Message msg, String serverId) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.msg.send.${msg.id}: sendMessage > item= $msg");
    }
    SendMqttPayload? payload;
    PayloadType type = _mqttModelConverter.toPayloadType(msg.type);
    switch (type) {
      case PayloadType.smsMessage:
      case PayloadType.mmsMessage:
      case PayloadType.textMessage:
      case PayloadType.mediaMessage:
        payload = _toMessagePayload(msg, serverId);
        break;

      default:
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#mqtt.msg.send.${msg.id}: sendMessage > type not implemented");
        }
        break;
    }
    if (payload != null) {
      var isMsg = isSentReceiptRequested(_mqttModelConverter.toPayloadType(msg.type));
      if (isMsg) {
        _updateMessage(msg);
      }
      bool result = await _mqttConnectionManager.send(payload, msg.id);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#mqtt.msg.send.${msg.id}: sendMessage > result: $result, isMsg: $isMsg");
      }
      if (isMsg) {
        return SendMessageStatus.waitingSentReceipt;
      }
      return SendMessageStatus.sent;
    }
    return SendMessageStatus.sent;
  }

  Future<bool> enqueueMessage(Message message, String serverId) async {
    SendQueueEntity sendQueueEntity = SendQueueEntity(message.id /*, serverId, SendMessageStatus.queued, message.time*/);
    sendQueueEntity.serverId = serverId;
    sendQueueEntity.status = SendMessageStatus.queued;
    sendQueueEntity.time = message.time;
    _sendQueueBox.put(sendQueueEntity);
    return await _execute();
  }

  Future<bool> _execute() async {
    List<SendQueueEntity> sendQueueList = _sendQueueBox.getAll();
    for (SendQueueEntity item in sendQueueList) {
      if (item.status == SendMessageStatus.queued) {
        Message? msg = await _messageRepository.findMessageById(item.luId);
        if (msg != null) {
          SendMessageStatus sendMessageStatus = await _sendMessage(msg, item.serverId!);
          if (AppLogger.isDebugEnabled) {
            _logger.debug(_tag, "#mqtt.msg.send.${item.id}: execute > sendMessageStatus $sendMessageStatus");
          }
          if (sendMessageStatus == SendMessageStatus.sent) {
            _sendQueueBox.remove(item.id);
          } else {
            item.status = sendMessageStatus;
            _sendQueueBox.put(item);
          }
        } else {
          if (AppLogger.isDebugEnabled) {
            _logger.debug(_tag, "#mqtt.msg.send.${item.id}: no message found");
          }
          _sendQueueBox.remove(item.id);
        }
      } else {
        //_sendQueueBox.remove(item.id);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#mqtt.msg.send.${item.id}: status ${item.status}");
        }
      }
    }
    return true;
  }

  SendMqttPayload _toMessagePayload(Message msg, String serverId) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.send.msg.${msg.id}: toMessagePayload > send  body = ${msg.toString()}");
    }
    return _mqttModelConverter.toSendMessagePayload(msg, serverId);
  }

  bool isSentReceiptRequested(PayloadType type) {
    switch (type) {
      case PayloadType.smsMessage:
      case PayloadType.mmsMessage:
      case PayloadType.textMessage:
      case PayloadType.mediaMessage:
        return true;

      default:
        return false;
    }
  }

  void _updateMessage(Message msg) {
    var data = Message(msg.id, msg.time, msg.type, msg.status, msg.inbound, msg.sender, msg.recipients, msg.conversationId);
    data.status = MessageStatus.sending;
    _messageRepository.updateById(data);
  }

  void sendPendingItems() {
    _execute();
  }
}
