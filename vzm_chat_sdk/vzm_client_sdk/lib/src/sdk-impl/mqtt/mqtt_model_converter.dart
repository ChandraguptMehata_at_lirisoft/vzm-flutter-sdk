/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:convert';

import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/profile_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/media_item_data.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/media_item_type.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/model/send_mqtt_payload.dart';

import '../../../vzm_client_sdk.dart';
import 'model/media_item.dart';
import 'model/send_message_data.dart';

class MqttModelConverter {
  late final AppLogger _logger;
  late final ProfileRepositoryImpl _profileManager;

  static const String _tag = "MQTT-MODEL-CONVERTER";

  MqttModelConverter() {
    _logger = serviceLocator.get<AppLogger>();
    _profileManager = serviceLocator.get<ProfileRepositoryImpl>();
  }

  SendMqttPayload toSendMessagePayload(Message src, String groupId) {
    var recipients = <String>[];
    if (src.isTelephony()) {
      src.recipients.map((e) => recipients.add(e.address)).toList();
    } else {
      recipients.addAll(getSubscriberIds(src.recipients));
    }

    String? body = src.body != null ? base64Encode(utf8.encode(src.body!)) : null;
    var data = SendMessageData(groupId: groupId, clientMsgId: src.clientMsgId, recipients: recipients, recipientMdns: recipients, text: body);

    var type = toPayloadType(src.type);

    if (src.attachments != null && src.attachments!.isNotEmpty) {
      // only one attachment per message
      var attachment = src.attachments![0];
      String mediaData = toAttachmentData(attachment);
      var item = MediaItem(toMediaItemType(attachment.fileType), mediaData);
      data.payload = [item];
    }

    return SendMqttPayload(type, data);
  }

  List<String> getSubscriberIds(List<Contact> src) {
    var recipients = <String>[];
    for (Contact contact in src) {
      ProfileEntity? profile = _profileManager.getProfile(contact.address);
      if (profile != null) {
        recipients.add(profile.profileId!);
      }
    }
    return recipients;
  }

  PayloadType toPayloadType(MessageType type) {
      switch (type) {
        case MessageType.sms:
          return PayloadType.smsMessage;
        case MessageType.mms:
          return PayloadType.mmsMessage;
        case MessageType.textMessage:
          return PayloadType.textMessage;
        case MessageType.mediaMessage:
          return PayloadType.mediaMessage;
        default:
          return PayloadType.unknown;
      }
  }

  String toAttachmentData(Attachment attachment) {
    var attachmentData = AttachmentData(attachment.serverUrl, attachment.thumbnail.toString(), attachment.mimeType, attachment.height,
        attachment.width, attachment.size.toInt(), attachment.duration, attachment.fileName);
    var json = jsonEncode(attachmentData);
    return json;
  }

  MediaItemType toMediaItemType(FileType? fileType) {
    switch (fileType) {
      case FileType.image:
        return MediaItemType.image;
      case FileType.audio:
        return MediaItemType.audio;
      case FileType.video:
        return MediaItemType.video;
      case FileType.vcard:
        return MediaItemType.vcard;
      default:
        return MediaItemType.unknown;
    }
  }
}
