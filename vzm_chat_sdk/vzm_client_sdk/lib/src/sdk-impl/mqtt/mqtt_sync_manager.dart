/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/persistence_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/sync/base_sync_manager.dart';

import '../../../vzm_client_sdk.dart';

class MqttSyncManager extends BaseSyncManager {
  late AppLogger _logger;
  late PersistenceManager _persistenceManager;
  static const String _tag = "MQTT-SYNC";

  MqttSyncManager() {
    _logger = serviceLocator.get<AppLogger>();
    _persistenceManager = serviceLocator.get<PersistenceManager>();
  }

  Future<void> process(Payload payload) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.msg: process message = ${payload.toString()}");
    }

    bool processedResult = false;

    switch (payload.type) {
      case PayloadType.textMessage:
      case PayloadType.mediaMessage:
      case PayloadType.smsMessage:
      case PayloadType.rcsTextMessage:
      case PayloadType.mmsMessage:
        processedResult = await handleMessage(payload);
        break;

      case PayloadType.createGroupEvent:
        await _persistenceManager.handleCreateGroupEvent(payload);
        processedResult = true;
        break;

      case PayloadType.groupUpdateEvent:
        processedResult = await handleGroupUpdateEvent(payload);
        break;

      case PayloadType.removeFromGroupEvent:
        processedResult = await _persistenceManager.handleRemoveFromGroupEvent(payload);
        break;

      case PayloadType.sentReceiptEvent:
        processedResult = await _persistenceManager.handleSentReceiptEvent(payload);
        break;

      default:
        break;
    }

    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.msg: processedResult = $processedResult");
    }
  }

  Future<bool> handleMessage(Payload payload) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.msg.${payload.object.id}:: handleMessage");
    }
    var missingProfiles = _persistenceManager.getMissingProfile(payload.object);
    if (missingProfiles.isNotEmpty) {
      restoreProfiles(missingProfiles);
    }

    var groupId = payload.object.groupId!;
    var thread = _persistenceManager.geConversation(groupId);
    thread ??= await createConversationForMessage(payload);

    if (thread != null) {
      return await _persistenceManager.handleMessagePayload(thread, payload);
    } else if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.msg.${payload.object.id}: failed.");
    }

    return false;
  }

  Future<bool> handleGroupUpdateEvent(Payload payload) async {
    var missingProfiles = _persistenceManager.getMissingProfile(payload.object);
    if (missingProfiles.isNotEmpty) {
      restoreProfiles(missingProfiles);
    }
    return await _persistenceManager.handleGroupUpdateEvent(payload);
  }
}
