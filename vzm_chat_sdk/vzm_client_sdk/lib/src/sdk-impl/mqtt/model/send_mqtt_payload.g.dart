// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_mqtt_payload.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendMqttPayload _$SendMqttPayloadFromJson(Map<String, dynamic> json) =>
    SendMqttPayload(
      $enumDecode(_$PayloadTypeEnumMap, json['type']),
      json['object'],
    );

Map<String, dynamic> _$SendMqttPayloadToJson(SendMqttPayload instance) =>
    <String, dynamic>{
      'type': _$PayloadTypeEnumMap[instance.type],
      'object': instance.obj,
    };

const _$PayloadTypeEnumMap = {
  PayloadType.sentReceiptEvent: 'SentReceiptEvent',
  PayloadType.mediaMessage: 'MediaMessage',
  PayloadType.deleteConversationEvent: 'DeleteConversationEvent',
  PayloadType.readConversationEvent: 'ReadConversationEvent',
  PayloadType.typingIndicatorEvent: 'TypingIndicatorEvent',
  PayloadType.removeFromGroupEvent: 'RemoveFromGroupEvent',
  PayloadType.inviteToGroupEvent: 'InviteToGroupEvent',
  PayloadType.groupUpdateEvent: 'GroupUpdateEvent',
  PayloadType.createGroupEvent: 'CreateGroupEvent',
  PayloadType.textMessage: 'TextMessage',
  PayloadType.msgDeliveredEvent: 'MsgDeliveredEvent',
  PayloadType.msgDeliveryFailedEvent: 'MsgDeliveryFailedEvent',
  PayloadType.msgDeletedEvent: 'MsgDeletedEvent',
  PayloadType.subscriberUpdateEvent: 'SubscriberUpdateEvent',
  PayloadType.messageCommentEvent: 'MessageCommentEvent',
  PayloadType.subscriberDeletedEvent: 'SubscriberDeletedEvent',
  PayloadType.unknown: 'RecallMessageEvent',
  PayloadType.smsMessage: 'SmsMessage',
  PayloadType.mmsMessage: 'MmsMessage',
  PayloadType.xmsMsgDeliveredEvent: 'XmsMsgDeliveredEvent',
  PayloadType.xmsMsgDeliveryFailedEvent: 'XmsMsgDeliveryFailedEvent',
  PayloadType.systemMessage: 'SystemMessage',
  PayloadType.xmsTypingIndicatorEvent: 'XmsTypingIndicatorEvent',
  PayloadType.botMessage: 'BotMessage',
  PayloadType.botMessageReply: 'BotMessageReply',
  PayloadType.rcsTextMessage: 'RcsTextMessage',
  PayloadType.rcsMediaMessage: 'RcsMediaMessage',
  PayloadType.rcsLocationMessage: 'RcsLocationMessage',
  PayloadType.rcsGroupTextMessage: 'RcsGroupTextMessage',
  PayloadType.rcsGroupMediaMessage: 'RcsGroupMediaMessage',
  PayloadType.rcsGroupLocationMessage: 'RcsGroupLocationMessage',
  PayloadType.rcsGroupTypingIndicatorEvent: 'RcsGroupTypingIndicatorEvent',
  PayloadType.rcsSentReceiptEvent: 'RcsSentReceiptEvent',
  PayloadType.rcsMsgDeliveredEvent: 'RcsMsgDeliveredEvent',
  PayloadType.rcsMsgReadEvent: 'RcsMsgReadEvent',
  PayloadType.rcsMsgDeliveryFailedEvent: 'RcsMsgDeliveryFailedEvent',
  PayloadType.rcsTypingIndicatorEvent: 'RcsTypingIndicatorEvent',
  PayloadType.rcsCreateGroupEvent: 'RcsCreateGroupEvent',
  PayloadType.rcsGroupUpdateEvent: 'RcsGroupUpdateEvent',
  PayloadType.rcsRemoveFromGroupEvent: 'RcsRemoveFromGroupEvent',
  PayloadType.rcsGroupMsgReadEvent: 'RcsGroupMsgReadEvent',
  PayloadType.rcsCapabilitiesEvent: 'RcsCapabilitiesEvent',
  PayloadType.rcsOpenConversationEvent: 'OpenConversationEvent',
};
