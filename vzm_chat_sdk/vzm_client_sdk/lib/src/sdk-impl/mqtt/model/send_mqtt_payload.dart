/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';

part 'send_mqtt_payload.g.dart';

@JsonSerializable()
class SendMqttPayload {

  @JsonKey(name: 'type')
  final PayloadType type;

  @JsonKey(name: 'object')
  final dynamic obj;

  SendMqttPayload(this.type, this.obj);

  factory SendMqttPayload.fromJson(Map<String, dynamic> json) =>
      _$SendMqttPayloadFromJson(json);

  Map<String, dynamic> toJson() => _$SendMqttPayloadToJson(this);

}
