// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_message_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendMessageData _$SendMessageDataFromJson(Map<String, dynamic> json) =>
    SendMessageData(
      id: json['id'] as String?,
      groupId: json['groupId'] as String?,
      clientMsgId: json['clientMsgId'] as String?,
      recipients: (json['recipients'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      recipientMdns: (json['recipientMdns'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      text: json['text'] as String?,
      timeOfLastMsgSeen: json['timeOfLastMsgSeen'] as int?,
      senderId: json['senderId'] as String?,
      senderMdn: json['senderMdn'] as String?,
      payload: (json['payload'] as List<dynamic>?)
          ?.map((e) => MediaItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      messageIds: (json['messageIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      toMdns:
          (json['toMdns'] as List<dynamic>?)?.map((e) => e as String).toList(),
      recipient: json['recipient'] as String?,
      replyParentMsgId: json['msgId'] as String?,
    );

Map<String, dynamic> _$SendMessageDataToJson(SendMessageData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'groupId': instance.groupId,
      'clientMsgId': instance.clientMsgId,
      'recipients': instance.recipients,
      'recipientMdns': instance.recipientMdns,
      'text': instance.text,
      'timeOfLastMsgSeen': instance.timeOfLastMsgSeen,
      'senderId': instance.senderId,
      'senderMdn': instance.senderMdn,
      'payload': instance.payload,
      'messageIds': instance.messageIds,
      'toMdns': instance.toMdns,
      'recipient': instance.recipient,
      'msgId': instance.replyParentMsgId,
    };
