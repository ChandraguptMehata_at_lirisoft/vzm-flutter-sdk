// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaItem _$MediaItemFromJson(Map<String, dynamic> json) => MediaItem(
      $enumDecode(_$MediaItemTypeEnumMap, json['type']),
      json['data'] as String,
    );

Map<String, dynamic> _$MediaItemToJson(MediaItem instance) => <String, dynamic>{
      'type': _$MediaItemTypeEnumMap[instance.mediaItemType],
      'data': instance.data,
    };

const _$MediaItemTypeEnumMap = {
  MediaItemType.image: 'IMAGE',
  MediaItemType.video: 'VIDEO',
  MediaItemType.location: 'LOCATION',
  MediaItemType.audio: 'AUDIO',
  MediaItemType.vcard: 'VCARD',
  MediaItemType.unknown: 'UNKNOWN',
};
