/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/media_item_type.dart';
part 'media_item.g.dart';

@JsonSerializable()
class MediaItem {

  @JsonKey(name: 'type')
  final MediaItemType mediaItemType;

  @JsonKey(name: 'data')
  final String data;

  MediaItem(this.mediaItemType, this.data);

  factory MediaItem.fromJson(Map<String, dynamic> json) =>
      _$MediaItemFromJson(json);

  Map<String, dynamic> toJson() => _$MediaItemToJson(this);

}
