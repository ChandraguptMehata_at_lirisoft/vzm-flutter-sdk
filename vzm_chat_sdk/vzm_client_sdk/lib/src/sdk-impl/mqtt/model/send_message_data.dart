/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/model/media_item.dart';

part 'send_message_data.g.dart';

@JsonSerializable()
class SendMessageData {
  @JsonKey(name: 'id')
  final String? id;

  @JsonKey(name: 'groupId')
  final String? groupId;

  @JsonKey(name: 'clientMsgId')
  final String? clientMsgId;

  @JsonKey(name: 'recipients')
  final List<String>? recipients;

  @JsonKey(name: 'recipientMdns')
  final List<String>? recipientMdns;

  @JsonKey(name: 'text')
  final String? text;

  @JsonKey(name: 'timeOfLastMsgSeen')
  final int? timeOfLastMsgSeen;

  @JsonKey(name: 'senderId')
  final String? senderId;

  @JsonKey(name: 'senderMdn')
  final String? senderMdn;

  @JsonKey(name: 'payload')
  List<MediaItem>? payload;

  @JsonKey(name: 'messageIds')
  final List<String>? messageIds;

  @JsonKey(name: 'toMdns')
  final List<String>? toMdns;

  @JsonKey(name: 'recipient')
  final String? recipient;

  @JsonKey(name: 'msgId')
  final String? replyParentMsgId;

  SendMessageData({this.id, this.groupId, this.clientMsgId, this.recipients, this.recipientMdns, this.text, this.timeOfLastMsgSeen,
    this.senderId, this.senderMdn, this.payload, this.messageIds, this.toMdns, this.recipient, this.replyParentMsgId});

  factory SendMessageData.fromJson(Map<String, dynamic> json) =>
      _$SendMessageDataFromJson(json);

  Map<String, dynamic> toJson() => _$SendMessageDataToJson(this);

}
