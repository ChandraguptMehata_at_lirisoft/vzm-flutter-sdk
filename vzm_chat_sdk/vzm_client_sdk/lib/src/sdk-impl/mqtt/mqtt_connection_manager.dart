/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/model/send_mqtt_payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_manager.dart';

class MqttConnectionManager {
  late MQTTManager _manager;
  late final AppLogger _logger;
  static const String _tag = "MQTT";

  MqttConnectionManager() {
    _logger = serviceLocator.get<AppLogger>();
  }

  void configureAndConnect() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.conn.configureAndConnect");
    }
    _manager = MQTTManager();
    _manager.initializeMQTTClient();
    _manager.connect();
  }

  void _disconnect() {
    _manager.disconnect();
  }

  void notifyMqttManager(bool isInBackground) {
    _manager.notifyMqttManager(isInBackground);
  }

  Future<bool> send(SendMqttPayload payload, int id) async {
    return await _manager.send(payload, id);
  }
}
