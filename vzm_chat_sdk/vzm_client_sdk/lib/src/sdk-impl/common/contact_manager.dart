/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/api/repositories/contact_irepository.dart';
import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';

import '../../../objectbox.g.dart';

class ContactManager extends IContactRepository {

  late final EntityConverter _entityConverter;
  late Box<ContactEntity> contactBox;
  late final AppLogger _logger;

  ContactManager(this.contactBox):
        _entityConverter = serviceLocator.get<EntityConverter>(),
        _logger = serviceLocator.get<AppLogger>(),
      super(contactBox);

  Contact addContact(Contact contact) {
    contact.id = contactBox.put(_entityConverter.toContactEntity(contact));
    if (AppLogger.isDebugEnabled) {
      _logger.debug("ContactManager.${contact.address}:", "addContact -> saved profile = $contact");
    }
    return contact;
  }

  @override
  Future<List<ContactEntity>> findWhereByPage(where, int offset, int limit) {
    // TODO: implement findWhereByPage
    throw UnimplementedError();
  }

  @override
  Future<bool> deleteById(Contact contact) async{
    bool result = contactBox.remove(contact.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("#repo.contact.up deleteById", "deleted $result");
    }
    return result;
  }

  @override
  Future<Contact?> updateById(Contact contact) async{
    if (AppLogger.isDebugEnabled) {
      _logger.debug("#repo.contact.up updateById", "contact $contact");
    }
    contact.id = contactBox.put(_entityConverter.toContactEntity(contact));
    return contact;
  }

  Contact? getContact(String address) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("#repo.contact.up getContact", "address $address");
    }
    var query = contactBox.query(ContactEntity_.address.equals(address)).build();
    ContactEntity? contactEntity = query.findFirst();
    if (contactEntity == null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("#repo.contact.up getContact", "contactEntity is null");
      }
      return null;
    }
    return _entityConverter.toContact(contactEntity);
  }

}