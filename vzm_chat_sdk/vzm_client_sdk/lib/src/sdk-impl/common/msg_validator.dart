/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/mime_type_media.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';


class MsgValidator {
  late final AppLogger _logger;
  late final OttConfigurationManager _ottConfigurationManager;

  static const String _tag = "MSG-VALIDATOR";

  static const double mmsSizeLimit = 1258291; //5242880; // 1258291;  // 1.2MB is max size of MMS
  static const double mmsVideoSizeLimit = 5242880;

  Set<String> mimeTypesText = {textPlain, textHtml, textVCalender, textVCard};

  Set<String> mimeTypesImage = {imageUnspecified, imageJpeg, imageJpg, imageGIF, imageWbmp, imageBmp, imageSVG, imagePng};

  Set<String> mimeTypesAudio = {
    audioUnspecified,
    audioAac,
    audioXAac,
    audioAacAdts,
    audioXMP3,
    audioXMid,
    audioXMpeg,
    audioXMpeg3,
    audioXMpg,
    audioWav,
    audioXWav,
    audioXMsWma,
    audioMP3,
    audioMP4,
    audioMp4a,
    audioAmr,
    audioWebAmr,
    audioMPEG,
    audioMPEG3,
    audioMPG,
    audioOGG,
    audioOGGCON
  };

  Set<String> mimeTypesVideo = {
    videoUnspecified,
    video3GP,
    video3GPP,
    video3G2,
    videoMP4,
    videoMP4ES,
    videoMPEG,
    videoQuickTime,
    videoAVI,
    videoInterLeave
  };

  MsgValidator() {
    _logger = serviceLocator.get<AppLogger>();
    _ottConfigurationManager = serviceLocator.get<OttConfigurationManager>();
  }

  bool isValidMimeType(String mimeType) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.send.msg: isValidMimeType -> mimeType = $mimeType");
    }
    return (mimeTypesText.contains(mimeType) ||
        mimeTypesImage.contains(mimeType) ||
        mimeTypesAudio.contains(mimeType) ||
        mimeTypesVideo.contains(mimeType));
  }

  bool isValidFileSize(MessageType type, String mimeType, int size) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.send.msg: isValidFileSize -> type = $type, " + "mimeType = $mimeType, size = $size");
    }
    switch (type) {
      case MessageType.mms:
        return isValidMMSSize(mimeType, size);
      case MessageType.mediaMessage:
        return isValidOttMediaSize(mimeType, size);

      default:
        return false;
    }
  }

  bool isValidMMSSize(String mimeType, int size) {
    if (mimeType.startsWith("video/")) {
      return size <= mmsVideoSizeLimit;
    } else {
      return size <= mmsSizeLimit;
    }
  }

  bool isValidOttMediaSize(String mimeType, int size) {
    var imageSizeLimit = _ottConfigurationManager.getImageSizeLimit();
    var fileSizeLimit = _ottConfigurationManager.getFileSizeLimit();
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          _tag,
          "#sdk.send.msg: isValidOttMediaSize -> " +
              "mimeType = $mimeType, size = $size, imageSizeLimit = $imageSizeLimit, " +
              "fileSizeLimit = $fileSizeLimit");
    }

    if (mimeType.startsWith("image/")) {
      return size <= imageSizeLimit;
    } else {
      return size <= fileSizeLimit;
    }
  }
}
