/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';

abstract class IMessageManager {
  Future<bool> handleSentReceiptEvent(Payload data);

  Future<bool> handleMediaMessage(Payload data);

  Future<bool> handleDeleteConversationEvent(Payload data);

  Future<bool> handleReadConversationEvent(Payload data);

  Future<bool> handleRemoveFromGroupEvent(Payload data);

  Future<bool> handleGroupUpdateEvent(Payload data);

  Future<bool> handleCreateGroupEvent(Payload data);

  Future<bool> handleTextMessage(Payload data);

  Future<bool> handleMsgDeliveredEvent(Payload data);

  Future<bool> handleMsgDeliveryFailedEvent(Payload data);

  Future<bool> handleMsgDeletedEvent(Payload data);

  Future<bool> handleSubscriberUpdateEvent(Payload data);

  Future<bool> handleMessageCommentEvent(Payload data);

  Future<bool> handleSubscriberDeletedEvent(Payload data);

  Future<bool> handleSmsMessage(Payload data);

  Future<bool> handleMmsMessage(Payload data);

  Future<bool> handleXmsMsgDeliveredEvent(Payload data);

  Future<bool> handleXmsMsgDeliveryFailedEvent(Payload data);

  Future<bool> handleSystemMessage(Payload data);

  Future<bool> handleBotMessage(Payload data);

  Future<bool> handleBotMessageReply(Payload data);

  Future<bool> handleRcsTextMessage(Payload data);

  Future<bool> handleRcsMediaMessage(Payload data);

  Future<bool> handleRcsLocationMessage(Payload data);

  Future<bool> handleRcsGroupTextMessage(Payload data);

  Future<bool> handleRcsGroupMediaMessage(Payload data);

  Future<bool> handleRcsGroupLocationMessage(Payload data);

  Future<bool> handleRcsGroupTypingIndicatorEvent(Payload data);

  Future<bool> handleRcsSentReceiptEvent(Payload data);

  Future<bool> handleRcsMsgDeliveredEvent(Payload data);

  Future<bool> handleRcsMsgReadEvent(Payload data);

  Future<bool> handleRcsMsgDeliveryFailedEvent(Payload data);

  Future<bool> handleRcsTypingIndicatorEvent(Payload data);

  Future<bool> handleRcsCreateGroupEvent(Payload data);

  Future<bool> handleRcsGroupUpdateEvent(Payload data);

  Future<bool> handleRcsRemoveFromGroupEvent(Payload data);

  Future<bool> handleRcsGroupMsgReadEvent(Payload data);

  Future<bool> handleRcsCapabilitiesEvent(Payload data);

  Future<bool> handleRcsOpenConversationEvent(Payload data);
}
