/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:collection/collection.dart';
import 'package:vzm_client_sdk/objectbox.g.dart';
import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/enroll_device_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/contact_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/profiles.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/models.dart';

class ProfileRepositoryImpl {
  final _cachedProfiles = <ProfileEntity>[];
  late final DatabaseManager _database = serviceLocator.get<DatabaseManager>();
  late AppLogger _logger;
  late AppCredential _appCredential;
  late final EntityConverter _entityConverter;
  static const String _tag = "PROFILE";

  ProfileRepositoryImpl() {
    _logger = serviceLocator.get<AppLogger>();
    _appCredential = serviceLocator.get<AppCredential>();
    _entityConverter = serviceLocator.get<EntityConverter>();
  }

  List<String> getMissingProfile(MessageByGroupResponse res) {
    return List.empty(growable: false);
  }

  List<Contact> addOrUpdateProfile(List<PublicProfiles> res) {
    return List.empty(growable: false);
  }

  ProfileEntity? getProfile(String mdnOrId) {
    ProfileEntity? profileEntity =
        _cachedProfiles.firstWhereOrNull((element) => (element.profileId == mdnOrId || element.address == mdnOrId));
    if (null == profileEntity) {
      var profileBox = _database.getProfileBox();
      profileEntity =
          profileBox.query(ProfileEntity_.address.equals(mdnOrId).or(ProfileEntity_.profileId.equals(mdnOrId))).build().findFirst();
      if (profileEntity != null) {
        _cachedProfiles.add(profileEntity);
      }
    }
    return profileEntity;
  }

  bool isExists(String? senderId) {
    var profileBox = _database.getProfileBox();
    if (senderId == null) {
      throw Exception("Profile not found exception");
    }
    var query = profileBox.query(ProfileEntity_.address.equals(senderId).or(ProfileEntity_.profileId.equals(senderId))).build();
    return query.count() > 0;
  }

  ///  Add or update the profiles and contacts database
  Future<void> save(List<ProfileEntity> profiles) async {
    var profileBox = _database.getProfileBox();
    for (ProfileEntity profileEntity in profiles) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("#restore.${profileEntity.id}:", "save profile address ${profileEntity.address}");
      }
      if (profileEntity.profileId != null) {
        var dbProfile = getProfile(profileEntity.profileId!);
        if (dbProfile != null) {
          if (profileEntity.updatedTime! > dbProfile.updatedTime!) {
            profileEntity.id = dbProfile.id;
            profileBox.put(profileEntity);
            // Update the contact repository of the app layer
            await saveContact(profileEntity);
          }
        } else {
          profileBox.put(profileEntity);
          await saveContact(profileEntity);
        }
      }
    }
    _cachedProfiles.clear(); //clear the cache
  }

  Future<void> saveContact(ProfileEntity profileEntity) async {
    ContactManager contactManager = serviceLocator.get<ContactManager>();
    var localContact = contactManager.getContact(profileEntity.address);
    if (localContact == null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("#restore.${profileEntity.id}:", "saveContact >> adding the contact");
      }
      Contact contact = Contact(profileEntity.address);
      contact.name = profileEntity.name;
      contact.avatarUrl = profileEntity.avatarUrl;
      contactManager.addContact(contact);
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("#restore.${profileEntity.id}:", "saveContact >> updating the contact");
      }
      localContact.name = profileEntity.name;
      localContact.avatarUrl = profileEntity.avatarUrl;
      contactManager.updateById(localContact);
    }
  }

  ProfileEntity? getLocalProfile() {
    var subscriberId = _appCredential.getSubscriberId();
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#repo.profile.$subscriberId: getLocalProfile -> ");
    }
    var profileEntity = _cachedProfiles.firstWhereOrNull((element) => element.profileId == subscriberId);
    if (null == profileEntity) {
      var profileBox = _database.getProfileBox();
      profileEntity = profileBox.query((ProfileEntity_.profileId.equals(subscriberId!))).build().findFirst();
      if (profileEntity != null) {
        _cachedProfiles.add(profileEntity);
      }
    }
    return profileEntity;
  }

  void addProfile(EnrollDeviceResponse response) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#repo.profile.${response.id}: addProfile -> response = ${response.toString()}");
    }
    var entity = ProfileEntity(response.mdn, response.name, response.id, response.avatar, response.createdTime, response.updatedTime);
    var id = _database.getProfileBox().put(entity);
    Contact contact = _entityConverter.toContactFromProfileEntity(entity);
    serviceLocator.get<ContactManager>().addContact(contact);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#repo.profile.${response.id}: addProfile -> saved profile id = $id");
    }
  }
}
