/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/profile_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/file_download_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/file_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/group.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/ott_profiles.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload_object.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/profiles.dart';

import 'conversation_manager.dart';
import 'message_repository_impl.dart';

class PersistenceManager {
  late MessageRepositoryImpl _mManager;
  late ConversationManager _conversationManager;
  late AppLogger _logger;
  late EntityConverter _entityConverter;
  late ProfileRepositoryImpl _profileManager;
  late AppCredential _appCredential;
  late FileManager _fileManager;
  late FileDownloadManager _fileDownloadManager;

  PersistenceManager() {
    _mManager = serviceLocator.get<MessageRepositoryImpl>();
    _logger = serviceLocator.get<AppLogger>();
    _entityConverter = serviceLocator.get<EntityConverter>();
    _profileManager = serviceLocator.get<ProfileRepositoryImpl>();
    _conversationManager = serviceLocator.get<ConversationManager>();
    _appCredential = serviceLocator.get<AppCredential>();
    _fileManager = serviceLocator.get<FileManager>();
    _fileDownloadManager = serviceLocator.get<FileDownloadManager>();
  }

  Conversation getOrCreateConversation(Payload payload) {
    return _conversationManager.getOrCreateConversation(payload, false);
  }

  Future<Conversation> createTelephonyConversation(Payload payload, bool restored) async {
    var profiles = findProfilesForMessage(payload.object);
    var conversation = _entityConverter.toTelConversation(payload, profiles);
    return await _conversationManager.addConversation(conversation);
  }

  Future<bool> handleMessagePayload(Conversation conv, Payload payload) async {
    var msg = _mManager.findByServerId(payload.object.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("handleMessagePayload", "#db.msg.${payload.object.id}: Lookup for message $msg");
    }
    if (msg == null) {
      var profiles = findProfilesForMessage(payload.object);
      var insert = _entityConverter.toMessageFromPayload(payload, conv, profiles);
      if (insert.attachments != null) {
         await _saveThumbnailData(insert.attachments!);
      }

      var add = await _mManager.addMessage(insert);

      if (add.attachments != null) {
         _startAttachmentDownload(add.attachments!, add.id);
      }
      //Update Conversation
      int? updatedConversationTime = await _conversationManager.getConversationUpdatedTime(conv.id);
      int? payloadCreationTime = payload.object.createdTime;
      if (AppLogger.isDebugEnabled) {
        _logger.debug("handleMessagePayload", "#db.msg.${payload.object.id}: payloadCreationTime $payloadCreationTime, updatedConversationTime: $updatedConversationTime");
      }
      if (payloadCreationTime == null || updatedConversationTime == null || (payloadCreationTime >= updatedConversationTime)) {
        Conversation? updatedConversation = await _conversationManager.updateById(getUpdatedConversation(conv.id, add));
        if (AppLogger.isDebugEnabled) {
          _logger.debug("handleMessagePayload", "#db.msg.${payload.object.id}: updatedConversation ${updatedConversation.toString()}");
        }
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug("handleMessagePayload", "#db.msg.${payload.object.id}: not updating conversation");
        }
      }
      return true;
    }
    return false;
  }

  Message toMessage(Payload payload, List<ProfileEntity> contacts, Conversation conversation) {
    List<String>? recipients = <String>[];
    List<String>? srcRecipients = payload.object.recipients;
    if (srcRecipients != null) {
      recipients.addAll(srcRecipients);
    } else {
      //SMS/MMS
      recipients.addAll(payload.object.recipientMdns!);
    }
    var msg = Message(
        0,
        payload.object.createdTime,
        toMessageType(payload.type),
        MessageStatus.sending,
        //todo: need to provide dynamically
        1,
        toContact(payload.object.getSenderIdOrMdn(), contacts),
        toContacts(recipients, contacts),
        conversation.id);
    return msg;
  }

  List<ProfileEntity> findProfilesForMessage(PayloadObject src) {
    var ids = List.of([src.getSenderIdOrMdn()]);
    if (src.recipients != null) {
      ids.addAll(src.recipients as List<String>);
    } else {
      ids.addAll(src.recipientMdns!);
    }
    return getProfiles(ids);
  }

  MessageType toMessageType(type) {
    if (type == PayloadType.smsMessage) {
      return MessageType.sms;
    }
    if (type == PayloadType.mmsMessage) {
      return MessageType.mms;
    }
    if (type == PayloadType.textMessage) {
      return MessageType.textMessage;
    }
    if (type == PayloadType.mediaMessage) {
      return MessageType.mediaMessage;
    }
    if (type == PayloadType.rcsTextMessage) {
      return MessageType.groupEventMessage;
    }
    if (type == PayloadType.smsMessage) {
      return MessageType.groupEventMessage;
    }
    if (type == PayloadType.smsMessage) {
      return MessageType.groupEventMessage;
    }
    if (type == PayloadType.smsMessage) {
      return MessageType.groupEventMessage;
    }
    return MessageType.groupEventMessage;
  }

  Contact toContact(String? senderIdOrMdn, List<ProfileEntity> profiles) {
    if (senderIdOrMdn == null) {
      throw Exception("contact is null");
    }
    Contact? contact;
    for (ProfileEntity profileEntity in profiles) {
      if (profileEntity.profileId == senderIdOrMdn || profileEntity.address == senderIdOrMdn) {
        contact = Contact(profileEntity.address);
        contact.name = profileEntity.name;
        contact.avatarUrl = profileEntity.avatarUrl;
      }
    }
    if (contact == null) {
      throw Exception("contact is null");
    }
    return contact;
  }

  List<Contact> toContacts(List<String> src, List<ProfileEntity> profiles) {
    List<Contact> contactList = List.empty();
    for (String id in src) {
      for (ProfileEntity profileEntity in profiles) {
        if (profileEntity.profileId == id || profileEntity.address == id) {
          Contact contact = Contact(profileEntity.address);
          contact.name = profileEntity.name;
          contact.avatarUrl = profileEntity.avatarUrl;
          contactList.add(contact);
        }
      }
    }
    return contactList;
  }

  List<ProfileEntity> getProfiles(List<String?> ids) {
    List<ProfileEntity> profiles = <ProfileEntity>[];
    for (int i = 0; i < ids.length; i++) {
      String? id = ids[i];
      if (id != null) {
        var profile = _profileManager.getProfile(id);
        if (profile != null) {
          profiles.add(profile);
        } else {
          if (AppLogger.isDebugEnabled) {
            _logger.error("getProfiles error, ", "#sync.profile.$id: Missing profiles.", Exception(""));
          }
          throw Exception("profile not found exception");
        }
      }
    }
    return profiles;
  }

  /// Find the missing profiles from local database
  List<String> getMissingProfile(PayloadObject payload) {
    var ids = <String>[];

    //Sender
    var senderId = payload.getSenderIdOrMdn();
    if (!_profileManager.isExists(senderId)) {
      ids.add(senderId!);
    }

    //ToAddress
    // TextMessage,MediaMessage
    var recipients = payload.recipients;
    if (recipients != null) {
      for (String recipient in recipients) {
        if (!_profileManager.isExists(recipient)) {
          ids.add(recipient);
        }
      }
    }

    // FOR SmsMessage, MmsMessage
    var recipientMdns = payload.recipientMdns;
    if (recipientMdns != null) {
      for (String mdn in recipientMdns) {
        if (!_profileManager.isExists(mdn)) {
          ids.add(mdn);
        }
      }
    }

    return ids;
  }

  Future<void> addOrUpdateProfile(List<PublicProfiles> src) async {
    var profiles = _entityConverter.toProfilesEntity(src);
    await _profileManager.save(profiles);
  }

  Future<void> addOrUpdateOttProfile(List<OttProfiles> src) async {
    var profiles = _entityConverter.toProfilesEntityFromOttProfile(src);
    await _profileManager.save(profiles);
  }

  Future<Conversation> createGroupChat(GroupChat? groupChat, bool restored) async {
    if (groupChat == null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("getOrCreateGroupChat", "#db.conv.add.${groupChat!.id}: group chat is null");
      }
      throw Exception("Group chat is null. Can not create conversation");
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug("getOrCreateGroupChat", "#db.conv.add.${groupChat.id}: creating conversation");
    }
    var thread = _conversationManager.findConversationByServerId(groupChat.id);
    if (thread == null) {
      var ids = <String>[];
      if (groupChat.members != null) {
        ids.addAll(groupChat.members!);
      }
      if (groupChat.adminIds != null) {
        ids.addAll(groupChat.adminIds!);
      }
      if (!ids.contains(groupChat.creatorId)) {
        ids.add(groupChat.creatorId!);
      }
      var profiles = getProfiles(ids);
      Conversation conversation = _entityConverter.toGroupConversation(groupChat, profiles);
      conversation = await _conversationManager.addConversation(conversation);

      if (!_entityConverter.isOneToOneChatId(groupChat.id)) {
        // Add event messages
        await _mManager.addCreateGroupEventMsg(conversation.id, groupChat.id, groupChat);
        var addedMembers = <String>[];
        for (String member in groupChat.members!) {
          if (member != groupChat.creatorId) {
            addedMembers.add(member);
          }
        }
        await _mManager.addGroupMembersAddedMsg(conversation.id, groupChat.creatorId!, groupChat.id, addedMembers, groupChat.createdTime! + 1000);

        if (AppLogger.isDebugEnabled) {
          _logger.debug("getOrCreateGroupChat", "#db.conv.add.${groupChat.id}: Added message ${conversation.id}");
        }
      }

      return conversation;
    } else {
      // todo: Need to handle the existing conversation in left state
      // REJOIN Flow for OTT Group -> Need to check the flow Create/Update event
      if (thread.disabled == true) {
        var entity = _conversationManager.findConversationByServerId(groupChat.id);
        if (entity != null) {
          if (entity.updatedTime < groupChat.updatedTime) {
            Conversation conversation = Conversation();
            conversation.id = thread.id;
            conversation.disabled = false;
            conversation.updatedTime = groupChat.updatedTime;
            thread = await _conversationManager.updateById(conversation);
            if (thread == null) {
              throw Exception("No conversation found for disable thread ${conversation.id}");
            }
          }
        } else {
          if (AppLogger.isDebugEnabled) {
            _logger.debug("getOrCreateGroupChat", "#db.conv.add.${groupChat.id}: disabled conversation not found");
          }
        }
      }
    }
    return thread;
  }

  Conversation getUpdatedConversation(int id, Message add) {
    Conversation conversation = Conversation();
    conversation.id = id;
    conversation.snippet = add.body;
    conversation.time = add.time;
    return conversation;
  }

  Future<Conversation> handleCreateGroupEvent(Payload data) async {
    GroupChat group = data.object.group!;
    // Save the profiles
    await addOrUpdateProfile(group.profiles!);
    //Save the conversation
    Conversation conversation = await createGroupChat(group, false);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("handleCreateGroupEvent", "#db.conv.add.${conversation.id}: ");
    }
    return conversation;
  }

  Future<bool> handleGroupUpdateEvent(Payload data) async {
    GroupChat group = data.object.group!;
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "handleGroupUpdateEvent",
          "#db.conv.update.${group.id}: SyncPayload = $data, group = $group, " +
              "changedFields = ${data.object.changedFields}, recipients = ${data.object.recipients}, " +
              "recipientMdns = ${data.object.recipientMdns}");
    }

    var thread = await getOrCreateGroupConversation(group);

    var payloadObject = data.object;
    for (int changeField in payloadObject.changedFields!) {
      switch (changeField) {
        case PayloadObject.FIELD_GROUP_NAME:
          await updateGroupName(thread!.id, payloadObject.group!, payloadObject.getSenderIdOrMdn(), payloadObject.id);
          break;

        case PayloadObject.FIELD_GROUP_AVATAR:
          await updateGroupAvatar(thread!.id, payloadObject.group!, payloadObject.getSenderIdOrMdn(), payloadObject.id);
          break;

        case PayloadObject.FIELD_GROUP_BACKGROUND:
          await updateGroupBackground(thread!.id, payloadObject.group!, payloadObject.getSenderIdOrMdn(), payloadObject.id);
          break;

        case PayloadObject.FIELD_GROUP_PARTICIPANTS:
          await updateGroupParticipants(thread!.id, payloadObject.group!, payloadObject.getSenderIdOrMdn(), payloadObject.id);
          break;

        case PayloadObject.FIELD_GROUP_ADMIN:
          await updateGroupAdmins(thread!.id, payloadObject.group!, payloadObject.getSenderIdOrMdn(), payloadObject.id);
          break;
      }
    }

    return true;
  }

  Future<Conversation?> getOrCreateGroupConversation(GroupChat groupChat) async {
    var thread = _conversationManager.findConversationByServerId(groupChat.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("getOrCreateGroupConversation", "#db.conv.update.${groupChat.id}: existing conversation = $thread");
    }
    if (thread == null) {
      var ids = <String>[];
      if (groupChat.members != null) {
        ids.addAll(groupChat.members!);
      }
      if (groupChat.adminIds != null) {
        ids.addAll(groupChat.adminIds!);
      }
      var profiles = getProfiles(ids);
      Conversation conversation = _entityConverter.toGroupConversation(groupChat, profiles);
      thread = await _conversationManager.addConversation(conversation);

      // Add event messages
      await _mManager.addCreateGroupEventMsg(thread.id, groupChat.id, groupChat);
      await _mManager.addGroupMembersAddedMsg(thread.id, groupChat.creatorId!, groupChat.id, groupChat.members!,
          groupChat.createdTime! + 1000 //Differentiating the event msg time from create group time
          );

      if (AppLogger.isDebugEnabled) {
        _logger.debug("getOrCreateGroupConversation", "#db.conv.update.${groupChat.id}: Added message ${thread.id}");
      }
    }
    return thread;
  }

  Future<void> updateGroupName(int convId, GroupChat group, String? senderId, String eventId) async {
    var entity = await _conversationManager.findConversationByServerId(group.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "updateGroupName",
          "#db.conv.update.${group.id}: updateGroupName -> convId = $convId, " +
              "group = $group, senderId = $senderId, eventId = $eventId" +
              "and found entity = $entity");
      if (entity == null) {
        throw Exception("Group not found");
      }

      if (entity.updatedTime != null && entity.updatedTime! < group.updatedTime!) {
        Conversation update = Conversation();
        update.id = entity.id;
        update.groupName = group.name;
        update.updatedTime = group.updatedTime;

        var thread = await _conversationManager.updateById(update);
        _mManager.addGroupNameChangedMsg(convId, senderId!, eventId, group.name!, group.updatedTime!);
      } else {
        //Add the missing message
        var msg = _mManager.findByServerId(eventId);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(
              "updateGroupName",
              "#db.conv.update.${group.id}: updateGroupName -> Ignoring the changes since " +
                  "local group update time is greater than remote update time. " +
                  "Event message = $msg");
        }
        if (msg == null) {
          _mManager.addGroupNameChangedMsg(convId, senderId!, eventId, group.name!, group.updatedTime!);
        }
      }
    }
  }

  Future<void> updateGroupAvatar(int convId, GroupChat groupChat, String? senderId, String eventId) async {
    var entity = _conversationManager.findConversationByServerId(groupChat.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "updateGroupAvatar",
          "#db.conv.update.${groupChat.id}: updateGroupAvatar -> " +
              "convId = $convId, group = $groupChat, senderId = $senderId, eventId = $eventId," +
              " found entity = $entity");
    }
    if (entity == null) {
      throw Exception("Group not found");
    }
    if (entity.updatedTime! < groupChat.updatedTime!) {
      if (groupChat.avatar != null) {
        //todo: perform download task
      } else {
        entity.groupAvatarUrl = groupChat.avatar;

        //Update app level conversation repo
        Conversation conversation = Conversation();
        conversation.id = convId;
        conversation.groupAvatarUrl = "";
        conversation.updatedTime = groupChat.updatedTime;
        await _conversationManager.updateById(conversation);
      }

      _mManager.addGroupAvatarChangedMsg(convId, senderId!, eventId, groupChat.updatedTime!);
    } else {
      //Add the missing message
      var msg = _mManager.findByServerId(eventId);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            "updateGroupAvatar",
            "#db.conv.update.${groupChat.id}: updateGroupAvatar -> Ignoring the changes since " +
                "local group update time is greater than remote update time." +
                "Event message = $msg");
      }

      if (msg == null) {
        _mManager.addGroupAvatarChangedMsg(convId, senderId!, eventId, groupChat.updatedTime!);
      }
    }
  }

  Future<void> updateGroupBackground(int convId, GroupChat groupChat, String? senderIdOrMdn, String eventId) async {
    var entity = _conversationManager.findConversationByServerId(groupChat.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "updateGroupAvatar",
          "#db.conv.update.${groupChat.id}: updateGroupAvatar -> " +
              "convId = $convId, group = $groupChat, senderId = $senderIdOrMdn, eventId = $eventId," +
              " found entity = $entity");
    }
    if (entity == null) {
      throw Exception("Group not found");
    }
    if (entity.updatedTime! < groupChat.updatedTime!) {
      if (groupChat.background != null) {
        //todo: perform download task
      } else {
        entity.groupBackgroundUrl = groupChat.background;

        //Update app level conversation repo
        Conversation conversation = Conversation();
        conversation.id = convId;
        conversation.groupBackgroundUrl = "";
        conversation.updatedTime = groupChat.updatedTime;
        await _conversationManager.updateById(conversation);
      }

      _mManager.addGroupBackgroundChangedMsg(convId, senderIdOrMdn!, eventId, groupChat.updatedTime!);
    } else {
      //Add the missing message
      var msg = _mManager.findByServerId(eventId);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            "updateGroupAvatar",
            "#db.conv.update.${groupChat.id}: updateGroupAvatar -> Ignoring the changes since " +
                "local group update time is greater than remote update time." +
                "Event message = $msg");
      }

      if (msg == null) {
        _mManager.addGroupBackgroundChangedMsg(convId, senderIdOrMdn!, eventId, groupChat.updatedTime!);
      }
    }
  }

  Future<void> updateGroupParticipants(int convId, GroupChat groupChat, String? senderIdOrMdn, String eventId) async {
    var entity = _conversationManager.findConversationByServerId(groupChat.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "updateGroupParticipants",
          "#db.conv.update.${groupChat.id}: updateGroupParticipants -> " +
              "convId = $convId, group = $groupChat, senderId = $senderIdOrMdn, eventId = $eventId," +
              " found entity = $entity");
    }
    if (entity == null) {
      throw Exception("Group not found");
    }
    if (entity.updatedTime < groupChat.updatedTime) {
      if (groupChat.membersAdded != null && groupChat.membersAdded!.isNotEmpty) {
        var addedProfiles = groupChat.profiles!.where((element) {
          var id = element.profile != null ? element.profile!.id : element.id;
          return groupChat.membersAdded!.contains(id);
        }).toList();

        //Save newly added profiles to SDK Profile repository
        addOrUpdateProfile(addedProfiles as List<PublicProfiles>);

        _mManager.addGroupMembersAddedMsg(convId, senderIdOrMdn!, eventId, groupChat.membersAdded!, groupChat.updatedTime!);
        Conversation conversation = Conversation();
        conversation.id = convId;
        conversation.updatedTime = groupChat.updatedTime;

        var profiles = getProfiles(groupChat.members!);
        conversation.groupMembers = _entityConverter.toContactsFromAddress(groupChat.members!, profiles);

        if (groupChat.membersAdded!.contains(_appCredential.getSubscriberId())) {
          conversation.disabled = false;
        }

        var thread = await _conversationManager.updateById(conversation);

        if (AppLogger.isDebugEnabled) {
          _logger.debug("updateGroupParticipants",
              "#db.conv.update.${groupChat.id}: updateGroupParticipants -> " + "updated conversation with added members = $thread");
        }
      }

      if (groupChat.membersRemoved != null && groupChat.membersRemoved!.isNotEmpty) {
        Conversation conversation = Conversation();
        conversation.id = convId;
        conversation.updatedTime = groupChat.updatedTime;

        var profiles = getProfiles(groupChat.members!);
        conversation.groupMembers = _entityConverter.toContactsFromAddress(groupChat.members!, profiles);

        if (groupChat.membersRemoved!.contains(_appCredential.getSubscriberId())) {
          conversation.disabled = false;
        }

        var thread = await _conversationManager.updateById(conversation);

        if (AppLogger.isDebugEnabled) {
          _logger.debug("updateGroupParticipants",
              "#db.conv.update.${groupChat.id}: updateGroupParticipants -> " + "updated conversation with removed members = $thread");
        }

        if (groupChat.membersRemoved!.length == 1 && groupChat.membersRemoved!.contains(senderIdOrMdn)) {
          await _mManager.addGroupMemberLeftMsg(convId, senderIdOrMdn!, eventId, groupChat.updatedTime!);
        } else {
          await _mManager.addGroupMembersRemovedMsg(convId, senderIdOrMdn!, eventId, groupChat.membersRemoved!, groupChat.updatedTime!);
        }
      }
    } else {
      //Add the missing message
      var msg = _mManager.findByServerId(eventId);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            "updateGroupParticipants",
            "#db.conv.update.${groupChat.id}: updateGroupParticipants -> Ignoring the changes since " +
                "local group update time is greater than remote update time. " +
                "Event message = $msg");
      }

      if (msg == null) {
        if (groupChat.membersAdded!.isNotEmpty) {
          await _mManager.addGroupMembersAddedMsg(convId, senderIdOrMdn!, eventId, groupChat.membersAdded!, groupChat.updatedTime!);
        } else {
          await _mManager.addGroupMembersRemovedMsg(convId, senderIdOrMdn!, eventId, groupChat.membersRemoved!, groupChat.updatedTime!);
        }
      }
    }
  }

  Future<void> updateGroupAdmins(int convId, GroupChat groupChat, String? senderIdOrMdn, String eventId) async {
    var entity = _conversationManager.findConversationByServerId(groupChat.id);

    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "updateGroupAdmins",
          "#db.conv.update.${groupChat.id}: updateGroupAdmins -> " +
              "convId = $convId, group = $groupChat, senderId = $senderIdOrMdn," +
              " eventId = $eventId, adminIds = ${groupChat.adminIds}, " +
              "promotedAdminIds = ${groupChat.promotedAdminIds}," +
              " demotedAdminIds = ${groupChat.demotedAdminIds} found entity = $entity");
    }

    if (entity == null) {
      throw Exception("Group not found");
    }
    if (entity.updatedTime < groupChat.updatedTime) {
      if (groupChat.promotedAdminIds != null && groupChat.promotedAdminIds!.isNotEmpty) {
        Conversation conversation = Conversation();
        conversation.id = convId;
        conversation.updatedTime = groupChat.updatedTime;

        var profiles = getProfiles(groupChat.adminIds!);
        conversation.groupAdmins = _entityConverter.toContactsFromAddress(groupChat.adminIds!, profiles);

        var thread = _conversationManager.updateById(conversation);

        if (AppLogger.isDebugEnabled) {
          _logger.debug("updateGroupAdmins",
              "#db.conv.update.${groupChat.id}: updateGroupAdmins -> " + "updated conversation with promoted admins = $thread");
        }

        await _mManager.addPromotedAdminsMsg(convId, senderIdOrMdn!, eventId, groupChat.promotedAdminIds!, groupChat.updatedTime!);
      }

      if (groupChat.demotedAdminIds != null && groupChat.demotedAdminIds!.isNotEmpty) {
        Conversation conversation = Conversation();
        conversation.id = convId;
        conversation.updatedTime = groupChat.updatedTime;

        if (groupChat.adminIds != null) {
          var profile = getProfiles(groupChat.adminIds!);
          conversation.groupAdmins = _entityConverter.toContactsFromAddress(groupChat.adminIds!, profile);
        }

        var thread = _conversationManager.updateById(conversation);
        if (AppLogger.isDebugEnabled) {
          _logger.debug("updateGroupAdmins",
              "#db.conv.update.${groupChat.id}: updateGroupAdmins -> " + "updated conversation with demoted admins = $thread");
        }
        await _mManager.addDemotedAdminsMsg(convId, senderIdOrMdn!, eventId, groupChat.demotedAdminIds!, groupChat.updatedTime!);
      } else {
        //Add the missing message
        var msg = _mManager.findByServerId(eventId);

        if (AppLogger.isDebugEnabled) {
          _logger.debug(
              "updateGroupAdmins",
              "#db.conv.update.${groupChat.id}: updateGroupAdmins -> Ignoring the changes since " +
                  "local group update time is greater than remote update time." +
                  " Event message = $msg");
        }
        if (msg == null) {
          if (groupChat.promotedAdminIds != null && groupChat.promotedAdminIds!.isNotEmpty) {
            await _mManager.addPromotedAdminsMsg(convId, senderIdOrMdn!, eventId, groupChat.promotedAdminIds!, groupChat.updatedTime!);
          }
          if (groupChat.demotedAdminIds != null && groupChat.demotedAdminIds!.isNotEmpty) {
            await _mManager.addPromotedAdminsMsg(convId, senderIdOrMdn!, eventId, groupChat.demotedAdminIds!, groupChat.updatedTime!);
          }
        }
      }
    }
  }

  Future<bool> handleRemoveFromGroupEvent(Payload payload) async {
    var groupId = payload.object.groupId!;
    var senderId = payload.object.senderId!;
    var eventId = payload.object.id;

    var thread = _conversationManager.findConversationByServerId(groupId);
    if (thread == null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("handleRemoveFromGroupEvent", "#db.conv.update.${groupId}: handleRemoveFromGroupEvent -> " + "thread not found");
      }
      return false;
    }

    var membersRemoved = payload.object.whom;
    if (membersRemoved!.isNotEmpty) {
      if (thread.disabled) {
        if (AppLogger.isDebugEnabled) {
          _logger.debug("handleRemoveFromGroupEvent",
              "#db.conv.update.$groupId: handleRemoveFromGroupEvent -> " + "group is disabled so ignoring the changes");
        }

        if (thread.updatedTime < payload.object.updatedTime!) {
          Conversation conversation = Conversation();
          conversation.id = thread.id;
          conversation.updatedTime = payload.object.updatedTime!;

          var threadUpdated = _conversationManager.updateById(conversation);

          if (AppLogger.isDebugEnabled) {
            _logger.debug("handleRemoveFromGroupEvent",
                "#db.conv.update.$groupId: handleRemoveFromGroupEvent -> " + "updated conversation with removed members = $threadUpdated");
          }
          return true;
        }
      } else {
        if (thread.updatedTime < payload.object.updatedTime!) {
          Conversation conversation = Conversation();
          conversation.id = thread.id;
          conversation.updatedTime = payload.object.updatedTime!;

          if (membersRemoved.contains(_appCredential.getSubscriberId())) {
            conversation.disabled = true;
          }

          List<String> address = thread.groupMembers!.map((e) => e.address).toList();
          var profiles = getProfiles(address);
          List<String>? memberIds =
              profiles.where((element) => !membersRemoved.contains(element.profileId)).map((e) => e.profileId).cast<String>().toList();

          conversation.groupMembers = _entityConverter.toContactsFromAddress(memberIds, profiles);
          var updatedThread = _conversationManager.updateById(conversation);

          if (AppLogger.isDebugEnabled) {
            _logger.debug("handleRemoveFromGroupEvent",
                "#db.conv.update.$groupId: handleRemoveFromGroupEvent -> " + "updated conversation with removed members = $updatedThread");
          }

          if (membersRemoved.length == 1 && membersRemoved.contains(senderId)) {
            await _mManager.addGroupMemberLeftMsg(thread.id, senderId, eventId, payload.object.updatedTime!);
          } else {
            await _mManager.addGroupMembersRemovedMsg(thread.id, senderId, eventId, membersRemoved, payload.object.updatedTime!);
          }
          return true;
        } else {
          // Add the missing event message
          var msg = _mManager.findByServerId(eventId);

          if (AppLogger.isDebugEnabled) {
            _logger.debug(
                "handleRemoveFromGroupEvent",
                "#db.conv.update.$groupId: handleRemoveFromGroupEvent -> " +
                    "Ignoring the changes since local group update time is greater" +
                    " than remote update time. Existing event message = $msg");
          }

          if (msg == null) {
            if (membersRemoved.length == 1 && membersRemoved.contains(senderId)) {
              await _mManager.addGroupMemberLeftMsg(thread.id, senderId, eventId, payload.object.updatedTime!);
            } else {
              await _mManager.addGroupMembersRemovedMsg(thread.id, senderId, eventId, membersRemoved, payload.object.updatedTime!);
            }
          }
        }
      }
    }
    return false;
  }

  Conversation? geConversation(String groupId) {
    return _conversationManager.findConversationByServerId(groupId);
  }

  Future<Conversation?> handleGroupAndProfiles(GroupChat group, List<PublicProfiles> profiles) async {
    // Save all the profiles
    await addOrUpdateProfile(profiles);
    // Create Group chat
    return await createGroupChat(group, false);
  }

  /// Sample payload received through MQTT channel
  ///
  ///    {
  ///      "type":"SentReceiptEvent",
  ///      "object":{
  ///                  "id":"a6c53d5e1000494091f0c1ad06_1626850193059_2c0000098001MA",
  ///                  "createdTime":1626850193059,
  ///                  "groupId":"a6c53d5e1000494091f0c1ad06",
  ///                  "sentMsgId":"a6c53d5e1000494091f0c1ad06_1626850193004_2c0000098001MA",
  ///                  "clientMsgId":"2c0000098001MA",
  ///                  "sentMsgTime":1626850193004
  ///                }
  ///    }
  Future<bool> handleSentReceiptEvent(Payload payload) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("handleSentReceiptEvent", "#db.msg.up.${payload.object.sentMsgId}: data = $payload");
    }
    Message? message = _mManager.findMessageByClientId(payload.object.clientMsgId!);
    if (message != null) {
      Message data = Message(message.id, message.time, message.type, message.status, message.inbound, message.sender, message.recipients, message.conversationId);
      data.status = MessageStatus.sent;
      data.serverId = payload.object.sentMsgId;
      Message? updated = await _mManager.updateById(data);

      // Delete the pending queued item
      _mManager.deleteQueuedItem(updated!.id);

      if (AppLogger.isDebugEnabled) {
        _logger.debug("handleSentReceiptEvent", "#db.msg.up.${message.clientMsgId}: msg = ${updated.toString()}");
      }
      return true;
    }

    return false;
  }

  Future<void> _saveThumbnailData(List<Attachment> attachments) async {
    for (Attachment attachment in attachments) {
      if (attachment.thumbnailData != null) {
        attachment.thumbnail = await _fileManager.saveThumbnail(attachment.thumbnailData!, FileManager.thumbTypeImage);
      } else {
        attachment.thumbnail = Uri.parse("");
      }
    }
  }

  void _startAttachmentDownload(List<Attachment> attachments, int localMsgId) {
     for (Attachment attachment in attachments) {
       _fileDownloadManager.put(localMsgId, attachment.serverUrl!, attachment.mimeType, attachment.id);
     }
  }

}
