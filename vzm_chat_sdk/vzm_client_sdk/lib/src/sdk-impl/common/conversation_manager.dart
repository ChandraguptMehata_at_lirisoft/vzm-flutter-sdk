/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:async';

import 'package:vzm_client_sdk/src/api/repositories/conversation_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/conversation_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';

import '../../../objectbox.g.dart';

class ConversationManager extends ConversationRepository {
  late final EntityConverter _entityConverter;
  late Box<ConversationEntity> conversationBox;
  late final AppLogger _logger;
  final StreamController _conversationChangeController = StreamController.broadcast();

  ConversationManager(this.conversationBox)
      : _entityConverter = serviceLocator.get<EntityConverter>(),
        _logger = serviceLocator.get<AppLogger>(),
        super(conversationBox);

  Conversation getOrCreateConversation(Payload payload, bool restored) {
    return Conversation();
  }

  Conversation getOrCreateGroupChat(group, bool restored) {
    return Conversation();
  }

  @override
  Future<Conversation> addConversation(Conversation conversation, {bool isLoadDBTest = false}) async {
    var contacts = getContacts(conversation);
    DatabaseManager _database = serviceLocator.get<DatabaseManager>();
    var conversationBox = _database.getConversationBox();
    conversation.id = conversationBox.put(_entityConverter.toConversationEntity(conversation, contacts));
    if (!isLoadDBTest) {
      _conversationChangeController.add(conversation);
    }
    return conversation;
  }

  @override
  Conversation? findConversationByServerId(String serverId) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("findConversationByServerId", "#db.conv.$serverId");
    }
    ConversationEntity? conversation = conversationBox.query(ConversationEntity_.serverId.equals(serverId)).build().findFirst();
    if (conversation != null) {
      return _entityConverter.toConversation(conversation);
    }
    return null;
  }

  @override
  Future<List<ConversationEntity>> findWhereByPage(where, int offset, int limit) {
    // TODO: implement findWhereByPage
    throw UnimplementedError();
  }

  List<ContactEntity> getContacts(Conversation data) {
    var contacts = <ContactEntity>[];
    var recipients = data.recipients;
    var groupMembers = data.groupMembers;
    var groupAdmins = data.groupAdmins;
    var creator = data.creator;

    for (Contact contact in recipients!) {
      contacts.add(getOrCreateContacts(contact));
    }
    if (groupMembers != null) {
      for (Contact contact in groupMembers) {
        contacts.add(getOrCreateContacts(contact));
      }
    }
    if (groupAdmins != null) {
      for (Contact contact in groupAdmins) {
        contacts.add(getOrCreateContacts(contact));
      }
    }
    if (creator != null) {
      contacts.add(getOrCreateContacts(creator));
    }
    return contacts;
  }

  ContactEntity getOrCreateContacts(Contact contact) {
    Box<ContactEntity> contactBox = serviceLocator.get<DatabaseManager>().getContactBox();
    var contactEntity = contactBox.query(ContactEntity_.address.equals(contact.address)).build().findUnique();
    if (contactEntity == null) {
      var newContactEntity = _entityConverter.toContactEntity(contact);
      newContactEntity.id = contactBox.put(newContactEntity);
      return newContactEntity;
    } else {
      return contactEntity;
    }
  }

  @override
  Future<bool> deleteById(Conversation conversation) async {
    bool result = conversationBox.remove(conversation.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("#repo.conv.up deleteById", "deleted $result");
    }
    return result;
  }

  @override
  Future<Conversation?> updateById(Conversation src) async {
    ConversationEntity? dest = conversationBox.get(src.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("updateById", "#repo.conv.up.${src.id}:  dest ${dest}");
    }
    if (dest != null) {
      if (src.serverId != null) {
        dest.serverId = src.serverId;
      }
      if (src.time != null) {
        dest.time = src.time!;
      }
      if (src.snippet != null) {
        dest.snippet = src.snippet;
      }

      if (src.type != null) {
        dest.type = src.type;
      }
      if (src.groupMembers != null) {
        dest.groupMembers.clear();
        for (Contact contact in src.groupMembers!) {
          dest.groupMembers.add(getOrCreateContacts(contact));
        }
      }
      conversationBox.put(dest);

      _conversationChangeController.add(src);

      return _entityConverter.toConversation(dest);
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("updateById", "#repo.conv.up.${src.id}: conv not found. update failed.");
      }
    }
    return null;
  }

  @override
  Stream<List<Conversation>> observeConversationEntityChange() {
    return conversationBox.query().watch(triggerImmediately: true).map((event) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("observeConversationEntityChange", "observeConversationEntityChange ${event.find().length}");
      }
      return _entityConverter.toConversations(event.find());
    });
  }

  @override
  Future<List<Conversation>> getAllConversation() async {
    List<ConversationEntity> conversationEntityList = await findAll();
    conversationEntityList.sort((a, b) => b.time!.compareTo(a.time!));
    return _entityConverter.toConversations(conversationEntityList);
  }

  void notifyConversationObjects() {
    _conversationChangeController.add([]);
  }

  @override
  Future<Conversation?> findConversationByThreadId(int? threadId) async {
    if (threadId == null) {
      throw Exception("Thread id is null");
    }
    ConversationEntity? conversation = await findById(threadId);
    if (conversation != null) {
      return _entityConverter.toConversation(conversation);
    }
    return null;
  }

  @override
  void dispose() {
    _conversationChangeController.close();
  }

  @override
  Stream subscribeForConversationChange() {
    return _conversationChangeController.stream;
  }

  Future<int?> getConversationUpdatedTime(int? threadId) async {
    if (threadId == null) {
      throw Exception("Thread id is null");
    }
    ConversationEntity? conversation = await findById(threadId);
    if (conversation != null) {
      return conversation.time;
    }
    return null;
  }
}
