/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:async';

import 'package:vzm_client_sdk/objectbox.g.dart';
import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/message_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/profile_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/attachment_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/message_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/send_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/group.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/models.dart';

class MessageRepositoryImpl extends MessageRepository {
  late final EntityConverter _entityConverter;
  late final AppLogger _logger;
  late final ProfileRepositoryImpl _profileManager;
  late Box<MessageEntity> messageBox;
  late Box<ContactEntity> contactBox = serviceLocator.get<DatabaseManager>().getContactBox();
  final StreamController _messageChangeController = StreamController.broadcast();

  MessageRepositoryImpl(this.messageBox)
      : _entityConverter = serviceLocator.get<EntityConverter>(),
        _logger = serviceLocator.get<AppLogger>(),
        _profileManager = serviceLocator.get<ProfileRepositoryImpl>(),
        super(messageBox);

  int saveMessages(Conversation conv, MessageByGroupResponse messages) {
    return 0;
  }

  @override
  Message? findByServerId(String serverId) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("findByServerId", "#db.msg.$serverId");
    }
    MessageEntity? msg = messageBox.query(MessageEntity_.serverId.equals(serverId)).build().findFirst();
    if (msg != null) {
      return _entityConverter.toMessage(msg);
    }
    return null;
  }

  @override
  Future<Message> addMessage(Message insert) async {
    var contacts = getContacts(insert);
    var entity = _entityConverter.toMessageEntity(insert, contacts);
    insert.id = messageBox.put(entity);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("addMessage", "#repo.msg.add.${insert.serverId}: addMessage saved message = ${insert.toString()}");
    }
    Message? message = await findMessageById(insert.id);//fetching the message object will have the proper local attachment id (After insertion)
    _messageChangeController.add(message);
    return message!;
  }

  Future<Message> addMessages(List<Message> insert) async {
    List<MessageEntity>list = <MessageEntity>[];
    for (Message message in insert) {
      var contacts = getContacts(message);
      list.add(_entityConverter.toMessageEntity(message, contacts));
    }
    messageBox.putMany(list);
    return Message(0, 123, MessageType.eventAddAdmins, MessageStatus.deleted, 0, Contact(""), [], 1);
  }

  List<ContactEntity> getContacts(Message message) {
    var contactList = <ContactEntity>[];
    contactList.add(getOrCreateContacts(message.sender));
    for (Contact contact in message.recipients) {
      contactList.add(getOrCreateContacts(contact));
    }
    return contactList;
  }

  ContactEntity getOrCreateContacts(Contact sender) {
    var contactEntity = contactBox.query(ContactEntity_.address.equals(sender.address)).build().findUnique();
    if (contactEntity == null) {
      var contact = _entityConverter.toContactEntity(sender);
      contact.id = contactBox.put(contact);
      return contact;
    } else {
      return contactEntity;
    }
  }

  @override
  Future<bool> deleteById(Message message) async {
    bool result = messageBox.remove(message.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("#repo.msg.up deleteById", "deleted $result");
    }
    return result;
  }

  @override
  Future<List<MessageEntity>> findWhereByPage(where, int offset, int limit) {
    // TODO: implement findWhereByPage
    throw UnimplementedError();
  }

  @override
  Future<Message?> updateById(Message src, {bool? notifyApp}) async {
    MessageEntity? dest = messageBox.get(src.id);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("updateById", "#repo.msg.up.${src.id}:  dest ${dest}");
    }
    if (dest != null) {
      if (src.time != null) {
        dest.time = src.time!;
      }
      if (src.body != null) {
        dest.body = src.body!;
      }
      if (src.subject != null) {
        dest.subject = src.subject!;
      }
      if (src.serverId != null) {
        dest.serverId = src.serverId;
      }
      if (src.read != null) {
        dest.read = src.read;
      }
      if (src.clientMsgId != null) {
        dest.clientMsgId = src.clientMsgId;
      }

      messageBox.put(dest);

      _messageChangeController.add(src);

      return _entityConverter.toMessage(dest);
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("updateById", "#repo.msg.up.${src.id}: msg not found. update failed.");
      }
    }
    return null;
  }

  Future<void> addCreateGroupEventMsg(int convId, String eventId, GroupChat data) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("addCreateGroupEventMsg", "#db.conv.update.${data.id}: addCreateGroupEventMsg ->" + " convId = $convId, data = $data");
    }

    var list = <String>[];
    list.add(data.creatorId!);
    var profiles = getProfiles(list);
    var fromContact = _entityConverter.toContactFromAddress(data.creatorId, profiles);

    if (AppLogger.isDebugEnabled) {
      _logger.debug("addCreateGroupEventMsg", "#db.conv.update.${data.id}: addCreateGroupEventMsg ->" + " fromContact = $fromContact");
    }

    var emptyList = <Contact>[];
    var message = Message(
        0, data.createdTime, MessageType.eventCreateGroup, MessageStatus.received, Message.inboundMessage, fromContact, emptyList, convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " created group ${data.name}";
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addGroupMembersAddedMsg(int convId, String fromMember, String eventId, List<String> addedMembers, int addedTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("addGroupMembersAddedMsg",
          "#db.conv.update: addGroupMembersAddedMsg ->" + " convId = $convId, fromMember = $fromMember, addedMembers = $addedMembers");
    }

    var ids = <String>[];
    ids.add(fromMember);
    ids.addAll(addedMembers);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(fromMember, profiles);
    var toContacts = _entityConverter.toContactsFromAddress(addedMembers, profiles);

    var message =
        Message(0, addedTime, MessageType.eventAddMembers, MessageStatus.received, Message.inboundMessage, fromContact, toContacts, convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " added ${toContacts.map((e) => e.name ?? e.address).toString()}";
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addGroupNameChangedMsg(int convId, String senderId, String eventId, String name, int msgTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "addGroupNameChangedMsg", "#db.conv.update: addGroupNameChangedMsg ->" + " convId = $convId, senderId = $senderId, name = $name");
    }

    var ids = <String>[];
    ids.add(senderId);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);

    var message =
        Message(0, msgTime, MessageType.eventAddMembers, MessageStatus.received, Message.inboundMessage, fromContact, <Contact>[], convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " changed group name to  $name";
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addGroupAvatarChangedMsg(int convId, String senderId, String eventId, int msgTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("addGroupAvatarChangedMsg",
          "#db.conv.update: addGroupAvatarChangedMsg ->" + " convId = $convId, senderId = $senderId, eventId = $eventId");
    }

    var ids = <String>[];
    ids.add(senderId);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);

    var message = Message(
        0, msgTime, MessageType.eventChangeAvatar, MessageStatus.received, Message.inboundMessage, fromContact, <Contact>[], convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " changed group avatar";
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addGroupBackgroundChangedMsg(int convId, String senderId, String eventId, int msgTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("addGroupBackgroundChangedMsg",
          "#db.conv.update: addGroupBackgroundChangedMsg ->" + " convId = $convId, senderId = $senderId, eventId = $eventId");
    }

    var ids = <String>[];
    ids.add(senderId);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);

    var message = Message(
        0, msgTime, MessageType.eventChangeBackground, MessageStatus.received, Message.inboundMessage, fromContact, <Contact>[], convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " changed group background";
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addGroupMemberLeftMsg(int convId, String senderId, String eventId, int updatedTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("addGroupMemberLeftMsg",
          "#db.conv.update: addGroupMemberLeftMsg ->" + " convId = $convId, senderId = $senderId, eventId = $eventId");
    }

    var ids = <String>[];
    ids.add(senderId);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);

    var message = Message(
        0, updatedTime, MessageType.eventLeaveGroup, MessageStatus.received, Message.inboundMessage, fromContact, <Contact>[], convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " left the group";
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addGroupMembersRemovedMsg(int convId, String senderId, String eventId, List<String> membersRemoved, int updatedTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "addGroupMembersRemovedMsg",
          "#db.conv.update: addGroupMembersRemovedMsg ->" +
              " convId = $convId, senderId = $senderId, eventId = $eventId, membersRemoved = $membersRemoved");
    }

    var ids = <String>[];
    ids.add(senderId);
    ids.addAll(membersRemoved);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);
    var toContacts = _entityConverter.toContactsFromAddress(membersRemoved, profiles);

    var message = Message(
        0, updatedTime, MessageType.eventRemoveMembers, MessageStatus.received, Message.inboundMessage, fromContact, toContacts, convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " removed ${toContacts.map((e) => e.name ?? e.address).toString()}";
    ;
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addPromotedAdminsMsg(int convId, String senderId, String eventId, List<String> promotedAdminIds, int updatedTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "addPromotedAdminsMsg",
          "#db.conv.update: addPromotedAdminsMsg ->" +
              " convId = $convId, senderId = $senderId, eventId = $eventId, promotedAdminIds = $promotedAdminIds");
    }

    var ids = <String>[];
    ids.add(senderId);
    ids.addAll(promotedAdminIds);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);
    var admins = _entityConverter.toContactsFromAddress(promotedAdminIds, profiles);

    var message = Message(
        0, updatedTime, MessageType.eventAddAdmins, MessageStatus.received, Message.inboundMessage, fromContact, <Contact>[], convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " promoted ${admins.map((e) => e.name ?? e.address).toString()}";
    ;
    message.serverId = eventId;
    await addMessage(message);
  }

  Future<void> addDemotedAdminsMsg(int convId, String senderId, String eventId, List<String> demotedAdminIds, int updatedTime) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "addDemotedAdminsMsg",
          "#db.conv.update: addDemotedAdminsMsg ->" +
              " convId = $convId, senderId = $senderId, eventId = $eventId, demotedAdminIds = $demotedAdminIds");
    }

    var ids = <String>[];
    ids.add(senderId);
    ids.addAll(demotedAdminIds);
    var profiles = getProfiles(ids);

    var fromContact = _entityConverter.toContactFromAddress(senderId, profiles);
    var admins = _entityConverter.toContactsFromAddress(demotedAdminIds, profiles);

    var message = Message(
        0, updatedTime, MessageType.eventRemoveAdmins, MessageStatus.received, Message.inboundMessage, fromContact, <Contact>[], convId);

    var address = fromContact.name ?? fromContact.address;
    message.body = address + " revoked admin privileges from ${admins.map((e) => e.name ?? e.address).toString()}";
    ;
    message.serverId = eventId;
    await addMessage(message);
  }

  List<ProfileEntity> getProfiles(List<String> ids) {
    var profiles = <ProfileEntity>[];
    for (String id in ids) {
      var profile = _profileManager.getProfile(id);
      if (profile != null) {
        profiles.add(profile);
      } else if (AppLogger.isDebugEnabled) {
        _logger.debug("getProfiles", "#sync.profile.$id: Missing profiles.");
        throw Exception(" Profile id $id not found");
      }
    }
    return profiles;
  }

  @override
  Future<List<Message>> getAllMessages(int? threadId) async {
    if (threadId == null) {
      throw Exception("Thread id can not be null");
    }
    List<MessageEntity> messageEntityList = messageBox.query(MessageEntity_.conversationId.equals(threadId)).build().find();
    messageEntityList.sort((a, b) => b.time.compareTo(a.time));
    return _entityConverter.toMessages(messageEntityList);
  }

  @override
  void dispose() {
    //todo: this is just a work around to show the all messages because it was throwing the error on relaunch of chat screen
    // need to dispose the notifier properly
    //super.dispose();
    _messageChangeController.close();
  }

  @override
  Message? findMessageByClientId(String clientMsgId) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("findMessageByClientId", "#db.msg.$clientMsgId");
    }
    MessageEntity? msg = messageBox.query(MessageEntity_.clientMsgId.equals(clientMsgId)).build().findFirst();
    if (msg != null) {
      return _entityConverter.toMessage(msg);
    }
    return null;
  }

  @override
  Future<Message?> findMessageById(int msgId) async {
    MessageEntity? message = await findById(msgId);
    if (message != null) {
      return _entityConverter.toMessage(message);
    }
    return null;
  }

  void deleteQueuedItem(int luId) {
    Box<SendQueueEntity> sendQueueBox = serviceLocator.get<DatabaseManager>().getSendQueueBox();
    SendQueueEntity? item = sendQueueBox.query(SendQueueEntity_.luId.equals(luId)).build().findFirst();
    if (item != null) {
      bool isDeleted = sendQueueBox.remove(item.id);
      if (AppLogger.isDebugEnabled) {
        _logger.debug("deleteQueuedItem", "#db.msg.up.$luId: deleteQueuedItem: isDeleted $isDeleted");
      }
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("deleteQueuedItem", "#db.msg.up.$luId: no item found");
      }
    }
  }

  @override
  Stream subscribeForMessageChange() {
    return _messageChangeController.stream;
  }

  @override
  Future<bool> updateAttachment(Attachment attachment) async {
    AttachmentEntity? data = serviceLocator.get<DatabaseManager>().getAttachmentBox().get(attachment.id);
    if (data != null) {
      if (attachment.status != null) {
        data.status = attachment.status;
      }
      if (attachment.mimeType != null) {
        data.mimeType = attachment.mimeType;
      }
      if (attachment.thumbnail != null) {
        data.thumbnailUri = attachment.thumbnail.toString();
      }
      if (attachment.path != null) {
        data.localUri = attachment.path.toString();
      }
      if (attachment.duration != null) {
        data.duration = attachment.duration;
      }
      if (attachment.progress != null) {
        data.progress = attachment.progress!;
      }
      if (attachment.serverUrl != null) {
        data.serverUrl = attachment.serverUrl;
      }
      if (attachment.fileType != null) {
        data.type = attachment.fileType;
      }
      serviceLocator.get<DatabaseManager>().getAttachmentBox().put(data);
      return true;
    }
    return false;
  }

}
