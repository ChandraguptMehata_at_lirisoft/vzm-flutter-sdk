/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/push_token.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/ams_api.dart';

class PushTokenManager {

  late AppCredential appCredential;
  late AmsApi amsApiClient;

  PushTokenManager() {
    appCredential = serviceLocator.get<AppCredential>();
    amsApiClient = serviceLocator.get<AmsApi>();
  }

  Future initialize(context) async {
    Firebase.initializeApp().whenComplete(() {
      print("#sdk.fcm: initialize completed");

      final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
      firebaseMessaging.getToken().then((fcmToken) {
        print("#sdk.fcm: initialize fcmToken $fcmToken");
        appCredential.saveFcmToken(fcmToken);
      });
    });
  }

  Future<void> updatePushToken(String token) async {
    String? devicePushId = appCredential.getDevicePushId();
    print("#sdk.fcm:updatePushToken :: " + "FCM TOKEN = $token, devicePushId = $devicePushId");
    var serverToken = "FCM|$token";
    String? deviceId = appCredential.getDeviceId();
    print("#sdk.fcm:updatePushToken :: " + "FCM TOKEN = $token, devicePushId = $devicePushId, deviceId = $deviceId");
    if (deviceId != null) {
      try {
        var pushToken = PushToken(appName: "VZM", deviceid: deviceId, pushId: serverToken);
        await amsApiClient.updatePushId(pushToken);
        print("#sdk.fcm:updatePushToken :: response no error");
        appCredential.setDevicePushId(token);
      } catch (exception, stacktrace)   {
        print("#sdk.fcm:updatePushToken :: throwing error ${exception.toString()} stacktrace $stacktrace");
      }
    }
  }
}
