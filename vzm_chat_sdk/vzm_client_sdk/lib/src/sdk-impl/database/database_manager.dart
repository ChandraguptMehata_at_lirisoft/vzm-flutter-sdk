/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:objectbox/objectbox.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/conversation_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/message_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/restore_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/send_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/upload_queue_entity.dart';

import 'entity/attachment_entity.dart';
import 'entity/file_queue_entity.dart';

abstract class DatabaseManager{

  Future<bool> init();

  Future<bool> reset();

  Box<RestoreQueueEntity> getRestoreQueue();

  Box<MessageEntity> getMessageBox();

  Box<ProfileEntity> getProfileBox();

  Box<ConversationEntity> getConversationBox();

  Box<ContactEntity> getContactBox();

  Box<SendQueueEntity> getSendQueueBox();

  Box<FileQueueEntity> getFileQueueBox();

  Box<AttachmentEntity> getAttachmentBox();

  Box<UploadQueueEntity> getUploadQueueBox();

}

