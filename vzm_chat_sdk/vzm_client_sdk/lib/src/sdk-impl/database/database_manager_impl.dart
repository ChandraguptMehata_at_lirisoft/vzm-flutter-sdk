/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:io';
import 'package:objectbox/objectbox.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/attachment_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/conversation_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/file_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/restore_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/send_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/upload_queue_entity.dart';

import '../../../objectbox.g.dart';
import 'entity/message_entity.dart';


class DatabaseManagerImpl extends DatabaseManager {
  late final Store _store;

  @override
  Future<bool> init() async {
    try {
      Directory? dir = await getApplicationDocumentsDirectory();
      _store = Store(getObjectBoxModel(), directory: dir.path + '/objectbox');
      return true;
    } on Exception catch (exception) {
      print("===exception " + exception.toString());
      return false;
    } catch (error) {
      print("===exception catch " + error.toString());
      return false;
    }
  }


  @override
  Future<bool> reset() async {
    _store.close();
    return false;
  }

  @override
  Box<RestoreQueueEntity> getRestoreQueue() {
    return _store.box<RestoreQueueEntity>();
  }

  @override
  Box<MessageEntity> getMessageBox() {
    return _store.box<MessageEntity>();
  }

  @override
  Box<ProfileEntity> getProfileBox() {
    return _store.box<ProfileEntity>();
  }

  @override
  Box<ConversationEntity> getConversationBox() {
    return _store.box<ConversationEntity>();
  }

  @override
  Box<ContactEntity> getContactBox() {
    return _store.box<ContactEntity>();
  }

  @override
  Box<SendQueueEntity> getSendQueueBox() {
    return _store.box<SendQueueEntity>();
  }

  @override
  Box<FileQueueEntity> getFileQueueBox() {
    return _store.box<FileQueueEntity>();
  }

  @override
  Box<AttachmentEntity> getAttachmentBox() {
    return _store.box<AttachmentEntity>();
  }

  @override
  Box<UploadQueueEntity> getUploadQueueBox() {
    return _store.box<UploadQueueEntity>();
  }

}