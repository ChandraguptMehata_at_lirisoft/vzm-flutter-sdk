/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:objectbox/objectbox.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';

@Entity()
class AttachmentEntity {
  // primary key
  int id = 0;
  String? mimeType;
  AttachmentStatus? status;

  String? thumbnailUri;
  String? localUri;
  int? progress = 0;
  String? serverUrl;
  int? duration = 0;
  FileType? type;
  double size = 0;

  int? get attachmentStatus {
    _ensureAttachmentStatusValues();
    return status!.index;
  }

  set attachmentStatus(int? value) {
    _ensureAttachmentStatusValues();
    if (value == null) {
      status = AttachmentStatus.queued;
    } else {
      status = AttachmentStatus.values[value]; // throws a RangeError if not found
      // or if you want to handle unknown values gracefully:
      status = value >= 0 && value < AttachmentStatus.values.length
          ? AttachmentStatus.values[value]
          : AttachmentStatus.queued;
    }
  }

  void _ensureAttachmentStatusValues() {
    assert(AttachmentStatus.queued.index == 0);
    assert(AttachmentStatus.uploading.index == 1);
    assert(AttachmentStatus.uploaded.index == 2);
    assert(AttachmentStatus.uploadFailed.index == 3);
    assert(AttachmentStatus.downloading.index == 4);
    assert(AttachmentStatus.downloaded.index == 5);
    assert(AttachmentStatus.downloadFailed.index == 6);
  }

  int? get fileType {
    _ensureFileTypeValues();
    return type!.index;
  }

  set fileType(int? value) {
    _ensureFileTypeValues();
    if (value == null) {
      type = FileType.unknown;
    } else {
      type = FileType.values[value]; // throws a RangeError if not found
      // or if you want to handle unknown values gracefully:
      type = value >= 0 && value < FileType.values.length
          ? FileType.values[value]
          : FileType.unknown;
    }
  }

  void _ensureFileTypeValues() {
    assert(FileType.text.index == 0);
    assert(FileType.image.index == 1);
    assert(FileType.audio.index == 2);
    assert(FileType.video.index == 3);
    assert(FileType.location.index == 4);
    assert(FileType.vcard.index == 5);
    assert(FileType.unknown.index == 6);
  }
}
