/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:objectbox/objectbox.dart';

@Entity()
class ContactEntity {

  int id = 0;

  @Unique()
  late String address;

  String? name;
  String? avatarUrl;
  String? email;
}

