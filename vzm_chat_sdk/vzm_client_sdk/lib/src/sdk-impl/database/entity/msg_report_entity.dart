/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:objectbox/objectbox.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';

@Entity()
class MessageReportEntity {
  // primary key
  int id = 0;
  late String mdn;
  late MessageStatus status;
  late int time;
}
