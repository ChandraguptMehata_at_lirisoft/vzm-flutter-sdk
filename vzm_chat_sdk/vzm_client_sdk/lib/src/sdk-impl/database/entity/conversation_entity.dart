/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:objectbox/objectbox.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/message_entity.dart';

@Entity()
class ConversationEntity {
  // primary key
  int id = 0;

  ConversationType? type;
  // List of MDNS (TO Address)
  List<String> recipients = <String>[];

  var creator = ToOne<ContactEntity>();

  int? time;

  int unreadCount = 0;
  bool muted = false;

  String? snippet;
  late bool read = false;

  // Group Chat
  String? groupName;
  String? groupAvatarUrl;
  String? groupBackgroundUrl;

  final groupMembers = ToMany<ContactEntity>();
  final groupAdmins = ToMany<ContactEntity>();

  // Last Message
  var lastMessage = ToOne<MessageEntity>();

  // Server Related information
  // MD5 hash or GroupId
  String? serverId;

  // OTT 1x1 ID A!B for OTT 1x1 or chatbot
  String? ottId;
  String? rcsId;

  //Server Group Chat Update Time;
  int? groupUpdatedTime;

  int? get conversationType {
    _ensureStableEnumValues();
    return type?.index;
  }

  set conversationType(int? value) {
    _ensureStableEnumValues();
    if (value == null) {
      type = null;
    } else {
      type = ConversationType.values[value]; // throws a RangeError if not found
      // or if you want to handle unknown values gracefully:
      type = value >= 0 && value < ConversationType.values.length
          ? ConversationType.values[value]
          : ConversationType.oneToOneChat;
    }
  }

  void _ensureStableEnumValues() {
    assert(ConversationType.oneToOneChat.index == 0);
    assert(ConversationType.mmsGroupChat.index == 1);
    assert(ConversationType.openGroupChat.index == 2);
    assert(ConversationType.closedGroupChat.index == 3);
    assert(ConversationType.broadCast.index == 4);
    assert(ConversationType.chatBot.index == 5);
  }

}
