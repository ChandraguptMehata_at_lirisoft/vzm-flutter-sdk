/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */


import 'package:objectbox/objectbox.dart';

@Entity()
class ProfileEntity {


  int id = 0;

  @Unique()
  late String address;

  String? name;
  String? profileId;//subscriber id
  String? avatarUrl;
  Uri? avatar;
  String? avatarChecksum;
  int? createdTime;

  int? updatedTime;

  ProfileEntity(this.address, this.name, this.profileId,
    this.avatarUrl, this.updatedTime, this.createdTime);
}
