/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:objectbox/objectbox.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/attachment_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/msg_report_entity.dart';

@Entity()
class MessageEntity {
  // primary key
  int id = 0;
  late int time;
  late MessageType type;
  late MessageStatus? status;
  late bool unread = false;

  String? clientMsgId;

  String? body;
  String? subject;
  String? serverId;
  int? read;
  String? parentMsgId;
  late int conversationId;

  //From
  var sender = ToOne<ContactEntity>();

  // To
  var recipients = ToMany<ContactEntity>();

  var reports = ToMany<MessageReportEntity>();

  final attachments = ToMany<AttachmentEntity>();

  int? get messageType {
    _ensureStableMessageTypeValues();
    return type.index;
  }

  set messageType(int? value) {
    _ensureStableMessageTypeValues();
    if (value == null) {
      type = MessageType.none;
    } else {
      type = MessageType.values[value]; // throws a RangeError if not found
      // or if you want to handle unknown values gracefully:
      type = value >= 0 && value < MessageType.values.length
          ? MessageType.values[value]
          : MessageType.none;
    }
  }

  void _ensureStableMessageTypeValues() {
    assert(MessageType.none.index == 0);
    assert(MessageType.sms.index == 1);
    assert(MessageType.mms.index == 2);
    assert(MessageType.textMessage.index == 3);
    assert(MessageType.mediaMessage.index == 4);
    assert(MessageType.groupEventMessage.index == 5);
    assert(MessageType.eventCreateGroup.index == 6);
    assert(MessageType.eventAddMembers.index == 7);
    assert(MessageType.eventChangeAvatar.index == 8);
    assert(MessageType.eventChangeBackground.index == 9);
    assert(MessageType.eventLeaveGroup.index == 10);
    assert(MessageType.eventRemoveMembers.index == 11);
    assert(MessageType.eventAddAdmins.index == 12);
    assert(MessageType.eventRemoveAdmins.index == 13);
  }

  int? get messageStatus {
    _ensureStableStatusValues();
    return status!.index;
  }

  set messageStatus(int? value) {
    _ensureStableStatusValues();
    if (value == null) {
      status = MessageStatus.none;
    } else {
      status = MessageStatus.values[value]; // throws a RangeError if not found
      // or if you want to handle unknown values gracefully:
      status = value >= 0 && value < MessageStatus.values.length
          ? MessageStatus.values[value]
          : MessageStatus.none;
    }
  }

  void _ensureStableStatusValues() {
    assert(MessageStatus.none.index == 0);
    assert(MessageStatus.draft.index == 1);
    assert(MessageStatus.queued.index == 2);
    assert(MessageStatus.uploading.index == 3);
    assert(MessageStatus.sending.index == 4);
    assert(MessageStatus.processing.index == 5);
    assert(MessageStatus.sent.index == 6);
    assert(MessageStatus.deliveredToSome.index == 7);
    assert(MessageStatus.delivered.index == 8);
    assert(MessageStatus.readToSome.index == 9);
    assert(MessageStatus.read.index == 10);
    assert(MessageStatus.deleted.index == 11);
    assert(MessageStatus.available.index == 12);
    assert(MessageStatus.downloading.index == 13);
    assert(MessageStatus.received.index == 14);
    assert(MessageStatus.failedReceive.index == 15);
  }

}
