/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:objectbox/objectbox.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';

@Entity()
class SendQueueEntity {
  int id = 0;

  @Unique()
  int luId;

  String? serverId;

  SendMessageStatus? status;

  int? time;

  SendQueueEntity(this.luId);

  int? get sendMessageStatus {
    _ensureStableStatusValues();
    return status!.index;
  }

  set sendMessageStatus(int? value) {
    _ensureStableStatusValues();
    if (value == null) {
      status = SendMessageStatus.queued;
    } else {
      status = SendMessageStatus.values[value];
      status = value >= 0 && value < SendMessageStatus.values.length ? SendMessageStatus.values[value] : SendMessageStatus.queued;
    }
  }

  void _ensureStableStatusValues() {
    assert(SendMessageStatus.queued.index == 0);
    assert(SendMessageStatus.waitingSentReceipt.index == 1);
    assert(SendMessageStatus.sent.index == 2);
    assert(SendMessageStatus.retryOnNextSession.index == 3);
  }

}
