/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:objectbox/objectbox.dart';

@Entity()
class RestoreQueueEntity {
  int id = 0;

  @Unique()
  late String groupId;
  int? updatedTime;

  RestoreQueueEntity(this.groupId);
}
