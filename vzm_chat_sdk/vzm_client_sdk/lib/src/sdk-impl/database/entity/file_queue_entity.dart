/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:vzm_client_sdk/src/sdk-impl/media/model/file_transfer_state.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/file_transfer_type.dart';

import 'package:objectbox/objectbox.dart';

@Entity()
class FileQueueEntity {
  int id = 0;

  int localMsgId;

  @Unique()
  int attachmentId;

  String? uploadUrl;

  String? downloadUrl;

  FileTransferState? transferState;

  FileTransferType? transferType;

  String? mimeType;

  String? localUrl;

  FileQueueEntity(this.localMsgId, this.attachmentId);

  int? get fileTransferState {
    _ensureStableStatusValues();
    return transferState!.index;
  }

  set fileTransferState(int? value) {
    _ensureStableStatusValues();
    if (value == null) {
      transferState = FileTransferState.queued;
    } else {
      transferState = FileTransferState.values[value];
      transferState = value >= 0 && value < FileTransferState.values.length ? FileTransferState.values[value] : FileTransferState.queued;
    }
  }

  void _ensureStableStatusValues() {
    assert(FileTransferState.queued.index == 0);
    assert(FileTransferState.running.index == 1);
    assert(FileTransferState.paused.index == 2);
    assert(FileTransferState.completed.index == 3);
    assert(FileTransferState.failed.index == 4);
  }

  int? get fileTransferType {
    _ensureTransferTypeValues();
    return transferType!.index;
  }

  set fileTransferType(int? value) {
    _ensureTransferTypeValues();
    if (value == null) {
      transferType = FileTransferType.download;
    } else {
      transferType = FileTransferType.values[value];
      transferType = value >= 0 && value < FileTransferType.values.length ? FileTransferType.values[value] : FileTransferType.download;
    }
  }

  void _ensureTransferTypeValues() {
    assert(FileTransferType.download.index == 0);
    assert(FileTransferType.upload.index == 1);
  }

  @override
  String toString() {
    return 'FileQueueEntity{id: $id, messageId: $localMsgId, attachmentId: $attachmentId, uploadUrl: $uploadUrl, downloadUrl: $downloadUrl, transferState: $transferState, transferType: $transferType, mimeType: $mimeType}';
  }
}
