/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:objectbox/objectbox.dart';

@Entity()
class UploadQueueEntity {
  int id = 0;

  int messageId;

  String? serverId;

  @Unique()
  int attachmentId;

  UploadType? type;

  UploadState? state;

  int? retryCount;

  String? uploadUrl;

  String? downloadUrl;

  UploadQueueEntity(this.messageId, this.attachmentId);

  int get uploadType {
    return type!.index;
  }

  set uploadType(int value) {
    type = UploadType.values[value];
    type = value >= 0 && value < UploadType.values.length ? UploadType.values[value] : UploadType.file;
  }

  int get uploadState {
    return state!.index;
  }

  set uploadState(int value) {
    state = UploadState.values[value];
    state = value >= 0 && value < UploadState.values.length ? UploadState.values[value] : UploadState.queued;
  }

  @override
  String toString() {
    return 'UploadQueueEntity{id: $id, messageId: $messageId, serverId: $serverId, attachmentId: $attachmentId, type: $type, state: $state, retryCount: $retryCount, uploadUrl: $uploadUrl, downloadUrl: $downloadUrl}';
  }
}

enum UploadType { file, groupAvatar, groupBackground, profileAvatar }

enum UploadState { queued, retryOnNextSession, success, failed }
