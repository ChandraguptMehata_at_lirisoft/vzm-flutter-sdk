/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_video_info/flutter_video_info.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vzm_client_sdk/src/api/chat/chat_manager.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/conversation_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/msg_validator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/mime_type_media.dart';

class FileManager {
  late AppLogger _logger;
  static const String thumbTypeImage = "image/jpeg";
  static const String _tag = "FILE-MANAGER";

  static Map fileExtensionMap = {
    imageJpeg: 'jpeg',
    imageJpg: 'jpg',
    imageGIF: 'gif',
    imagePng: 'png',
    imageBmp: 'bmp',

    audioAac: 'aac',
    audioXAac: 'aac',
    audioAacAdts: 'aac',
    audioXMsWma: 'wma',
    audioQcp: 'qcp',
    audioQcelp: 'qcelp',
    audioVndqcp: 'qcelp',
    audioWav: 'wav',
    audioXWav: 'wav',
    audioMp4a: 'mp4',
    audioEvrc: 'evrc',
    audioEvrcQcp: 'qcp',
    audioAmr: 'amr',
    audioWebAmr: 'amr',
    audioImeLody: 'imelody',
    audioMID: 'mid',
    audioMIDI: 'midi',
    audioMP3: 'mp3',
    audioMPEG3: 'mpeg3',
    audioMPEG: 'mpeg',
    audioMPG: 'mpg',
    audioMP4: 'mp4',
    audioXMid: 'mid',
    audioXMidi: 'midi',
    audioXMP3: 'mp3',
    audioXMpg: 'mpg',
    audio3GPP: '3gpp',
    audio3GPP2: '3gpp2',
    audioM4A: 'm4a',
    audioOGGCON: 'ogg',

    video3GP: '3gp',
    video3GPP: '3gp',
    videoH2632000: 'mp4',
    videoH264: 'mp4',
    videoMP4ES: 'mp4',
    video3G2: '3g2',
    videoH263: 'mp4',
    videoMP4: 'mp4',
    videoMPEG: 'mpeg'
  };

  FileManager() {
    _logger = serviceLocator.get<AppLogger>();
  }

  Future<Uri?> saveThumbnail(String thumbnailData, String mimeType) async {
    Uri? savedUri;
    try {
      File file = await _writeImageTemp(thumbnailData, mimeType);
      savedUri = file.uri;
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#file.mgr: saveThumbnail -> saved $savedUri");
      }
    } on Exception catch (_e) {
      if (AppLogger.isDebugEnabled) {
        _logger.error(_tag, "#file.mgr: saveThumbnail -> error saving", _e);
      }
    }
    return savedUri;
  }

  Future<File> _writeImageTemp(String base64Image, String mimeType) async {
    File file = await _localFile(mimeType);
    await file.writeAsBytes(base64.decode(base64Image));
    return file;
  }

  Future<File> _localFile(String mimeType) async {
    final path = await _localPath;
    String fileName = getFileName(mimeType);
    return File("$path/" + fileName);
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  String getFileName(String? mimeType) {
    String name = DateTime.now().millisecondsSinceEpoch.toString();
    String? extensionType = extensionFromMime(mimeType!);
    extensionType ??= fileExtensionMap[mimeType];
    String ext = ".$extensionType";
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#file.mgr: getFileName -> mimeType $mimeType and extension typ: $ext");
    }
    return name + ext;
  }

  Future<String> get externalPath async {
    final directory = await getExternalStorageDirectory();
    return directory!.path;
  }

  Future<FileMetadata> getFileMetadata(Uri fileUri, {bool skipDuration = false}) async {
    String fileName;
    FileMetadata? fileMetadata;
    fileName = (fileUri.path.split('/').last);
    var mimeType = mime(fileUri.path);
    File file = File(fileUri.path);
    var size = file.lengthSync().toInt();
    fileMetadata = FileMetadata(fileUri, fileName, mimeType!, size);

    if (skipDuration) {
      return fileMetadata;
    }
    if (mimeType.startsWith("video/")) {
      final videoInfo = FlutterVideoInfo();
      var info = await videoInfo.getVideoInfo(fileUri.path);
      if (info != null) {
        fileMetadata.duration = info.duration != null ? info.duration!.toInt(): 0;
      }
    }
    if (mimeType.startsWith("audio/")) {
      final player = AudioPlayer();
      var duration = await player.setUrl(fileUri.path);
      fileMetadata.duration = duration;
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#file.mgr: getFileMetadata ${fileMetadata.toString()}");
    }
    return fileMetadata;
  }

  FileType? getFileType(String mimeType1) {
    var mimeType = mimeType1.toLowerCase();
    if (mimeType.startsWith("image/")) {
      return FileType.image;
    } else if (mimeType.startsWith("video/")) {
      return FileType.video;
    } else if (mimeType.startsWith("audio/")) {
      return FileType.audio;
    } else if (mimeType.startsWith("text/")) {
      return FileType.text;
    } else {
      return FileType.unknown;
    }
  }

  Future<bool> shouldCompressImage(String path, String conversationId) async {
    Conversation? conversation = await serviceLocator.get<ConversationManager>().findConversationByThreadId(int.parse(conversationId));
    if (conversation == null) {
      throw Exception("Conversation not found");
    }
    var type = serviceLocator.get<ChatManager>().getFileMessageType(conversation.type);
    FileMetadata meta = await getFileMetadata(Uri.parse(path), skipDuration: true);
    if (meta.mimeType.startsWith("image/")) {
      return !(MsgValidator().isValidFileSize(type, meta.mimeType, meta.size));
    }
    return false;
  }

}

class FileMetadata {
  Uri uri;
  String fileName;
  String mimeType;
  int size;
  int? width;
  int? height;
  int? duration;

  FileMetadata(this.uri, this.fileName, this.mimeType, this.size);

  @override
  String toString() {
    return 'FileMetadata{uri: $uri, fileName: $fileName, mimeType: $mimeType, size: $size, width: $width, height: $height, duration: $duration}';
  }
}
