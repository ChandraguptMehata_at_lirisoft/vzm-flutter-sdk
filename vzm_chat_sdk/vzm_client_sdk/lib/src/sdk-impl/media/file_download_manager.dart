/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:dio/dio.dart';
import 'package:vzm_client_sdk/src/api/repositories/message_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/file_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/file_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/sync/attachment_model_converter.dart';

import '../../../objectbox.g.dart';
import 'model/file_transfer_state.dart';
import 'model/file_transfer_type.dart';

class FileDownloadManager {
  late AppLogger _logger;
  late final OttConfigurationManager _ottConfigManager;
  late final FileManager _fileManager;
  late final MessageRepository _messageRepository;
  late final AttachmentModelConverter _attachmentModelConverter;

  static const String _tag = "FILE-DOWNLOAD";
  static RegExp reg = RegExp(r'.*mediadownload.*\.vzmessages.com.*');
  static const String _authorization = "Authorization";
  final Dio _dio = Dio();

  late final Box<FileQueueEntity> _fileQueueBox;

  FileDownloadManager() {
    _logger = serviceLocator.get<AppLogger>();
    _fileQueueBox = serviceLocator.get<DatabaseManager>().getFileQueueBox();
    _ottConfigManager = serviceLocator.get<OttConfigurationManager>();
    _fileManager = serviceLocator.get<FileManager>();
    _messageRepository = serviceLocator.get<MessageRepository>();
    _attachmentModelConverter = serviceLocator.get<AttachmentModelConverter>();
  }

  void put(int localMsgId, String url, String? mimeType, int dbId) {
    _fileQueueBox.removeAll();
    FileQueueEntity entity = FileQueueEntity(localMsgId, dbId);
    entity.transferType = FileTransferType.download;
    entity.transferState = FileTransferState.queued;
    entity.mimeType = mimeType;
    entity.downloadUrl = url;
    int id = _fileQueueBox.put(entity);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#file.download.$localMsgId: putMessage -> " + "added attachment to queue with queue id =$id");
    }
    _startDownload();
  }

  void _startDownload() {
    List<FileQueueEntity> fileQueueList = _fileQueueBox.getAll();
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#file.download." + " queued list ${fileQueueList.length}");
    }
    for (FileQueueEntity item in fileQueueList) {
      _download(item);
    }
  }

  getHeaders(String? downloadUrl) {
    return {_authorization: _ottConfigManager.getBasicAuthorization()!};
  }

  Future<void> _download(FileQueueEntity item) async {
    if (AppLogger.isDebugEnabled) {
      int startTime = DateTime.now().millisecondsSinceEpoch;
      _logger.debug(_tag, "#file.download.${item.localMsgId}: " + "download -> fileQueue = $item, startTime = $startTime");
    }
    var downloadUrl = item.downloadUrl;
    downloadUrl = updateIfRequired(item, downloadUrl);

    try {
      String directoryPath = await _fileManager.externalPath;
      final savePath = directoryPath + _fileManager.getFileName(item.mimeType!);
      final response = await _dio.download(
          downloadUrl, savePath,
          onReceiveProgress: (count, total) {
            String progress = (count / total * 100).toStringAsFixed(0);
            var percentage = progress + "%";
            if (AppLogger.isDebugEnabled) {
              _logger.debug(_tag, "#file.download.${item.localMsgId}: progress >> $percentage");
            }
            item.localUrl = savePath;
            item.transferState = _attachmentModelConverter.getTransferState(progress);
            updateAttachment(item);
          },
          options: Options(headers: getHeaders(downloadUrl)));
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            _tag, "#file.download.${item.localMsgId}: download -> event " + "downloaded : response status code ${response.statusCode}");
      }
      if (response.statusCode == 200 || response.statusCode == 201) {
        _fileQueueBox.remove(item.id);
      } else {
        item.transferState = FileTransferState.paused;
        _fileQueueBox.put(item);
      }
    } catch (error) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            _tag,
            "#file.download.${item.localMsgId}: download -> event " +
                "from vzm domain: and message id = ${item.localMsgId} error >> ${error.toString()}");
      }
    }
  }

  String updateIfRequired(FileQueueEntity item, String? downloadUrl) {
    if (reg.hasMatch(downloadUrl!)) {
      downloadUrl = item.downloadUrl! + "/" + item.localMsgId.toString();
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            _tag,
            "#file.download.${item.localMsgId}: download -> event " +
                "from vzm domain: and message id = ${item.localMsgId} and url changed to $downloadUrl");
      }
    }
    return downloadUrl;
  }

  Future<void> updateAttachment(FileQueueEntity item) async {
    Attachment attachment = Attachment(_attachmentModelConverter.getAttachmentStatus(item.transferState), item.mimeType!, null, Uri.parse(item.localUrl!), 0, 0, null);
    attachment.id = item.attachmentId;
    bool result = await _messageRepository.updateAttachment(attachment);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#file.download.${item.localMsgId}: download -> event updateAttachment > result >> $result");
    }
    updateMessageStatus(item);
  }

  Future<void> updateMessageStatus(FileQueueEntity item) async {
    Message? message = await _messageRepository.findMessageById(item.localMsgId);
    if (message != null) {
      message.status = _attachmentModelConverter.getMessageStatus(item.transferState);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#file.download.${item.localMsgId}: updateMessageStatus message.status : ${message.status}");
      }
      await _messageRepository.updateById(message, notifyApp: true);
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#file.download.${item.localMsgId}: updateMessageStatus >> no message found");
      }
    }
  }

}
