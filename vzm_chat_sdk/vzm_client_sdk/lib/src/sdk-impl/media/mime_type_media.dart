/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */


const String textPlain = "text/plain";
const String textHtml = "text/html";

const String textMailTo = "text/mailto";
const String textTel = "text/tel";
const String textNameCard = "text/namecard";
const String textLink = "text/link";

const String textVCalender = "text/x-vcalendar";
const String textXVCard = "text/x-vcard";
const String textVCard = "text/vcard";

const String imageUnspecified = "image/*";
const String imageJpeg = "image/jpeg";
const String imageJpg = "image/jpg";
const String imageGIF = "image/gif";
const String imageWbmp = "image/vnd.wap.wbmp";
const String imageBmp = "image/bmp";
const String imageSVG = "image/sav+xml";
const String imagePng = "image/png";
const String imageMbmp = "image/x-ms-bmp";
const String imageSVGXml = "image/svg+xml";
const String imageXIcon = "image/x-icon";
const String imageHEIF = "image/heif";
const String imageHEIC = "image/heic";
const String imageWebp = "image/webp";

const String audioUnspecified = "audio/*";
const String audioAac = "audio/aac";
const String audioXAac = "audio/x-aac";
const String audioAacAdts = "audio/aac-adts";
const String audioXMsWma = "audio/x-ms-wma";
const String audioAmr = "audio/amr";
const String audioWebAmr = "audio/amr-wb";
const String audioVndqcp = "audio/vnd.qcelp";
const String audioQcelp = "audio/qcelp";
const String audioQcp = "audio/qcp";
const String audioEvrc = "audio/evrc";
const String audioEvrcQcp = "audio/evrc-qcp";
const String audioImeLody = "audio/imelody";
const String audioMID = "audio/mid";
const String audioMIDI = "audio/midi";
const String audioMP3 = "audio/mp3";
const String audioMPEG3 = "audio/mpeg3";
const String audioMPEG = "audio/mpeg";
const String audioMPG = "audio/mpg";
const String audioMPG4 = "audio/mpg";
const String audioMP4 = "audio/mpg4";
const String audioM4A = "audio/m4a";
const String audioXMid = "audio/x-mid";
const String audioXMidi = "audio/x-midi";
const String audioWav = "audio/wav";
const String audioXWav = "audio/x-wav";
const String audioMp4a = "audio/mp4a-latm";
const String audioXMP3 = "audio/x-mp3";
const String audioXMpeg3 = "audio/x-mpeg3";
const String audioXMpeg = "audio/x-mpeg";
const String audioXMpg = "audio/x-mpg";
const String audio3GPP = "audio/3gpp";
const String audio3GPP2 = "audio/3gpp2";
const String audioOGG = "application/ogg";
const String audioSPMIDI = "audio/sp-midi";
const String audioOGGCON = "audio/ogg";
const String audioVndDlna = "audio/vnd.dlna.adts";

const String videoUnspecified = "video/*";
const String video3GPP = "video/3gpp";
const String video3GP = "video/3gp";
const String videoH2632000 = "video/h263-2000";
const String videoMP4ES = "video/mp4v-es";
const String video3G2 = "video/3gpp2";
const String videoH263 = "video/h263";
const String videoH264 = "video/h264";
const String videoMP4 = "video/mp4";
const String videoMPEG = "video/mpeg";
const String videoQuickTime = "video/quicktime";
const String videoAVI = "video/avi";
const String videoInterLeave = "video/x-msvideo";
