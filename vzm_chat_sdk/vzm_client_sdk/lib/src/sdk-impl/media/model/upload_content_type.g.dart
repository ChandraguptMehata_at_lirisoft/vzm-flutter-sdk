// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_content_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UploadContentType _$UploadContentTypeFromJson(Map<String, dynamic> json) =>
    UploadContentType(
      json['contentType'] as String?,
      json['contentSize'] as String?,
    );

Map<String, dynamic> _$UploadContentTypeToJson(UploadContentType instance) =>
    <String, dynamic>{
      'contentType': instance.contentType,
      'contentSize': instance.contentSize,
    };
