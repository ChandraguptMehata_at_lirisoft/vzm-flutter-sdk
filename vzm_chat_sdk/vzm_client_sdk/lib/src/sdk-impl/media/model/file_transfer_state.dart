/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

enum FileTransferState {
  queued,
  running,
  paused,
  completed, //Used in case of file upload to get access to download url for sending the msg
  failed
}
