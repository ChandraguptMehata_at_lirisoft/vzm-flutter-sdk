/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';

part 'upload_content_type.g.dart';

@JsonSerializable()
class UploadContentType {

  @JsonKey(name: 'contentType')
  final String? contentType;

  @JsonKey(name: 'contentSize')
  final String? contentSize;

  UploadContentType(this.contentType, this.contentSize);

  factory UploadContentType.fromJson(Map<String, dynamic> json) =>
      _$UploadContentTypeFromJson(json);

  Map<String, dynamic> toJson() => _$UploadContentTypeToJson(this);

  @override
  String toString() {
    return 'UploadContentType{contentType: $contentType, contentSize: $contentSize}';
  }
}