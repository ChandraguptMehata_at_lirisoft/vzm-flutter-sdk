// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_urls.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaUrls _$MediaUrlsFromJson(Map<String, dynamic> json) => MediaUrls(
      uploadURL: json['uploadURL'] as String?,
      downloadURL: json['downloadURL'] as String?,
    );

Map<String, dynamic> _$MediaUrlsToJson(MediaUrls instance) => <String, dynamic>{
      'uploadURL': instance.uploadURL,
      'downloadURL': instance.downloadURL,
    };
