/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';

part 'media_urls.g.dart';

@JsonSerializable()
class MediaUrls {

  @JsonKey(name: 'uploadURL')
  final String? uploadURL;

  @JsonKey(name: 'downloadURL')
  final String? downloadURL;

  MediaUrls({this.uploadURL, this.downloadURL});

  factory MediaUrls.fromJson(Map<String, dynamic> json) =>
      _$MediaUrlsFromJson(json);

  Map<String, dynamic> toJson() => _$MediaUrlsToJson(this);

  @override
  String toString() {
    return 'MediaUrls{uploadURL: $uploadURL, downloadURL: $downloadURL}';
  }
}