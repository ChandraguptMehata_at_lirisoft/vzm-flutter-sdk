/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:async';
import 'dart:isolate';

import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/conversation_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/message_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';

import '../../../vzm_client_sdk.dart';
import '../app_credential.dart';
import '../app_utils.dart';

class LoadDBTest {
  late AppLogger _logger;
  late ConversationManager _conversationManager;
  late MessageRepositoryImpl _messageRepository;
  late AppCredential _appCredential;
  static const String _tag = "LOAD-TEST";
  late Isolate isolate;

  LoadDBTest() {
    _logger = serviceLocator.get<AppLogger>();
    _logger = serviceLocator.get<AppLogger>();
    _conversationManager = serviceLocator.get<ConversationManager>();
    _messageRepository = serviceLocator.get<MessageRepositoryImpl>();
    _appCredential = serviceLocator.get<AppCredential>();
  }

  Conversation buildConversation(int conversationIndex) {
    Conversation conversation = Conversation();
    conversation.time = DateTime.now().millisecondsSinceEpoch;
    conversation.createdTime = DateTime.now().millisecondsSinceEpoch;
    conversation.serverId = _buildServerId(conversationIndex);
    conversation.groupMembers = _buildGroupMembers();
    conversation.recipients = _buildRecipients(conversationIndex);
    conversation.type = _getConversationType(conversation.recipients!);
    conversation.snippet = "Hello I am snippet of conversation $conversationIndex";
    conversation.creator = null;
    conversation.serverTime = DateTime.now().millisecondsSinceEpoch;
    return conversation;
  }

  String? _buildServerId(int id) {
    var profileMdn = serviceLocator.get<AppCredential>().profileMdn;
    String serverId = AppUtils.getTelChatGroupId(<String>{profileMdn!, id.toString()});
    return serverId;
  }

  List<Contact>? _buildGroupMembers() {
    return null;
  }

  List<Contact>? _buildRecipients(int id) {
    List<Contact> list = <Contact>[];
    Contact contact = Contact("+19253248993");
    contact.name = "Contact ${contact.address}";
    list.add(contact);
    if (id % 5 == 0) {
      Contact contact = Contact("+19253248990");
      contact.name = "Contact ${contact.address}";
      list.add(contact);
    }
    return list;
  }

  ConversationType? _getConversationType(List<Contact> recipients) {
    if (recipients.length > 2) {
      return ConversationType.mmsGroupChat;
    } else {
      return ConversationType.oneToOneChat;
    }
  }

  Future<bool> loadDummyConversation() async {
    return await _execute();
  }

  Future<bool> loadMessages(Conversation conversation) async {
    var future = Future(() {});
    List<Message>messageList = <Message>[];
    for (var messageIndex = 0; messageIndex < 100; messageIndex++) {
      Message message = Message(
          0,
          DateTime.now().millisecondsSinceEpoch,
          MessageType.textMessage,
          MessageStatus.received,
          _getMessageInboundType(messageIndex),
          _getSenderAddress(conversation.recipients!, messageIndex),
          conversation.recipients!,
          conversation.id);
      message.body = "${message.inbound == 0 ? "this message belongs to incoming. all the messages are having same content. "
          "Only difference in message index" : "outgoing"} text message $messageIndex";
      message.serverId = conversation.serverId! + messageIndex.toString();
      message.conversationId = conversation.id;
      messageList.add(message);
    }
    int startingTime = DateTime.now().millisecondsSinceEpoch;
    future = future.then((_) {
      return Future.delayed(const Duration(milliseconds: 1), () async {
        Message insertedMessage = await _messageRepository.addMessages(messageList);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "message insertion difference for conversationId ${insertedMessage.conversationId} is ${DateTime.now().millisecondsSinceEpoch - startingTime}");
        }
      });
    });
    await future;
    if (AppLogger.isDebugEnabled) {
      serviceLocator.get<AppLogger>().debug("Load Test.", "#load.test. message insertion completed for conversation ${conversation.id}");
    }
    return true;
  }

  int _getMessageInboundType(int messageIndex) {
    if (messageIndex % 2 == 0) {
      return 0;
    }
    return 1;
  }

  Contact _getSenderAddress(List<Contact> list, int messageIndex) {
    if (messageIndex % 2 == 0) {
      return list[0];
    }
    return Contact(_appCredential.profileMdn!);
  }

  Future<bool> _execute() async {
    List<Conversation> convList = <Conversation>[];
    for (var conversationIndex = 0; conversationIndex < 100; conversationIndex++) {
      int index = conversationIndex;
      Conversation conversation = buildConversation(index);
      convList.add(conversation);
    }
    await insertConversation(convList);
    return false;
  }

  Future<void> insertConversation(List<Conversation> convList) async {
    int initialTime = DateTime.now().millisecondsSinceEpoch;
    var future = Future(() {});
    for (Conversation conversation in convList) {
      int startingTime = DateTime.now().millisecondsSinceEpoch;
      future = future.then((_) {
        return Future.delayed(const Duration(milliseconds: 1), () async {
          Conversation insertedConversation = await _conversationManager.addConversation(conversation, isLoadDBTest: true);
          await loadMessages(insertedConversation);
          if (AppLogger.isDebugEnabled) {
            _logger.debug(_tag, "conversation insertion difference for serverId ${conversation.serverId} is ${DateTime.now().millisecondsSinceEpoch - startingTime}");
          }
          if (conversation == convList.last) {
            _conversationManager.notifyConversationObjects();
          }
        });
      });
    }
    await future;
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "conversation insertion difference for all conversation is ${DateTime.now().millisecondsSinceEpoch - initialTime}");
    }
  }
}
