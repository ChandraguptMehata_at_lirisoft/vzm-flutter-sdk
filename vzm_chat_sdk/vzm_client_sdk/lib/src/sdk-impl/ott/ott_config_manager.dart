/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */


import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:vzm_client_sdk/src/common/app_channel.dart';
import 'package:vzm_client_sdk/src/common/app_preferences.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/auth_result.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/config.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/configuration.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/config_api.dart';

class OttConfigurationManager {

  String? userAgent;
  static const String USER_AGENT = "USER_AGENT";

  static const String DEFAULT_CONFIG_URL = "https://config.vzmessages.com";
  static const String DEFAULT_AMS_URL = "https://api.vzmessages.com";
  static const String DEFAULT_SYNC_URL = "https://sync.vzmessages.com";
  static const String DEFAULT_MQTT_URL = "https://sync.vzmessages.com";
  static const String DEFAULT_ABS_URL = "https://abs.vzmessages.com";

  static const String META_OTT_SYNC_URL = "ottHandler.sync.url";
  static const String META_OTT_MQTT_URL = "ottHandler.mqtt.url";
  static const String META_OTT_API_URL = "ottHandler.api.url";
  static const String META_OTT_ABS_URL = "ottHandler.abs.url";
  static const String META_OTT_MOJI_URL = "ottHandler.moji.url";

  static const KEY_IMAGE_SIZE_IN_MB = "ottHandler.image.size.in.mb";
  static const KEY_PIN_LONG_CODES = "ottHandler.pin.longcodes";
  static const KEY_SMS_LONG_CODES = "ottHandler.sms.longcodes";
  static const KEY_LONG_CODES_PATTERN = "ottHandler.longcodes.patterns";
  static const KEY_IS_CONFIG_CHANGED = "ottHandler.is.config.changed";
  static const KEY_CONFIG_EXPIRY_TIME = "ottHandler.config.expiry.time";
  static const KEY_MAX_MEDIA_SIZE_IN_MB = "ottHandler.max.media.size.in.mb";
  static const KEY_CONFIG_WEARABLE_HOST = "ottHandler.config.wearable.host";

  static const KEY_SHORTCODE_MAPPINGS = "shortcode.mappings";
  static const KEY_SHORTCODE_MAPPING_NAME = "name";
  static const KEY_SHORTCODE_MAPPING_VALUE = "shortcode";

  static const KEY_WEB_PREVIEW_HOST = "pref_key_web_preview_host";

  static const double DEFAULT_OTT_IMAGE_SIZE = 1.2; //size in mb

  static const double DEFAULT_OTT_VIDEO_SIZE = 100; //size in mb

  static const double DEFAULT_OTT_GIF_SIZE = 5; //size in mb

  late AppPreferences _appPreferences;

  OttConfigurationManager() {
    _appPreferences = serviceLocator.get<AppPreferences>();
  }

  Future<void> setUserAgent() async {
     if (userAgent == null) {
       final String userAgent = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(USER_AGENT, null);
       this.userAgent = userAgent;
     }
  }

  String? getUserAgent() {
    return userAgent;
  }

  Future<bool> saveSyncBaseUrl(String? primaryAmsSyncBaseUri) async {
    print("saveSyncBaseUrl -> url = $primaryAmsSyncBaseUri");
    if (primaryAmsSyncBaseUri != null) {
      return await _appPreferences.put(META_OTT_SYNC_URL, primaryAmsSyncBaseUri);
    }
    return false;
  }

  String getSyncBaseUrl() {
    String? syncBaseUrl = _appPreferences.getString(META_OTT_SYNC_URL);
    print("getSyncBaseUrl -> url = $syncBaseUrl");
    if (syncBaseUrl == null || syncBaseUrl.isEmpty) {
      syncBaseUrl = DEFAULT_SYNC_URL;
    }
    return syncBaseUrl;
  }

  String? getBasicAuthorization() {
    AppCredential appCredential = serviceLocator.get<AppCredential>();
    String? basicAuth = appCredential.constructBasicAuth();
    print("getBasicAuthorization, authorization $basicAuth");
    return basicAuth;
  }

  Future<AuthResult?> downloadConfigurations() async {
    print("#ottHandler.config: downloadConfigurations");
    AuthResult? authResult;
    ConfigApi _configApi = serviceLocator.get<ConfigApi>();
    try {
      HttpResponse<Configuration> configurationResponse = await _configApi.getConfig();
      if (configurationResponse.response.statusCode == 200) {
        Configuration configuration = configurationResponse.data;
        print("#ottHandler.config: downloadConfigurations response ${configuration.toString()}");

        //todo: schedule job for config update
        if (configuration.configList != null) {
          saveConfiguration(configuration.configList);
          authResult = AuthResult(AuthStatus.SUCCESS, "");
        }
      } else {
        print("#ottHandler.config: downloadConfigurations failed with error code: ${configurationResponse.response.statusCode}" +
            "error msg: ${configurationResponse.response.statusMessage}");
        authResult = AuthResult(AuthStatus.FAILED, configurationResponse.response.statusMessage);
      }
    } catch (exception, stacktrace)  {
      print("#ottHandler.config: downloadConfigurations throwing error ${exception.toString()} stacktrace $stacktrace");
      switch (exception.runtimeType) {
        case DioError:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
          break;
        default:
          authResult = AuthResult(AuthStatus.FAILED, exception.toString());
      }
    }
    return authResult;
  }

  void saveConfiguration(List<Config>? configList) {
    print("#ottHandler.config: saveConfiguration configList size ${configList!.length}");
    for (Config config in configList) {
      print("#ottHandler.config: saveConfiguration config ${config.toString()}");
      if (config.isApiHost() && config.getApiHost() != null) {
        if (_appPreferences.getString(META_OTT_API_URL)!.isEmpty  || config.overwrite!) {
          _appPreferences.put(META_OTT_API_URL, config.getApiHost()!);
        }
      } else if (config.isMQTTHost() && config.getMQTTHost() != null) {
        if (_appPreferences.getString(META_OTT_MQTT_URL)!.isEmpty || config.overwrite!) {
          _appPreferences.put(META_OTT_MQTT_URL, config.getMQTTHost()!);
        }
      } else if (config.isAbsHost() && config.getAbsHost() != null) {
        if (_appPreferences.getString(META_OTT_ABS_URL)!.isEmpty || config.overwrite!) {
          _appPreferences.put(META_OTT_ABS_URL, config.getAbsHost()!);
        }
      } else if (config.isMojiHost() && config.getMojiHost() != null) {
        if (_appPreferences.getString(META_OTT_MOJI_URL)!.isEmpty || config.overwrite!) {
          _appPreferences.put(META_OTT_MOJI_URL, config.getMojiHost()!);
        }
      } else if (config.isImageSizeMB() && config.getImageSizeMB() != null) {
        if (_appPreferences.getDouble(KEY_IMAGE_SIZE_IN_MB) == null || config.overwrite!) {
          _appPreferences.putDouble(KEY_IMAGE_SIZE_IN_MB, config.getImageSizeMB() as double);
        }
      } else if (config.isPinLongCodes() && config.getPinLongCodes() != null) {
        List<String>? pinLongCodes = _appPreferences.getStringList(KEY_PIN_LONG_CODES);
        List<String>? configPinCodes = config.getPinLongCodes();
        if ((pinLongCodes == null || pinLongCodes.isEmpty || config.overwrite!) && configPinCodes != null) {
          _appPreferences.putList(KEY_PIN_LONG_CODES, configPinCodes);
        }
      } else if (config.isSmsLongCodes() && config.getSmsLongCodes() != null) {
        var smsLongCodes = _appPreferences.getStringList(KEY_SMS_LONG_CODES);
        if ((smsLongCodes == null || smsLongCodes.isEmpty || config.overwrite!)) {
          _appPreferences.putList(KEY_SMS_LONG_CODES, config.getSmsLongCodes()!);
        }
      } else if (config.isLongCodePatterns()) {
        if (_appPreferences.getStringList(KEY_LONG_CODES_PATTERN) == null) {
          if (config.overwrite! && config.longCodesPatterns() != null) {
            List<String>? configLongCodesPatterns = config.longCodesPatterns();
            if (configLongCodesPatterns != null) {
              _appPreferences.putList(KEY_LONG_CODES_PATTERN, configLongCodesPatterns);
            }
          }
        }
      } else if (config.isMaxMediaSizeMB()) {
        String? configMaxMediaSize = config.getMaxMediaSizeMB();
        if ((_appPreferences.getDouble(KEY_MAX_MEDIA_SIZE_IN_MB) == null || config.overwrite!) && configMaxMediaSize != null) {
          _appPreferences.put(KEY_MAX_MEDIA_SIZE_IN_MB, configMaxMediaSize);
        }
      } else if (config.isWearableHost() && config.getWearableHost() != null) {
        if (_appPreferences.getString(KEY_CONFIG_WEARABLE_HOST)!.isEmpty || config.overwrite!) {
          _appPreferences.put(KEY_CONFIG_WEARABLE_HOST, config.getWearableHost()!);
        }
      } else if (config.isWebpreviewHost() && config.getWebpreviewHost() != null) {
        if (_appPreferences.getString(KEY_WEB_PREVIEW_HOST)!.isEmpty || config.overwrite!) {
          _appPreferences.put(KEY_WEB_PREVIEW_HOST, config.getWebpreviewHost()!);
        }
      } else if (config.isShortCodeNameMappings() && config.shortcodeNameMappings != null) {
        if (_appPreferences.getString(KEY_SHORTCODE_MAPPINGS) == null || _appPreferences.getString(KEY_SHORTCODE_MAPPINGS)!.isEmpty || config.overwrite!) {
          List<Map<String, String>>? configMappings = config.shortcodeNameMappings();
          if (configMappings != null) {
            _appPreferences.put(
                KEY_SHORTCODE_MAPPINGS,
                setShortCodeNameMappings(configMappings)
            );
          }
        }
      } else if (config.isBot() && config.getBot() != null) {
        //chatbotManager.get().saveChatbotConfig(config.getBot()!!)
        //todo: implement chatbot manager
      } else if (config.isTranslations()) {
        // config.getTranslations()?.let { translationManager.get().saveTranslations(it) }
      }
    }
  }

  String setShortCodeNameMappings(List<Map<String, String>>? mappingList) {
    print("setShortCodeNameMappings -> data = $mappingList");
    var shortCodeMapping = Map<String, String>();
    for (var object in mappingList!) {
      shortCodeMapping[object[KEY_SHORTCODE_MAPPING_NAME]!] =
      object[KEY_SHORTCODE_MAPPING_VALUE]!;
    }
    return jsonEncode(shortCodeMapping);
  }

  String getMqttBaseUrl() {
    /*String? mqttBaseUrl = _appPreferences.getString(META_OTT_MQTT_URL);
    return mqttBaseUrl ?? DEFAULT_MQTT_URL;*/
    return "mqtt.vzmessages.com";
  }

  getImageSizeLimit() {
    var sizeInMB = _appPreferences.getDouble(KEY_IMAGE_SIZE_IN_MB);
    sizeInMB ??= DEFAULT_OTT_IMAGE_SIZE;
    return ((sizeInMB * 1024 * 1024).toDouble());
  }

  getFileSizeLimit() {
    var sizeInMB = _appPreferences.getDouble(KEY_MAX_MEDIA_SIZE_IN_MB);
    sizeInMB ??= DEFAULT_OTT_VIDEO_SIZE;
    return ((sizeInMB * 1024 * 1024).toDouble());
  }

  getGifImageSizeLimit() {
    return (DEFAULT_OTT_GIF_SIZE * 1024 * 1024).toDouble();
  }

}