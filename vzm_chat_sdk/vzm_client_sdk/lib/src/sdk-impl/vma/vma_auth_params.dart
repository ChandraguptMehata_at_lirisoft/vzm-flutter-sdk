class VmaAuthParams {
  static const String PARAM_MDN             = "mdn";
  static const String PARAM_IS_PRIMARY      = "isPrimary";
  static const String PARAM_DEVICE_TYPE     = "deviceType";
  static const String PARAM_DEVICE_NAME     = "deviceName";
  static const String PARAM_DEVICE_MAKE     = "deviceMake";
  static const String PARAM_DEVICE_MODEL    = "deviceModel";
  static const String PARAM_DEVICE_ID       = "deviceId";
  static const String PARAM_VERSION         = "version";
  static const String PARAM_PIN             = "pin";
  static const String PARAM_LOGIN_TOKEN     = "loginToken";
  static const String KEY_SERVICE_ID        = "serviceId";
}

