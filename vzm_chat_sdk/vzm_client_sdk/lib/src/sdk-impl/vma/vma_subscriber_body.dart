/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
part 'vma_subscriber_body.g.dart';

@JsonSerializable()
class VmaSubscriberBody {

  String registrationId;

  VmaSubscriberBody({required this.registrationId});

  factory VmaSubscriberBody.fromJson(Map<String, dynamic> json) =>
      _$VmaSubscriberBodyFromJson(json);

  Map<String, dynamic> toJson() => _$VmaSubscriberBodyToJson(this);

}
