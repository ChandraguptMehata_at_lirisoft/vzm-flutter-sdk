// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vma_subscriber_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VmaSubscriberBody _$VmaSubscriberBodyFromJson(Map<String, dynamic> json) =>
    VmaSubscriberBody(
      registrationId: json['registrationId'] as String,
    );

Map<String, dynamic> _$VmaSubscriberBodyToJson(VmaSubscriberBody instance) =>
    <String, dynamic>{
      'registrationId': instance.registrationId,
    };
