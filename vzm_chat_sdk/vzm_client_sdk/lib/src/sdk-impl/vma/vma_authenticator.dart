/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:dio/dio.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';

class VMAAuthenticator extends Interceptor {
  static const String _authorization = "Authorization";
  static const String _tag = "VMAAuthenticator";

  final _ottConfigManager = serviceLocator.get<OttConfigurationManager>();
  final _logger = serviceLocator.get<AppLogger>();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    String? basicAuthorization = _ottConfigManager.getBasicAuthorization();
    if (basicAuthorization != null) {
      options.headers[_authorization] = basicAuthorization;
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#authenticator.vma: request[${options.method}] => PATH: ${options.path} Header ${options.headers}");
    }
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#authenticator.vma: response[${response.statusCode}] => PATH: ${response.requestOptions.path}");
    }
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#authenticator.vma: ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}}");
    }
    return super.onError(err, handler);
  }
}
