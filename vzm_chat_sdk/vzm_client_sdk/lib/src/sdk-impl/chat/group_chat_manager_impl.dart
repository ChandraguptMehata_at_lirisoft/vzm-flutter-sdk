/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:retrofit/retrofit.dart';
import 'package:vzm_client_sdk/src/api/chat/group_chat_manager.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_utils.dart';
import 'package:vzm_client_sdk/src/sdk-impl/chat/model/create_group_request.dart';
import 'package:vzm_client_sdk/src/sdk-impl/chat/upload_connection_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/persistence_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/group_chat_api.dart';

import '../../../vzm_client_sdk.dart';

class GroupChatManagerImpl extends GroupChatManager {

  late final AppLogger _logger;
  late final GroupChatApi _groupApi;
  late final PersistenceManager _persistenceManager;
  late final UploadConnectionManager _uploadConnectionManager = UploadConnectionManager();

  static const String _tag = "GROUP_CHAT_MANAGER";

  GroupChatManagerImpl() {
    _logger = serviceLocator.get<AppLogger>();
    _persistenceManager = serviceLocator.get<PersistenceManager>();
    _groupApi = serviceLocator.get<GroupChatApi>();
  }

  @override
  Future<Conversation?> createClosedGroupChat(List<String> participants, String? name, String? avatar) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#ott.gc.add: createClosedGroupChat:: Admin Group $participants, name = $name, avatar = $avatar");
    }
    if (!AppUtils.isValidMdns(participants.toSet())){
      throw Exception("$participants does not contain valid phone numbers");
    }
    var request = CreateGroupRequest(name, participants, avatar, null, false, true);
    if (avatar != null) {
      // Save the Avatar in local cache and upload
      var url = await _uploadConnectionManager.getS3Url(Uri.parse(avatar));
      if (url != null) {
        request.avatar = url;
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#ott.gc.add: createClosedGroupChat:: s3 upload failed. try again");
        }
        throw Exception("Failed to upload the group avatar");
      }
    }
    try {
      HttpResponse<List<Payload>> httpResponse = await _groupApi.createGroupChat(request);
      if (httpResponse.response.statusCode == 200) {
        var msg = httpResponse.data[0];
        return await _persistenceManager.handleCreateGroupEvent(msg);
      } else {
        throw Exception("Failed to create group conversation");
      }
    } on Exception catch (_e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#ott.gc.add: createClosedGroupChat:: Admin Group exception  ${_e.toString()}");
      }
      throw Exception("Failed to create group conversation: ${_e.toString()}");
    }
  }

  @override
  Future<Conversation> createOpenGroupChat(List<String> participants, String? name, String? avatar) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#ott.gc.add: createOpenGroupChat:: Admin Group $participants, name = $name, avatar = $avatar");
    }
    if (!AppUtils.isValidMdns(participants.toSet())){
      throw Exception("$participants does not contain valid phone numbers");
    }
    var request = CreateGroupRequest(name, participants, avatar, null, false, false);
    if (avatar != null) {
      // Save the Avatar in local cache and upload
      var url = await _uploadConnectionManager.getS3Url(Uri.parse(avatar));
      if (url != null) {
        request.avatar = url;
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#ott.gc.add: createOpenGroupChat:: s3 upload failed. try again");
        }
        throw Exception("Failed to upload the group avatar");
      }
    }
    try {
      HttpResponse<List<Payload>> httpResponse = await _groupApi.createGroupChat(request);
      if (httpResponse.response.statusCode == 200 || httpResponse.response.statusCode == 201) {
        var msg = httpResponse.data[0];
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#ott.gc.add: createOpenGroupChat:: Admin Group response  ${msg.toString()}");
        }
        return await _persistenceManager.handleCreateGroupEvent(msg);
      } else {
        throw Exception("Failed to create group conversation");
      }
    } on Exception catch (_e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#ott.gc.add: createOpenGroupChat:: Admin Group exception  ${_e.toString()}");
      }
      throw Exception("Failed to create group conversation: ${_e.toString()}");
    }
  }

}