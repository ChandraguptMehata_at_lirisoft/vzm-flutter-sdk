/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/api/chat/chat_manager.dart';
import 'package:vzm_client_sdk/src/api/repositories/conversation_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_utils.dart';
import 'package:vzm_client_sdk/src/sdk-impl/chat/upload_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/msg_validator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/file_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_message_sender.dart';

import '../../../vzm_client_sdk.dart';

class ChatManagerImpl extends ChatManager {
  late final AppLogger _logger;
  late final ConversationRepository _conversationRepository;
  late final AppCredential _appCredential;
  late final MessageRepository _messageRepository;
  late final MqttMessageSender _mqttMessageSender;
  late final FileManager _fileManager;
  late final UploadManager _uploadManager;
  final MsgValidator _msgValidator = MsgValidator();

  static const String _tag = "CHAT_MANAGER";

  ChatManagerImpl() {
    _logger = serviceLocator.get<AppLogger>();
    _conversationRepository = serviceLocator.get<ConversationRepository>();
    _appCredential = serviceLocator.get<AppCredential>();
    _messageRepository = serviceLocator.get<MessageRepository>();
    _mqttMessageSender = serviceLocator.get<MqttMessageSender>();
    _fileManager = serviceLocator.get<FileManager>();
    _uploadManager = serviceLocator.get<UploadManager>();
  }

  @override
  Future<Conversation> create(String mdn) async {
    var profileMdn = _appCredential.profileMdn;
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#chat.1x1.add:  to-> $mdn");
    }
    var groupId = AppUtils.getTelChatGroupId(<String>{profileMdn!, mdn});
    var conversation = _conversationRepository.findConversationByServerId(groupId);
    if (conversation != null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#chat.1x1.add.$groupId: conversation found. $conversation");
      }
      return conversation;
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#chat.1x1.add.$groupId: conversation not found. create new");
    }
    //todo: Fetch the public profile and save to database for delivery reports and
    conversation = Conversation();
    conversation.type = ConversationType.oneToOneChat;
    conversation.time = DateTime.now().millisecondsSinceEpoch;
    conversation.recipients = <Contact>[Contact(mdn)];
    conversation.serverId = groupId;
    Conversation result = await _conversationRepository.addConversation(conversation);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#chat.1x1.add.$groupId: conversation created: ${result.toString()}");
    }
    return result;
  }

  @override
  Future<Conversation> createMmsGroupChat(Set<String> recipients) async {
    if (recipients.length > 20){
      throw Exception("participants should not exceed 20");
    }
    var profileMdn = _appCredential.profileMdn;
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#chat.1xM.add: createMmsGroupChat:: from-> $recipients, to-> $profileMdn");
    }

    var mdns = {profileMdn!};
    mdns.addAll(recipients);
    var groupId = AppUtils.getTelChatGroupId(mdns);
    var conversation = _conversationRepository.findConversationByServerId(groupId);
    if (conversation != null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#chat.1xM.add.$groupId: createMmsGroupChat:: conversation found. $conversation");
      }
      return conversation;
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#chat.1xM.add.$groupId: createMmsGroupChat:: conversation not found. create new");
    }
    //todo: Fetch the public profile and save to database for delivery reports and
    conversation = Conversation();
    conversation.type = ConversationType.mmsGroupChat;
    conversation.time = DateTime.now().millisecondsSinceEpoch;
    conversation.recipients = recipients.map((e) => Contact(e)).toList();
    conversation.serverId = groupId;
    Conversation result = await _conversationRepository.addConversation(conversation);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#chat.1xM.add.$groupId: createMmsGroupChat:: conversation created: ${result.toString()}");
    }
    return result;
  }

  @override
  Future<int> deleteChat(int conversationId) {
    // TODO: implement deleteChat
    throw UnimplementedError();
  }

  @override
  Future<int> deleteChats(Set<int> ids) {
    // TODO: implement deleteChats
    throw UnimplementedError();
  }

  @override
  Future<int> deleteMessage(int id) {
    // TODO: implement deleteMessage
    throw UnimplementedError();
  }

  @override
  Future<int> deleteMessages(Set<int> ids) {
    // TODO: implement deleteMessages
    throw UnimplementedError();
  }

  @override
  Future<SendMessageResult> sendMediaMessage(int conversationId, String filePath, {String? body}) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.msg.send.$conversationId:  filePath = $filePath");
    }
    var fileUri = Uri.parse(filePath);
    Conversation? conversation = await _conversationRepository.findConversationByThreadId(conversationId);
    if (conversation == null) {
      throw Exception("Conversation not found");
    }

    var type = getFileMessageType(conversation.type);

    //todo: validate vcard

    FileMetadata meta = await _fileManager.getFileMetadata(fileUri);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.send.msg.${conversation.id}: input file meta data = $meta");
    }

    if (!_msgValidator.isValidMimeType(meta.mimeType)) {
      return SendMessageResult(status: SendMessageStatus.queued, errorMessage: "File mime type ${meta.mimeType} is not supported");
    }

    if (!_msgValidator.isValidFileSize(type, meta.mimeType, meta.size)) {
      return SendMessageResult(status: SendMessageStatus.queued, errorMessage: "File mime type ${meta.mimeType} is not supported");
    }

    Message message = Message(0, DateTime.now().millisecondsSinceEpoch.toInt(), type, MessageStatus.queued,
        Message.outboundMessage, Contact(_appCredential.profileMdn!), conversation.recipients!, conversationId);

    message.body = body;

    var attachment = Attachment(AttachmentStatus.queued, meta.mimeType, fileUri, fileUri, meta.width, meta.height, meta.duration);
    attachment.fileType = _fileManager.getFileType(meta.mimeType);
    attachment.size = meta.size.toDouble();
    message.attachments = <Attachment>[attachment];

    _sendMessage(message, conversation.serverId!, hasAttachment: true);

    return SendMessageResult(status: SendMessageStatus.queued);
  }

  @override
  Future<SendMessageResult> sendMms(int conversationId, String file, {String? subject, String? body}) {
    // TODO: implement sendMediaMessage
    throw UnimplementedError();
  }

  @override
  Future<bool> sendRead(int conversationId, int lastMessageTime) {
    // TODO: implement sendRead
    throw UnimplementedError();
  }

  @override
  Future<SendMessageResult> sendText(int conversationId, String body) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.msg.send.$conversationId:  message = $body");
    }
    Conversation? conversation = await _conversationRepository.findConversationByThreadId(conversationId);
    if (conversation == null) {
      throw Exception("Conversation not found");
    }

    Message message = Message(0, DateTime.now().millisecondsSinceEpoch.toInt(), _getMessageType(conversation.type),
        MessageStatus.queued, Message.outboundMessage, Contact(_appCredential.profileMdn!), conversation.recipients!, conversationId);

    message.body = body;
    return _sendMessage(message, conversation.serverId!);
  }

  MessageType _getMessageType(ConversationType? type) {

    switch (type) {
      case ConversationType.oneToOneChat:
        return MessageType.sms;

      case ConversationType.broadCast:
        return MessageType.sms;

      case ConversationType.mmsGroupChat:
        return MessageType.mms;

      case ConversationType.openGroupChat:
        return MessageType.textMessage;

      case ConversationType.closedGroupChat:
        return MessageType.textMessage;

      default:
        throw Exception("Conversation type is not implemented");
    }
  }

  @override
  MessageType getFileMessageType(ConversationType? type) {

    switch (type) {
      case ConversationType.oneToOneChat:
      case ConversationType.broadCast:
      case ConversationType.mmsGroupChat:
        return MessageType.mms;

      case ConversationType.openGroupChat:
      case ConversationType.closedGroupChat:
        return MessageType.mediaMessage;

      default:
        throw Exception("Conversation type is not implemented");
    }
  }

  Future<SendMessageResult> _sendMessage(Message src, String serverId, {bool hasAttachment = false}) async {
    //Save the msg to App layer Msg repository
    //Create client id, change msg status to Sending and update the msg to Msg repository
    src.status = MessageStatus.queued;
    src = await _messageRepository.addMessage(src);

    src.clientMsgId = _generateClientMsgId(src.id);
    Message? msg  = await _messageRepository.updateById(src);//update the client msg id


    //Update the conversation with latest msg timestamp
    var updateConversation = Conversation();
    updateConversation.id = msg!.conversationId;
    updateConversation.time = msg.time;
    updateConversation.snippet = msg.body;
    Conversation? conversation = await _conversationRepository.updateById(updateConversation);

    if (hasAttachment) {
      // Add to send queue
      var attachmentId = msg.attachments![0].id;
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up:.${msg.id}: sendMessage > attachmentId: $attachmentId");
      }
      _uploadManager.enqueue(msg.id, attachmentId, conversation!.serverId);
      return SendMessageResult(status: SendMessageStatus.queued);
    }

    var result = await _mqttMessageSender.enqueueMessage(msg, serverId);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#mqtt.msg.send.${msg.id}: sendMessage > result: $result");
    }
    SendMessageStatus sendMessageStatus = result ? SendMessageStatus.sent : SendMessageStatus.queued;
    return SendMessageResult(status: sendMessageStatus);
  }

  String? _generateClientMsgId(int id) {
    String devicePrefix = _appCredential.devicePrefix ?? "";
    String clientId = devicePrefix + id.toString().padLeft(7, '0') + '001' + 'M' + 'A';//for tablet a, for pod i, windows 0
    return clientId;
  }

}
