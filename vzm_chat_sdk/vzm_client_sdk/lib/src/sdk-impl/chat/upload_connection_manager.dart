/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:async';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/media_urls.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/upload_content_type.dart';
import 'package:vzm_client_sdk/src/sdk-impl/ott/ott_config_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/group_chat_api.dart';

class UploadConnectionManager {
  late final AppLogger _logger;
  late final GroupChatApi _groupChatApi;

  static const String _tag = "UPLOAD-CONNECTION";

  UploadConnectionManager() {
    _logger = serviceLocator.get<AppLogger>();
    _groupChatApi = serviceLocator.get<GroupChatApi>();
  }

  Future<MediaUrls?> getMediaUrls(String mimeType, double fileSize) async {
    try {
      UploadContentType contentType = UploadContentType(mimeType, fileSize.toInt().toString());
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up: getMediaUrls() , request body : ${contentType.toString()}");
      }
      var response = await _groupChatApi.getMediaUrls(contentType);
      if (response.response.statusCode == 200) {
        MediaUrls mediaUrls = response.data;
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#s3.up: getMediaUrls() mediaUrls >> ${mediaUrls.toString()}");
        }
        return mediaUrls;
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag,
              "#s3.up: getMediaUrls()>> statusCode is not 200. code: ${response.response.statusCode}, status msg : ${response.response.statusMessage}");
        }
      }
    } catch (exception, stacktrace) {
      _logger.debug(_tag, "#s3.up: getMediaUrls() exception >> ${exception.toString()}");
    }
    return null;
  }

  Future<bool> upload(Uri? path, String? uploadUrl, double size, String mimeType, int messageId, {bool isProfileFlow = false}) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          _tag, "#s3.up.$messageId: upload:: path: $path, uploadUrl: $uploadUrl, size: $size, mimeType: $mimeType, messageId: $messageId");
    }
    try {
      var response = await http.put(Uri.parse(uploadUrl!), body: File(path!.path).readAsBytesSync(), headers: getHeaders(mimeType, isProfileFlow));
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up.$messageId: upload:: response: status code: ${response.statusCode}, body: ${response.body}");
      }
      if (response.statusCode == 200) {
        return true;
      }
    } catch (e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up.$messageId: upload:: response: error ${e.toString()}");
      }
    }
    return false;
  }

  getHeaders(String mimeType, bool isProfileFlow) {
    if (isProfileFlow) {
      return {"Content-Type": mimeType};
    }
    return {
      "Authorization": serviceLocator.get<OttConfigurationManager>().getBasicAuthorization()!,
      "User-Agent": serviceLocator.get<OttConfigurationManager>().userAgent!,
      "Content-Type": mimeType
    };
  }

  void publishProgressState(int count, int total, String uploadUrl, int messageId) {
    String progress = (count / total * 100).toStringAsFixed(0);
    var percentage = progress + "%";
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#s3.up:$messageId: progress >> $percentage");
    }
  }

  Future<String?> getS3Url(Uri uri) async {
    var mimeType = mime(uri.path);
    var tempMediaUrls = await _getTempMediaUrls(mimeType);
    if (tempMediaUrls != null) {
      var uploadUrl = tempMediaUrls.uploadURL;
      if (uploadUrl != null) {
         bool result = await upload(uri, uploadUrl, 0, mimeType!, -1, isProfileFlow: true);
         if (result == true) {
           return uploadUrl;
         } else {
           if (AppLogger.isDebugEnabled) {
             _logger.debug(_tag, "#ott.gc.s3.up: getS3Url:: failed while uploading to url: $uploadUrl");
           }
         }
      }
    }
    return null;
  }

  Future<MediaUrls?> _getTempMediaUrls(String? mimeType) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#ott.gc.s3.up: getTempMediaUrls:: mimeType= $mimeType");
    }
    try {
      var response = await _groupChatApi.getTempMediaHandlerUrls(UploadContentType(mimeType, null));
      if (response.response.statusCode == 200) {
        MediaUrls mediaUrls = response.data;
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#ott.gc.s3.up: getTempMediaUrls:: mediaUrls >> ${mediaUrls.toString()}");
        }
        return mediaUrls;
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag,
              "#ott.gc.s3.up: getMediaUrls()>> getTempMediaUrls:: statusCode is not 200. code: ${response.response.statusCode}, status msg : ${response.response.statusMessage}");
        }
      }
    } catch (e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#ott.gc.s3.up: getTempMediaUrls:: exception ${e.toString()}");
      }
    }
  }
}
