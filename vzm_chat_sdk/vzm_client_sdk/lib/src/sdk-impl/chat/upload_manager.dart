/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:vzm_client_sdk/src/common/data/data_connectivity_controller.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/chat/upload_connection_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/database_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/upload_queue_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/media_urls.dart';
import 'package:vzm_client_sdk/src/sdk-impl/mqtt/mqtt_message_sender.dart';

import '../../../objectbox.g.dart';
import '../../../vzm_client_sdk.dart';

class UploadManager {
  late final AppLogger _logger;
  late final MessageRepository _messageRepository;
  late final MqttMessageSender _mqttMessageSender;
  late final Box<UploadQueueEntity> _uploadQueueBox;
  late final UploadConnectionManager _uploadConnectionManager = UploadConnectionManager();
  late DataConnectivityController _dataConnectivityController;

  static const String _tag = "UPLOAD";

  UploadManager() {
    _logger = serviceLocator.get<AppLogger>();
    _messageRepository = serviceLocator.get<MessageRepository>();
    _mqttMessageSender = serviceLocator.get<MqttMessageSender>();
    _uploadQueueBox = serviceLocator.get<DatabaseManager>().getUploadQueueBox();
    _dataConnectivityController = serviceLocator.get<DataConnectivityController>();
    _subscribeForDataConnectivityChange();
  }

  void _subscribeForDataConnectivityChange() {
    _dataConnectivityController.connectionChange.listen((network) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up.subscribeForDataConnectivityChange: network: ${network.toString()}");
      }
      if (network.hasConnected) {
        _execute();
      }
    });
  }

  Future<void> enqueue(int messageId, int attachmentId, String? serverId) async {
    UploadQueueEntity uploadQueueEntity = UploadQueueEntity(messageId, attachmentId);
    uploadQueueEntity.serverId = serverId;
    uploadQueueEntity.type = UploadType.file;
    uploadQueueEntity.state = UploadState.queued;
    _uploadQueueBox.put(uploadQueueEntity);
    await _execute();
  }

  Future<bool> _execute() async {
    List<UploadQueueEntity> uploadQueueList = _uploadQueueBox.getAll();
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#s3.up.: _execute:: uploadQueueList size : ${uploadQueueList.length}");
    }
    for (UploadQueueEntity item in uploadQueueList) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up.${item.id}: _execute:: item: ${item.toString()}");
      }
      if (item.state == UploadState.queued || item.state == UploadState.retryOnNextSession) {
        if (item.type == UploadType.file) {
          UploadState state = await _uploadMessage(item);
          if (AppLogger.isDebugEnabled) {
            _logger.debug(_tag, "#s3.up.${item.id}: _execute:: UploadState: $state");
          }
        }
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#s3.up.${item.id}: status ${item.state}");
        }
      }
    }
    return true;
  }

  Future<UploadState> _uploadMessage(UploadQueueEntity item) async {
    Message? msg = await _messageRepository.findMessageById(item.messageId);
    if (msg != null) {
      msg.status = MessageStatus.uploading;
      _messageRepository.updateById(msg);
      var attachment = msg.attachments![0];
      var mediaUrls = await _uploadConnectionManager.getMediaUrls(attachment.mimeType, attachment.size);
      if (mediaUrls != null) {
        item = _updateItemUrls(mediaUrls, item);
        bool result =
        await _uploadConnectionManager.upload(attachment.path, item.uploadUrl, attachment.size, attachment.mimeType, item.messageId);
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#s3.up.${item.id}: uploadMessage:: result $result");
        }
        if (result) {
          await _handleSuccessCase(item, attachment);
          _sendMessage(item); //send the message
          return UploadState.success;
        } else {
          await _handleFailedCase(item, attachment);
        }
        return UploadState.retryOnNextSession;
      } else {
        await _handleFailedCase(item, attachment);
        return item.state!;
      }
    } else {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#s3.up.${item.id}: uploadMessage:: no message found");
      }
      _uploadQueueBox.remove(item.id);
    }
    return UploadState.failed;
  }

  UploadQueueEntity _updateItemUrls(MediaUrls mediaUrls, UploadQueueEntity item) {
    item.downloadUrl = mediaUrls.downloadURL;
    item.uploadUrl = mediaUrls.uploadURL;
    int id = _uploadQueueBox.put(item);
    return _uploadQueueBox.query(UploadQueueEntity_.id.equals(id)).build().findFirst()!;
  }

  Future<void> _handleSuccessCase(UploadQueueEntity item, Attachment attachment) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#s3.up.${item.messageId}: _handleSuccessCase::");
    }
    _uploadQueueBox.remove(item.id);
    attachment.status = AttachmentStatus.uploaded;
    await _updateAttachmentTable(item, attachment);
  }

  Future<void> _handleFailedCase(UploadQueueEntity item, Attachment attachment) async {
    if (item.state == UploadState.retryOnNextSession) {
      //if its already in this state it means we already retried it. So remove the item from queue
      _uploadQueueBox.remove(item.id);
      attachment.status = AttachmentStatus.uploadFailed;
      _updateAttachmentTable(item, attachment);
    } else {
      item.state = UploadState.retryOnNextSession;
      _uploadQueueBox.put(item);
      attachment.status = AttachmentStatus.uploading;
      _updateAttachmentTable(item, attachment);
    }
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#s3.up.${item.messageId}: upload:: handleFailedCase: state: ${item.state}");
    }
  }

  Future<void> _updateAttachmentTable(UploadQueueEntity item, Attachment attachment) async {
    attachment.serverUrl = item.downloadUrl;
    bool updateStatus = await _messageRepository.updateAttachment(attachment);
    Message? message = await _messageRepository.findMessageById(item.messageId);
    await _messageRepository.updateById(message!, notifyApp: true);
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#s3.up.${item.messageId}: _updateAttachmentTable:: updateStatus: $updateStatus");
    }
  }

  Future<void> _sendMessage(UploadQueueEntity item) async {
    Message? message = await _messageRepository.findMessageById(item.messageId);
    _mqttMessageSender.enqueueMessage(message!, item.serverId!);
  }
}
