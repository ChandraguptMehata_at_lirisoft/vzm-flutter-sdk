// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_group_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateGroupRequest _$CreateGroupRequestFromJson(Map<String, dynamic> json) =>
    CreateGroupRequest(
      json['name'] as String?,
      (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['avatar'] as String?,
      json['background'] as String?,
      json['private'] as bool?,
      json['admin'] as bool?,
    );

Map<String, dynamic> _$CreateGroupRequestToJson(CreateGroupRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'members': instance.members,
      'avatar': instance.avatar,
      'background': instance.background,
      'private': instance.isPrivate,
      'admin': instance.isAdmin,
    };
