/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';

part 'create_group_request.g.dart';

@JsonSerializable()
class CreateGroupRequest {

  CreateGroupRequest(this.name, this.members, this.avatar, this.background, this.isPrivate, this.isAdmin);

  @JsonKey(name: 'name')
  late final String? name;

  @JsonKey(name: 'members')
  late final List<String>? members;

  @JsonKey(name: 'avatar')
  late final String? avatar;

  @JsonKey(name: 'background')
  late final String? background;

  @JsonKey(name: 'private')
  late bool? isPrivate;

  @JsonKey(name: 'admin')
  late bool? isAdmin;

  factory CreateGroupRequest.fromJson(Map<String, dynamic> json) => _$CreateGroupRequestFromJson(json);

  Map<String, dynamic> toJson() => _$CreateGroupRequestToJson(this);

}