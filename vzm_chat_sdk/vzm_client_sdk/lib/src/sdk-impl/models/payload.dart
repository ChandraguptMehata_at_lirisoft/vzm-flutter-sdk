/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload_object.dart';

part 'payload.g.dart';

@JsonEnum()
enum PayloadType {
  @JsonValue("SentReceiptEvent")
  sentReceiptEvent,

  @JsonValue("MediaMessage")
  mediaMessage,

  @JsonValue("DeleteConversationEvent")
  deleteConversationEvent,

  @JsonValue("ReadConversationEvent")
  readConversationEvent,

  @JsonValue("TypingIndicatorEvent")
  typingIndicatorEvent,

  @JsonValue("RemoveFromGroupEvent")
  removeFromGroupEvent,

  @JsonValue("InviteToGroupEvent")
  inviteToGroupEvent,

  @JsonValue("GroupUpdateEvent")
  groupUpdateEvent,

  @JsonValue("CreateGroupEvent")
  createGroupEvent,

  @JsonValue("TextMessage")
  textMessage,

  @JsonValue("MsgDeliveredEvent")
  msgDeliveredEvent,

  @JsonValue("MsgDeliveryFailedEvent")
  msgDeliveryFailedEvent,

  @JsonValue("MsgDeletedEvent")
  msgDeletedEvent,

  @JsonValue("SubscriberUpdateEvent")
  subscriberUpdateEvent,

  @JsonValue("MessageCommentEvent")
  messageCommentEvent,

  @JsonValue("SubscriberDeletedEvent")
  subscriberDeletedEvent,

  @JsonValue("RecallMessageEvent")
  unknown,

// Telephony payload
  @JsonValue("SmsMessage")
  smsMessage, // Send/receive SMS message over OTT

  @JsonValue("MmsMessage")
  mmsMessage, // Send/receive MMS message over OTT

  @JsonValue("XmsMsgDeliveredEvent")
  xmsMsgDeliveredEvent,

  @JsonValue("XmsMsgDeliveryFailedEvent")
  xmsMsgDeliveryFailedEvent,

  @JsonValue("SystemMessage")
  systemMessage,

  @JsonValue("XmsTypingIndicatorEvent")
  xmsTypingIndicatorEvent,

  @JsonValue("BotMessage")
  botMessage,

  @JsonValue("BotMessageReply")
  botMessageReply,

//RCS Message Payloads

  @JsonValue("RcsTextMessage")
  rcsTextMessage,

  @JsonValue("RcsMediaMessage")
  rcsMediaMessage,

  @JsonValue("RcsLocationMessage")
  rcsLocationMessage,

  @JsonValue("RcsGroupTextMessage")
  rcsGroupTextMessage,

  @JsonValue("RcsGroupMediaMessage")
  rcsGroupMediaMessage,

  @JsonValue("RcsGroupLocationMessage")
  rcsGroupLocationMessage,

  @JsonValue("RcsGroupTypingIndicatorEvent")
  rcsGroupTypingIndicatorEvent,

  @JsonValue("RcsSentReceiptEvent")
  rcsSentReceiptEvent,

  @JsonValue("RcsMsgDeliveredEvent")
  rcsMsgDeliveredEvent,

  @JsonValue("RcsMsgReadEvent")
  rcsMsgReadEvent,

  @JsonValue("RcsMsgDeliveryFailedEvent")
  rcsMsgDeliveryFailedEvent,

  @JsonValue("RcsTypingIndicatorEvent")
  rcsTypingIndicatorEvent,

  @JsonValue("RcsCreateGroupEvent")
  rcsCreateGroupEvent,

  @JsonValue("RcsGroupUpdateEvent")
  rcsGroupUpdateEvent,

  @JsonValue("RcsRemoveFromGroupEvent")
  rcsRemoveFromGroupEvent,

  @JsonValue("RcsGroupMsgReadEvent")
  rcsGroupMsgReadEvent,

  @JsonValue("RcsCapabilitiesEvent")
  rcsCapabilitiesEvent,

  @JsonValue("OpenConversationEvent") //todo: Need to crosse verify whether this mapping is correct or not
  rcsOpenConversationEvent,
}

@JsonSerializable()
class Payload {
  late PayloadType type;
  late PayloadObject object;

  @JsonKey(name: "deleted")
  bool? deleted;

  Payload(this.type, this.object);

  factory Payload.fromJson(Map<String, dynamic> json) =>
      _$PayloadFromJson(json);

  Map<String, dynamic> toJson() => _$PayloadToJson(this);

  @override
  String toString() {
    return 'Payload{type: $type, object: ${object.toString()}, deleted: $deleted}';
  }
}
