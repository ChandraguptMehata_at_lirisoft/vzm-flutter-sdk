/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';

@JsonEnum()
enum MediaItemType {
  @JsonValue("IMAGE")
  image,
  @JsonValue("VIDEO")
  video,
  @JsonValue("LOCATION")
  location,
  @JsonValue("AUDIO")
  audio,
  @JsonValue("VCARD")
  vcard,
  @JsonValue("UNKNOWN")
  unknown
}
