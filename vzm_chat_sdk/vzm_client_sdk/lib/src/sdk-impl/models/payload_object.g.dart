// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payload_object.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PayloadObject _$PayloadObjectFromJson(Map<String, dynamic> json) =>
    PayloadObject(
      json['id'] as String,
      json['senderId'] as String?,
      json['createdTime'] as int?,
    )
      ..ottProfile = json['ottProfile'] == null
          ? null
          : OttProfile.fromJson(json['ottProfile'] as Map<String, dynamic>)
      ..recipients = (json['recipients'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..source = json['source'] as String?
      ..groupId = json['groupId'] as String?
      ..text = json['text'] as String?
      ..clientMsgId = json['clientMsgId'] as String?
      ..attachments = (json['payload'] as List<dynamic>?)
          ?.map((e) => MediaPayload.fromJson(e as Map<String, dynamic>))
          .toList()
      ..group = json['group'] == null
          ? null
          : GroupChat.fromJson(json['group'] as Map<String, dynamic>)
      ..changedFields = (json['changedFields'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList()
      ..recipientMdns = (json['recipientMdns'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..senderMdn = json['senderMdn'] as String?
      ..whom =
          (json['whom'] as List<dynamic>?)?.map((e) => e as String).toList()
      ..updatedTime = json['groupUpdateTime'] as int?
      ..sentMsgId = json['sentMsgId'] as String?
      ..sentMsgTime = json['sentMsgTime'] as int?
      ..messageIds = (json['messageIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..timeOfLastMsgSeen = json['timeOfLastMsgSeen'] as int?
      ..creatorId = json['creatorId'] as String?
      ..parentMsgId = json['msgId'] as String?
      ..deleted = json['deleted'] as bool?;

Map<String, dynamic> _$PayloadObjectToJson(PayloadObject instance) =>
    <String, dynamic>{
      'id': instance.id,
      'senderId': instance.senderId,
      'createdTime': instance.createdTime,
      'ottProfile': instance.ottProfile,
      'recipients': instance.recipients,
      'source': instance.source,
      'groupId': instance.groupId,
      'text': instance.text,
      'clientMsgId': instance.clientMsgId,
      'payload': instance.attachments,
      'group': instance.group,
      'changedFields': instance.changedFields,
      'recipientMdns': instance.recipientMdns,
      'senderMdn': instance.senderMdn,
      'whom': instance.whom,
      'groupUpdateTime': instance.updatedTime,
      'sentMsgId': instance.sentMsgId,
      'sentMsgTime': instance.sentMsgTime,
      'messageIds': instance.messageIds,
      'timeOfLastMsgSeen': instance.timeOfLastMsgSeen,
      'creatorId': instance.creatorId,
      'msgId': instance.parentMsgId,
      'deleted': instance.deleted,
    };

MediaPayload _$MediaPayloadFromJson(Map<String, dynamic> json) => MediaPayload(
      json['type'] as String?,
      json['data'] as String?,
    );

Map<String, dynamic> _$MediaPayloadToJson(MediaPayload instance) =>
    <String, dynamic>{
      'type': instance.type,
      'data': instance.data,
    };
