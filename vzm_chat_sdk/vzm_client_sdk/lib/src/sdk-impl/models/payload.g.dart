// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payload.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Payload _$PayloadFromJson(Map<String, dynamic> json) => Payload(
      $enumDecode(_$PayloadTypeEnumMap, json['type']),
      PayloadObject.fromJson(json['object'] as Map<String, dynamic>),
    )..deleted = json['deleted'] as bool?;

Map<String, dynamic> _$PayloadToJson(Payload instance) => <String, dynamic>{
      'type': _$PayloadTypeEnumMap[instance.type],
      'object': instance.object,
      'deleted': instance.deleted,
    };

const _$PayloadTypeEnumMap = {
  PayloadType.sentReceiptEvent: 'SentReceiptEvent',
  PayloadType.mediaMessage: 'MediaMessage',
  PayloadType.deleteConversationEvent: 'DeleteConversationEvent',
  PayloadType.readConversationEvent: 'ReadConversationEvent',
  PayloadType.typingIndicatorEvent: 'TypingIndicatorEvent',
  PayloadType.removeFromGroupEvent: 'RemoveFromGroupEvent',
  PayloadType.inviteToGroupEvent: 'InviteToGroupEvent',
  PayloadType.groupUpdateEvent: 'GroupUpdateEvent',
  PayloadType.createGroupEvent: 'CreateGroupEvent',
  PayloadType.textMessage: 'TextMessage',
  PayloadType.msgDeliveredEvent: 'MsgDeliveredEvent',
  PayloadType.msgDeliveryFailedEvent: 'MsgDeliveryFailedEvent',
  PayloadType.msgDeletedEvent: 'MsgDeletedEvent',
  PayloadType.subscriberUpdateEvent: 'SubscriberUpdateEvent',
  PayloadType.messageCommentEvent: 'MessageCommentEvent',
  PayloadType.subscriberDeletedEvent: 'SubscriberDeletedEvent',
  PayloadType.unknown: 'RecallMessageEvent',
  PayloadType.smsMessage: 'SmsMessage',
  PayloadType.mmsMessage: 'MmsMessage',
  PayloadType.xmsMsgDeliveredEvent: 'XmsMsgDeliveredEvent',
  PayloadType.xmsMsgDeliveryFailedEvent: 'XmsMsgDeliveryFailedEvent',
  PayloadType.systemMessage: 'SystemMessage',
  PayloadType.xmsTypingIndicatorEvent: 'XmsTypingIndicatorEvent',
  PayloadType.botMessage: 'BotMessage',
  PayloadType.botMessageReply: 'BotMessageReply',
  PayloadType.rcsTextMessage: 'RcsTextMessage',
  PayloadType.rcsMediaMessage: 'RcsMediaMessage',
  PayloadType.rcsLocationMessage: 'RcsLocationMessage',
  PayloadType.rcsGroupTextMessage: 'RcsGroupTextMessage',
  PayloadType.rcsGroupMediaMessage: 'RcsGroupMediaMessage',
  PayloadType.rcsGroupLocationMessage: 'RcsGroupLocationMessage',
  PayloadType.rcsGroupTypingIndicatorEvent: 'RcsGroupTypingIndicatorEvent',
  PayloadType.rcsSentReceiptEvent: 'RcsSentReceiptEvent',
  PayloadType.rcsMsgDeliveredEvent: 'RcsMsgDeliveredEvent',
  PayloadType.rcsMsgReadEvent: 'RcsMsgReadEvent',
  PayloadType.rcsMsgDeliveryFailedEvent: 'RcsMsgDeliveryFailedEvent',
  PayloadType.rcsTypingIndicatorEvent: 'RcsTypingIndicatorEvent',
  PayloadType.rcsCreateGroupEvent: 'RcsCreateGroupEvent',
  PayloadType.rcsGroupUpdateEvent: 'RcsGroupUpdateEvent',
  PayloadType.rcsRemoveFromGroupEvent: 'RcsRemoveFromGroupEvent',
  PayloadType.rcsGroupMsgReadEvent: 'RcsGroupMsgReadEvent',
  PayloadType.rcsCapabilitiesEvent: 'RcsCapabilitiesEvent',
  PayloadType.rcsOpenConversationEvent: 'OpenConversationEvent',
};
