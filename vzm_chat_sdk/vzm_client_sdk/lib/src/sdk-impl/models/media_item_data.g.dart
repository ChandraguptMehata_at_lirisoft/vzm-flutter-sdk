// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_item_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaItemData _$MediaItemDataFromJson(Map<String, dynamic> json) =>
    MediaItemData(
      json['url'] as String?,
      json['thumbnailUri'] as String?,
      json['thumbnail'] as String?,
      json['mimeType'] as String?,
      json['height'] as int?,
      json['width'] as int?,
      json['size'] as int?,
      (json['duration'] as num?)?.toDouble(),
      json['fileName'] as String?,
    );

Map<String, dynamic> _$MediaItemDataToJson(MediaItemData instance) =>
    <String, dynamic>{
      'url': instance.url,
      'thumbnailUri': instance.thumbnailUri,
      'thumbnail': instance.thumbnail,
      'mimeType': instance.mimeType,
      'height': instance.height,
      'width': instance.width,
      'size': instance.size,
      'duration': instance.duration,
      'fileName': instance.fileName,
    };

AttachmentData _$AttachmentDataFromJson(Map<String, dynamic> json) =>
    AttachmentData(
      json['url'] as String?,
      json['thumbnail'] as String?,
      json['mimeType'] as String?,
      json['height'] as int?,
      json['width'] as int?,
      json['size'] as int?,
      json['duration'] as int?,
      json['fileName'] as String?,
    );

Map<String, dynamic> _$AttachmentDataToJson(AttachmentData instance) =>
    <String, dynamic>{
      'url': instance.url,
      'thumbnail': instance.thumbnail,
      'mimeType': instance.mimeType,
      'height': instance.height,
      'width': instance.width,
      'size': instance.size,
      'duration': instance.duration,
      'fileName': instance.fileName,
    };
