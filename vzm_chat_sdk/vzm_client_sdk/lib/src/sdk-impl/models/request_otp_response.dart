/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */


class RequestOtpResponse {
  var statusInfo;

  RequestOtpResponse({required this.status, this.statusInfo});

  late String status;
  late bool isOk = false;
  late bool isError = false;
  late bool isFailed = false;
  late bool isVBlock = false;
  late bool isSuspended = false;
  late bool isNonVZUser = false;
  late bool isNotEligible = false;
  late bool isUserBENotEligible = false;
  late bool isRequestOverLimit = false;
  late bool isUserNoText = false;

  RequestOtpResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json.containsKey("statusInfo")) {
      statusInfo = json['statusInfo'];
    }

    status == StatusHolder.STATUS_OK ? isOk = true : isOk = false;
    status == StatusHolder.STATUS_FAIL ? isFailed = true : isFailed = false;
    status == StatusHolder.STATUS_ERROR ? isError = true : isError = false;
    status == StatusHolder.STATUS_V_BLOCK ? isVBlock = true : isVBlock = false;
    status == StatusHolder.STATUS_SUSPENDED
        ? isSuspended = true
        : isSuspended = false;
    status == StatusHolder.STATUS_NOT_VZW_MDN
        ? isNonVZUser = true
        : isNonVZUser = false;
    status == StatusHolder.STATUS_NOT_ELIGIBLE
        ? isNotEligible = true
        : isNotEligible = false;
    status == StatusHolder.STATUS_BE_NOT_ELIGIBLE
        ? isUserBENotEligible = true
        : isUserBENotEligible = false;
    status == StatusHolder.STATUS_OVER_LIMIT
        ? isRequestOverLimit = true
        : isRequestOverLimit = false;
    status == StatusHolder.STATUS_NO_TEXT
        ? isUserNoText = true
        : isUserNoText = false;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['status'] = status;

    if (_data.containsKey("statusInfo")) {
      _data['statusInfo'] = statusInfo;
    }
    return _data;
  }

  @override
  String toString() {
    return 'RequestotpResponse{statusInfo: $statusInfo, status: $status, isOk: $isOk, isError: $isError, isFailed: $isFailed, isVBlock: $isVBlock, isSuspended: $isSuspended, isNonVZUser: $isNonVZUser, isNotEligible: $isNotEligible, isUserBENotEligible: $isUserBENotEligible, isRequestOverLimit: $isRequestOverLimit, isUserNoText: $isUserNoText}';
  }
}

class StatusHolder {
  static final String STATUS_FAIL = "FAIL";
  static final String STATUS_ERROR = "ERROR";
  static final String STATUS_V_BLOCK = "VBLOCK";
  static final String STATUS_SUSPENDED = "SUSPENDED";
  static final String STATUS_OK = "OK";
  static final String STATUS_NOT_VZW_MDN = "NOTVZWMDN";
  static final String STATUS_NOT_ELIGIBLE = "NOTELIGIBLE";
  static final String STATUS_BE_NOT_ELIGIBLE = "BE_NOT_ELIGIBLE";
  static final String STATUS_OVER_LIMIT = "OVERLIMIT";
  static final String STATUS_NO_TEXT = "NOTEXT";
}
