// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ott_profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OttProfile _$OttProfileFromJson(Map<String, dynamic> json) => OttProfile(
      json['id'] as String,
      json['mdn'] as String,
      json['createdTime'] as int?,
      json['updatedTime'] as int?,
    )
      ..name = json['name'] as String?
      ..avatar = json['avatar'] as String?
      ..provisioned = json['provisioned'] as bool?
      ..disable1to1 = json['disable1to1'] as bool?
      ..xmsInAmsStartTime = json['xmsInAmsStartTime'] as int?
      ..birthday = json['birthday'] as String?
      ..email = json['email'] as String?;

Map<String, dynamic> _$OttProfileToJson(OttProfile instance) =>
    <String, dynamic>{
      'id': instance.id,
      'mdn': instance.mdn,
      'createdTime': instance.createdTime,
      'updatedTime': instance.updatedTime,
      'name': instance.name,
      'avatar': instance.avatar,
      'provisioned': instance.provisioned,
      'disable1to1': instance.disable1to1,
      'xmsInAmsStartTime': instance.xmsInAmsStartTime,
      'birthday': instance.birthday,
      'email': instance.email,
    };
