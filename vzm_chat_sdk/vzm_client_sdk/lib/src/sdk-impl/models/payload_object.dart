/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'dart:core';

import 'package:json_annotation/json_annotation.dart';

import 'group.dart';
import 'ott_profile.dart';

part 'payload_object.g.dart';

@JsonSerializable()
class PayloadObject {

  String id;

  @JsonKey(name: "senderId")
  String? senderId;

  @JsonKey(name: "createdTime")
  int? createdTime;

  PayloadObject(this.id, this.senderId, this.createdTime);

  OttProfile? ottProfile;

  List<String>? recipients;

  String? source;
  String? groupId;
  String? text;
  String? clientMsgId;

  @JsonKey(name: 'payload')
  List<MediaPayload>? attachments;

  @JsonKey(name: 'group')
  GroupChat? group;
  List<int>? changedFields;
  List<String>? recipientMdns;

  String? senderMdn;

  @JsonKey(name: 'whom')
  List<String>? whom;

  @JsonKey(name: 'groupUpdateTime')
  int? updatedTime = 0;

  //SentReceiptEvent
  @JsonKey(name: "sentMsgId")
  String? sentMsgId;

  @JsonKey(name: "sentMsgTime")
  int? sentMsgTime;

  //MsgDeliveredEvent, XmsMsgDeliveredEvent, MsgDeletedEvent over Mqtt channel
  @JsonKey(name: "messageIds")
  List<String>? messageIds;

  //ReadConversationEvent
  @JsonKey(name: "timeOfLastMsgSeen")
  int? timeOfLastMsgSeen;

  @JsonKey(name: "creatorId")
  String? creatorId;

  @JsonKey(name: "msgId")
  String? parentMsgId;

  @JsonKey(name: "deleted")
  bool? deleted;

  factory PayloadObject.fromJson(Map<String, dynamic> json) =>
      _$PayloadObjectFromJson(json);

  Map<String, dynamic> toJson() => _$PayloadObjectToJson(this);

  String? getSenderIdOrMdn() {
    if (senderId != null) {
      return senderId;
    } else {
      return senderMdn;
    }
  }

  @override
  String toString() {
    return 'PayloadObject{id: $id, ottProfile: $ottProfile, recipients: $recipients, source: $source, groupId: $groupId, text: $text, clientMsgId: $clientMsgId, attachments: $attachments, group: $group, changedFields: $changedFields, recipientMdns: $recipientMdns, senderMdn: $senderMdn, whom: $whom, updatedTime: $updatedTime, sentMsgId: $sentMsgId, sentMsgTime: $sentMsgTime, messageIds: $messageIds, timeOfLastMsgSeen: $timeOfLastMsgSeen, creatorId: $creatorId, parentMsgId: $parentMsgId, deleted: $deleted}';
  }

  //changedFields for Group update events
  static const int FIELD_GROUP_NAME          = 1;
  static const int FIELD_GROUP_PARTICIPANTS  = 2;
  static const int FIELD_GROUP_AVATAR        = 3;
  static const int FIELD_GROUP_BACKGROUND    = 4;
  static const int FIELD_GROUP_ADMIN         = 5;
}

@JsonSerializable()
class MediaPayload {
  String? type;
  String? data;

  MediaPayload(this.type, this.data);

  factory MediaPayload.fromJson(Map<String, dynamic> json) =>
      _$MediaPayloadFromJson(json);

  Map<String, dynamic> toJson() => _$MediaPayloadToJson(this);

  @override
  String toString() {
    return 'MediaPayload{type: $type, data: $data}';
  }
}
