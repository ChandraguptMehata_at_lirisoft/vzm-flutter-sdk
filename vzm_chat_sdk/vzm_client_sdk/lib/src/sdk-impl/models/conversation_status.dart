/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';
part 'conversation_status.g.dart';

@JsonSerializable()
class ReadConversationStatus {
  String member;
  int time;

  ReadConversationStatus(this.member, this.time);

  factory ReadConversationStatus.fromJson(Map<String, dynamic> json) =>
      _$ReadConversationStatusFromJson(json);

  Map<String, dynamic> toJson() => _$ReadConversationStatusToJson(this);
}

@JsonSerializable()
class DeleteConversationStatus {
  String member;
  int time;

  DeleteConversationStatus(this.member, this.time);

  factory DeleteConversationStatus.fromJson(Map<String, dynamic> json) =>
      _$DeleteConversationStatusFromJson(json);

  Map<String, dynamic> toJson() => _$DeleteConversationStatusToJson(this);
}
