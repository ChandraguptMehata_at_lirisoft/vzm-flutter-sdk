// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'conversation_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadConversationStatus _$ReadConversationStatusFromJson(
        Map<String, dynamic> json) =>
    ReadConversationStatus(
      json['member'] as String,
      json['time'] as int,
    );

Map<String, dynamic> _$ReadConversationStatusToJson(
        ReadConversationStatus instance) =>
    <String, dynamic>{
      'member': instance.member,
      'time': instance.time,
    };

DeleteConversationStatus _$DeleteConversationStatusFromJson(
        Map<String, dynamic> json) =>
    DeleteConversationStatus(
      json['member'] as String,
      json['time'] as int,
    );

Map<String, dynamic> _$DeleteConversationStatusToJson(
        DeleteConversationStatus instance) =>
    <String, dynamic>{
      'member': instance.member,
      'time': instance.time,
    };
