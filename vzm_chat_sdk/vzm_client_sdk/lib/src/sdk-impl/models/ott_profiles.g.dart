// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ott_profiles.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OttProfiles _$OttProfilesFromJson(Map<String, dynamic> json) => OttProfiles(
      json['id'] as String?,
      json['profile'] == null
          ? null
          : OttProfile.fromJson(json['profile'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OttProfilesToJson(OttProfiles instance) =>
    <String, dynamic>{
      'id': instance.id,
      'profile': instance.profile,
    };
