// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profiles.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PublicProfiles _$PublicProfilesFromJson(Map<String, dynamic> json) =>
    PublicProfiles(
      json['id'] as String,
      json['profile'] == null
          ? null
          : PublicProfile.fromJson(json['profile'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PublicProfilesToJson(PublicProfiles instance) =>
    <String, dynamic>{
      'id': instance.id,
      'profile': instance.profile,
    };

PublicProfile _$PublicProfileFromJson(Map<String, dynamic> json) =>
    PublicProfile(
      json['id'] as String,
      json['mdn'] as String,
      json['createdTime'] as int?,
      json['updatedTime'] as int?,
    )
      ..name = json['name'] as String?
      ..avatar = json['avatar'] as String?
      ..provisioned = json['provisioned'] as bool?
      ..disable1to1 = json['disable1to1'] as bool?
      ..xmsInAmsStartTime = json['xmsInAmsStartTime'] as int?;

Map<String, dynamic> _$PublicProfileToJson(PublicProfile instance) =>
    <String, dynamic>{
      'id': instance.id,
      'mdn': instance.mdn,
      'name': instance.name,
      'avatar': instance.avatar,
      'createdTime': instance.createdTime,
      'updatedTime': instance.updatedTime,
      'provisioned': instance.provisioned,
      'disable1to1': instance.disable1to1,
      'xmsInAmsStartTime': instance.xmsInAmsStartTime,
    };
