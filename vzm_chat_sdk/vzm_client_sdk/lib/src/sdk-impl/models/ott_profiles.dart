/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/ott_profile.dart';

import 'ott_profile.dart';

part 'ott_profiles.g.dart';

@JsonSerializable()
class OttProfiles {
  String? id;
  OttProfile? profile;

  OttProfiles(this.id, this.profile);

  factory OttProfiles.fromJson(Map<String, dynamic> json) =>
      _$OttProfilesFromJson(json);

  Map<String, dynamic> toJson() => _$OttProfilesToJson(this);
}
