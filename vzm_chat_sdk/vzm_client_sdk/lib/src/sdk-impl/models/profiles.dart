/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:json_annotation/json_annotation.dart';

part 'profiles.g.dart';

@JsonSerializable()
class PublicProfiles {
  String id;
  PublicProfile? profile;

  PublicProfiles(this.id, this.profile);

  factory PublicProfiles.fromJson(Map<String, dynamic> json) =>
      _$PublicProfilesFromJson(json);

  Map<String, dynamic> toJson() => _$PublicProfilesToJson(this);
}
//"profile": {
//    "id": "d1e8efda1000541c265977fed5",
//    "mdn": "+16692600417",
//    "name": "MDN 0417",
//    "avatar": "https://vzw-avatarbackground-or.s3-us-west-2.amazonaws.com/2e95cec53f9726e40d53af0367793fefe4a82a64?AWSAccessKeyId=AKIAJNYE3MTK6KJDKSRA&Expires=2098067665&Signature=AjlJwcSzXiePzx1YW2a7Bc%2FwW3g%3D",
//    "createdTime": 1625053132784,
//    "provisioned": true,
//    "updatedTime": 1625054342576,
//    "disable1to1": true,
//    "xmsInAmsStartTime": 0
//}

@JsonSerializable()
class PublicProfile {
  String id;
  String mdn;
  String? name;
  String? avatar;
  int? createdTime;
  int? updatedTime;
  bool? provisioned;
  bool? disable1to1;
  int? xmsInAmsStartTime;
  PublicProfile(this.id, this.mdn, this.createdTime, this.updatedTime);

  factory PublicProfile.fromJson(Map<String, dynamic> json) =>
      _$PublicProfileFromJson(json);

  Map<String, dynamic> toJson() => _$PublicProfileToJson(this);
}
