/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';

part 'media_item_data.g.dart';

@JsonSerializable()
class MediaItemData {

  String? url;
  String? thumbnailUri;
  String? thumbnail;
  String? mimeType;
  int? height;
  int? width;
  int? size;
  double? duration;
  String? fileName;

  MediaItemData(this.url, this.thumbnailUri, this.thumbnail, this.mimeType, this.height, this.width, this.size, this.duration, this.fileName);

  factory MediaItemData.fromJson(Map<String, dynamic> json) =>
      _$MediaItemDataFromJson(json);

  Map<String, dynamic> toJson() => _$MediaItemDataToJson(this);
}

@JsonSerializable()
class AttachmentData {

  String? url;
  String? thumbnail;
  String? mimeType;
  int? height;
  int? width;
  int? size;
  int? duration;
  String? fileName;

  AttachmentData(this.url, this.thumbnail, this.mimeType, this.height, this.width, this.size, this.duration, this.fileName);

  factory AttachmentData.fromJson(Map<String, dynamic> json) =>
      _$AttachmentDataFromJson(json);

  Map<String, dynamic> toJson() => _$AttachmentDataToJson(this);

}
