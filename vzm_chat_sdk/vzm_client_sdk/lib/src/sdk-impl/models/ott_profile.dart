/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';

part 'ott_profile.g.dart';

@JsonSerializable()
class OttProfile {
  String id;
  String mdn;
  int? createdTime;
  int? updatedTime;
  String? name;
  String? avatar;
  bool? provisioned;
  bool? disable1to1;
  int? xmsInAmsStartTime;
  String? birthday;
  String? email;

  OttProfile(this.id, this.mdn, this.createdTime, this.updatedTime);

  factory OttProfile.fromJson(Map<String, dynamic> json) =>
      _$OttProfileFromJson(json);

  Map<String, dynamic> toJson() => _$OttProfileToJson(this);
}
