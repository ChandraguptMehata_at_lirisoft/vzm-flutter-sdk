/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/profiles.dart';

part 'group.g.dart';

@JsonSerializable()
class GroupChat {
  String id;
  int? createdTime;
  int updatedTime;
  List<String>? members;
  String? creatorId;

  String? name;
  String? avatar;
  String? background;

  @JsonKey(name: 'profiles')
  List<PublicProfiles>? profiles;
  bool? admin = false;

  @JsonKey(name: 'private')
  bool? isPrivate = false;

  List<String>? adminIds;
  List<String>? membersAdded;
  List<String>? membersRemoved;
  List<String>? promotedAdminIds;
  List<String>? demotedAdminIds;

  GroupChat(this.id, this.createdTime, this.updatedTime, this.members,
      this.creatorId, this.admin, this.isPrivate,
      {this.name,
      this.avatar,
      this.background,
      this.profiles,
      this.adminIds,
      this.membersAdded,
      this.membersRemoved,
      this.promotedAdminIds,
      this.demotedAdminIds});

  factory GroupChat.fromJson(Map<String, dynamic> json) =>
      _$GroupChatFromJson(json);

  Map<String, dynamic> toJson() => _$GroupChatToJson(this);
}

@JsonSerializable()
class GroupAndProfiles {
  List<GroupInfo> groupInfo;
  List<PublicProfiles>? memberInfo;

  GroupAndProfiles(this.groupInfo, this.memberInfo);

  factory GroupAndProfiles.fromJson(Map<String, dynamic> json) =>
      _$GroupAndProfilesFromJson(json);

  Map<String, dynamic> toJson() => _$GroupAndProfilesToJson(this);
}

@JsonSerializable()
class GroupInfo {
  String id;
  GroupChat? group;

  GroupInfo(this.id, this.group);

  factory GroupInfo.fromJson(Map<String, dynamic> json) =>
      _$GroupInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GroupInfoToJson(this);
}
