// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroupChat _$GroupChatFromJson(Map<String, dynamic> json) => GroupChat(
      json['id'] as String,
      json['createdTime'] as int?,
      json['updatedTime'] as int,
      (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['creatorId'] as String?,
      json['admin'] as bool?,
      json['private'] as bool?,
      name: json['name'] as String?,
      avatar: json['avatar'] as String?,
      background: json['background'] as String?,
      profiles: (json['profiles'] as List<dynamic>?)
          ?.map((e) => PublicProfiles.fromJson(e as Map<String, dynamic>))
          .toList(),
      adminIds: (json['adminIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      membersAdded: (json['membersAdded'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      membersRemoved: (json['membersRemoved'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      promotedAdminIds: (json['promotedAdminIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      demotedAdminIds: (json['demotedAdminIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$GroupChatToJson(GroupChat instance) => <String, dynamic>{
      'id': instance.id,
      'createdTime': instance.createdTime,
      'updatedTime': instance.updatedTime,
      'members': instance.members,
      'creatorId': instance.creatorId,
      'name': instance.name,
      'avatar': instance.avatar,
      'background': instance.background,
      'profiles': instance.profiles,
      'admin': instance.admin,
      'private': instance.isPrivate,
      'adminIds': instance.adminIds,
      'membersAdded': instance.membersAdded,
      'membersRemoved': instance.membersRemoved,
      'promotedAdminIds': instance.promotedAdminIds,
      'demotedAdminIds': instance.demotedAdminIds,
    };

GroupAndProfiles _$GroupAndProfilesFromJson(Map<String, dynamic> json) =>
    GroupAndProfiles(
      (json['groupInfo'] as List<dynamic>)
          .map((e) => GroupInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['memberInfo'] as List<dynamic>?)
          ?.map((e) => PublicProfiles.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GroupAndProfilesToJson(GroupAndProfiles instance) =>
    <String, dynamic>{
      'groupInfo': instance.groupInfo,
      'memberInfo': instance.memberInfo,
    };

GroupInfo _$GroupInfoFromJson(Map<String, dynamic> json) => GroupInfo(
      json['id'] as String,
      json['group'] == null
          ? null
          : GroupChat.fromJson(json['group'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GroupInfoToJson(GroupInfo instance) => <String, dynamic>{
      'id': instance.id,
      'group': instance.group,
    };
