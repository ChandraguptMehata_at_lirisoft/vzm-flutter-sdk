/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:intl/intl.dart';
import 'package:vzm_client_sdk/src/common/app_preferences.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/restore.dart';

import 'database/database_manager.dart';
import 'di/locator.dart';

class AppUtils {

  static bool isNumeric(String? s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  static void clearDb() {
    serviceLocator.get<DatabaseManager>().getRestoreQueue().removeAll();
    serviceLocator.get<DatabaseManager>().getContactBox().removeAll();
    serviceLocator.get<DatabaseManager>().getConversationBox().removeAll();
    serviceLocator.get<DatabaseManager>().getMessageBox().removeAll();
    serviceLocator.get<DatabaseManager>().getSendQueueBox().removeAll();
    serviceLocator.get<DatabaseManager>().getUploadQueueBox().removeAll();
    serviceLocator.get<DatabaseManager>().getAttachmentBox().removeAll();
    serviceLocator.get<AppPreferences>().putBoolean(RestoreManager.keyFetchConversationIds, false);
  }

  static getTelChatGroupId(Set<String> set) {
    var participants = <String>[];
    participants.addAll(set);
    participants.sort();
    return generateMd5(participants.join(";"));
  }

  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  static bool isValidMdns(Set<String> set) {
    //todo: provide implementation
    return true;
  }

  static String formatTime(int millisecondsSinceEpoch) {
    final String formattedDateTime = DateFormat('MM/dd/yyyy, HH:mm:ss a').format(DateTime.fromMillisecondsSinceEpoch(millisecondsSinceEpoch)).toString();
    return formattedDateTime;
  }

}