/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:convert';

import 'package:vzm_client_sdk/src/common/app_preferences.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/enroll_device_response.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/linked_device.dart';
import 'package:vzm_client_sdk/src/sdk-impl/auth/models/vma_subscriber_status.dart';

import 'di/locator.dart';
import 'ott/ott_config_manager.dart';

class AppCredential {
  static const fcmToken = "ottHandler.fcm.token";
  static const keyVolteUser = "com.vzm.user.volte";
  static const keyVoiceUser = "com.vzm.user.voice";
  static const keyVMAProvisioned = "vma.status.provision";
  static const keyOTTProfileMdn = "ottHandler.profile.mdn";
  static const keyOTTSubscriberId = "ottHandler.subscriber.id";
  static const keyProfileDeviceId = "ottHandler.profile.device.id";
  static const keyOTTSecret = "ottHandler.login.key";
  static const keyOTTInitializeIv = "ottHandler.encrypt.key";
  static const keyOTTProfilePwd = "ottHandler.profile.pwd";
  static const keyOTTProfileEmail = "ottHandler.profile.email";
  static const keyOTTProfileName = "ottHandler.profile.name";
  static const keyOTTProfileAvatar = "ottHandler.profile.avatar";
  static const keyDevicePrefix = "ottHandler.device.prefix";
  static const keyRenewAfterTime = "ottHandler.renew.after.time";
  static const keyDevicePushId = "ottHandler.device.pushId";

  late AppPreferences appPreferences;
  late OttConfigurationManager ottConfigurationManager;

  AppCredential() {
    appPreferences = serviceLocator.get<AppPreferences>();
    ottConfigurationManager = serviceLocator.get<OttConfigurationManager>();
  }

  void saveFcmToken(String? token) {
    appPreferences.put(fcmToken, token!);
  }

  String getFcmToken() {
    String? token = appPreferences.getString(fcmToken);
    if (token == null) {
      throw NullThrownError();
    }
    return token;
  }

  void saveVmaSubscriberStatus(VMASubscriberStatus vmaSubscriberStatus) {
    appPreferences.putBoolean(keyVolteUser, vmaSubscriberStatus.isVolteUser());
    appPreferences.putBoolean(keyVoiceUser, vmaSubscriberStatus.isVoiceUser());
  }

  void setIsVmaProvisioned(bool provisioned) {
    appPreferences.putBoolean(keyVMAProvisioned, provisioned);
  }

  void clearVmaCredentials() {
    appPreferences.putBoolean(keyVMAProvisioned, false);
  }

  Future<void> saveOttCredentials(EnrollDeviceResponse response) async {
    var profileMdn = response.mdn;
    print("#auth.ottHandler.$profileMdn: saveOttCredentials -> " + "response = $response");
    await appPreferences.put(keyOTTSubscriberId, response.id);
    appPreferences.put(keyOTTProfileMdn, response.mdn);
    var email = response.email;
    if (email != null) {
      appPreferences.put(keyOTTProfileEmail, email);
    }
    var name = response.name;
    if (name != null) {
      appPreferences.put(keyOTTProfileName, name);
    }
    var avatar = response.avatar;
    if (avatar != null) {
      appPreferences.put(keyOTTProfileAvatar, avatar);
    }

    List<LinkedDevice>? linkedDevices = response.devices;
    if (linkedDevices != null) {
      LinkedDevice linkedDevice = linkedDevices.lastWhere((element) => element.password != null);
      print("#auth.ottHandler.$profileMdn: saveOttCredentials ->" +
          " device = ${linkedDevice.toString()}");
      await setDeviceId(linkedDevice.deviceid!);
      await setPassword(linkedDevice.password!);
      setDevicePrefix(linkedDevice.prefix!);
      setRenewAfterTime(linkedDevice.renewAfterTime);
      constructBasicAuth();
    }
    if (response.primaryAmsSyncBaseUri != null) {
      var status = await ottConfigurationManager.saveSyncBaseUrl(response.primaryAmsSyncBaseUri);
      print("#auth.ottHandler.$profileMdn: saveOttCredentials ->" +
          " saveSyncBaseUrl status = $status");
    }
  }

  Future<bool> setPassword(String password) async {
    //todo:need to encrypt the password before saving it
    return await appPreferences.put(keyOTTSecret, password);
  }

  void setDevicePrefix(String value) {
    appPreferences.put(keyDevicePrefix, value);
  }

  void setRenewAfterTime(int renewAfterTime) {
    appPreferences.putInt(keyRenewAfterTime, renewAfterTime);
  }

  String? getDevicePushId() {
    String? pushId = appPreferences.getString(keyDevicePushId);
    return pushId;
  }

  void setDevicePushId(String? token) {
    appPreferences.put(keyDevicePushId, token!);
  }

  String? getDeviceId() {
    String? deviceId = appPreferences.getString(keyProfileDeviceId);
    print("#auth.ottHandler.: getDeviceId ->" + " deviceId = $deviceId");
    return deviceId;
  }

  /// Update the Local Device Id
  /// @param deviceId
  Future<bool> setDeviceId(String deviceId) async {
    return await appPreferences.put(keyProfileDeviceId, deviceId);
  }

  String? getUserName() {
    String? userName;
    String? subscriberId = getSubscriberId();
    String? userId = getDeviceId();
    if (subscriberId != null && userId != null) {
      userName = subscriberId + "_" + userId;
    }
    return userName;
  }

  String? getSubscriberId() {
    String? subscriberId = appPreferences.getString(keyOTTSubscriberId);
    return subscriberId;
  }

  String? getPassword() {
    String? password = appPreferences.getString(keyOTTSecret);
    return password;
  }

  String? constructBasicAuth() {
    String? username = getUserName();
    String? password = getPassword();
    print("constructBasicAuth userName is $username , password is $password");
    if (username == null || password == null) {
      return null;
    }
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));
    return basicAuth;
  }

  bool isOTTProvisioned() {
    String? subId = getSubscriberId();
    String? userName = getUserName();
    String? password = getPassword();
    print(
        "#auth.ottHandler.: isOttProvisioned -> " +
            "subId = $subId, userName = $userName, password = $password");
    if (subId == null || userName == null || password == null) {
      return false;
    }
    return subId.isNotEmpty && userName.isNotEmpty && password.isNotEmpty;
  }

  bool isMyId(String id) {
    return id == appPreferences.getString(keyOTTSubscriberId);
  }

  bool isMyMdn(String mdn) {
    return mdn == appPreferences.getString(keyOTTProfileMdn);
  }

  String? get profileMdn {
    return appPreferences.getString(keyOTTProfileMdn);
  }

  String? get devicePrefix {
    return appPreferences.getString(keyDevicePrefix);
  }

}
