/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/api/sync/sync_manager.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/sync/delta_sync_manager.dart';

class SyncManagerImpl extends SyncManager {
  late AppLogger _logger;
  late DeltaSyncManager _deltaSync;

  SyncManagerImpl() {
    _logger = serviceLocator.get<AppLogger>();
    _deltaSync = serviceLocator.get<DeltaSyncManager>();
  }

  @override
  SyncResult syncAllChanges() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("syncAllChanges", "#sync: syncAllChanges Fire delta sync");
    }
    /*Workmanager().initialize(callbackDispatcher, // The top level function, aka callbackDispatcher
        isInDebugMode: true // If enabled it will post a notification whenever the task is running. Handy for debugging tasks
        );
    Workmanager().registerOneOffTask("1", "simpleTask");*/
    _deltaSync.startSync();
    return SyncResult.success;
  }

  @override
  SyncResult syncNewMessages() {
    // TODO: implement syncNewMessages
    throw UnimplementedError();
  }

  void callbackDispatcher() {
   //
  }
}
