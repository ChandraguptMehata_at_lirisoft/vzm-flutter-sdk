/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/persistence_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/group.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/profiles.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/group_chat_api.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/profile_api.dart';

abstract class BaseSyncManager {

  late final EntityConverter _entityConverter = serviceLocator.get<EntityConverter>();
  late final AppLogger _logger = serviceLocator.get<AppLogger>();
  late final GroupChatApi _groupChatApi = serviceLocator.get<GroupChatApi>();
  late final PersistenceManager _persistenceManager = serviceLocator.get<PersistenceManager>();
  late final ProfileApi _profileApi = serviceLocator.get<ProfileApi>();

  Future<Conversation?> createConversationForMessage(Payload src) async {
    var groupId = src.object.groupId!;
    bool? isGroupChatId = await _entityConverter.isGroupChatId(groupId);
    if (isGroupChatId != null && isGroupChatId) {
      var conv = createGroupChat(groupId);
      if (null == conv) {
        if (AppLogger.isDebugEnabled) {
          _logger.debug("createConversationForMessage", "#sync.delta.${src.object.id}: Group $groupId is not active.");
        }
      }
      return conv;
    } else {
      // Save the missing profiles on local databases
      var missingProfiles = _persistenceManager.getMissingProfile(src.object);
      if (missingProfiles.isNotEmpty) {
        await restoreProfiles(missingProfiles);
      }
      return await _persistenceManager.createTelephonyConversation(src, false);
    }
  }

  Future<Conversation?> createGroupChat(String groupId) async {
    GroupAndProfiles response = await getGroupChat(groupId);
    var group = response.groupInfo[0].group;
    if (group != null) {
      return await _persistenceManager.handleGroupAndProfiles(group, response.memberInfo!);
    }
    return null;
  }

  Future<GroupAndProfiles> getGroupChat(String groupId) async {
    var response = await _groupChatApi.getGroupsAndProfiles(List.from([groupId]));
    if (response.response.statusCode == 200) {
      return response.data;
    }
    throw Exception("Failed to getProfiles. error=${response.response.statusCode}");
  }

  Future<void> restoreProfiles(List<String> missingProfiles) async {
    var profiles = await getProfiles(missingProfiles);
    await _persistenceManager.addOrUpdateProfile(profiles);
  }

  Future<List<PublicProfiles>> getProfiles(List<String> missingProfiles) async {
    var httpResponse = await _profileApi.getPublicProfiles(missingProfiles);
    if (httpResponse.response.statusCode == 200) {
      List<PublicProfiles> profiles = httpResponse.data;
      return profiles;
    }
    throw Exception("Failed to getProfiles. error=${httpResponse.response.statusCode}");
  }
}
