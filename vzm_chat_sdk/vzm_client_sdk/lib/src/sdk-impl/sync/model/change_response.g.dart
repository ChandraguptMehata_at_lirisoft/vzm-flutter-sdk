// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangeResponse _$ChangeResponseFromJson(Map<String, dynamic> json) =>
    ChangeResponse(
      json['lastEventTimeStr'] as int?,
      json['lastSyncTime'] as int?,
      json['lastSyncTimeShift'] as int?,
      json['lastSyncTimeRegion'] as String?,
    )..events = (json['events'] as List<dynamic>?)
        ?.map((e) => ChangeEvent.fromJson(e as Map<String, dynamic>))
        .toList();

Map<String, dynamic> _$ChangeResponseToJson(ChangeResponse instance) =>
    <String, dynamic>{
      'lastEventTimeStr': instance.lastEventTimeStr,
      'lastSyncTime': instance.lastSyncTime,
      'lastSyncTimeShift': instance.lastSyncTimeShift,
      'lastSyncTimeRegion': instance.lastSyncTimeRegion,
      'events': instance.events,
    };

ChangeEvent _$ChangeEventFromJson(Map<String, dynamic> json) => ChangeEvent(
      json['messageId'] as String,
      $enumDecodeNullable(_$PayloadTypeEnumMap, json['msgType']),
      json['timeOfLastMsgSeen'] as int?,
      json['groupId'] as String?,
      json['senderId'] as String?,
      json['deviceId'] as String?,
      (json['ids'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['time'] as int?,
      (json['senders'] as List<dynamic>?)
          ?.map((e) => XmsMsgDeliveryFailed.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChangeEventToJson(ChangeEvent instance) =>
    <String, dynamic>{
      'messageId': instance.messageId,
      'msgType': _$PayloadTypeEnumMap[instance.type],
      'timeOfLastMsgSeen': instance.timeOfLastMsgSeen,
      'groupId': instance.groupId,
      'senderId': instance.senderId,
      'deviceId': instance.deviceId,
      'ids': instance.ids,
      'time': instance.time,
      'senders': instance.senders,
    };

const _$PayloadTypeEnumMap = {
  PayloadType.sentReceiptEvent: 'SentReceiptEvent',
  PayloadType.mediaMessage: 'MediaMessage',
  PayloadType.deleteConversationEvent: 'DeleteConversationEvent',
  PayloadType.readConversationEvent: 'ReadConversationEvent',
  PayloadType.typingIndicatorEvent: 'TypingIndicatorEvent',
  PayloadType.removeFromGroupEvent: 'RemoveFromGroupEvent',
  PayloadType.inviteToGroupEvent: 'InviteToGroupEvent',
  PayloadType.groupUpdateEvent: 'GroupUpdateEvent',
  PayloadType.createGroupEvent: 'CreateGroupEvent',
  PayloadType.textMessage: 'TextMessage',
  PayloadType.msgDeliveredEvent: 'MsgDeliveredEvent',
  PayloadType.msgDeliveryFailedEvent: 'MsgDeliveryFailedEvent',
  PayloadType.msgDeletedEvent: 'MsgDeletedEvent',
  PayloadType.subscriberUpdateEvent: 'SubscriberUpdateEvent',
  PayloadType.messageCommentEvent: 'MessageCommentEvent',
  PayloadType.subscriberDeletedEvent: 'SubscriberDeletedEvent',
  PayloadType.unknown: 'RecallMessageEvent',
  PayloadType.smsMessage: 'SmsMessage',
  PayloadType.mmsMessage: 'MmsMessage',
  PayloadType.xmsMsgDeliveredEvent: 'XmsMsgDeliveredEvent',
  PayloadType.xmsMsgDeliveryFailedEvent: 'XmsMsgDeliveryFailedEvent',
  PayloadType.systemMessage: 'SystemMessage',
  PayloadType.xmsTypingIndicatorEvent: 'XmsTypingIndicatorEvent',
  PayloadType.botMessage: 'BotMessage',
  PayloadType.botMessageReply: 'BotMessageReply',
  PayloadType.rcsTextMessage: 'RcsTextMessage',
  PayloadType.rcsMediaMessage: 'RcsMediaMessage',
  PayloadType.rcsLocationMessage: 'RcsLocationMessage',
  PayloadType.rcsGroupTextMessage: 'RcsGroupTextMessage',
  PayloadType.rcsGroupMediaMessage: 'RcsGroupMediaMessage',
  PayloadType.rcsGroupLocationMessage: 'RcsGroupLocationMessage',
  PayloadType.rcsGroupTypingIndicatorEvent: 'RcsGroupTypingIndicatorEvent',
  PayloadType.rcsSentReceiptEvent: 'RcsSentReceiptEvent',
  PayloadType.rcsMsgDeliveredEvent: 'RcsMsgDeliveredEvent',
  PayloadType.rcsMsgReadEvent: 'RcsMsgReadEvent',
  PayloadType.rcsMsgDeliveryFailedEvent: 'RcsMsgDeliveryFailedEvent',
  PayloadType.rcsTypingIndicatorEvent: 'RcsTypingIndicatorEvent',
  PayloadType.rcsCreateGroupEvent: 'RcsCreateGroupEvent',
  PayloadType.rcsGroupUpdateEvent: 'RcsGroupUpdateEvent',
  PayloadType.rcsRemoveFromGroupEvent: 'RcsRemoveFromGroupEvent',
  PayloadType.rcsGroupMsgReadEvent: 'RcsGroupMsgReadEvent',
  PayloadType.rcsCapabilitiesEvent: 'RcsCapabilitiesEvent',
  PayloadType.rcsOpenConversationEvent: 'OpenConversationEvent',
};

XmsMsgDeliveryFailed _$XmsMsgDeliveryFailedFromJson(
        Map<String, dynamic> json) =>
    XmsMsgDeliveryFailed(
      json['id'],
      json['time'],
    );

Map<String, dynamic> _$XmsMsgDeliveryFailedToJson(
        XmsMsgDeliveryFailed instance) =>
    <String, dynamic>{
      'time': instance.time,
      'id': instance.id,
    };
