/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
part 'change_response.g.dart';

@JsonSerializable()
class ChangeResponse {
  ChangeResponse(this.lastEventTimeStr, this.lastSyncTime, this.lastSyncTimeShift, this.lastSyncTimeRegion);

  @JsonKey(name: 'lastEventTimeStr')
  late final int? lastEventTimeStr;

  @JsonKey(name: 'lastSyncTime')
  late final int? lastSyncTime;

  @JsonKey(name: 'lastSyncTimeShift')
  late final int? lastSyncTimeShift;

  @JsonKey(name: 'lastSyncTimeRegion')
  late final String? lastSyncTimeRegion;

  @JsonKey(name: 'events')
  late List<ChangeEvent>? events;

  factory ChangeResponse.fromJson(Map<String, dynamic> json) => _$ChangeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ChangeResponseToJson(this);
}

@JsonSerializable()
class ChangeEvent {
  ChangeEvent(this.messageId, this.type, this.timeOfLastMsgSeen, this.groupId, this.senderId, this.deviceId, this.ids, this.time, this.senders);

  @JsonKey(name: 'messageId')
  late final String messageId;

  @JsonKey(name: 'msgType')
  late final PayloadType? type;

  @JsonKey(name: 'timeOfLastMsgSeen')
  int? timeOfLastMsgSeen;

  @JsonKey(name: 'groupId')
  String? groupId;

  @JsonKey(name: 'senderId')
  String? senderId;

  @JsonKey(name: 'deviceId')
  String? deviceId;

  @JsonKey(name: 'ids')
  List<String>? ids;

  @JsonKey(name: 'time')
  int? time;

  @JsonKey(name: 'senders')
  List<XmsMsgDeliveryFailed>? senders;

  factory ChangeEvent.fromJson(Map<String, dynamic> json) => _$ChangeEventFromJson(json);

  Map<String, dynamic> toJson() => _$ChangeEventToJson(this);
}

@JsonSerializable()
class XmsMsgDeliveryFailed {
  XmsMsgDeliveryFailed(id, time);

  @JsonKey(name: 'time')
  late final int? time;

  @JsonKey(name: 'id')
  late final String id;

  factory XmsMsgDeliveryFailed.fromJson(Map<String, dynamic> json) => _$XmsMsgDeliveryFailedFromJson(json);

  Map<String, dynamic> toJson() => _$XmsMsgDeliveryFailedToJson(this);
}
