/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:convert';

import 'package:collection/src/iterable_extensions.dart';
import 'package:mime_type/mime_type.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/attachment_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/media/model/file_transfer_state.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/media_item_data.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/media_item_type.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload_object.dart';

class AttachmentModelConverter {
  late AppLogger _logger;

  AttachmentModelConverter() {
    _logger = serviceLocator.get<AppLogger>();
  }

  List<Attachment> getAttachment(List<MediaPayload> attachments) {
    var attachmentList = <Attachment>[];
    for (MediaPayload item in attachments) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("", "#db.msg.attachment : attachment = ${item.toString()}");
      }
      //todo: need to provide implementation for reply message on the basis of type
      var media = toMediaItemData(item.data!);
      MediaItemType mediaItemType = getValue(item.type);
      var attachment = toAttachmentFromMediaItem(media!, mediaItemType);
      attachmentList.add(attachment);
    }
    return attachmentList;
  }

  MediaItemData? toMediaItemData(String data) {
    var mediaItemData = MediaItemData.fromJson(json.decode(data));
    if (mediaItemData.mimeType == null) {
      String? ext;
      var slash = mediaItemData.fileName!.lastIndexOf('/');
      if (slash > 0) {
        ext = mediaItemData.fileName!.substring(slash + 1);
      }
      if (ext != null) {
        mediaItemData.mimeType = mimeFromExtension(ext);
      }
    }
    return mediaItemData;
  }

  Attachment toAttachmentFromMediaItem(MediaItemData media, MediaItemType mediaItemType) {
    Attachment attachment = Attachment(AttachmentStatus.queued, media.mimeType!, null, null, media.width, media.height,
        media.duration != null ? media.duration!.toInt() : 0);
    attachment.fileType = toFileType(mediaItemType);
    attachment.fileName = media.fileName;
    attachment.thumbnailData = media.thumbnail;
    attachment.serverUrl = media.url;
    return attachment;
  }

  Attachment toAttachmentFromEntity(AttachmentEntity src) {
    var thumbnail = Uri.parse("");
    if (src.thumbnailUri != null) {
      thumbnail = Uri.parse(src.thumbnailUri!);
    }
    var path = Uri.parse("");
    if (src.localUri != null) {
      path = Uri.parse(src.localUri!);
    }
    Attachment attachment = Attachment(src.status!, src.mimeType!, thumbnail, path, 0, 0, 0);

    attachment.id = src.id;
    attachment.serverUrl = src.serverUrl;
    attachment.duration = src.duration;
    attachment.fileType = src.type;
    attachment.size = src.size;

    return attachment;
  }

  AttachmentStatus getAttachmentStatus(FileTransferState? transferState) {
    switch (transferState) {
      case FileTransferState.completed:
        return AttachmentStatus.downloaded;
      case FileTransferState.running:
        return AttachmentStatus.downloading;
      case FileTransferState.failed:
        return AttachmentStatus.downloadFailed;
      default:
        return AttachmentStatus.queued;
    }
  }

  MessageStatus getMessageStatus(FileTransferState? transferState) {
    switch (transferState) {
      case FileTransferState.completed:
        return MessageStatus.received;
      case FileTransferState.running:
        return MessageStatus.downloading;
      case FileTransferState.failed:
        return MessageStatus.failedReceive;
      default:
        return MessageStatus.none;
    }
  }

  FileTransferState? getTransferState(String progress) {
    if (progress == "100") {
      return FileTransferState.completed;
    }
    return FileTransferState.running;
  }

  MediaItemType getValue(String? type) {
    if (type == null) {
      return MediaItemType.unknown;
    }
    MediaItemType? mediaItemType = MediaItemType.values.firstWhereOrNull((e) => e.toString().split(".").last == type.toLowerCase());
    mediaItemType ??= MediaItemType.unknown;
    return mediaItemType;
  }

  FileType toFileType(MediaItemType type) {
    switch (type) {
      case MediaItemType.image:
        return FileType.image;
      case MediaItemType.audio:
        return FileType.audio;
      case MediaItemType.video:
        return FileType.video;
      case MediaItemType.vcard:
        return FileType.vcard;
      case MediaItemType.location:
        return FileType.location;

      default:
        return FileType.unknown;
    }
  }

}
