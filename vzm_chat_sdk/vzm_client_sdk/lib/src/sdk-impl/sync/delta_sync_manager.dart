/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:retrofit/dio.dart';
import 'package:vzm_client_sdk/src/api/sync/sync_manager.dart';
import 'package:vzm_client_sdk/src/common/app_preferences.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_utils.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/persistence_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/restore/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/sync_api.dart';
import 'package:vzm_client_sdk/src/sdk-impl/sync/base_sync_manager.dart';

import 'model/change_response.dart';

class DeltaSyncManager extends BaseSyncManager{
  late AppLogger _logger;
  late AppPreferences _appPreferences;
  late SyncApi _syncApi;
  late PersistenceManager _persistenceManager;

  static const String syncAnchor = "sdk.sync.last.anchor";
  static const int syncLimit = 100;

  DeltaSyncManager() {
    _logger = serviceLocator.get<AppLogger>();
    _appPreferences = serviceLocator.get<AppPreferences>();
    _syncApi = serviceLocator.get<SyncApi>();
    _persistenceManager = serviceLocator.get<PersistenceManager>();
  }

  Future<bool> startSync() async {
    while (true) {
      int? anchor = _appPreferences.getInt(syncAnchor);
      anchor ??= DateTime.now().millisecondsSinceEpoch;
      if (AppLogger.isDebugEnabled) {
        _logger.debug("startSync", "#sync.delta: Last sync anchor $anchor -> ${AppUtils.formatTime(anchor)}");
      }

      var result = await fetchChanges(anchor);
      break;
    }
    return true;
  }

  Future<SyncResult> fetchChanges(int anchor) async {
    try {
      HttpResponse<ChangeResponse> httpResponse = await _syncApi.getChanges(anchor, syncLimit);
      if (httpResponse.response.statusCode == 200) {
        ChangeResponse changeResponse = httpResponse.data;
        List<ChangeEvent>? changes = changeResponse.events;
        if (AppLogger.isDebugEnabled) {
          _logger.debug("fetchChanges", "#sync.delta: Changes found ${changes?.length}");
        }

        if (changes != null) {
          for (ChangeEvent changeEvent in changes) {
            try {
              var res = await process(changeEvent);
              if (AppLogger.isDebugEnabled) {
                _logger.debug("fetchChanges", "#sync.delta.${changeEvent.messageId}: Processed ${changeEvent.type} r=$res");
              }
            } on Exception catch (_e) {
              if (AppLogger.isDebugEnabled) {
                _logger.debug("startSync", "#sync.delta: process error ${_e.toString()}");
              }
            }
          }
        } else {
          if (AppLogger.isDebugEnabled) {
            _logger.debug(
                "startSync", "#sync.delta: status code no changes found");
          }
        }
        if (AppLogger.isDebugEnabled) {
          _logger.debug(
              "startSync", "#sync.delta: fetchChanges:: lastSyncTime ${AppUtils.formatTime(changeResponse.lastSyncTime!)}");
        }
        _appPreferences.putInt(syncAnchor, changeResponse.lastSyncTime!);
        return SyncResult.success;
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug("startSync", "#sync.delta: status code ${httpResponse.response.statusCode} message ${httpResponse.response.statusMessage}");
        }
      }
    } on Exception catch (_e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("startSync", "#sync.delta: error ${_e.toString()}");
      }
    }
    return SyncResult.failed;
  }

  Future<bool> process(ChangeEvent changeEvent) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("process", "#sync.delta.${changeEvent.messageId}: Processing ${changeEvent.type} change");
    }
    switch (changeEvent.type) {
      case PayloadType.smsMessage:
      case PayloadType.mmsMessage:
      case PayloadType.textMessage:
      case PayloadType.mediaMessage:
        return await processMessage(changeEvent);

      case PayloadType.createGroupEvent:
        return await processCreateGroupEvent(changeEvent);

      case PayloadType.groupUpdateEvent:
        return await processGroupUpdateEvent(changeEvent);

      case PayloadType.removeFromGroupEvent:
        return await processRemoveFromGroupEvent(changeEvent);

      default: {
        if (AppLogger.isDebugEnabled) {
          _logger.debug("process", "#sync.delta.${changeEvent.messageId}: Not Implemented ${changeEvent.type}");
        }
      }
    }
    return false;
  }

  Future<bool> processMessage(ChangeEvent changeEvent) async {
    var message = await getMessage(changeEvent.messageId);
    if (AppLogger.isDebugEnabled) {
      _logger.debug("processMessage ", "#sync.delta.${changeEvent.messageId}: Fetch msg = ${message.toString()}");
    }
    var groupId = message.object.groupId!;
    var thread = _persistenceManager.geConversation(groupId);
    thread ??= await createConversationForMessage(message);
    if (thread != null) {
      return await _persistenceManager.handleMessagePayload(thread, message);
    } else if (AppLogger.isDebugEnabled) {
      _logger.debug("processMessage ", "#sync.delta.${changeEvent.messageId}: failed.");
    }
    return false;
  }

  Future<Payload> getMessage(String messageId) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("getMessage ", "#sync.delta.msg.$messageId: Fetch message ");
    }
    GetMessagesRequest getMessagesRequest = GetMessagesRequest(List.from([GetMessageRequestId(messageId, Content.all)]));
    HttpResponse<List<Payload>> response = await _syncApi.getMessages(getMessagesRequest);
    if (response.response.statusCode == 200) {
      var result = response.data;
      return result[0];
    }
    throw Exception("Failed to getMessage. error=${response.response.statusCode}");
  }

  Future<bool> processCreateGroupEvent(ChangeEvent changeEvent) async {
    var group = await getMessage(changeEvent.messageId);
    _persistenceManager.handleCreateGroupEvent(group);
    return true;
  }

  Future<bool> processGroupUpdateEvent(ChangeEvent changeEvent) async {
    var group = await getMessage(changeEvent.messageId);
    var missingProfiles = _persistenceManager.getMissingProfile(group.object);
    if (missingProfiles.isNotEmpty) {
      restoreProfiles(missingProfiles);
    }
    return await _persistenceManager.handleGroupUpdateEvent(group);
  }

  Future<bool> processRemoveFromGroupEvent(ChangeEvent changeEvent) async {
    var group = await getMessage(changeEvent.messageId);
    return await _persistenceManager.handleRemoveFromGroupEvent(group);
  }
}
