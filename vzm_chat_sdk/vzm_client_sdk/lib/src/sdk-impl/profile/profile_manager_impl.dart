/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:vzm_client_sdk/src/api/profile/profile_manager.dart';
import 'package:vzm_client_sdk/src/api/repositories/entity_converter.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/chat/upload_connection_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/profile_repository_impl.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/ott_profile.dart';
import 'package:vzm_client_sdk/src/sdk-impl/profile/model/update_profile_request.dart';
import 'package:vzm_client_sdk/src/sdk-impl/retrofit/profile_api.dart';

import '../app_credential.dart';

class ProfileManagerImpl extends ProfileManager {
  late AppLogger _logger;
  late ProfileApi _profileApi;
  late ProfileRepositoryImpl _profileRepositoryImpl;
  late final EntityConverter _entityConverter;
  late AppCredential _appCredential;
  late final UploadConnectionManager _uploadConnectionManager = UploadConnectionManager();

  static const String _tag = "PROFILE";

  ProfileManagerImpl() {
    _logger = serviceLocator.get<AppLogger>();
    _profileApi = serviceLocator.get<ProfileApi>();
    _entityConverter = serviceLocator.get<EntityConverter>();
    _profileRepositoryImpl = serviceLocator.get<ProfileRepositoryImpl>();
    _appCredential = serviceLocator.get<AppCredential>();
  }

  @override
  Contact getProfile() {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#repo.profile.$getProfile:: >> ");
    }
    ProfileEntity profileEntity = _profileRepositoryImpl.getLocalProfile()!;
    Contact contact = _entityConverter.toContactFromProfileEntity(profileEntity);
    return contact;
  }

  @override
  Future<bool> setAvatar(String filePath) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.profile.up: setAvatar:: filePath = $filePath");
    }
    var avatarUri = Uri.parse(filePath);
    ProfileEntity profileEntity = _profileRepositoryImpl.getLocalProfile()!;
    UpdateProfileRequest request = UpdateProfileRequest(profileEntity.profileId!, profileEntity.updatedTime!, null, null, null);
    var url = await getAvatarUrl(avatarUri);
    if (url == null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#sdk.profile.up: setAvatar:: s3 upload failed. try again");
      }
      throw Exception("Failed to upload the avatar");
    }
    request.avatar = url;
    bool updated = await _update(request);
    return updated;
  }

  @override
  Future<bool> setName(String name) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#repo.profile.up: setName:: name = $name");
    }
    ProfileEntity profileEntity = _profileRepositoryImpl.getLocalProfile()!;
    UpdateProfileRequest updateProfileRequest =
        UpdateProfileRequest(profileEntity.profileId!, profileEntity.updatedTime!, name, null, null);
    bool updated = await _update(updateProfileRequest);
    return updated;
  }

  @override
  Future<bool> updateProfile(String? name, String? filePath) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#repo.profile.up: updateProfile:: name = $name, filePath = $filePath");
    }
    ProfileEntity profileEntity = _profileRepositoryImpl.getLocalProfile()!;
    UpdateProfileRequest request =
        UpdateProfileRequest(profileEntity.profileId!, profileEntity.updatedTime!, null, null, null);

    if (filePath != null) {
      var url = await getAvatarUrl(Uri.parse(filePath));
      if (url == null) {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#sdk.profile.up: updateProfile:: s3 upload failed. try again");
        }
        throw Exception("Failed to upload the avatar");
      }
      request.avatar = url;
    }

    if (name != null && name.trim().isEmpty) {
      if (name == '') {
        request.name = _appCredential.profileMdn;
      } else {
        request.name = name;
      }
    }
    return await _update(request);
  }

  Future<bool> _update(UpdateProfileRequest updateProfileRequest) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          _tag, "#sdk.profile.up.${updateProfileRequest.id}: updateMyProfile:: " + "request = ${updateProfileRequest.toString()}");
    }
    try {
      HttpResponse<OttProfile> httpResponse = await _profileApi.updateSubscriber(updateProfileRequest);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(
            _tag, "#sdk.profile.up.${updateProfileRequest.id}: updateMyProfile:: " + "httpResponse = ${httpResponse.response.statusCode}");
      }
      if (httpResponse.response.statusCode == 200 || httpResponse.response.statusCode == 201) {
        OttProfile profile = httpResponse.data;
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#sdk.profile.up.${updateProfileRequest.id}: updateMyProfile:: " + "response = ${profile.toString()}");
        }
        _updateDatabase(profile);
        return true;
      } else {
        if (AppLogger.isDebugEnabled) {
          _logger.debug(_tag, "#sdk.profile.up.${updateProfileRequest.id}: updateMyProfile:: Failed to update profile");
        }
        throw Exception("Failed to update profile ${httpResponse.response.statusCode}, ${httpResponse.response.statusMessage}");
      }
    } on Exception catch (_e) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#sdk.profile.up.${updateProfileRequest.id}: updateMyProfile:: error ${_e.toString()}");
      }
      if (_e is DioError) {
        if (_e.response != null && _e.response!.statusCode == 409) {
          await _resolveConflictIssue(updateProfileRequest);
          return false;
        }
      }
      throw Exception("Failed to update profile ${_e.toString()}");
    }
  }

  void _updateDatabase(OttProfile src) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.profile.up.${src.id}: updateDatabase " + "OttPrivateProfile = $src");
    }
    var entity = ProfileEntity(src.mdn, src.name, src.id, src.avatar, src.updatedTime, src.createdTime);
    _profileRepositoryImpl.save(<ProfileEntity>[entity]);
  }

  getAvatarUrl(Uri avatarUri) async {
    if (avatarUri != null) {
      var url = await _uploadConnectionManager.getS3Url(avatarUri);
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#sdk.profile.up: getAvatarUrl:: url $url");
      }
      return url;
    }
    return null;
  }

  Future<void> _resolveConflictIssue(UpdateProfileRequest updateProfileRequest) async {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(_tag, "#sdk.profile.up. _resolveConflictIssue:: ");
    }
    // profile was updated by someone else: persist and return the new updatedTime
    var httpResponse = await _profileApi.getSubscriber();
    if (httpResponse.response.statusCode == 200 || httpResponse.response.statusCode == 201) {
      OttProfile profile = httpResponse.data;
      if (AppLogger.isDebugEnabled) {
        _logger.debug(_tag, "#sdk.profile.up.${updateProfileRequest.id}: _resolveConflictIssue:: " + "response = ${profile.toString()}");
      }
      _updateDatabase(profile);
    } else {
      throw Exception("Failed to update the profile ${httpResponse.response.statusCode}, ${httpResponse.response.statusMessage}");
    }
  }
}
