
import 'package:vzm_client_sdk/src/common/app_channel.dart';
import 'package:vzm_client_sdk/src/common/app_preferences.dart';
import 'package:vzm_client_sdk/src/common/device/device_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';

class DeviceManagerImpl implements DeviceManager {

  static const String DEVICE_INFO = "DEVICE_INFO";
  static const String DEVICE_TYPE = "DEVICE_TYPE";
  static const String DEVICE_MODEL = "DEVICE_MODEL";
  static const String DEVICE_OS_VERSION = "DEVICE_OS_VERSION";
  static const String DEVICE_ID = "DEVICE_ID";
  static const String DEVICE_MAKE = "DEVICE_MAKE";
  static const String DEVICE_NAME = "DEVICE_NAME";
  static const APP_MDN = "app.mdn";

  late AppPreferences appPreferences;

  DeviceManagerImpl() {
    appPreferences = serviceLocator.get<AppPreferences>();
  }

  @override
  Future<String> getDeviceId() async {
    final String deviceId = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_ID, null);
    print("deviceInfo *** $deviceId");
    return deviceId;
  }

  @override
  Future<String> getDeviceMake() async {
    final String deviceMake = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_MAKE, null);
    print("deviceInfo *** $deviceMake");
    return deviceMake;
  }

  @override
  Future<String> getDeviceModel() async {
    final String deviceModel = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_MODEL, null);
    print("deviceInfo *** $deviceModel");
    return deviceModel;
  }

  @override
  Future<String> getDeviceName() async {
    final String deviceName = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_NAME, null);
    print("deviceInfo *** $deviceName");
    return deviceName;
  }

  @override
  Future<String> getDeviceOSVersion() async {
    final String deviceOSVersion = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_OS_VERSION, null);
    print("deviceInfo *** $deviceOSVersion");
    return deviceOSVersion;
  }

  @override
  Future<String> getDeviceType() async {
    final String deviceType = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_TYPE, null);
    print("deviceInfo *** $deviceType");
    return deviceType;
  }

  @override
  Future<String> getMdn() async {
    final String mdn = await AppChannel.CHAT_SDK_CHANNEL.invokeMethod(DEVICE_INFO, null);
    print("deviceInfo *** $mdn");
    return mdn;
  }

  @override
  Future<String> generateDeviceId() {
    // TODO: implement generateDeviceId
    throw UnimplementedError();
  }

  @override
  void setMdn(String? mdn) {
    if (mdn == null) {
      throw NullThrownError();
    }
    appPreferences.put(APP_MDN, mdn);
  }

}