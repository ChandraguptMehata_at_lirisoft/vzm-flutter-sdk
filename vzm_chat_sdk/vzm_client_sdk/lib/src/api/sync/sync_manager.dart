/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
abstract class SyncManager {
  SyncResult syncAllChanges();

  SyncResult syncNewMessages();
}

enum SyncResult { success, failed, unauthorized, scheduled }
