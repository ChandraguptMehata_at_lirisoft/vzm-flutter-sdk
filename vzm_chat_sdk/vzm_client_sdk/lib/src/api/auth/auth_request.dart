/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
abstract class AuthRequest {
   Future<dynamic> requestOtp(String mdn);
   bool isProvisioned();

   /// Used for Otp verification
   Future<dynamic> verifyOtp(String mdn, String otp);
}
