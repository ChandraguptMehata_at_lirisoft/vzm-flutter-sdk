/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/cupertino.dart';
import 'package:objectbox/src/native/box.dart';
import 'package:vzm_client_sdk/src/api/repositories/base_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/conversation_entity.dart';

abstract class ConversationRepository extends BaseRepository<Conversation, ConversationEntity> with ChangeNotifier{
  ConversationRepository(Box<ConversationEntity> store) : super(store);

  Conversation? findConversationByServerId(String serverId);

  Stream<List<Conversation>> observeConversationEntityChange();

  Future<List<Conversation>> getAllConversation();

  Future<Conversation> addConversation(Conversation conversation);

  Future<Conversation?> findConversationByThreadId(int? threadId);

  Stream subscribeForConversationChange();

}


