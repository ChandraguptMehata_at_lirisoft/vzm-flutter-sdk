/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:objectbox/src/native/box.dart';
import 'package:vzm_client_sdk/src/api/repositories/base_repository.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';

abstract class IContactRepository extends BaseRepository<Contact, ContactEntity>{
  IContactRepository(Box<ContactEntity> store) : super(store);

}


