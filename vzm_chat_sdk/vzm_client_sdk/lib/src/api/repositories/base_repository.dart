
/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import '../../../objectbox.g.dart';

abstract class BaseRepository<K, T> {

  Box<T> store;

  BaseRepository(this.store);

  Future<T?> findById(int id) async {
    return store.get(id);
  }

  Future<List<T>> findAll() async {
    return store.getAll();
  }

  Future<List<T>> findAllByPage(int offset, int limit) async {
    return store.getAll();
  }

  Future<List<T>> findWhereByPage(dynamic where, int offset, int limit);

  Future<K?> updateById(K id);

  Future<bool> deleteById(K id);

}
