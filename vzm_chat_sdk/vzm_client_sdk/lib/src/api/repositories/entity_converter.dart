/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:objectbox/src/relations/to_many.dart';
import 'package:vzm_client_sdk/objectbox.g.dart';
import 'package:vzm_client_sdk/src/api/repositories/models.dart';
import 'package:vzm_client_sdk/src/common/logger.dart';
import 'package:vzm_client_sdk/src/sdk-impl/app_credential.dart';
import 'package:vzm_client_sdk/src/sdk-impl/common/contact_manager.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/attachment_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/contact_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/conversation_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/message_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/database/entity/profile_entity.dart';
import 'package:vzm_client_sdk/src/sdk-impl/di/locator.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/group.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/ott_profile.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/ott_profiles.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/payload.dart';
import 'package:vzm_client_sdk/src/sdk-impl/models/profiles.dart';
import 'package:vzm_client_sdk/src/sdk-impl/sync/attachment_model_converter.dart';

class EntityConverter {
  late AppLogger _logger;
  late AppCredential _appCredential;
  late AttachmentModelConverter _attachmentModelConverter;

  static const int _OTT_GROUP_ID_LENGTH = 26;
  static const String ONE_TO_ONE_GROUP_ID_SEPARATOR = "!";

  EntityConverter() {
    _logger = serviceLocator.get<AppLogger>();
    _appCredential = serviceLocator.get<AppCredential>();
    _attachmentModelConverter = serviceLocator.get<AttachmentModelConverter>();
  }

  Message toMessage(MessageEntity src) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug(
          "toMessage",
          "#repo.msg.${src.id}: toMessage -> " +
              "message entity =$src, attachment size = ${src.attachments.length}, " +
              "reports size = ${src.reports.length}");
    }
    var msg = Message(
        src.id,
        src.time,
        src.type,
        src.status!,
        //todo: need to provide dynamically
        1,
        toContact(src.sender.target),
        toContacts(src.recipients),
        src.conversationId);
    msg.body = src.body;
    msg.serverId = src.serverId;
    msg.clientMsgId = src.clientMsgId;
    msg.parentMsgId = src.parentMsgId;
    msg.attachments = src.attachments.toList(growable: false).map((e) => _attachmentModelConverter.toAttachmentFromEntity(e)).toList();
    return msg;
  }

  Contact toContact(ContactEntity? contactEntity) {
    var contact = Contact(contactEntity!.address);
    contact.name = contactEntity.name;
    contact.email = contactEntity.email;
    contact.avatarUrl = contactEntity.avatarUrl;
    contact.id = contactEntity.id;
    return contact;
  }

  Contact toContactFromAddress(String? src, List<ProfileEntity> profiles) {
    var profile = profiles.firstWhere((element) => element.profileId == src || element.address == src);
    var contact = Contact(profile.address);
    contact.name = profile.name;
    contact.avatarUrl = profile.avatarUrl;
    return contact;
  }

  Contact toContactFromProfileEntity(ProfileEntity? profileEntity) {
    var contact = Contact(profileEntity!.address);
    contact.name = profileEntity.name;
    contact.avatarUrl = profileEntity.avatarUrl;
    return contact;
  }

  List<Contact> toContacts(ToMany<ContactEntity> recipients) {
    List<Contact> contactList = <Contact>[];
    for (ContactEntity contactEntity in recipients) {
      contactList.add(toContact(contactEntity));
    }
    return contactList;
  }

  List<ProfileEntity> toProfilesEntity(List<PublicProfiles> src) {
    var profileMetaList = <ProfileEntity>[];
    for (PublicProfiles publicProfile in src) {
      var profile = publicProfile.profile;
      if (profile != null) {
        profileMetaList.add(toProfileMetaEntity(profile));
      } else {
        var vzNumbers = ProfileEntity(publicProfile.id, null, publicProfile.id, null, -1, -1);
        profileMetaList.add(vzNumbers);
      }
    }
    return profileMetaList;
  }

  ProfileEntity toProfileMetaEntity(PublicProfile profile) {
    return ProfileEntity(profile.mdn, profile.name, profile.id, profile.avatar, profile.updatedTime, profile.createdTime);
  }

  ProfileEntity toProfileMetaEntityOtt(OttProfile profile) {
    return ProfileEntity(profile.mdn, profile.name, profile.id, profile.avatar, profile.updatedTime, profile.createdTime);
  }

  List<ProfileEntity> toProfilesEntityFromOttProfile(List<OttProfiles> src) {
    var profileMetaList = <ProfileEntity>[];
    for (OttProfiles ottProfile in src) {
      var profile = ottProfile.profile;
      if (profile != null) {
        profileMetaList.add(toProfileMetaEntityOtt(profile));
      } else {
        var vzNumbers = ProfileEntity(ottProfile.id!, null, ottProfile.id, null, -1, -1);
        profileMetaList.add(vzNumbers);
      }
    }
    return profileMetaList;
  }

  ProfileEntity toProfileMetaEntityFromOttProfile(OttProfile profile) {
    return ProfileEntity(profile.mdn, profile.name, profile.id, profile.avatar, profile.updatedTime, profile.createdTime);
  }

  Conversation toTelConversation(Payload payload, List<ProfileEntity> profiles) {
    var type = ConversationType.oneToOneChat;
    if (profiles.length > 2) {
      type = ConversationType.mmsGroupChat;
    } else {
      type = ConversationType.oneToOneChat;
    }
    var recipients = <Contact>[];
    var payloadRecipients = payload.object.recipients;
    if (payloadRecipients != null) {
      for (String recipient in payloadRecipients) {
        if (!_appCredential.isMyId(recipient)) {
          var profile = profiles.firstWhere((element) => element.profileId == recipient);
          recipients.add(toContactFromProfileEntity(profile));
        }
      }
    } else {
      //SMS/MMS
      var payloadMdns = payload.object.recipientMdns;
      if (payloadMdns != null) {
        for (String mdn in payloadMdns) {
          if (!_appCredential.isMyId(mdn) && !_appCredential.isMyMdn(mdn)) {
            var profile = profiles.firstWhere((element) => element.profileId == mdn || element.address == mdn);
            recipients.add(toContactFromProfileEntity(profile));
          }
        }
      }
    }

    if (!isMyIdOrMdn(payload.object.senderId, payload.object.senderMdn)) {
      var profile = profiles
          .firstWhereOrNull((element) => element.profileId == payload.object.senderMdn || element.address == payload.object.senderMdn);
      if (profile != null) {
        recipients.add(toContactFromProfileEntity(profile));
      }
    }

    var conversation = Conversation();
    conversation.type = type;
    conversation.recipients = recipients;
    conversation.time = payload.object.createdTime;
    conversation.serverId = payload.object.groupId;

    return conversation;
  }

  bool isMyIdOrMdn(String? id, String? mdn) {
    if (id != null) {
      return _appCredential.isMyId(id);
    } else if (mdn != null) {
      return _appCredential.isMyMdn(mdn);
    } else {
      return false;
    }
  }

  ConversationEntity toConversationEntity(Conversation conversation, List<ContactEntity> contacts) {
    ConversationEntity conversationEntity = ConversationEntity();
    conversationEntity.time = conversation.time;
    conversationEntity.type = conversation.type;
    conversationEntity.groupName = conversation.groupName;
    conversationEntity.serverId = conversation.serverId;
    conversationEntity.snippet = conversation.snippet;

    if (conversation.creator != null) {
      var contact = contacts.firstWhereOrNull((element) => element.address == conversation.creator!.address);
      conversationEntity.creator.target = contact;
    }

    if (conversation.type!.isGroupChat()) {
      if (conversation.groupMembers != null) {
        for (Contact contact in conversation.groupMembers!) {
          var contactEntity = contacts.firstWhereOrNull((element) => element.address == contact.address);
          conversationEntity.groupMembers.add(contactEntity!);
          conversationEntity.recipients.add(contactEntity.address);
        }
      }
    } else {
      if (conversation.recipients != null) {
        for (Contact contact in conversation.recipients!) {
          var contactEntity = contacts.firstWhereOrNull((element) => element.address == contact.address);
          conversationEntity.groupMembers.add(contactEntity!);
          conversationEntity.recipients
              .add(contactEntity.address); //todo: do we need recipient field at sdk layer ? We are not using in android sdk.
        }
      }
    }

    return conversationEntity;
  }

  Message toMessageFromPayload(Payload payload, Conversation conv, List<ProfileEntity> contacts) {
    var recipients = <String>[];
    var payloadRecipients = payload.object.recipients;
    if (payloadRecipients != null) {
      recipients.addAll(payloadRecipients);
    } else {
      //SMS/MMS
      recipients.addAll(payload.object.recipientMdns!);
    }

    var msg = Message(0, payload.object.createdTime, toMessageType(payload.type), MessageStatus.sending, 0,
        toContactFromAddress(payload.object.getSenderIdOrMdn(), contacts), toContactsFromAddress(recipients, contacts), conv.id);

    if (payload.object.text != null) {
      msg.body = decode(payload.object.text);
    }
    msg.serverId = payload.object.id;

    if (payload.object.attachments != null) {
      if (AppLogger.isDebugEnabled) {
        _logger.debug("", "#db.msg.${payload.object.id}: attachment");
      }
      List<Attachment> attachmentList = _attachmentModelConverter.getAttachment(payload.object.attachments!);
      msg.attachments = attachmentList;
      //If its replied message
      if (attachmentList.isEmpty) {
        msg.type = MessageType.textMessage;
      }
    }

    return msg;
  }

  MessageType toMessageType(PayloadType src) {
    if (src == PayloadType.smsMessage) {
      return MessageType.sms;
    } else if (src == PayloadType.mmsMessage) {
      return MessageType.mms;
    } else if (src == PayloadType.textMessage) {
      return MessageType.textMessage;
    } else if (src == PayloadType.mediaMessage) {
      return MessageType.mediaMessage;
    } else {
      return MessageType.sms;
    }
  }

  List<Contact> toContactsFromAddress(List<String> recipients, List<ProfileEntity> contacts) {
    var des = <Contact>[];
    for (String recipient in recipients) {
      var profile = contacts.firstWhere((element) => element.profileId == recipient || element.address == recipient);
      if (profile != null) {
        var contact = Contact(profile.address);
        contact.name = profile.name;
        contact.avatarUrl = profile.avatarUrl;
        des.add(contact);
      }
    }
    return des;
  }

  ContactEntity toContactEntity(Contact sender) {
    var savedContact = serviceLocator.get<ContactManager>().getContact(sender.address);
    var contactEntity = ContactEntity();
    contactEntity.id = savedContact != null ? savedContact.id : 0;
    contactEntity.address = sender.address;
    contactEntity.name = sender.name;
    contactEntity.email = sender.email;
    contactEntity.avatarUrl = sender.avatarUrl;
    return contactEntity;
  }

  toMessageEntity(Message insert, List<ContactEntity> contacts) {
    var messageEntity = MessageEntity();
    messageEntity.id = insert.id;
    messageEntity.type = insert.type;
    messageEntity.status = insert.status;
    messageEntity.time = insert.time!;
    messageEntity.conversationId = insert.conversationId;
    messageEntity.body = insert.body;
    messageEntity.serverId = insert.serverId;
    messageEntity.clientMsgId = insert.clientMsgId;

    messageEntity.sender.target = contacts.firstWhere((element) => element.address == insert.sender.address);

    for (Contact contact in insert.recipients) {
      ContactEntity contactResult = contacts.firstWhere((element) => element.address == contact.address);
      messageEntity.recipients.add(contactResult);
    }

    if (insert.attachments != null) {
       insert.attachments!.map((attachment) => messageEntity.attachments.add(toAttachmentEntity(attachment))).toList();
    }
    return messageEntity;
  }

  Conversation? toConversation(ConversationEntity src) {
    var recipients = <Contact>[];
    var groupMembers = <Contact>[];
    var groupAdmins = <Contact>[];
    ConversationType? type = src.type;

    if (src.groupMembers.isNotEmpty) {
      for (ContactEntity contactEntity in src.groupMembers) {
        recipients.add(toContact(contactEntity));
        if (type != null && type.isGroupChat()) {
          groupMembers.add(toContact(contactEntity));
        }
      }
    }
    if (src.groupAdmins.isNotEmpty) {
      for (ContactEntity contactEntity in src.groupMembers) {
        if (type != null && type.isGroupChat()) {
          groupAdmins.add(toContact(contactEntity));
        }
      }
    }

    Conversation conversation = Conversation();
    conversation.id = src.id;
    conversation.serverId = src.serverId;
    conversation.time = src.time;
    if (src.creator.target != null) {
      conversation.creator = toContact(src.creator.target);
    }
    conversation.type = src.type;
    conversation.groupName = src.groupName;

    conversation.snippet = src.snippet;
    conversation.recipients = recipients;

    if (type != null && type.isGroupChat()) {
      conversation.groupMembers = groupMembers;
      conversation.groupAdmins = groupAdmins;
    }

    conversation.read = src.read;
    conversation.groupAvatarUrl = src.groupAvatarUrl;
    conversation.groupBackgroundUrl = src.groupBackgroundUrl;

    return conversation;
  }

  List<Conversation> toConversations(List<ConversationEntity> src) {
    var conversationList = <Conversation>[];
    for (ConversationEntity conversationEntity in src) {
      conversationList.add(toConversation(conversationEntity)!);
    }
    return conversationList;
  }

  List<Message> toMessages(List<MessageEntity> src) {
    var messageList = <Message>[];
    for (MessageEntity messageEntity in src) {
      messageList.add(toMessage(messageEntity));
    }
    return messageList;
  }

  String? decode(String? text) {
    if (AppLogger.isDebugEnabled) {
      _logger.debug("toMessage", "decode: text $text");
    }
    if (text != null) {
      String decodedBody = utf8.decode(base64.decode(text));
      if (AppLogger.isDebugEnabled) {
        _logger.debug("toMessage", "decode: decodedBody $decodedBody");
      }
      return decodedBody;
    }
    return null;
  }

  Future<bool?> isGroupChatId(String groupId) async {
    if (groupId.length == _OTT_GROUP_ID_LENGTH) {
      RegExp regExp = RegExp(
        r"^[0-9A-Fa-f]{26}$",
        caseSensitive: false,
        multiLine: false,
      );
      return regExp.hasMatch(groupId);
    } else {
      false;
    }
  }

  bool isOneToOneChatId(String groupId) {
    return (groupId.contains(ONE_TO_ONE_GROUP_ID_SEPARATOR));
  }

  Conversation toGroupConversation(GroupChat src, List<ProfileEntity> profiles) {
    var conversationType = ConversationType.openGroupChat;
    if (src.admin != null && src.admin!) {
      conversationType = ConversationType.closedGroupChat;
    }

    var to = <Contact>[];
    if (src.members != null) {
      for (String id in src.members!) {
        if (!_appCredential.isMyId(id)) {
          ProfileEntity? profileEntity = profiles.firstWhereOrNull((element) => element.profileId == id);
          to.add(toContactFromProfileEntity(profileEntity));
        }
      }
    }
    if (src.adminIds != null) {
      for (String id in src.adminIds!) {
        if (!_appCredential.isMyId(id)) {
          ProfileEntity? profileEntity = profiles.firstWhereOrNull((element) => element.profileId == id);
          to.add(toContactFromProfileEntity(profileEntity));
        }
      }
    }

    Conversation des = Conversation();
    des.type = conversationType;
    des.recipients = to;
    des.time = src.createdTime;
    des.serverId = src.id;
    des.groupName = src.name;
    des.creator = toContactFromAddress(src.creatorId, profiles);
    //Every one is admin so
    if (src.members != null && src.members!.isNotEmpty) {
      des.groupMembers = toContactsFromAddress(src.members!, profiles);
    }

    if (src.admin != null && src.admin!) {
      des.groupAdmins = toContactsFromAddress(src.adminIds!, profiles);
    }
    des.createdTime = src.createdTime;
    des.updatedTime = src.updatedTime;

    return des;
  }

  AttachmentEntity toAttachmentEntity(Attachment attachment) {
    AttachmentEntity entity = AttachmentEntity();
    entity.mimeType = attachment.mimeType;
    entity.thumbnailUri = attachment.thumbnail.toString();
    entity.status = attachment.status;
    entity.localUri = attachment.path.toString();
    entity.serverUrl = attachment.serverUrl;
    entity.id = attachment.id;
    entity.type = attachment.fileType;
    entity.size = attachment.size;
    return entity;
  }

}
