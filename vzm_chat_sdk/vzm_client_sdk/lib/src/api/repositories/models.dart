/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

class Contact {
  int id = 0;
  String address;
  String? name;
  String? profileId;
  String? avatarUrl;
  String? checksum;
  String? email;

  Contact(this.address);

  @override
  String toString() {
    return 'Contact{id: $id, address: $address, name: $name, profileId: $profileId, avatarUrl: $avatarUrl, checksum: $checksum, email: $email}';
  }
}

enum ConversationType {
  oneToOneChat,
  mmsGroupChat,
  openGroupChat,
  closedGroupChat,
  broadCast,
  chatBot
}

extension ConversationTypeExtension on ConversationType {

  bool isGroupChat() {
    if (this == ConversationType.openGroupChat ||
        this == ConversationType.closedGroupChat) {
      return true;
    }
    return false;
  }
}

class Conversation {
  int id = 0;
  ConversationType? type;
  List<Contact>? recipients;
  int? time;
  String? serverId;
  String? snippet;
  Contact? creator;

  late bool read = false;
  bool muted = false;

  String? groupName;
  String? groupAvatarUrl;
  String? groupBackgroundUrl;

  int? lastReadTime;
  int? serverTime;

  List<Contact>? groupMembers;
  List<Contact>? groupAdmins;

  int? createdTime;
  int updatedTime = 0;

  bool disabled = false;

  @override
  String toString() {
    return 'Conversation{id: $id, type: $type, recipients: $recipients, time: $time, serverId: $serverId, snippet: $snippet, creator: $creator, read: $read, muted: $muted, groupName: $groupName, groupAvatarUrl: $groupAvatarUrl, groupBackgroundUrl: $groupBackgroundUrl, lastReadTime: $lastReadTime, serverTime: $serverTime, groupMembers: $groupMembers, groupAdmins: $groupAdmins, createdTime: $createdTime, updatedTime: $updatedTime, disabled: $disabled}';
  }
}

enum MessageType {
  none,
  sms,
  mms,
  textMessage,
  mediaMessage,
  groupEventMessage,
  eventCreateGroup,
  eventAddMembers,
  eventChangeAvatar,
  eventChangeBackground,
  eventLeaveGroup,
  eventRemoveMembers,
  eventAddAdmins,
  eventRemoveAdmins
}

enum MessageStatus {
  none,
  draft,
  queued,
  uploading,
  sending,
  processing,
  sent,
  deliveredToSome,
  delivered,
  readToSome,
  read,
  deleted,
  available,
  downloading,
  received,
  failedReceive
}

class Message {
  int id = 0;
  late int? time;
  late MessageType type;
  String? serverId;
  int? read;

  // Status
  late MessageStatus status;
  String? clientMsgId;
  String? parentMsgId;

  //From
  late Contact sender;

  // TO
  late List<Contact> recipients;

  String? body;
  String? subject;

  List<MessageComments>? comments;
  List<Attachment>? attachments;
  List<MessageReport>? reports;

  int inbound;
  int conversationId;

  Message(this.id, this.time, this.type, this.status, this.inbound, this.sender, this.recipients, this.conversationId);

  static const int inboundMessage = 0;
  static const int outboundMessage = 1;

  bool isTelephony() {
    return (type == MessageType.sms
        || type == MessageType.mms);
  }

  @override
  String toString() {
    return 'Message{id: $id, time: $time, type: $type, serverId: $serverId, read: $read, status: $status, clientMsgId: $clientMsgId, parentMsgId: $parentMsgId, sender: ${sender.toString()}, recipients: $recipients, body: $body, subject: $subject, comments: $comments, attachments: $attachments, reports: $reports, inbound: $inbound, conversationId: $conversationId}';
  }
}

class MessageReport {
  int id = 0;
  late String mdn;
  late MessageStatus status;
  int? time;
}

class MessageComments {
  late List<Contact> sender;
  late String data;
  late int time;
}

enum AttachmentStatus {
  queued,
  uploading,
  uploaded,
  uploadFailed,
  downloading,
  downloaded,
  downloadFailed
}

class Attachment {
  int id = 0;
  Uri? uri;

  // Use mime library
  String mimeType;
  AttachmentStatus status;

  String? fileName;
  String? serverUrl;//download url
  Uri? thumbnail;
  Uri? path;//local path

  int? width;
  int? height;
  int? duration;
  double size = 0;

  String? thumbnailData;
  int? progress;
  FileType? fileType;

  Attachment(this.status, this.mimeType, this.thumbnail, this.path, this.width, this.height, this.duration);

  @override
  String toString() {
    return 'Attachment{id: $id, uri: $uri, mimeType: $mimeType, status: $status, fileName: $fileName, serverUrl: $serverUrl, thumbnail: $thumbnail, path: $path, width: $width, height: $height, duration: $duration, size: $size, thumbnailData: $thumbnailData, progress: $progress, fileType: $fileType}';
  }
}

enum FileType { text, image, audio, video, location, vcard, unknown }

enum SendMessageStatus{
  queued,
  waitingSentReceipt,
  sent,
  retryOnNextSession,
}

class SendMessageResult {
  late SendMessageStatus status;
  late String? errorMessage;
  late int? errorCode;
  late Message? message;

  SendMessageResult({required this.status, this.errorMessage, this.errorCode, this.message});
}
