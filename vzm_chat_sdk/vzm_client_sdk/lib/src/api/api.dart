/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:flutter/material.dart';
import 'package:vzm_client_sdk/src/api/auth/auth_request.dart';

abstract class SdkManager {
  Future<bool> init(BuildContext? context);

  Future<bool> reset();

  Future<bool> setupdatabase();

  Future<bool> mqttSetup({BuildContext? context});

  AuthRequest getAuthManager();
}
