/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/sdk-impl/chat/chat_manager_impl.dart';

import '../../../vzm_client_sdk.dart';

abstract class GroupChatManager extends ChatManagerImpl{

  Future<Conversation?> createOpenGroupChat(List<String> participants, String? name, String? avatar);

  Future<Conversation?> createClosedGroupChat(List<String> participants, String? name, String? avatar);


}