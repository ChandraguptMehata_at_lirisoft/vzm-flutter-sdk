/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */

import 'package:vzm_client_sdk/src/api/repositories/models.dart';

abstract class ChatManager {
  /// Create one to one chat conversation
  ///
  /// Throws an [ArgumentError] if the [mdn] is invalid.
  /// Returns the new [Conversation].
  Future<Conversation> create(String mdn);

  /// Create MMS Group chat conversation
  ///
  /// Throws an [ArgumentError] if any [recipients] are invalid.
  /// Returns the new [Conversation].
  Future<Conversation> createMmsGroupChat(Set<String> recipients);

  /// Send the text message for the conversation
  ///
  /// Throws an [ArgumentError] if any [conversationId] are invalid or
  /// [body] size is too large.
  /// Returns the new [SendMessageResult].
  Future<SendMessageResult> sendText(int conversationId, String body);

  /// Send the text message for the conversation
  ///
  /// Throws an [ArgumentError] if any [conversationId] are invalid or
  /// [file] size is too large or file Path not found,
  /// Returns the new [SendMessageResult].
  Future<SendMessageResult> sendMms(int conversationId, String filePath,
      {String subject, String body});

  /// Send the text message for the conversation
  ///
  /// Throws an [ArgumentError] if any [conversationId] are invalid or
  /// [file] size is too large or file Path not found,
  /// Returns the new [SendMessageResult].
  Future<SendMessageResult> sendMediaMessage(int conversationId, String filePath,
      {String? body});

  /// Send conversation read event
  ///
  /// Throws an [ArgumentError] if the [conversationId] is invalid or
  /// last Message Server Time [Message.serverTime]5/3
  /// Returns the count [bool].
  Future<bool> sendRead(int conversationId, int lastMessageTime);

  /// Delete the conversation chat
  ///
  /// Throws an [ArgumentError] if the [id] is invalid.
  /// Returns the count [int].
  Future<int> deleteMessage(int id);

  /// Delete the conversation chat
  ///
  /// Throws an [ArgumentError] if the [ids] are invalid.
  /// Returns the count [int].
  Future<int> deleteMessages(Set<int> ids);

  /// Delete the conversation chat
  ///
  /// Throws an [ArgumentError] if the [conversationId] is invalid.
  /// Returns the count [int].
  Future<int> deleteChat(int conversationId);

  /// Delete the conversation chat
  ///
  /// Throws an [ArgumentError] if the [ids] are invalid.
  /// Returns the count [int].
  Future<int> deleteChats(Set<int> ids);

  MessageType getFileMessageType(ConversationType? type);
}
