/*
 * *****************************************************************************
 * Copyright (C) Verizon Wireless, Inc - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * *****************************************************************************
 *
 */
import '../../../vzm_client_sdk.dart';

abstract class ProfileManager {
  Contact getProfile();

  Future<bool> setName(String name);

  Future<bool> setAvatar(String filePath);

  Future<bool> updateProfile(String? name, String? filePath);
}
